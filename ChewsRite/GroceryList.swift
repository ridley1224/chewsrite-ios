//
//  GroceryList.swift
//  ChewsRite
//
//  Created by Randall Ridley on 8/22/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class GroceryList: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var groceryBtn: UIButton!
    @IBOutlet weak var groceryTable: UITableView!
    
    @IBOutlet weak var statusLbl: UILabel!
    var groceryList = [NSMutableDictionary]()
    var receipeList = [NSMutableDictionary]()
    //var selectedItems = [NSMutableDictionary]()
    var selectedItems = [GroceryItem]()
    var selectedGroceryItem : GroceryItem?
    var selectedRecipeItem : RecipeItem?
    
    var showingGrocery : Bool?
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var groceryLbl: UILabel!
    
    @IBOutlet weak var search: UISearchBar!
    
    var itemArrayRecipes = [RecipeItem]()
    var currentRecipesItemArray = [RecipeItem]()
    
    var itemArrayGrocery = [GroceryItem]()
    var currentRecipesItemArrayGrocery = [GroceryItem]()
    
    var statusImageCache = [String:UIImage]()
    
    @IBOutlet weak var menuBtn: UIButton!
    
    @IBOutlet weak var familyCV: UICollectionView!
    var selectedUser : NSDictionary?
    var familyList = [NSDictionary]()
    var removedSubstitutes = [GroceryItem]()
    
    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        selectedUser = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!, "usertype":"0"] as NSDictionary
        familyList.append(selectedUser!)
        
        if appDelegate.userImage != nil
        {
            let urlString = "\(appDelegate.serverDestination!)userimages/\(appDelegate.profileImg!)"
            self.statusImageCache[urlString] = appDelegate.userImage
        }
        
        familyCV.delegate = self
        familyCV.dataSource = self
        
        let tb : TabController = self.parent as! TabController
        
        menuBtn.addTarget(self, action: #selector(tb.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
        showingGrocery = true
        search.delegate = self
        search.backgroundImage = UIImage()
        
        UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        
        search.placeholder = "Search in Grocery"
        
        statusView.isHidden = true
        
        groceryTable.delegate = self
        groceryTable.dataSource = self
        groceryTable.tableFooterView = UIView()
        
        
        //debug()
        
        getFamily()
        getMyRecipes()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateGroceryItemsTabNotification),
            name: NSNotification.Name(rawValue: "updateGroceryItemsTab"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateSavedGroceryItemsNotification),
            name: NSNotification.Name(rawValue: "updateSavedGroceryItems"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateSubstituteItemsNotification),
            name: NSNotification.Name(rawValue: "updateSubstituteItems"),
            object: nil)
    }
    
    @objc func updateSubstituteItemsNotification (notification: NSNotification) {
        
        //remove substituted item
        
        let item = notification.object as! GroceryItem
        
        itemArrayGrocery.removeAll(where: {$0.ingredientid == selectedGroceryItem?.ingredientid})
        currentRecipesItemArrayGrocery.removeAll(where: {$0.ingredientid == selectedGroceryItem?.ingredientid})
        selectedItems.removeAll(where: {$0.ingredientid == selectedGroceryItem?.ingredientid})
        
        selectedGroceryItem = nil
        
        //add substitution from EditGrcoeryIngredient.swift
        
        itemArrayGrocery.append(item)
        currentRecipesItemArrayGrocery.append(item)
        selectedItems.append(item)
        
        self.groceryTable.reloadData()
    }
    
    @objc func updateSavedGroceryItemsNotification (notification: NSNotification) {
        
        //from EditRecipeGroceryList.swift
        
        //repopulate selected items
        
        let savedItems = notification.object as! [GroceryItem]
        
        selectedItems.removeAll(where: {$0.recipeid == selectedRecipeItem?.recipeid})
        
        print("hi")
        
        for dict in savedItems
        {
            selectedItems.append(dict)
            
            if !currentRecipesItemArrayGrocery.contains(dict)
            {
                currentRecipesItemArrayGrocery.append(dict)
            }
        }
        
        //update recipe item count
        
        for dict in currentRecipesItemArray
        {
            if dict.recipeid == selectedRecipeItem?.recipeid
            {
                dict.remainingCount = dict.recipeIngredientsCount - savedItems.count
                
                break
            }
        }
        
        self.groceryTable.reloadData()
    }
    
    @objc func updateGroceryItemsTabNotification (notification: NSNotification) {
        
        //from RecipeDetails.swift
        
        let selectedItem = notification.object as! NSDictionary
        
        print("selectedItem: \(selectedItem)")
        
        
        let active = selectedItem.object(forKey: "active") as! Bool
        
        if active == true
        {
            let item = GroceryItem(ingredientname: selectedItem.object(forKey: "ingredientname") as! String, serving: "\(selectedItem.object(forKey: "quantity") as! String) \(selectedItem.object(forKey: "unit") as! String)", active: false, recipeid: selectedItem.object(forKey: "recipeid") as! String, ingredientid: selectedItem.object(forKey: "ingredientid") as! String, ingredientsid: "", isSubstitute: selectedItem.object(forKey: "isSubstitute") as! Bool, imagename: "")
            
            selectedItems.append(item)
            
            if !currentRecipesItemArrayGrocery.contains(item)
            {
                currentRecipesItemArrayGrocery.append(item)
            }
        }
        else
        {
            currentRecipesItemArrayGrocery.removeAll(where: {$0.ingredientname == selectedItem.object(forKey: "ingredientname") as! String && $0.ingredientid == selectedItem.object(forKey: "ingredientid") as! String && $0.recipeid == selectedItem.object(forKey: "recipeid") as! String})
            
            selectedItems.removeAll(where: {$0.ingredientname == selectedItem.object(forKey: "ingredientname") as! String && $0.ingredientid == selectedItem.object(forKey: "ingredientid") as! String && $0.recipeid == selectedItem.object(forKey: "recipeid") as! String})
        }
                
        //update recipe item count
        
        for dict in currentRecipesItemArray
        {
            if dict.recipeid == selectedItem.object(forKey: "recipeid") as! String
            {
                if active == true
                {
                    dict.remainingCount -= 1
                }
                else
                {
                    dict.remainingCount += 1
                }

                break
            }
        }
        
        if currentRecipesItemArrayGrocery.count > 0
        {
            statusView.isHidden = true
        }
        
        self.groceryTable.reloadData()
    }
    
    func debug () {
        
//        self.itemArrayGrocery.append(GroceryItem(name: "chicken breast", serving: "1 oz", active: false, imagename: ""))
//        self.itemArrayGrocery.append(GroceryItem(name: "quinoa", serving: "2 oz", active: false, imagename: ""))
//
//        self.currentRecipesItemArrayGrocery = self.itemArrayGrocery
//
//        self.itemArray.append(GroceryItem(name: "apple raisin", serving: "4 ingredients to be added", active: false, imagename: ""))
//        self.itemArray.append(GroceryItem(name: "risotto", serving: "0 ingredients to be added", active: false, imagename: ""))
//
//        self.currentRecipesItemArray = self.itemArray
//
//        groceryTable.reloadData()
    }
    
    // MARK: Webservice
    
    func getMyRecipes() {
        
        //displayItems.removeAll()
        
        itemArrayGrocery.removeAll()
        itemArrayRecipes.removeAll()
        selectedItems.removeAll()
        currentRecipesItemArrayGrocery.removeAll()
        currentRecipesItemArray.removeAll()
        
        print("getMyRecipes")
        
        let dataString = "recipeData"
        
        let urlString = "\(appDelegate.serverDestination!)getGroceryRecipesIngredients.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        //let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        var paramString = ""
        
        if selectedUser != nil
        {
            let usertype = selectedUser?.object(forKey: "usertype") as! String
            
            if usertype == "0"
            {
                paramString = "\(paramString)&userid=\(selectedUser?.object(forKey: "userid") as! String)"
            }
            else
            {
                paramString = "\(paramString)&userid=\(selectedUser?.object(forKey: "familyid") as! String)"
            }
        }
        else
        {
            paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        }
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("meal plan recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.statusView.isHidden = false
                        self.groceryTable.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            //self.displayItems.append(dict2)
                            
                            let ingredients = dict2.object(forKey: "ingredients") as! [NSDictionary]
                            
                            let recipeItem = RecipeItem(
                                                        recipename: dict2.object(forKey: "recipename") as! String,
                                                        recipeid: dict2.object(forKey: "recipeid") as! String,
                                                        recipeIngredientsCount: dict2.object(forKey: "recipeIngredientsCount") as! Int,
                                                        calories: dict2.object(forKey: "calories") as! String,
                                                        imagename: dict2.object(forKey: "imagename") as! String,
                                                        ingredients: ingredients,
                                                        remainingCount: dict2.object(forKey: "remainingCount") as! Int,
                                                        groceryItemsCount: dict2.object(forKey: "groceryItemsCount") as! Int
                            )
                            
                            self.itemArrayRecipes.append(recipeItem)
                            
                            for ingredient1 in ingredients
                            {
                                let ingredient = ingredient1 as! NSDictionary
                                
                                let item = GroceryItem(
                                    ingredientname: ingredient.object(forKey: "ingredientname") as! String,
                                    serving: "\(ingredient.object(forKey: "quantity") as! String) \(ingredient.object(forKey: "unit") as! String)",
                                    active: false,
                                    recipeid: ingredient.object(forKey: "recipeid") as! String,
                                    ingredientid: ingredient.object(forKey: "ingredientid") as! String,
                                    ingredientsid: ingredient.object(forKey: "ingredientsid") as! String,
                                    isSubstitute: ingredient.object(forKey: "isSubstitute") as! Bool, imagename: ingredient.object(forKey: "imagename") as! String)
                                
                                self.itemArrayGrocery.append(item)
                                self.selectedItems.append(item)
                            }
                        }
                        
                        self.currentRecipesItemArrayGrocery = self.itemArrayGrocery
                        self.currentRecipesItemArray = self.itemArrayRecipes
                        
                        DispatchQueue.main.sync(execute: {
                            
                            //                            self.activityView.stopAnimating()
                            //                            self.activityView.isHidden = true
                            
                            self.groceryTable.reloadData()
                            
                            if hasData == true
                            {
                                self.statusView.isHidden = true
                            }
                            else
                            {
                                print("no data")
                                
                                self.groceryTable.reloadData()
                                self.statusView.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.statusView.isHidden = false
                            self.groceryTable.reloadData()
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
            }.resume()
    }
    
    func getFamily() {
        
        familyList.removeAll()
        
        print("getFamily")
        
        let dataString = "familyData"
        
        let urlString = "\(appDelegate.serverDestination!)getFamilyMembers.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        selectedUser = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!, "usertype":"0"] as NSDictionary
        familyList.append(selectedUser!)
        
        self.familyCV.reloadData()
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("family jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
//                        self.familyCV.isHidden = true
//                        self.familyCV.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let members = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (members.count > 0)
                    {
                        hasData = true
                        
                        for dict in members
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.familyList.append(dict2)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if hasData == true
                            {
                                self.familyCV.isHidden = false
                                self.familyCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
//                                self.familyCV.isHidden = true
//                                self.familyCV.reloadData()
                            }
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    // MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                
        if showingGrocery == true
        {
            return self.currentRecipesItemArrayGrocery.count
        }
        else
        {
            return self.currentRecipesItemArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if showingGrocery == true
        {
            selectedGroceryItem = currentRecipesItemArrayGrocery[indexPath.row]
            
            performSegue(withIdentifier: "EditIngredient", sender: nil)
        }
        else
        {
            selectedRecipeItem = currentRecipesItemArray[indexPath.row]
            
            performSegue(withIdentifier: "EditRecipeGroceryList", sender: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if showingGrocery == true
        {
            return 65
        }
        else
        {
            return 95
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            
            // handle delete (by removing the data from your array and updating the tableview)
            
            if showingGrocery == true
            {
                self.currentRecipesItemArrayGrocery.remove(at: indexPath.row)
            }
            else
            {
                self.currentRecipesItemArray.remove(at: indexPath.row)
            }
            
            //add webservice call
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if showingGrocery == true
        {
            let cell : GroceryListCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! GroceryListCell
            
            let dict = self.currentRecipesItemArrayGrocery[(indexPath as NSIndexPath).row]
            
            let reviewdate = dict.ingredientname
            let username = dict.serving
            
            cell.selectionStyle = .none
            
            cell.ingredientLbl.text = reviewdate
            cell.servingLbl.text = username
            
            if selectedItems.contains(dict)
            {
                cell.selectBtn.setBackgroundImage(#imageLiteral(resourceName: "selected"), for: .normal)
            }
            else
            {
                cell.selectBtn.setBackgroundImage(#imageLiteral(resourceName: "unselected"), for: .normal)
            }
            
            cell.selectBtn.restorationIdentifier = "\(indexPath.row)"
            cell.selectBtn.addTarget(self, action: #selector(self.selectItem(_:)), for: UIControlEvents.touchUpInside)
            
            return cell
        }
        else
        {
            let cell : GroceryRecipeListCell = tableView.dequeueReusableCell(withIdentifier: "cell1")! as! GroceryRecipeListCell
            
            let dict = self.currentRecipesItemArray[(indexPath as NSIndexPath).row]
            
//            let c1 = dict.recipeIngredientsCount
//            let c2 = dict.groceryItemCount
            
            //let countNotAdded = c1 - c2
            
            let ingredientname = dict.recipename
            //let username = "\(countNotAdded) ingredients to be added"
            let displayString = "\(dict.remainingCount) ingredients to be added"
            
            cell.selectionStyle = .none
            
            cell.titleLbl.text = ingredientname
            cell.ingredientsLbl.text = displayString
            //cell.recipeIV.image = #imageLiteral(resourceName: "risotto")
            
            let imagename = dict.imagename
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
                
                if let img = statusImageCache[urlString]
                {
                    cell.recipeIV.image = img
                }
                else
                {
                    cell.recipeIV.image = UIImage(named: "profile-icon")
                    
                    self.downloadImage(imagename: imagename, urlString: urlString, iv: cell.recipeIV)
                }
            }
            
            return cell
        }
    }
    
    func downloadImage (imagename : String, urlString : String, rv: GroceryRecipeListCell ) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache[urlString] = image
                        rv.recipeIV.image = image
                    }
                    
                    //println("test")
                })
                return nil
            }))
        }
        else
        {
            rv.recipeIV.image = UIImage.init(named: "profile-icon")
        }
    }
    
    // MARK: Actions
    
    @IBAction func changeGroceryView(_ sender: Any) {
        
        let btn = sender as? UIButton
        
        if showingGrocery == true
        {
            btn?.setTitle("VIEW MASTER LIST", for: .normal)
            showingGrocery = false
            
            groceryLbl.text = "BY RECIPE"
        }
        else
        {
            btn?.setTitle("VIEW BY RECIPE", for: .normal)
            showingGrocery = true
            
            groceryLbl.text = "MASTER GROCERY LIST"
        }
        
        groceryTable.reloadData()
    }
    
    @IBAction func selectItem(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        //let item = btn.restorationIdentifier as! String
        
        let ind = Int(btn.restorationIdentifier!)
        
        selectedGroceryItem = currentRecipesItemArrayGrocery[ind!]
        
        //delete from groceryitems where ingredientid ==
        
        var adding = true
        
        if let row = self.selectedItems.index(where: {$0.ingredientname == selectedGroceryItem?.ingredientname}) {
            //array[row] = newValue
            
            print("remove item")
            
            let item = selectedItems[row]
            
            if item.isSubstitute == true
            {
                removedSubstitutes.append(item)
            }
            
            btn.setBackgroundImage(#imageLiteral(resourceName: "unselected"), for: .normal) //replace with check
            selectedItems.remove(at: row)
            
            adding = false
            //selectedGroceryItem?.groceryItemCount -= 1
        }
        else
        {
            print("add item")
            
            btn.setBackgroundImage(#imageLiteral(resourceName: "selected"), for: .normal) //replace with check
            selectedItems.append(selectedGroceryItem!)
            
            removedSubstitutes.removeAll(where: { $0.ingredientname == selectedGroceryItem!.ingredientname })
            
            //selectedGroceryItem?.groceryItemCount += 1
        }
                
        updateRecipeItem(adding: adding)
        
        groceryTable.reloadData()
    }
    
    func updateRecipeItem(adding:Bool) {
        
        for dict in currentRecipesItemArray
        {
            if dict.recipeid == selectedGroceryItem!.recipeid
            {
                if adding == true
                {
                    dict.remainingCount -= 1
                }
                else
                {
                    dict.remainingCount += 1
                }
                
                return
            }
        }
    }
    
    // MARK: Search
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if showingGrocery == true
        {
            currentRecipesItemArrayGrocery = itemArrayGrocery.filter({ item -> Bool in
                
                switch searchBar.selectedScopeButtonIndex {
                case 0:
                    if searchText.isEmpty { return true }
                    return item.ingredientname.lowercased().contains(searchText.lowercased())
                default:
                    return false
                }
            })
        }
        else
        {
            currentRecipesItemArray = itemArrayRecipes.filter({ item -> Bool in
                
                switch searchBar.selectedScopeButtonIndex {
                case 0:
                    if searchText.isEmpty { return true }
                    return item.recipename.lowercased().contains(searchText.lowercased())
                default:
                    return false
                }
            })
        }
        
        groceryTable.reloadData()
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: 42, height: 42)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.familyList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FamilyCVCell
        
        let dict = familyList[indexPath.row]
        
        let imagename = dict.object(forKey: "userimage") as? String
        
        if selectedUser == dict
        {
            let img = UIImage.init(named: "selectWhite")
            cell?.si.image = img
        }
        else
        {
            cell?.si.image = nil
        }
        
        if imagename != nil
        {
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
            
            //let url2 = NSURL(string:urlString)!
            
            // If this image is already cached, don't re-download
            
            if let img = statusImageCache[urlString]
            {
                //print("load cached status image")
                
                cell?.userImg.image = img
            }
            else
            {
                //print("status image not
                
                let imagename = dict.object(forKey: "userimage") as? String
                
                if imagename != nil && imagename != ""
                {
                    let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                    
                    print("img urlString: \(urlString)")
                    
                    if let img = statusImageCache[urlString]
                    {
                        cell?.userImg.image = img
                    }
                    else
                    {
                        cell?.userImg.image = UIImage(named: "profile-icon")
                        
                        self.downloadImage(imagename: imagename!, urlString: urlString, iv: ((cell?.userImg!)!))
                    }
                }
                else
                {
                    cell?.userImg.image = UIImage(named: "profile-icon")
                }
            }
        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedUser = familyList[indexPath.row]
        
        getMyRecipes()
        
        //
        //            print("selectedUser: \(selectedUser)")
                
        familyCV.reloadData()
    }
    
    func downloadImage (imagename : String, urlString : String, iv: UIImageView ) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache[urlString] = image
                        iv.image = image
                        
                        if urlString.contains(self.appDelegate.profileImg!)
                        {
                            self.appDelegate.userImage = image
                        }
                    }
                    
                    //println("test")
                })
                return nil
            }))
        }
        else
        {
            iv.image = UIImage.init(named: "profile-icon")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "EditRecipeGroceryList"
        {
            //print("itemArrayGrocery: \(itemArrayGrocery)")
            
            let recipeItems = itemArrayGrocery.filter({ item1 -> Bool in
                
                return item1.recipeid == selectedRecipeItem!.recipeid
            })
            
            let imagename = selectedRecipeItem?.imagename
            
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
            
            var selectedImage : UIImage?
            
            
            if statusImageCache[urlString] != nil
            {
                selectedImage = statusImageCache[urlString]
            }
            else
            {
                print("no")
            }
            
//            selectedRecipeItems = [GroceryItem]()
//
//            for dict in selectedItems
//            {
//                if dict.recipeid == selectedRecipeItem?.recipeid
//                {
//                    selectedRecipeItems?.append(dict)
//                }
//            }
            
            let selectedGroceryItems = selectedItems.filter({ item1 -> Bool in
                
                return item1.recipeid == selectedRecipeItem!.recipeid
            })
            
            let destination = segue.destination as! EditRecipeGroceryList
            //destination.groceryList = recipeItems
            destination.selectedImage = selectedImage
            destination.selectedRecipeItem = selectedRecipeItem!
            destination.selectedGroceryItems = selectedGroceryItems
            destination.recipeID = selectedRecipeItem!.recipeid
            destination.removedSubstitutes = removedSubstitutes
        }
        else if segue.identifier == "EditIngredient"
        {
            //print("itemArrayGrocery: \(itemArrayGrocery)")
            
            let destination = segue.destination as! EditGroceryIngredient
            destination.selectedItem = selectedGroceryItem!
            destination.statusImageCache = statusImageCache
            
        }
    }
}

