//
//  CreateGroup.swift
//  ChewsRite
//
//  Created by Randall Ridley on 9/13/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class CreateGroup: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    @IBOutlet weak var uploadView: UIView!
    @IBOutlet weak var uploadBtn: UIButton!
    
    @IBOutlet weak var uploadLbl: UILabel!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var imageIV: UIImageView!
    @IBOutlet weak var friendsCV: UICollectionView!
    
    @IBOutlet weak var descriptionTV: UITextView!
    @IBOutlet weak var groupnameTxt: UITextField!
    
    var isFiltered : Bool?
    
    var friendList = [NSMutableDictionary]()
    var filteredList = [NSMutableDictionary]()
    var selectedList = [NSMutableDictionary]()
    
    var imagePicker : UIImagePickerController!
    
    var selectedImage : UIImage?
    
    var statusImageCache = [String:UIImage]()
    var groupId : String?
    var selectedGroup : GroupItem?
    
    var inviteList : String?
    
    var uploadImageName : String?
    var newImage : Bool?
    var valuesChanged : Bool?
    
    // MARK: viewDidLoad
    
    @IBOutlet weak var statusTV: UITextView!
    
    var isFamilyMember : Bool?
    var userid : String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        friendsCV.delegate = self
        friendsCV.dataSource = self
        
        groupnameTxt.delegate = self
        descriptionTV.delegate = self
        
        statusTV.isHidden = true
        
        descriptionTV.text = "Group Description"
        
        activityView.isHidden = true
        
        //descriptionTV.text = ""
        //groupnameTxt.text = "test group"
        
        if groupId != nil || isEditing == true
        {
            groupnameTxt.text = selectedGroup?.groupname
            
            if selectedGroup?.groupdescription == ""
            {
                descriptionTV.text = "Group Description"
            }
            else
            {
                descriptionTV.text = selectedGroup?.groupdescription
            }
            
            groupId = selectedGroup?.groupid
            headerLbl.text = "Edit Group"
            uploadImageName = selectedGroup?.groupimage
        }
        
        if selectedImage != nil
        {
            imageIV.image = selectedImage
        }
        
        if groupId == nil
        {
            groupId = ""
        }
        
        //debugGroup()
        
        getFriends()
        
        let btnItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(dismissKeyboard))
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width:  self.view.frame.size.width, height: 50))
        toolbar.backgroundColor = UIColor.darkGray
        toolbar.barStyle = UIBarStyle.default
        toolbar.tintColor = UIColor.black
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            btnItem]
        
        toolbar.sizeToFit()
        
        groupnameTxt.inputAccessoryView = toolbar
        descriptionTV.inputAccessoryView = toolbar
        
        //debug()
    }
    
    override func viewDidLayoutSubviews() {
        
        descriptionTV.setContentOffset(.zero, animated: false)
    }
    
    // MARK: Textfield Methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        valuesChanged = true
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        valuesChanged = true
        
        return true
    }
    
    // MARK: Textview Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Group Description"
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""
        {
            textView.text = "Group Description"
        }
    }
    
    @objc func dismissKeyboard () {
        
        self.view.endEditing(true)
    }
    
    func debugGroup () {
        
        groupnameTxt.text = "G"
        descriptionTV.text = "GD"
    }
    
    func debug () {
        
        var dict : NSMutableDictionary?
        
        dict  = ["friendname" : "Friend 1", "userid" : "1", "imagename" : "1", "isSelected" : "0"]
        friendList.append(dict!)
        
        dict  = ["friendname" : "Friend 2", "userid" : "2", "imagename" : "1", "isSelected" : "0"]
        friendList.append(dict!)
        
        self.activityView.stopAnimating()
        self.activityView.isHidden = true
        
        friendsCV.reloadData()
    }
    
    // MARK: Webservice
    
    func getFriends() {
        
        friendList.removeAll()
        
//        var dict : NSMutableDictionary?
//
//        dict  = ["firstname" : "Friend","lastname" : "l", "userid" : "1", "imagename" : "1", "isSelected" : "0"]
//        friendList.append(dict!)
        
        print("getFriends")
        
        let dataString = "friendsData"
        
        let urlString = "\(appDelegate.serverDestination!)getUserFriendsJSON.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        //let paramString = "userid=\(appDelegate.userid!)&groupid=\(groupId!)&groupfilter=true&devStatus=\(appDelegate.devStage!)"
        
        let paramString = "userid=\(appDelegate.userid!)&groupid=\(groupId!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("friends jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.statusTV.isHidden = false
                        
                        //self.friendsCV.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            dict2.setValue("0", forKey: "isSelected")
                            
                            self.friendList.append(dict2)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            //                            self.activityView.stopAnimating()
                            //                            self.activityView.isHidden = true
                            
                            if hasData == true
                            {
                                //self.recipeStatusTV.text = "You currently have no friend requests."
                                
                                self.friendsCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
                                self.statusTV.isHidden = false
                                
                                //self.noResultsMain.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.friendsCV.reloadData()
                            
                            //self.noResultsMain.isHidden = false
                            
                            self.statusTV.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func uploadGroupData() {
        
        self.activityView.startAnimating()
        self.activityView.isHidden = false
        
        print("uploadGroupData")
        
        inviteList = ""
        
        let dataString = "groupData"
        
        let urlString = "\(appDelegate.serverDestination!)manageGroup.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var paramString = "userid=\(userid!)&groupname=\(groupnameTxt.text!)&groupdescription=\(descriptionTV.text!)&imagename=\(uploadImageName!)&devStatus=\(appDelegate.devStage!)&isFamilyMember=\(isFamilyMember!)"
        
        if isEditing == true
        {
            paramString = "\(paramString)&update=true&groupid=\(groupId!)"
            selectedGroup!.groupname = groupnameTxt.text!
            selectedGroup!.groupdescription = descriptionTV.text!
        }
        else
        {
            paramString = "\(paramString)&insert=true"
        }
        
        if selectedList.count > 0
        {
            var i = 0
            
            for a in selectedList
            {
                if i == 0
                {
                    inviteList = a.object(forKey: "userid") as? String
                }
                else
                {
                    inviteList = "\(inviteList!),\(a.object(forKey: "userid") as! String)"
                }
                
                i += 1
            }
            
            paramString = "\(paramString)&invitelist=\(inviteList!)"
        }
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("add group jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.activityView.stopAnimating()
                        self.activityView.isHidden = true
                        
                        //self.noResultsMain.isHidden = false
                        
                        //                        self.activityView.stopAnimating()
                        //                        self.activityView.isHidden = true
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    //print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    self.groupId = "\(uploadData.object(forKey: "groupid") as! Int)"
                    
                    print("groupId: \(self.groupId!)")
                    
                    var isSaved = false
                    
                    if status == "group saved" || status == "group updated"
                    {
                        isSaved = true
                        self.valuesChanged = false
                        self.selectedList.removeAll()
                        
                        self.isFiltered = false
                        
                        for friend in self.friendList
                        {
                            friend.setValue("0", forKey: "isSelected")
                        }
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.activityView.stopAnimating()
                        self.activityView.isHidden = true
                        
                        if isSaved == true
                        {
                            NotificationCenter.default.post(name: Notification.Name("refreshGroups"), object: nil)
                            NotificationCenter.default.post(name: Notification.Name("refreshGroup"), object: self.selectedGroup)
                            
                            if self.isEditing != true
                            {
                                self.friendsCV.reloadData()
                                self.isEditing = true
                                //self.groupId = uploadData.object(forKey: "groupid") as? String
                                self.performSegue(withIdentifier: "GroupFeed", sender: nil)
                            }
                            else
                            {
                                let alert = UIAlertController(title: nil, message: "Update Successful", preferredStyle: UIAlertControllerStyle.alert)
                                
                                self.present(alert, animated: true, completion: nil)
                                
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                    switch action.style{
                                    case .default:
                                        print("default")
                                        
                                        self.dismiss()
                                        
                                    case .cancel:
                                        print("cancel")
                                        
                                    case .destructive:
                                        print("destructive")
                                    }
                                }))
                            }
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    // MARK: Actions
    
    @IBAction func selectPhoto(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "Select Photo", preferredStyle: UIAlertControllerStyle.alert)
       
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Open Photo Library", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
                self.choosePhoto()
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
                self.takePhoto()
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        dismissKeyboard()
        dismiss()
    }
    
    func dismiss () {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func done(_ sender: Any) {
        
        if valuesChanged != true
        {
            //randall notes
            //if nothing was changed, done button dismisses view
            
            dismiss()
        }
        else
        {
                
            if groupnameTxt.text != "" && selectedImage != nil
            {
                if isEditing == true
                {
                    if newImage == true
                    {
                        uploadS3()
                    }
                    else
                    {
                        uploadGroupData()
                    }
                }
                else
                {
                    uploadS3()
                }
            }
            else
            {
                if groupnameTxt.text == ""
                {
                    showBasicAlert(string: "Please enter Group Name")
                }
                else if selectedImage == nil
                {
                    showBasicAlert(string: "Please select an Image")
                }
            }
            
        }
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    // MARK: Image Picker
    
    func takePhoto () {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func choosePhoto () {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        print("cancel")
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        print("did pick")
        
        selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        imageIV.image = selectedImage
        uploadLbl.text = "Change Picture"
        
        uploadLbl.textColor = .white
        
        newImage = true
        valuesChanged = true
        
        self.dismiss(animated: true, completion: {
            
            //self.uploadToServer(name:"registration-\(self.appDelegate.userid!)")
        });
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let kWhateverHeightYouWant = 190
        
        return CGSize.init(width: 600, height: CGFloat(kWhateverHeightYouWant))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if isFiltered == true
        {
            return filteredList.count
        }
        else
        {
            return friendList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? GroupsCollectionCell
        
        var dict: NSMutableDictionary?
        
        if isFiltered == true
        {
            dict = filteredList[indexPath.row]
        }
        else
        {
            dict = friendList[indexPath.row]
        }
        
        //cell?.groupIV.image = UIImage.init(named: (dict?.object(forKey: "imagename") as? String)!)
        
        let imagename = dict?.object(forKey: "userimage") as? String
        
        if imagename != nil
        {
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
            
            if let img = statusImageCache[urlString]
            {
                cell?.groupIV.image = img
            }
            else
            {
                cell?.groupIV.image = UIImage(named: "profile-icon")
                
                self.downloadImage(imagename: imagename!, urlString: urlString, rv: cell!)
            }
        }
        
        let fn = dict?.object(forKey: "firstname") as? String
        let ln = dict?.object(forKey: "lastname") as? String
        
        cell?.nameLbl.text = "\(fn!) \(ln!)"
        
        cell?.selectedBtn.restorationIdentifier = dict?.object(forKey: "userid") as? String
        cell?.selectedBtn.accessibilityIdentifier = "\(indexPath.row)"
        
        let isSelected = dict?.object(forKey: "isSelected") as? String

        if isSelected == "1"
        {
            cell?.selectedBtn.setBackgroundImage(#imageLiteral(resourceName: "selectOrange"), for: .normal)
            cell?.isSelected = true
            //dict?.setValue(true, forKey: "isSelected")
        }
        else
        {
            cell?.selectedBtn.setBackgroundImage(nil, for: .normal)
            cell?.isSelected = false
            //dict?.setValue(false, forKey: "isSelected")
        }

        cell?.selectedBtn.addTarget(self, action: #selector(self.manageFavorite(_:)), for: UIControlEvents.touchUpInside)
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? GroupsCollectionCell
        
        var dict: NSMutableDictionary?
        
        if isFiltered == true
        {
            dict = filteredList[indexPath.row]
        }
        else
        {
            dict = friendList[indexPath.row]
        }
        
        let isSelected = dict?.object(forKey: "isSelected") as? String
        
        if isSelected == "0"
        {
            cell?.selectedBtn.setBackgroundImage(#imageLiteral(resourceName: "selectOrange"), for: .normal)
            cell?.isSelected = true
            dict?.setValue("1", forKey: "isSelected")
            
            selectedList.append(dict!)
        }
        else
        {
            cell?.selectedBtn.setBackgroundImage(nil, for: .normal)
            cell?.isSelected = false
                        
            dict?.setValue("0", forKey: "isSelected")
            
            let ind = selectedList.index(of: dict!)
            selectedList.remove(at: ind!)
        }
        
        collectionView.reloadData()
    }
    
    @IBAction func manageFavorite(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        let dict: NSMutableDictionary?
        
        if isFiltered == true
        {
            dict = filteredList[Int(btn.accessibilityIdentifier!)!]
        }
        else
        {
            dict = friendList[Int(btn.accessibilityIdentifier!)!]
        }
        
        if dict?.object(forKey: "isSelected") as! String == "0"
        {
            dict?.setValue("1", forKey: "isSelected")
            btn.setBackgroundImage(#imageLiteral(resourceName: "selectOrange"), for: .normal)
        }
        else
        {
            //print("")
            dict?.setValue("0", forKey: "isSelected")
            btn.setBackgroundImage(nil, for: .normal)
        }
        
        friendsCV.reloadData()
        
        //manageFavorite(recipeID: btn.restorationIdentifier!)
    }
    
    func uploadS3 () {

        if isEditing == true
        {
            uploadImageName = selectedGroup?.groupimage
        }
        else
        {
            uploadImageName = "\(appDelegate.randomString(length: 10)).jpeg"
        }

        let image = selectedImage!
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(uploadImageName!)
        let imageData = UIImageJPEGRepresentation(image, 0)
        fileManager.createFile(atPath: path as String, contents: imageData, attributes: nil)

        let fileUrl = NSURL(fileURLWithPath: path)

        print("upload")
        
        let transferManager = AWSS3TransferManager.default()

        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.bucket = "chewsrite/images"
        uploadRequest!.key = uploadImageName!
        uploadRequest?.body = fileUrl as URL
        uploadRequest?.contentType = "image/jpeg"

        uploadRequest?.acl = AWSS3ObjectCannedACL.publicReadWrite;

        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in

            if let error = task.error as NSError? {

                if error.domain == AWSS3TransferManagerErrorDomain,

                    let code = AWSS3TransferManagerErrorType(rawValue: error.code)
                {
                    switch code
                    {
                    case .cancelled, .paused:
                        break
                    default:
                        print("Error uploading 1: \(uploadRequest?.key!) Error: \(error)")
                    }
                }
                else
                {
                    print("Error uploading: \(uploadRequest?.key!) Code: \(error.code) Error: \(error)")
                }
                
                return nil
            }

            //let uploadOutput = task.result

            print("Upload complete for: \(uploadRequest!.key!)")
            
            self.newImage = false
            self.uploadGroupData()
            
            //Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.doneUploading), userInfo: nil, repeats: false)

            return nil
        })
    }
    
    func downloadImage (imagename : String, urlString : String, rv: GroupsCollectionCell ) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache[urlString] = image
                        rv.groupIV.image = image
                    }
                    
                    //println("test")
                })
                return nil
            }))
        }
        else
        {
            rv.groupIV.image = UIImage.init(named: "profile-icon")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let selectedGroup = GroupItem(groupname: groupnameTxt.text!, groupid: groupId!, groupimage: uploadImageName!, adminid: groupId!, groupdescription: descriptionTV.text!)
        
        let destination = segue.destination as! GroupFeed
        destination.selectedImage = selectedImage
        destination.selectedGroup = selectedGroup
        destination.groupId = groupId!
        destination.groupName = groupnameTxt.text!
        destination.isAdmin = true
        destination.isFamilyMember = isFamilyMember
        destination.userid = userid
    }
}
