//
//  EditProfile.swift
//  ChewsRite
//
//  Created by Randall Ridley on 11/6/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class EditProfile: BaseViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
     let appDelegate = UIApplication.shared.delegate as! AppDelegate

     let prefs = UserDefaults.standard
     
     @IBOutlet weak var activityView: UIActivityIndicatorView!

     @IBOutlet weak var pregnantBtn: UIButton!

     @IBOutlet weak var breastfeedingBtn: UIButton!
     @IBOutlet weak var submitBtn: UIButton!
     @IBOutlet weak var backBtn: UIButton!

     @IBOutlet weak var pregnantView: UIView!
     @IBOutlet weak var instructionsLbl: UILabel!
     @IBOutlet weak var genderSegment: UISegmentedControl!

     var isPregnant : Bool?
     var isBreastfeeding : Bool?

     @IBOutlet weak var fullnameLbl: UILabel!
     @IBOutlet weak var fullnameTxt: UITextField!
     @IBOutlet weak var aboutMeTxt: UITextField!

     @IBOutlet weak var emailTxt: UILabel!
     
     @IBOutlet weak var dobLbl: UILabel!
     @IBOutlet weak var dobTxt: UITextField!

     @IBOutlet weak var dobTxtTopPadding: NSLayoutConstraint!
     @IBOutlet weak var fullnameTxtTopPadding: NSLayoutConstraint!

     @IBOutlet weak var dobPicker: UIDatePicker!
     @IBOutlet weak var dateView: UIView!

     @IBOutlet weak var imageIV : UIImageView!

     var selectedDate : String?

     var imagePicker : UIImagePickerController!
     var selectedImage : UIImage?

     var statusImageCache = [String:UIImage]()

     var inviteList : String?
     var uploadImageName : String?

    @IBOutlet weak var profileBtn: UIButton!
     
     var validateFields : [UITextField]?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        isPregnant = false
        isBreastfeeding = false
        
        activityView.isHidden = true
        dateView.isHidden = true
        pregnantView.isHidden = true
     
        dobTxtTopPadding.constant = 0
     
          validateFields = [fullnameTxt,dobTxt]
          
          backBtn.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
          
        //fullnameTxtTopPadding.constant = 0

          if appDelegate.userImage != nil
          {
               profileBtn.setBackgroundImage(appDelegate.userImage, for: .normal)
          }
          else if appDelegate.profileImg != ""
          {
               downloadImage(imagename: appDelegate.profileImg!)
          }
     
//     if let firstname = appDelegate.firstname {
//
//          fullnameLbl.isHidden = false
//          fullnameTxt.text = "\(firstname) \(appDelegate.lastname!)"
//     }
//
//     if let bday = appDelegate.birthday {
//
//          dobLbl.isHidden = false
//          dobTxt.text = bday
//     }
//
//     if let gender = appDelegate.gender {
//
//          genderSegment.selectedSegmentIndex = gender
//     }
     
     
          getProfileInfo()
     
        //years in days * seconds in a day
        
        let d = Date(timeInterval: -6570*86400, since: NSDate() as Date)
        
        //dobPicker.setDate(NSDate() as Date, animated: false)
        dobPicker.setDate(d, animated: false)
        
        //pregnantView.isHidden = true
        fullnameLbl.isHidden = true
        dobLbl.isHidden = true
     
          let v = UIView.init(frame: CGRect.init(x: 0, y: profileBtn.frame.height-25, width: 300, height: 50))
          v.backgroundColor = .white
          v.isUserInteractionEnabled = false

          profileBtn.addSubview(v)
        
        let attributedString = NSMutableAttributedString(string: "* You can change this from your Account Settings", attributes: [
            .font: UIFont(name: "Avenir-Book", size: 12.0)!,
            .foregroundColor: appDelegate.crWarmGray
            ])
        attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 12.0)!, range: NSRange(location: 32, length: 16))
        
        //instructionsLbl.attributedText = attributedString
        
        let btnItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(dismissKeyboard))
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width:  self.view.frame.size.width, height: 50))
        numberToolbar.backgroundColor = UIColor.darkGray
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.tintColor = UIColor.black
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            btnItem]
        
        numberToolbar.sizeToFit()
        
        fullnameTxt.inputAccessoryView = numberToolbar
        dobTxt.inputAccessoryView = numberToolbar
     
          if !(appDelegate.gender != nil)
          {
               appDelegate.gender = 1
          }
     }

     @objc func dismissKeyboard () {
        
        self.view.endEditing(true)
     }
     
     // MARK: Actions

     @IBAction func goBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
     }

     @IBAction func saveInfo(_ sender: Any) {
          
          for field in validateFields!
          {
               if field.text == ""
               {
                    self.showBasicAlert(string: "Please enter \(field.restorationIdentifier)")
                    return
               }
          }
        
        self.uploadUserData()
     }

     @IBAction func showDatePopup(_ sender: Any) {
        
        dismissKeyboard()
        
        dateView.isHidden = false
     }

     @IBAction func selectCusines(_ sender: Any) {
        
        //self.performSegue(withIdentifier: "SelectCuisines", sender: self)
        
        if fullnameTxt.text == ""
        {
            self.showBasicAlert(string: "Please Enter Full Name")
            return
        }
        
        if dobTxt.text == ""
        {
            self.showBasicAlert(string: "Please Enter Date of Birth")
            return
        }
        
        uploadUserData()
     }

     func showBasicAlert(string:String)
     {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
               
            case .default:
                print("default")
               
            case .cancel:
                print("cancel")
               
            case .destructive:
                print("destructive")
            }
        }))
     }

     @IBAction func selectDate(_ sender: Any) {
        
        selectedDate = appDelegate.convertDateToSQLDate(date: dobPicker.date)
        
        let df = DateFormatter()
        df.dateFormat = "MM/d/yy"
        
        let displayString = df.string(from: dobPicker.date)
        
        dobTxt.text = displayString
        dateView.isHidden = true
     }

     @IBAction func managePregnant(_ sender: Any) {
        
        if isPregnant == true
        {
            pregnantBtn.setBackgroundImage(UIImage.init(named: "unselected"), for: .normal)
            isPregnant = false
        }
        else
        {
            pregnantBtn.setBackgroundImage(UIImage.init(named: "selected"), for: .normal)
            isPregnant = true
        }
     }

     @IBAction func manageBreastfeeding(_ sender: Any) {
        
        if isBreastfeeding == true
        {
            breastfeedingBtn.setBackgroundImage(UIImage.init(named: "unselected"), for: .normal)
            isBreastfeeding = false
        }
        else
        {
            breastfeedingBtn.setBackgroundImage(UIImage.init(named: "selected"), for: .normal)
            isBreastfeeding = true
        }
     }

     @IBAction func manageGender(_ sender: Any) {
        
        if genderSegment.selectedSegmentIndex != 0
        {
            pregnantView.isHidden = false
        }
        else
        {
            pregnantView.isHidden = true
        }
     }

     @IBAction func selectPhoto(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "Change your profile picture", preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Open Photo Library", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
                self.choosePhoto()
               
            case .cancel:
                print("cancel")
               
            case .destructive:
                print("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
                self.takePhoto()
               
            case .cancel:
                print("cancel")
               
            case .destructive:
                print("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
               
            case .cancel:
                print("cancel")
               
            case .destructive:
                print("destructive")
            }
        }))
     }
     
     @IBAction func selectCuisines(_ sender: Any) {
          
          performSegue(withIdentifier: "FavoriteCuisines", sender: nil)
     }

     // MARK: Image Picker

     func takePhoto () {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
          
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = false
          
            self.present(imagePicker, animated: true, completion: nil)
        }
     }

     func choosePhoto () {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
          
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
          
            self.present(imagePicker, animated: true, completion: nil)
        }
     }

     func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        print("cancel")
     }

     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        print("did pick")
        
        selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        imageIV.image = selectedImage
        
        self.dismiss(animated: true, completion: {
          
          self.uploadS3()
          
            //self.uploadToServer(name:"registration-\(self.appDelegate.userid!)")
        });
     }
     
     // MARK: Web service

     func uploadS3 () {
        
        if appDelegate.profileImg == ""
        {
            uploadImageName = "\(appDelegate.randomString(length: 10)).jpeg"
        }
        else
        {
            uploadImageName = appDelegate.profileImg
        }
        
        let image = selectedImage!
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(uploadImageName!)
        let imageData = UIImageJPEGRepresentation(image, 0)
        fileManager.createFile(atPath: path as String, contents: imageData, attributes: nil)
        
        let fileUrl = NSURL(fileURLWithPath: path)
        
        print("upload")
        
        let transferManager = AWSS3TransferManager.default()
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.bucket = "chewsrite/images"
        uploadRequest!.key = uploadImageName
        uploadRequest?.body = fileUrl as URL
        uploadRequest?.contentType = "image/jpeg"
        
        uploadRequest?.acl = AWSS3ObjectCannedACL.publicReadWrite;
        
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
          
            if let error = task.error as NSError? {
               
                if error.domain == AWSS3TransferManagerErrorDomain,
                    
                    let code = AWSS3TransferManagerErrorType(rawValue: error.code)
                {
                    switch code
                    {
                    case .cancelled, .paused:
                        break
                    default:
                        print("Error uploading 1: \(uploadRequest?.key!) Error: \(error)")
                    }
                }
                else
                {
                    print("Error uploading: \(uploadRequest?.key!) Code: \(error.code) Error: \(error)")
                }
               
                return nil
            }
          
            //let uploadOutput = task.result
          
            print("Upload complete for: \(uploadRequest!.key!)")
          
            //self.updateUserImage()
          
            return nil
        })
     }
     
     func getProfileInfo () {
          
          print("getProfileInfo")
          
          let urlString = "\(appDelegate.serverDestination!)getProfileInfo.php"
          
          print("urlString: \(urlString)")
          
          let url = URL(string: urlString)
          
          var request = URLRequest(url: url!)
          
          request.httpMethod = "POST"
          
          let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
          
          print("paramString: \(paramString)")
          
          request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
          
          let session = URLSession.shared
          
          session.dataTask(with: request) {data, response, err in
               
               do {
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    
                    print("profile jsonResult: \(jsonResult)")
                    
                    let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                    
                    if dataDict["profileData"]! is NSNull
                    {
                         print("no user")
                    }
                    else
                    {
                         let uploadData : NSDictionary = dataDict["profileData"] as! NSDictionary
                         
                         self.isPregnant = uploadData.object(forKey: "isPregnant") as? Bool
                         self.isBreastfeeding = uploadData.object(forKey: "isPregnant") as? Bool
                         
                         let firstname = uploadData.object(forKey: "firstname") as? String
                         let lastname = uploadData.object(forKey: "lastname") as? String
                         self.selectedDate = uploadData.object(forKey: "dob") as? String
                                                  
                         let gender = (uploadData.object(forKey: "gender") as? Int)!
                         
                         DispatchQueue.main.sync(execute: {
                              
                              self.fullnameLbl.isHidden = false
                              self.dobLbl.isHidden = false
                              
                              self.fullnameTxt.text = "\(firstname!) \(lastname!)"
                              self.dobTxt.text = self.appDelegate.convertSQLDateTimeProfile(origDate: self.selectedDate!)
                              self.emailTxt.text = uploadData.object(forKey: "email") as? String
                              self.aboutMeTxt.text = uploadData.object(forKey: "story") as? String
                              
                              self.dobPicker.setDate((self.selectedDate?.sqlToDate())!, animated: false)
                              
                              self.genderSegment.selectedSegmentIndex = gender
                              
                              if gender == 0
                              {
                                   self.pregnantView.isHidden = true
                              }
                              else
                              {
                                   self.pregnantView.isHidden = false
                                   
                                   if self.isPregnant == true
                                   {
                                        self.pregnantBtn.setBackgroundImage(UIImage.init(named: "selected"), for: .normal)
                                   }
                                   else
                                   {
                                        self.pregnantBtn.setBackgroundImage(UIImage.init(named: "unselected"), for: .normal)
                                   }
                                   
                                   if self.isBreastfeeding == true
                                   {
                                        self.breastfeedingBtn.setBackgroundImage(UIImage.init(named: "selected"), for: .normal)
                                   }
                                   else
                                   {
                                        self.breastfeedingBtn.setBackgroundImage(UIImage.init(named: "unselected"), for: .normal)
                                   }
                              }
                         })
                    }
               }
               catch let err as NSError
               {
                    print("error: \(err.description)")
               }
               
          }.resume()
     }

     func updateUserImage () {
        
        print("updateUserImage")
        
        let urlString = "\(appDelegate.serverDestination!)updateProfileImage.php"
        
        print("urlString: \(urlString)")
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&userimage=\(uploadImageName!)&devStatus=\(appDelegate.devStage!)"
        
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
          
            do {
               
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
               
                print("image jsonResult: \(jsonResult)")
               
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
               
                if dataDict["userImageData"]! is NSNull
                {
                    print("no user")
                }
                else
                {
                    let userDict : NSDictionary = dataDict["userImageData"] as! NSDictionary
                    
                    let status : String = userDict["status"] as! String
                    
                    print("status: \(status)")
                    
                    if (status == "profile image saved")
                    {
                        
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                         
                            self.showBasicAlert(string: "Profile image not saved. Please check your internet connection.")
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
          
          }.resume()
     }

     func uploadUserData() {
        
          submitBtn.isEnabled = false

          activityView.isHidden = false
          activityView.startAnimating()

          print("uploadUserData")

          let urlString = "\(appDelegate.serverDestination!)updateProfile.php"

          print("urlString: \(urlString)")

          let url = URL(string: urlString)

          var request = URLRequest(url: url!)

          request.httpMethod = "POST"

          var paramString = "fullname=\(fullnameTxt.text!)&dob=\(selectedDate!)&gender=\(genderSegment.selectedSegmentIndex)&isbreastfeeding=\(isBreastfeeding!)&ispregnant=\(isPregnant!)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"

          if self.isEditing == true
          {
               paramString = "\(paramString)&updating=true"
          }
          else
          {
               paramString = "\(paramString)&basicprofile=true"
          }

          //paramString = "\(paramString)&basicprofile=true"

          print("paramString: \(paramString)")

          request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))

          let session = URLSession.shared

          session.dataTask(with: request) {data, response, err in

            do {
               
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
               
                print("add user jsonResult: \(jsonResult)")
               
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
               
                if dataDict["userData"]! is NSNull
                {
                    print("no user")
                }
                else
                {
                    let userDict : NSDictionary = dataDict["userData"] as! NSDictionary
                    
                    let status : String = userDict["status"] as! String
                    
                    print("status: \(status)")
                    
                    if (status == "basic info saved")
                    {
                        DispatchQueue.main.sync(execute: {
                         
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                         
                            self.performSegue(withIdentifier: "SelectCuisines", sender: self)
                        })
                    }
                    else if (status == "profile updated")
                    {
                         DispatchQueue.main.sync(execute: {
                              
                              self.activityView.isHidden = true
                              self.activityView.stopAnimating()
                              
                              let name = self.fullnameTxt.text
                              let arr = name?.split(separator: " ")
                              
                              self.appDelegate.firstname = String(arr![0])
                              self.appDelegate.lastname = String(arr![1])
                              
                              if self.appDelegate.firstname != nil {
                                   self.prefs.setValue(self.appDelegate.firstname , forKey: "firstname")
                              }
                              if self.appDelegate.lastname != nil {
                                   self.prefs.setValue(self.appDelegate.lastname , forKey: "lastname")
                              }
                              
                              self.prefs.synchronize()
                              
                              //syncronize defaults
                              
                              //NotificationCenter.default.post(name: Notification.Name("updateUserInfo"), object: nil)
                              
                              //self.performSegue(withIdentifier: "SelectCuisines", sender: self)
                         })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                         
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                         
                            self.submitBtn.isEnabled = true
                            self.submitBtn.alpha = 1
                         
                            //self.showBasicAlert(string: status)
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }

          }.resume()
     }
     
     // MARK: Textfield Delegate
     
     func textFieldDidEndEditing(_ textField: UITextField) {
          
          if textField.restorationIdentifier == "fullname" && textField.text != ""
          {
               fullnameLbl.isHidden = false
               //usernameLblHeight.constant = 18
               
               //fullnameTxtTopPadding.constant = 0
          }
          else if textField.restorationIdentifier == "dob" && textField.text != ""
          {
               dobLbl.isHidden = false
               //emailLblHeight.constant = 18
               dobTxtTopPadding.constant = 5
          }
     }

     func downloadImage (imagename : String) {
          
          if appDelegate.downloadImages == true
          {
               let s3BucketName = "chewsrite/images"
               
               let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
               let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
               
               // Set the logging to verbose so we can see in the debug console what is happening
               //AWSLogger.default().logLevel = .none
               
               let downloadRequest = AWSS3TransferManagerDownloadRequest()
               downloadRequest?.bucket = s3BucketName
               downloadRequest?.key = imagename
               downloadRequest?.downloadingFileURL = downloadingFileURL
               
               let transferManager = AWSS3TransferManager.default()
               
               //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
               
               transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                    (task: AWSTask!) -> AWSTask<AnyObject>? in
                    
                    DispatchQueue.main.async(execute: {
                         
                         if task.error != nil
                         {
                              print("AWS Error downloading image")
                              
                              print(task.error.debugDescription)
                         }
                         else
                         {
                              //print("AWS download successful")
                              
                              var downloadOutput = AWSS3TransferManagerDownloadOutput()
                              
                              downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                              
                              print("downloadOutput photo: \(downloadOutput)");
                              print("downloadFilePath photo: \(downloadFilePath)");
                              
                              let image = UIImage(contentsOfFile: downloadFilePath)
                              
                              self.imageIV.image = image
                              self.appDelegate.userImage = image
                         }
                         
                         //println("test")
                    })
                    return nil
               }))
          }
          else
          {
               self.imageIV.image = UIImage.init(named: "profile-icon")
          }
     }

     override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
     }
     
     // MARK: - Navigation
     
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "FavoriteCuisines"
        {
          appDelegate.isAddingFamily = true
          let destination = segue.destination as! FavoriteCuisines
          //destination.isEditing = true
        }
    }
}

extension String {
     func sqlToDate(withFormat format: String = "yyyy-MM-dd 00:00:00") -> Date {
          let dateFormatter = DateFormatter()
          dateFormatter.dateFormat = format
          guard let date = dateFormatter.date(from: self) else {
               preconditionFailure("Take a look to your format")
          }
          return date
     }
}
