//
//  EditGroceryIngredient.swift
//  ChewsRite
//
//  Created by Randall Ridley on 8/22/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class EditGroceryIngredient: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var groceryTable: UITableView!
    @IBOutlet weak var substituteLbl: UILabel!
    
    @IBOutlet weak var ingredientLbl: UILabel!
    @IBOutlet weak var quantityTxt: UITextField!
    var groceryList = [GroceryItem]()
    var selectedItem : GroceryItem?
    var showingGrocery : Bool?
    var quantity: String?
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var ingredientIV: UIImageView!
    var item : NSMutableDictionary?
    var substituteList = [NSDictionary]()
    var selectedSubstituteName : String?
    var selectedSubstituteID : String?
    var statusImageCache : [String:UIImage]?
    
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    var recipeName: String?
    //var imageName: String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        activityView.isHidden = true
        statusLbl.isHidden = true
        
        ingredientLbl.text = recipeName
        
        //randall to do
        //does ingredient have photo?
        
        let imagename = selectedItem?.imagename
        
        let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
        
        if let img = statusImageCache![urlString]
        {
            ingredientIV.image = img
        }
        else
        {
            ingredientIV.image = UIImage(named: "profile-icon")
            
            self.downloadImage(imagename: imagename!, urlString: urlString)
        }
        
        quantity = "1 oz"
        quantityTxt.text = quantity
        
        let attributedString = NSMutableAttributedString(string: "If you don\'t have \(selectedItem!.ingredientname) you can substitute per cup of \(selectedItem!.ingredientname) with (please select one): ", attributes: [
            .font: UIFont(name: "Avenir-Light", size: 14.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0)
            ])
        attributedString.addAttribute(.foregroundColor, value: appDelegate.crWarmGray, range: NSRange(location: 55+(selectedItem!.ingredientname.count * 2), length: 19))
        
        substituteLbl.attributedText = attributedString
        
        showingGrocery = false
        
        groceryTable.delegate = self
        groceryTable.dataSource = self
        groceryTable.tableFooterView = UIView()
        
        getSubstitution()
        //debug()
        
        let btnItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(dismissKeyboard))
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width:  self.view.frame.size.width, height: 50))
        numberToolbar.backgroundColor = UIColor.darkGray
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.tintColor = UIColor.black
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            btnItem]
        
        numberToolbar.sizeToFit()
        
        quantityTxt.inputAccessoryView = numberToolbar
    }
    
    @objc func dismissKeyboard () {
        
        self.view.endEditing(true)
    }
    
    func debug () {
        
        ingredientLbl.text = recipeName
        ingredientIV.image = #imageLiteral(resourceName: "risotto")
        
        quantity = "1 oz"
        quantityTxt.text = quantity
        
//        var dict : NSMutableDictionary = ["name" : "Millet","serving" : "1 oz", "active" : false]
//        groceryList.append(dict)
//
//        dict  = ["name" : "quinoa","serving" : "2 oz", "active" : false]
//        groceryList.append(dict)
        
                
//        substituteList.append("Millet")
//        substituteList.append("Rice")
        
        groceryTable.reloadData()
    }
    
    // MARK: Webservice
    
    func getSubstitution() {
        
        substituteList.removeAll()
        
        print("getSubstitution")
        
        let dataString = "substitutesData"
        
        let urlString = "\(appDelegate.serverDestination!)getSubstitutions.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)&ingredientname=\(selectedItem!.ingredientname)"
        
        //let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)&ingredientname=Butter"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("substitution jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString] is NSNull
                {
                    print("no sub")
                }
                else
                {
                    let uploadData = dataDict[dataString]! as! NSDictionary

                    if uploadData["substitutions"] == nil {
                        
                        print("no data")
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.statusLbl.isHidden = false
                            
                            self.substituteList.removeAll()
                            self.groceryTable.reloadData()
                        })
                    }
                    else
                    {
                        //let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                        
                        
                        let substitutionsData = (uploadData["substitutions"]! as! NSArray).mutableCopy() as! NSMutableArray
                        
                        let substitutions = substitutionsData as! [NSDictionary]
                        
                        var hasData = false
                        
                        if (substitutions.count > 0)
                        {
                            hasData = true
                            
                            for dict in substitutions
                            {
                                self.substituteList.append(dict)
                            }
                            
                            DispatchQueue.main.sync(execute: {
                                
                                if hasData == true
                                {
                                   self.groceryTable.reloadData()
                                }
                                else
                                {
                                    print("no data")
                                    
                                   self.statusLbl.isHidden = false
                                    self.substituteList.removeAll()
                                    self.groceryTable.reloadData()
                                }
                            })
                        }
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func saveItems() {
        
        self.activityView.isHidden = false
        self.activityView.startAnimating()
        
        submitBtn.isEnabled = false
        submitBtn.backgroundColor = appDelegate.crWarmGray
        
        print("saveItems")
                
        let dataString = "groceryItemsData"
        
        let urlString = "\(appDelegate.serverDestination!)manageSingleGroceryItem.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        //to do randall
        
        //remove current ingredientid from recipeingredients (or add enabled field to table and check for enabled in related queries)
        //add subtitute id recipe subtitutes table
        
        let paramString = "userid=\(appDelegate.userid!)&subid=\(selectedSubstituteID!)&ingredientid=\(selectedItem!.ingredientid)&recipeid=\(selectedItem!.recipeid)&devStatus=\(appDelegate.devStage!)&active=true&isSubstitute=true&insert=true"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("substitute items jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.showBasicAlert(string: "No data error")
                    })
                }
                else
                {
                    let dict : NSDictionary = dataDict[dataString] as! NSDictionary
                    let status : String = dict["status"] as! String
                    //let subID : String = dict["subID"] as! String
                    
                    print("status: \(status)")
                    
                    if (status == "substitution added")
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.backgroundColor = self.appDelegate.crOrange
                            
                            //update previous screen selections
                            
                            var substituteItems = [GroceryItem]()
                            
                            let item = GroceryItem(ingredientname: self.selectedSubstituteName!, serving: "", active: true, recipeid: self.selectedItem!.recipeid, ingredientid: self.selectedSubstituteID!, ingredientsid: "", isSubstitute: true, imagename: "")
                            
                            //substituteItems.append(item)
                            
                            //update GroceryList.swift
                            
                            NotificationCenter.default.post(name: Notification.Name("updateSubstituteItems"), object: item)
                            
                            //show alert
                            
                            let alert = UIAlertController(title: nil, message: "Substitute Saved Successfully", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style {
                                    
                                case .default:
                                    print("default")
                                    
                                    self.navigationController?.popViewController(animated: true)
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.backgroundColor = self.appDelegate.crOrange
                            
                            self.showBasicAlert(string: status)
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getSubstitutionOrig () {
        
        print("getSubstitution")
        
        //var ingredient = "butter"
        
        let headers = [
            "X-Mashape-Key": "A6jIcNophWmshQEMCrJucq0gQW1xp1oyts4jsnqkBJhn4r3Iu3",
            "X-Mashape-Host": "spoonacular-recipe-food-nutrition-v1.p.mashape.com",
            "Cache-Control": "no-cache",
            "Postman-Token": "a29c5037-371f-4de0-846a-5410e183b0d6"
        ]
        
        let a = ingredientLbl.text?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        //print(selectedIngredient!)
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/food/ingredients/substitutes?ingredientName=\(a!)")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if (error != nil) {
                print(error)
            } else {
                
                //                let httpResponse = response as? HTTPURLResponse
                //                print(httpResponse)
                
                do {
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    
                    print("substitutes jsonResult: \(jsonResult)")
                    
                    //let dataDict : NSDictionary = jsonResult.object(forKey: "ingredientData") as! NSDictionary
                    //let status : String = jsonResult.object(forKey: "status") as! String
                    
                    if jsonResult["status"]! is NSNull || jsonResult["message"] as! String == "Could not find any substitutes for that ingredient."  {
                        
                        print("no data")
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.substituteList.removeAll()
                            self.groceryTable.reloadData()
                        })
                    }
                    else
                    {
                        let substituteData : NSMutableArray = (jsonResult["substitutes"]! as! NSArray).mutableCopy() as! NSMutableArray
                        
                        if (substituteData.count > 0)
                        {
                            DispatchQueue.main.sync(execute: {
                                
                                //self.noResultsMain.isHidden = true
                            })
                            
                            //self.selectedList = substituteData as! [NSMutableDictionary]
                            self.substituteList = substituteData as! [NSDictionary]
                            
                            DispatchQueue.main.sync(execute: {
                                
                                self.groceryTable.reloadData()
                                //self.groceryTable.isHidden = false
                            })
                        }
                        else
                        {
                            DispatchQueue.main.sync(execute: {
                                
                                //self.noResultsMain.isHidden = false
                            })
                        }
                    }
                }
                catch let err as NSError
                {
                    print("error: \(err.description)")
                }
            }
        })
        
        dataTask.resume()
    }
    
    // MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.substituteList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func downloadImage (imagename : String, urlString : String) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache![urlString] = image
                        self.ingredientIV.image = image
                    }
                    
                    //println("test")
                })
                return nil
            }))
        }
        else
        {
            self.ingredientIV.image = UIImage.init(named: "profile-icon")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : GroceryIngredientCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! GroceryIngredientCell
        
        //let dict = self.groceryList[(indexPath as NSIndexPath).row] as NSDictionary
        //let reviewdate = dict.object(forKey: "name") as! String
        
        
        let dict = self.substituteList[(indexPath as NSIndexPath).row]
        let str = dict.object(forKey: "ingredientname") as! String
        let id = dict.object(forKey: "subid") as! String
        
        cell.selectionStyle = .none
        cell.ingredientLbl.text = str
        
        cell.selectBtn.restorationIdentifier = "\(indexPath.row)"
        cell.selectBtn.addTarget(self, action: #selector(self.selectItem(_:)), for: UIControlEvents.touchUpInside)
        
        if selectedSubstituteID == id
        {
            cell.selectBtn.setBackgroundImage(#imageLiteral(resourceName: "checkGreen"), for: .normal)
        }
        else
        {
            cell.selectBtn.setBackgroundImage(#imageLiteral(resourceName: "unselected-1"), for: .normal)
        }
        
        return cell
    }
    
    @IBAction func selectItem(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        let ind = Int(btn.restorationIdentifier!)
        
        //item = groceryList[ind!]
        selectedSubstituteName = (substituteList[ind!].object(forKey: "ingredientname") as! String)
        selectedSubstituteID = (substituteList[ind!].object(forKey: "subid") as! String)
        
        groceryTable.reloadData()
    }
    
    @IBAction func saveIngredient(_ sender: Any) {
        
        if selectedSubstituteID != nil
        {
            saveItems()
        }
        else
        {
            showBasicAlert(string: "Please select a substitution")
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
     // MARK: - Navigation
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destination = segue.destination as! RecipeDetails
                
        destination.recipeID = selectedItem?.recipeid
        destination.selectedImage = ingredientIV.image
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
