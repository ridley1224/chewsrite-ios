//
//  FavoriteCuisines.swift
//  ChewsRite
//
//  Created by Randall Ridley on 5/19/18.
//  Copyright 2018 RT. All rights reserved.
//

import UIKit

class FavoriteCuisines: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var cuisineCV: UICollectionView!
    
    var cuisines = NSMutableArray()
    var selectedCuisines = [NSDictionary]()
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var submitBtn: UIButton!
    
    var newFamilyMemberID : Int?
    var firstname : String?
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var headerLeading: NSLayoutConstraint!
    
    
    @IBOutlet weak var progressView: UIView!
    
    var creatingFamily : Bool?
    
    override func viewDidLoad() {
        
        
        let pg = UIView.init(frame: CGRect.init(x: 0, y: 0, width: progressView.frame.width * 0.38, height: progressView.frame.height))
        
        pg.backgroundColor = appDelegate.crLightBlue
        progressView.addSubview(pg)
        
        super.viewDidLoad()

        cuisineCV.delegate = self
        cuisineCV.dataSource = self
        
        activityView.isHidden = true
        
//        if !isEditing
//        {
//            backBtn.isHidden = true
//        }
        
        
        print("frame size: \(self.view.frame.width)")
        
        var fontSize : CGFloat = 16.0
        
        if self.view.frame.width <= 320 && self.view.frame.width <= 400
        {
            fontSize = 14.0
        }
        else if self.view.frame.width > 400
        {
            //fontSize = 14.0
            
            headerLeading.constant = 30
        }
        
        var header = "Step 1: Let's start with favorite cuisines"
        
        let attributedString = NSMutableAttributedString(string:header, attributes: [
            .font: UIFont(name: "Avenir-Medium", size: fontSize)!,
            .foregroundColor: UIColor.white
            ])
        
        if firstname != nil
        {
            header = "Step 1: \(firstname!)'s favorite cuisines"
        }
            
        attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Book", size: fontSize)!, range: NSRange(location: 0, length: 7))
        
        headerLbl.attributedText = attributedString
        
        getCuisines()
    }
    
    func getCuisines() {
        
        print("getMyCuisines")
        
        let dataString = "cuisineData"
        
        let urlString = "\(appDelegate.serverDestination!)getCuisines.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "register=true&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("cuisines jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.cuisineCV.isHidden = true
                        //self.recipeStatusTV.isHidden = false
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.cuisines.add(dict2)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if hasData == true
                            {
                                self.cuisineCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
                                self.cuisineCV.isHidden = true
                                //self.noResultsMain.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func uploadLikesDislikes() {
        
        submitBtn.isEnabled = false
        
        activityView.isHidden = false
        activityView.startAnimating()
        
        print("uploadLikesDislikes")
        
        var cuisineString = ""
        
        if selectedCuisines.count > 0
        {
            var cuisineids = ""
            var i = 0
            
            for a in selectedCuisines
            {
                if i == 0
                {
                    cuisineids = (a.object(forKey: "cuisineid") as? String)!
                }
                else
                {
                    cuisineids = "\(cuisineids),\(a.object(forKey: "cuisineid") as! String)"
                }
                
                i += 1
            }
            
            cuisineString = "\(cuisineids)"
        }
        
        let urlString = "\(appDelegate.serverDestination!)manageFavoriteCuisines.php"
        
        print("urlString: \(urlString)")
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var paramString = ""

        if newFamilyMemberID != nil
        {
            paramString = "selections=\(cuisineString)&userid=\(newFamilyMemberID!)&devStatus=\(appDelegate.devStage!)"
        }
        else
        {
            paramString = "selections=\(cuisineString)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        }
        
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("cuisines jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict["favoriteData"]! is NSNull
                {
                    print("no user")
                }
                else
                {
                    let userDict : NSDictionary = dataDict["favoriteData"] as! NSDictionary
                    
                    let status : String = userDict["status"] as! String
                    
                    print("status: \(status)")
                    
                    if (status == "usercuisines saved" || status == "usercuisines updated")
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.alpha = 1
                            
                            if (self.appDelegate.signingUp == true || self.newFamilyMemberID != nil)
                            {
                                self.performSegue(withIdentifier: "DietaryConcerns", sender: self)
                            }
                            else
                            {
                                //refresh family members
                                
                                NotificationCenter.default.post(name: Notification.Name("refreshFamily"), object: nil)
                                
                                let alert = UIAlertController(title: nil, message: "Cusines Updated", preferredStyle: UIAlertControllerStyle.alert)
                                
                                self.present(alert, animated: true, completion: nil)
                                
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                    switch action.style{
                                    case .default:
                                        print("default")
                                        
                                        for controller in self.navigationController!.viewControllers as Array {
                                            
                                            if controller.isKind(of: ManageFamilyMembers.self) {
                                                
                                                self.navigationController!.popToViewController(controller, animated: true)
                                                break
                                            }
                                        }
                                        
                                    case .cancel:
                                        print("cancel")
                                        
                                    case .destructive:
                                        print("destructive")
                                    }
                                }))
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.alpha = 1
                            
                            //self.showBasicAlert(string: status)
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }

    @IBAction func next(_ sender: Any) {
        
        uploadLikesDislikes()
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let kWhateverHeightYouWant = 190
        
        return CGSize.init(width: 600, height: CGFloat(kWhateverHeightYouWant))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.cuisines.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let dict = self.cuisines[indexPath.row] as! NSDictionary
        
        //let id = dict.object(forKey: "cuisineid") as! String
        let name = dict.object(forKey: "cuisinename") as! String
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? CuisinesCollectionviewCell
        
        cell?.cuisineLbl.text = name
        
        if selectedCuisines.contains(dict)
        {
            cell?.cuisineLbl.backgroundColor = appDelegate.crLightBlue
            cell?.cuisineLbl.textColor = UIColor.white
        }
        else
        {
            cell?.cuisineLbl.backgroundColor = UIColor.white
            cell?.cuisineLbl.textColor = appDelegate.crLightBlue
        }
                
        cell?.cuisineLbl.layer.borderWidth = 1.0
        cell?.cuisineLbl.layer.borderColor = appDelegate.crLightBlue.cgColor
        cell?.cuisineLbl.layer.cornerRadius = 4.0
        
        //cell?.statusLbl.isHidden = true
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dict = self.cuisines[indexPath.row] as! NSDictionary
        
        //let id = dict.object(forKey: "cuisineid") as! String
//        let name = dict.object(forKey: "cuisinename") as! String
//
//        print("cuisinename: \(name)")
        
        if selectedCuisines.contains(dict)
        {
            let ind = selectedCuisines.firstIndex(of: dict)
            selectedCuisines.remove(at: ind!)
        }
        else
        {
            selectedCuisines.append(dict)
        }
        
        print("selectedCuisines: \(selectedCuisines)")
        
        cuisineCV.reloadData()
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation
    
    @IBAction func goBack(_ sender: Any) {
        
        dismiss()
    }
    
    func dismiss() {
        
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "DietaryConcerns"
        {
            let destination = segue.destination as! DietaryConcerns
            destination.newFamilyMemberID = newFamilyMemberID
            destination.firstname = firstname
        }
    }
}
