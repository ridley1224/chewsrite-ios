//
//  AllReviews.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/21/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class AllReviews: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var folderTable: UITableView!
    @IBOutlet weak var folderNameTxt: UITextField!
    @IBOutlet weak var recipeIV: UIImageView!
    
    @IBOutlet weak var headerLbl: UILabel!
    var foldersList = NSMutableArray()
    var friendsList = NSMutableArray()
    var selectedList = [NSDictionary]()
    var selectedItems = [NSDictionary]()
    
    var isSharingFriends : Bool?
    var recipeID : String?
    var recipeName : String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        folderTable.delegate = self
        folderTable.dataSource = self
        folderTable.tableFooterView = UIView()
        
        //debug()
        
        headerLbl.text = recipeName
        
        getReviews()
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        keyboardDoneButtonView.items = [doneButton]
        
        folderNameTxt?.inputAccessoryView = keyboardDoneButtonView
    }
    
    func debug () {
        
        let dict : NSMutableDictionary  = ["username" : "Marguerite_Mullins", "reviewdate" : "05/09/2017", "review" : "This delicious recipe tops the good-for-you quinoa grain with pistachio-crusted chicken, a refreshing chopped salad, and a bit of jalapeno for good measure.", "rating" : "4"]
        
        selectedList.append(dict)
        
        folderTable.reloadData()
    }
    
    @objc func doneClicked(sender: UIButton!) {
        
        //selectedList.append(folderNameTxt.text!)
        folderTable.reloadData()
        
        self.view.endEditing(true)
    }
    
    // MARK: Webservice
    
    func getReviews() {
        
        print("getReviews")
        
        let dataString = "reviewsData"
        
        let urlString = "\(appDelegate.serverDestination!)getAllReviews.php"
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "recipeid=\(recipeID!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    self.selectedList = dataDict[dataString]! as! [NSDictionary]
                    
                    if self.selectedList.count > 0
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.folderTable.reloadData()
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.showBasicAlert(string: "Please check your internet connection.")
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func saveRecipe(_ sender: Any) {
        
        saveRecipe()
    }
    
    func saveRecipe () {
        
        //Alamofire to send dict
        
        NotificationCenter.default.post(name: Notification.Name("sharedToFriendsGroups"), object: nil)
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.selectedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ReviewsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "review-cell")! as! ReviewsTableViewCell
        
        let dict = self.selectedList[(indexPath as NSIndexPath).row]
        
        let reviewdate = dict.object(forKey: "reviewdate") as! String
        let username = dict.object(forKey: "username") as! String
        let review = dict.object(forKey: "review") as! String
        let rating = dict.object(forKey: "rating") as! String
        
        if indexPath.row % 2 == 0
        {
            cell.backgroundColor = appDelegate.whitefive
        }
        else
        {
            cell.backgroundColor = .white
        }
        
        if rating == "1"
        {
            cell.ratingIV.image = #imageLiteral(resourceName: "b1star")
        }
        else if rating == "2"
        {
            cell.ratingIV.image = #imageLiteral(resourceName: "b2star")
        }
        else if rating == "3"
        {
            cell.ratingIV.image = #imageLiteral(resourceName: "b3star")
        }
        else if rating == "4"
        {
            cell.ratingIV.image = #imageLiteral(resourceName: "b4star")
        }
        else if rating == "5"
        {
            cell.ratingIV.image = #imageLiteral(resourceName: "b5star")
        }
        
        cell.selectionStyle = .none
        
        cell.dateLbl.text = reviewdate
        cell.usernameLbl.text = username
        cell.reviewLbl.text = review
        
        //cell.titleLbl.font = UIFont(name: "Avenir-Medium", size: 14.0)
        
        return cell
    }
    
    @objc func saveToFolder(_ sender : UIButton) {
        
        let btn = sender as UIButton
        let ind = Int(btn.restorationIdentifier!)
        
        let dict = self.selectedList[ind!]
        
        let active = dict.object(forKey: "active") as! Bool
        
        if active == true
        {
            dict.setValue(false, forKey: "active")
            
            if selectedItems.contains(dict)
            {
                selectedItems.remove(at: selectedItems.index(of: dict)!)
            }
        }
        else
        {
            dict.setValue(true, forKey: "active")
            selectedItems.append(dict)
        }
        
        folderTable.reloadData()
        
        print("selectedItems: \(selectedItems)")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //var selectedIem = self.foldersList[(indexPath as NSIndexPath).row]
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
