//
//  ManageFamilyMembers.swift
//  ChewsRite
//
//  Created by DTO MacBook11 on 11/6/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class ManageFamilyMembers: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var membersTable: UITableView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var menuBtn: UIButton!
    
    var selectedMember : NSMutableDictionary?
    
    var familyList = [NSMutableDictionary]()
    
    var statusImageCache = [String:UIImage]()
    var selectedImage : UIImage?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        menuBtn.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
//        let tb : TabController = self.parent as! TabController
//
//        menuBtn.addTarget(self, action: #selector(tb.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
        membersTable.delegate = self
        membersTable.dataSource = self
        membersTable.isHidden = true
        membersTable.tableFooterView = UIView()
        
        membersTable.separatorStyle = .none
        
        statusView.isHidden = true
        
        getFamily()
        
        NotificationCenter.default.addObserver(self,
                                   selector: #selector(refreshFamilyNotification),
                                   name: NSNotification.Name(rawValue: "refreshFamily"),
                                   object: nil)

        // Do any additional setup after loading the view.
    }
    
    @objc func refreshFamilyNotification (notification: NSNotification) {
        
        if notification.object != nil
        {
            let urlString = notification.object as! String
            
            if statusImageCache[urlString] != nil
            {
                print("remove cached image")
                statusImageCache[urlString] = nil
            }
        }
        
        getFamily()
    }
    
    func getFamily() {
        
        familyList.removeAll()
        
        print("getFamily")
        
        let dataString = "familyData"
        
        let urlString = "\(appDelegate.serverDestination!)getFamilyMembers.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
//        let dict = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!, "fullname":appDelegate.firstname!] as NSMutableDictionary
//        familyList.append(dict)
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("friends jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.statusView.isHidden = false
                        self.membersTable.isHidden = true
                        self.membersTable.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let members = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (members.count > 0)
                    {
                        hasData = true
                        
                        for dict in members
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.familyList.append(dict2)
                        }
                                                
                        DispatchQueue.main.sync(execute: {
                            
                            if hasData == true
                            {
                                //self.recipeStatusTV.text = "You currently have no friend requests."
                                self.statusView.isHidden = true
                                self.membersTable.reloadData()
                                self.membersTable.isHidden = false
                            }
                            else
                            {
                                print("no data")
                                
                                self.statusView.isHidden = false
                                self.membersTable.isHidden = true
                            }
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    // MARK: Tableview
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //
    //        if tableView.restorationIdentifier == "friendTable"
    //        {
    //            return 44
    //        }
    //        else
    //        {
    //            return 0
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.familyList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 88
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedMember = familyList[indexPath.row]
        
        let imagename = selectedMember?.object(forKey: "userimage") as! String
        
        if imagename != ""
        {
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
            
            if let img = statusImageCache[urlString]
            {
                selectedImage = img
            }
        }
        
        performSegue(withIdentifier: "EditFamilyMember", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var dict : NSDictionary?
        
        var cell : FriendRequestCell?
        
        cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as? FriendRequestCell
        dict = self.familyList[(indexPath as NSIndexPath).row] as NSDictionary
        
        cell?.friendIV.image = UIImage(named: "profile-icon")
        
        let imagename = dict?.object(forKey: "userimage") as? String
        
        if imagename != nil
        {
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
            
            if let img = statusImageCache[urlString]
            {
                cell?.friendIV.image = img
            }
            else
            {
                self.downloadImage(imagename: imagename!, urlString: urlString, cell: (cell)!)
            }
        }
        
        let firstname = dict?.object(forKey: "fullname") as! String

        cell?.selectionStyle = .none
        cell?.nameLbl.text = "\(firstname)"
        
        return (cell)!
    }
    
    func downloadImage (imagename : String, urlString : String, cell: FriendRequestCell?) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache[urlString] = image
                        
                        cell?.friendIV.image = image
                    }
                    
                    //println("test")
                })
                
                return nil
            }))
        }
        else
        {
            cell?.friendIV.image = UIImage.init(named: "profile-icon")
        }
    }
    
    @IBAction func  cancel() {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "EditFamilyMember"
        {
            let destination = segue.destination as! AddFamilyMember
            destination.selectedImage = selectedImage
            destination.selectedMember = selectedMember
            destination.isEditing = true
        }
    }
}
