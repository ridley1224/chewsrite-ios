//
//  NutritionalGoals.swift
//  ChewsRite
//
//  Created by Randall Ridley on 5/19/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class NutritionalGoals: UIViewController,UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var otherTxt: UITextField!
    
    var category : String?
    
    @IBOutlet weak var v1: UIView!
    @IBOutlet weak var v2: UIView!
    
    @IBOutlet weak var b1: UIButton!
    @IBOutlet weak var b2: UIButton!
    
    var selectedList : NSMutableArray?
    var currentButtons = [UIButton]()
    var isUpdating: Bool?
    
    var firstname : String?
    
    var nutritionLabelList = ["Calories","Carbohydrates","Protein","Fat","     Saturated Fat","     Unsaturated Fat","     Trans Fat","Cholestrol","Sodium","Potassium","Fiber","Sugars","Vitamin A","Vitamin C","Calcium","Iron"]
    
    var dailyList = NSMutableArray()
    var weeklyList = NSMutableArray()
    var savedVals = NSMutableArray()
    var fields = [UITextField]()
    
    @IBOutlet weak var nutritionTable: UITableView!
    
    var numberToolbar : UIToolbar?
    
    @IBOutlet weak var submitBtn: UIButton!
    
    var currentTextfieldValue = ""
    var newFamilyMemberID : Int?
    
    @IBOutlet weak var progressView: UIView!
    
    // MARK: viewDidLoad
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //appDelegate.userid = "1"
        
        category = "0"
        selectedList = dailyList
        
        for val in nutritionLabelList
        {
            if val == "Calories"
            {
                dailyList.add("0")
                weeklyList.add("0")
            }
            else
            {
                dailyList.add("0.0")
                weeklyList.add("0.0")
            }
        }
        
        nutritionTable.delegate = self
        nutritionTable.dataSource = self
        
        activityView.isHidden = true        
        //v2.isHidden = true
        v2.backgroundColor = .white
        
        //print("frame size: \(self.view.frame.width)")
        
        var fontSize : CGFloat = 16.0
        
        if self.view.frame.width <= 320
        {
            fontSize = 14.0
        }
        
        var header = "Step 4: Your Custom Nutritional Goals"
        var name = "Step 4:"
        
        if firstname != nil
        {
            //header = "Step 4: \(firstname!)'s Custom Nutritional Goals"
            header = "Step 4: \(firstname!)'s Custom Nutritional Goals"
            name = "\(firstname!)'s"
            
            //progressView.isHidden = true
        }
        else if self.isEditing
        {
            header = "\(appDelegate.firstname!)'s Custom Nutritional Goals"
            headerLbl.textAlignment = .center
            
            name = "\(appDelegate.firstname!)'s"
            
            getNutritionGoals()
            
            //progressView.isHidden = true
        }
        else
        {
            let pg = UIView.init(frame: CGRect.init(x: 0, y: 0, width: progressView.frame.width * 1.0, height: progressView.frame.height))
            
            pg.backgroundColor = appDelegate.crLightBlue
            progressView.addSubview(pg)
            
            populateDefaults()
        }
        
        let attributedString = NSMutableAttributedString(string: header, attributes: [
            .font: UIFont(name: "Avenir-Medium", size: fontSize)!,
            .foregroundColor: UIColor.white
            ])
        
        attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Light", size: fontSize)!, range: NSRange(location: 0, length: name.count))
        
        headerLbl.attributedText = attributedString
        
        let btnItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(dismissKeyboard))
        
        numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width:  self.view.frame.size.width, height: 50))
        
        numberToolbar?.backgroundColor = UIColor.darkGray
        numberToolbar?.barStyle = UIBarStyle.default
        numberToolbar?.tintColor = UIColor.black
        numberToolbar?.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            btnItem]
        
        numberToolbar?.sizeToFit()
        
        nutritionTable.tableFooterView = UIView()
        nutritionTable.reloadData()
        
        scrollToBottom()
        scrollToTop()
    }
    
    func populateDefaultsCD() {
        
//        self.weeklyList[0] = "0"
//        self.weeklyList[1] = "0"
//        self.weeklyList[2] = "0"
//        self.weeklyList[3] = "0"
//        self.weeklyList[4] = "0"
//        self.weeklyList[5] = "0"
//        self.weeklyList[6] = "0"
//        self.weeklyList[7] = "0"
//        self.weeklyList[8] = "0"
//        self.weeklyList[9] = "0"
//        self.weeklyList[10] = "0"
//        self.weeklyList[11] = "0"
//        self.weeklyList[12] = "0"
//        self.weeklyList[13] = "0"
//        self.weeklyList[14] = "0"
//        self.weeklyList[15] = "0"
//
//        self.dailyList[0] = "0"
//        self.dailyList[1] = "0"
//        self.dailyList[2] = "0"
//        self.dailyList[3] = "0"
//        self.dailyList[4] = "0"
//        self.dailyList[5] = "0"
//        self.dailyList[6] = "0"
//        self.dailyList[7] = "0"
//        self.dailyList[8] = "0"
//        self.dailyList[9] = "0"
//        self.dailyList[10] = "0"
//        self.dailyList[11] = "0"
//        self.dailyList[12] = "0"
//        self.dailyList[13] = "0"
//        self.dailyList[14] = "0"
//        self.dailyList[15] = "0"
//
//        nutritionTable.reloadData()
    }
    
    @objc func dismissKeyboard () {
        
        self.view.endEditing(true)
    }
    
    func scrollToBottom() {
        
        DispatchQueue.main.async {
            
            let indexPath = IndexPath(row: self.fields.count-1, section: 0)
            self.nutritionTable.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
    func scrollToTop() {
        
        DispatchQueue.main.async {
            
            let indexPath = IndexPath(row: 0, section: 0)
            self.nutritionTable.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
    // MARK: Actions
    
    @IBAction func next(_ sender: Any) {
        
        uploadGoals()
    }
    
    @IBAction func showOptions(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        if btn.restorationIdentifier == "b1"
        {
//            v1.isHidden = false
//            v2.isHidden = true
            
            v1.backgroundColor = appDelegate.crLightBlue
            v2.backgroundColor = .white
            
            selectedList = dailyList
            
            category = "0"
        }
        else if btn.restorationIdentifier == "b2"
        {
//            v1.isHidden = true
//            v2.isHidden = false
            
            v1.backgroundColor = .white
            v2.backgroundColor = appDelegate.crLightBlue
            
            selectedList = weeklyList
            
            category = "1"
        }
        
        fields.removeAll()
        nutritionTable.reloadData()
        
        //        scrollToBottom()
        //        scrollToTop()
    }
    
    // MARK: Webservice
    
    func populateDefaults() {
        
        print("getNutritionGoals")
        
        let dataString = "nutritionData"
        
        let urlString = "\(appDelegate.serverDestination!)getDefaultNutrition.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        appDelegate.dob = "1998-11-05 00:00:00"
        
        let dobDate = appDelegate.dob?.sqlToDate()
        
        let form = DateComponentsFormatter()
        form.maximumUnitCount = 1
        form.unitsStyle = .short
        form.allowedUnits = [.year]
        let s = form.string(from: dobDate!, to: Date())

        //let calendar = NSCalendar.current
        
        let age = s?.split(separator: " ")
        
        //let diff = dobDate.timeIntervalSinceDate(Date())

        
        print("s: \(age![0])")
        
//        $_POST['userid'] = "22";
//        $_POST['gender'] = "0";
//        $_POST['age'] = 36;
        
//        appDelegate.userid = "22"
//        appDelegate.gender = 0
        
        let paramString = "userid=\(appDelegate.userid!)&age=\(age![0])&gender=\(appDelegate.gender!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("nutrition jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.requestsTable.reloadData()
                        //self.recipeStatusTV.isHidden = false
                    })
                }
                else
                {
                    //cholestrol and sodium (ex. <500)
                    
                    let nutritionData = dataDict[dataString]! as! NSDictionary
                    
                    self.dailyList[0] = nutritionData.object(forKey: "dCalories") as! String
                    self.dailyList[1] = nutritionData.object(forKey: "dCarbohydrates") as! String
                    self.dailyList[2] = nutritionData.object(forKey: "dProtein") as! String
                    self.dailyList[3] = nutritionData.object(forKey: "dFat") as! String
                    self.dailyList[4] = nutritionData.object(forKey: "dSaturatedFat") as! String
                    self.dailyList[5] = nutritionData.object(forKey: "dUnsaturatedFat") as! String
                    self.dailyList[6] = nutritionData.object(forKey: "dTransFat") as! String
                    self.dailyList[7] = nutritionData.object(forKey: "dCholestrol") as! String
                    self.dailyList[8] = nutritionData.object(forKey: "dSodium") as! String
                    self.dailyList[9] = nutritionData.object(forKey: "dPotassium") as! String
                    self.dailyList[10] = nutritionData.object(forKey: "dFiber") as! String
                    self.dailyList[11] = nutritionData.object(forKey: "dSugars") as! String
                    self.dailyList[12] = nutritionData.object(forKey: "dVitaminA") as! String
                    self.dailyList[13] = nutritionData.object(forKey: "dVitaminC") as! String
                    self.dailyList[14] = nutritionData.object(forKey: "dCalcium") as! String
                    self.dailyList[15] = nutritionData.object(forKey: "dIron") as! String
                    
                    self.weeklyList[0] = nutritionData.object(forKey: "wCalories") as! String
                    self.weeklyList[1] = nutritionData.object(forKey: "wCarbohydrates") as! String
                    self.weeklyList[2] = nutritionData.object(forKey: "wProtein") as! String
                    self.weeklyList[3] = nutritionData.object(forKey: "wFat") as! String
                    self.weeklyList[4] = nutritionData.object(forKey: "wSaturatedFat") as! String
                    self.weeklyList[5] = nutritionData.object(forKey: "wUnsaturatedFat") as! String
                    self.weeklyList[6] = nutritionData.object(forKey: "wTransFat") as! String
                    self.weeklyList[7] = nutritionData.object(forKey: "wCholestrol") as! String
                    self.weeklyList[8] = nutritionData.object(forKey: "wSodium") as! String
                    self.weeklyList[9] = nutritionData.object(forKey: "wPotassium") as! String
                    self.weeklyList[10] = nutritionData.object(forKey: "wFiber") as! String
                    self.weeklyList[11] = nutritionData.object(forKey: "wSugars") as! String
                    self.weeklyList[12] = nutritionData.object(forKey: "wVitaminA") as! String
                    self.weeklyList[13] = nutritionData.object(forKey: "wVitaminC") as! String
                    self.weeklyList[14] = nutritionData.object(forKey: "wCalcium") as! String
                    self.weeklyList[15] = nutritionData.object(forKey: "wIron") as! String
                    
                    
                    
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.nutritionTable.reloadData()
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getNutritionGoals() {
        
        print("getNutritionGoals")
        
        let dataString = "nutritionData"
        
        let urlString = "\(appDelegate.serverDestination!)getUserNutritionInfo.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("nutrition jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.requestsTable.reloadData()
                        //self.recipeStatusTV.isHidden = false
                    })
                }
                else
                {
                    let nutritionData = dataDict[dataString]! as! NSDictionary
                    
                    self.weeklyList[0] = nutritionData.object(forKey: "wCalories") as! String
                    self.weeklyList[1] = nutritionData.object(forKey: "wCarbohydrates") as! String
                    self.weeklyList[2] = nutritionData.object(forKey: "wProtein") as! String
                    self.weeklyList[3] = nutritionData.object(forKey: "wFat") as! String
                    self.weeklyList[4] = nutritionData.object(forKey: "wSaturatedFat") as! String
                    self.weeklyList[5] = nutritionData.object(forKey: "wUnsaturatedFat") as! String
                    self.weeklyList[6] = nutritionData.object(forKey: "wTransFat") as! String
                    self.weeklyList[7] = nutritionData.object(forKey: "wCholestrol") as! String
                    self.weeklyList[8] = nutritionData.object(forKey: "wSodium") as! String
                    self.weeklyList[9] = nutritionData.object(forKey: "wPotassium") as! String
                    self.weeklyList[10] = nutritionData.object(forKey: "wFiber") as! String
                    self.weeklyList[11] = nutritionData.object(forKey: "wSugars") as! String
                    self.weeklyList[12] = nutritionData.object(forKey: "wVitaminA") as! String
                    self.weeklyList[13] = nutritionData.object(forKey: "wVitaminC") as! String
                    self.weeklyList[14] = nutritionData.object(forKey: "wCalcium") as! String
                    self.weeklyList[15] = nutritionData.object(forKey: "wIron") as! String
                    
                    self.dailyList[0] = nutritionData.object(forKey: "dCalories") as! String
                    self.dailyList[1] = nutritionData.object(forKey: "dCarbohydrates") as! String
                    self.dailyList[2] = nutritionData.object(forKey: "dProtein") as! String
                    self.dailyList[3] = nutritionData.object(forKey: "dFat") as! String
                    self.dailyList[4] = nutritionData.object(forKey: "dSaturatedFat") as! String
                    self.dailyList[5] = nutritionData.object(forKey: "dUnsaturatedFat") as! String
                    self.dailyList[6] = nutritionData.object(forKey: "dTransFat") as! String
                    self.dailyList[7] = nutritionData.object(forKey: "dCholestrol") as! String
                    self.dailyList[8] = nutritionData.object(forKey: "dSodium") as! String
                    self.dailyList[9] = nutritionData.object(forKey: "dPotassium") as! String
                    self.dailyList[10] = nutritionData.object(forKey: "dFiber") as! String
                    self.dailyList[11] = nutritionData.object(forKey: "dSugars") as! String
                    self.dailyList[12] = nutritionData.object(forKey: "dVitaminA") as! String
                    self.dailyList[13] = nutritionData.object(forKey: "dVitaminC") as! String
                    self.dailyList[14] = nutritionData.object(forKey: "dCalcium") as! String
                    self.dailyList[15] = nutritionData.object(forKey: "dIron") as! String
                    
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.nutritionTable.reloadData()
                    })
                    
//                    if (recipes1.count > 0)
//                    {
//                        DispatchQueue.main.sync(execute: { })
//                    }
//                    else
//                    {
//                        DispatchQueue.main.sync(execute: { })
//                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func uploadGoals() {
        
        submitBtn.isEnabled = false
        self.submitBtn.alpha = 0.5
                
        var paramString = ""
        
        if newFamilyMemberID != nil
        {
            paramString = "familyid=\(newFamilyMemberID!)&insert=true&devStatus=\(appDelegate.devStage!)"
        }
        else
        {
            paramString = "userid=\(appDelegate.userid!)&insert=true&devStatus=\(appDelegate.devStage!)"
        }
        
        //week params
        
        var i = 0
        
        for field in nutritionLabelList
        {
            let val = field.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
            
            //to do strip all white space from param name
            paramString = "\(paramString)&w\(val)=\(weeklyList[i] as! String)&devStatus=\(appDelegate.devStage!)"
            i += 1
        }
        
        //daily params
        
        i = 0
        
        for field in nutritionLabelList
        {
            let val = field.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
            //to do strip all white space from param name
            paramString = "\(paramString)&d\(val)=\(dailyList[i] as! String)&devStatus=\(appDelegate.devStage!)"
            i += 1
        }
        
        activityView.isHidden = false
        activityView.startAnimating()
        
        let urlString = "\(appDelegate.serverDestination!)addNutritionGoals.php"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            //print("Entered the completionHandler: \(response)")
            
            //var err: NSError?
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("goalsData: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                //print("dataDict: \(dataDict)")
                
                let uploadData : NSDictionary = dataDict.object(forKey: "goalsData") as! NSDictionary
                
                if (uploadData != nil)
                {
                    let status = uploadData.object(forKey: "status") as? String
                    
                    if (status == "goals info saved")
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.alpha = 1
                            
                            if self.newFamilyMemberID != nil
                            {
                                NotificationCenter.default.post(name: Notification.Name("refreshFamily"), object: nil)
                                
                                for controller in self.navigationController!.viewControllers as Array {
                                    
                                    if controller.isKind(of: ManageFamilyMembers.self) {
                                        
                                        self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                            }
                            else
                            {
                                if (self.self.appDelegate.signingUp == true)
                                {
                                    self.performSegue(withIdentifier: "login", sender: self)
                                }
                                else
                                {
                                    self.showBasicAlert(string: "Nutrition Goals Updated")
                                }
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.alpha = 1
                            
                            self.showBasicAlert(string: status!)
                        })
                    }
                }
                else
                {
                    //data not returned
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    // MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.nutritionLabelList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : GoalsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! GoalsTableViewCell
        
        cell.selectionStyle = .none
        
        let val = self.nutritionLabelList[(indexPath as NSIndexPath).row]
        
        cell.itemLbl?.text = val
        
        cell.tf.text = selectedList![indexPath.row] as? String
        
        if val == "Cholestrol" || val == "Sodium" || val == "Potassium"
        {
            //cell.tf.text = "0.00"
            cell.unitLbl.text = "mg"
        }
        else if val == "Vitamin A" || val == "Vitamin C" || val == "Calcium" || val == "Iron"
        {
            //cell.tf.text = "0.00"
            cell.unitLbl.text = "%"
            
        }
        else if val == "Calories"
        {
            cell.unitLbl.text = ""
            cell.tf.keyboardType = .numberPad
        }
        else
        {
            //cell.tf.text = "0.00"
            cell.unitLbl.text = "g"
        }
        
        cell.tf.inputAccessoryView = numberToolbar
        cell.tf.restorationIdentifier = "\(indexPath.row)"
        cell.tf.delegate = self
        cell.tf.keyboardType = .decimalPad

        if !fields.contains(cell.tf)
        {
            fields.append(cell.tf)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //println("You selected cell #\(indexPath.row)!")
        
        //selectedIem = self.items[(indexPath as NSIndexPath).row]
        
        //self.performSegue(withIdentifier: "GoToCategoryCards", sender: self)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        currentTextfieldValue = textField.text!
        
        print("currentTextfieldValue: \(currentTextfieldValue)")
        
        textField.text = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
        print("tf ind: \(textField.restorationIdentifier!) \(textField.text!)")
        
        if appDelegate.isStringAnInt(string: textField.text!)
        {
            let ind = Int(textField.restorationIdentifier!)
            
            if category == "1"
            {
                weeklyList[ind!] = textField.text!
            }
            else
            {
                dailyList[ind!] = textField.text!
            }
        }
        else
        {
            textField.text = currentTextfieldValue
            showBasicAlert(string: "Invalid format. Please enter numberic value.")
        }
        
        nutritionTable.reloadData()
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
//        if segue.identifier == "AddIngredients"
//        {
//            let destination = segue.destination as! AddIngredients
//            destination.category = category
//        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
