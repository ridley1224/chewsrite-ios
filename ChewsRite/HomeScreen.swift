//
//  HomeScreen.swift
//  ChewsRite
//
//  Created by Randall Ridley on 5/19/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3
import AVFoundation
import AVKit

class HomeScreen: BaseViewController, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UISearchBarDelegate  {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var colors:[UIColor] = [UIColor.red, UIColor.blue, UIColor.green]
    var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
    
    @IBOutlet weak var pageControl : UIPageControl?
    
    @IBOutlet weak var scrollview1: UIScrollView!
    @IBOutlet weak var scrollview2: UIScrollView!
    @IBOutlet weak var scrollview3: UIScrollView!
    
    var featuredList = [NSMutableDictionary]()
    var concernList1 = [NSMutableDictionary]()
    var concernList2 = [NSMutableDictionary]()
    
    @IBOutlet weak var familyCV: UICollectionView!
    var selectedUser : NSDictionary?
    
    
    @IBOutlet weak var contentScrollview: UIScrollView!
    //@IBOutlet weak var searchTxt: UITextField!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    var isSearching : Bool?
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var recipeCV: UICollectionView!
    
    var recipes = [NSDictionary]()
    var familyList = [NSDictionary]()
    
    @IBOutlet weak var searchHeaderLbl: UILabel!
    @IBOutlet weak var filterCountLbl: UILabel!
    
    var selectedList : NSMutableArray?
    var selectedDiets = NSMutableArray()
    var selectedMeals = NSMutableArray()
    var selectedCuisines = NSMutableArray()
    var selectedIngredients = NSMutableArray()
    
    @IBOutlet weak var recentSearchesView: UIView!
    @IBOutlet weak var noResultsView: UIView!
    
    @IBOutlet weak var clearSearchBtn: UIButton!
    
    @IBOutlet weak var recentSearchesTable: UITableView!
    
     var recentSearches = NSMutableArray()
    
    var paramString1 : String?
    var isFiltered : Bool?
    var completeQuery = ""
    
    @IBOutlet weak var noResultsMain: UIView!
    
    @IBOutlet weak var search: UISearchBar!
    var itemArray = [FeedItem]()
    var currentItemArray = [FeedItem]()
    
    var profileImageCache = [String:UIImage]()
    var statusImageCache = [String:UIImage]()
    
    var selectedRecipe : NSMutableDictionary?
    
    @IBOutlet weak var menuBtn: UIButton!
    var concernDicts : [NSDictionary]?
    
    @IBOutlet weak var featuredHeaderLbl: UILabel!
    @IBOutlet weak var categoryHeaderLbl1: UILabel!
    
    @IBOutlet weak var categoryHeaderLbl2: UILabel!
    
    var selectedVideo : Video?
    var recentSearchesCD : [RecentSearches]?
    
    @IBOutlet weak var scrollview1Height: NSLayoutConstraint?
    @IBOutlet weak var header1Height: NSLayoutConstraint?
    @IBOutlet weak var pageControlHeight: NSLayoutConstraint?
    @IBOutlet weak var catHeader1Padding: NSLayoutConstraint?
    @IBOutlet weak var featSVPadding: NSLayoutConstraint?
    @IBOutlet weak var featLblPadding: NSLayoutConstraint?
    
    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if appDelegate.userImage != nil
        {
            let urlString = "\(appDelegate.serverDestination!)userimages/\(appDelegate.profileImg!)"
            self.statusImageCache[urlString] = appDelegate.userImage
        }
        
        recentSearchesTable.delegate = self
        recentSearchesTable.dataSource = self
        
        
        selectedUser = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!, "usertype":"0"] as NSDictionary
        
        appDelegate.signingUp = false
                
        search.delegate = self
        search.backgroundImage = UIImage()
        
        noResultsMain.isHidden = true
        searchView.isHidden = true
        recentSearchesView.isHidden = true
        
        recentSearchesTable.tableFooterView = UIView()
        isFiltered = false
        filterCountLbl.isHidden = true
        
        categoryHeaderLbl1.text = ""
        categoryHeaderLbl2.text = ""
        
        //searchTxt.delegate = self
        //clearSearchBtn.isHidden = true
        
        //menuBtn.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
        let tb : TabController = self.parent as! TabController
        
        menuBtn.addTarget(self, action: #selector(tb.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        keyboardDoneButtonView.items = [doneButton]
        
        search.inputAccessoryView = keyboardDoneButtonView
        
        //cv
        
        recipeCV.delegate = self
        recipeCV.dataSource = self
        
        familyCV.delegate = self
        familyCV.dataSource = self
        
        let attributedString = NSMutableAttributedString(string: "BEST MATCHES (based on your dietary restrictions)", attributes: [
            .font: UIFont(name: "Avenir-Book", size: 12.0)!,
            .foregroundColor: UIColor.black,
            .kern: 0.19
            ])
        attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 16.0)!, range: NSRange(location: 0, length: 12))
        
        searchHeaderLbl.attributedText = attributedString
        
        //searchbar font
        
        UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        
        getRecipes(param: "")
        getFamily()
        
        //debug()
        //debug2()
        //debugRecentSearches()
        
        getRecentSearches()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applyFiltersNotification),
            name: NSNotification.Name(rawValue: "applyFilters"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(showMenuNotification),
            name: NSNotification.Name(rawValue: "showMenu"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(refreshHomeFeedNotification),
            name: NSNotification.Name(rawValue: "refreshHomeFeed"),
            object: nil)
    }
    
    // MARK: - CoreData
    
    func getRecentSearches () {
        
        let context = getContext()
        
        let fetchRequest: NSFetchRequest<RecentSearches> = RecentSearches.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "userid == %@", appDelegate.userid!)
        fetchRequest.fetchLimit = 10
        
        let sort = NSSortDescriptor(key: #keyPath(RecentSearches.date), ascending: false)
        fetchRequest.sortDescriptors = [sort]
        
        do {

            recentSearchesCD = try context.fetch(fetchRequest)
            
            print("count: \(recentSearchesCD!.count)")
            
            print("searchResults: \(recentSearchesCD!)")

            for searchVal in recentSearchesCD!
            {
                //historyList.add(trans)

                if searchVal.type == 1
                {
                    recentSearches.add( ["type" : "Diet", "name" : searchVal.value!, "image" : UIImage.init(named: searchVal.image1!)!, "image2" : UIImage.init(named: searchVal.image2!)!])
                }
                else
                {
                    //print("shot synced")
                    
                    recentSearches.add( ["type" : "User Input", "name" : searchVal.value!, "image" : nil, "image2" : nil])
                }
                
            }
        } catch { print("Error with recent searches request: \(error)") }


        //print("notsynced: \(notSyncedShots)")

//        if netAvailable == true && (notSyncedShots.count > 0 || notSyncedRounds.count > 0)
//        {
//            saveOfflineData(shots: notSyncedShots, rounds: notSyncedRounds)
//        }
//
//        //print("historyList: \(historyList)")
    }
    
    func deleteData () {
        
//        let context = self.getContext()
//        
//        let fetchRequest: NSFetchRequest<Shots> = Shots.fetchRequest()
//        fetchRequest.predicate = NSPredicate(format: "userid == %@", appDelegate.genuserid)
//        
//        do {
//            
//            //go get the results
//            
//            let searchResults = try getContext().fetch(fetchRequest)
//            
//            for trans in searchResults as [NSManagedObject]
//            {
//                let shot : Shots = trans as! Shots
//                
//                context.delete(shot)
//            }
//        }
//        catch
//        {
//            print("Error with shots request: \(error)")
//        }
//        
//        let fetchRequest2: NSFetchRequest<Rounds> = Rounds.fetchRequest()
//        
//        fetchRequest.predicate = NSPredicate(format: "userid == %@", appDelegate.genuserid)
//        
//        do {
//            
//            let searchResults = try getContext().fetch(fetchRequest2)
//            
//            for trans in searchResults as [NSManagedObject] {
//                
//                let round : Rounds = trans as! Rounds
//                
//                context.delete(round)
//            }
//        }
//        catch
//        {
//            print("Error with rounds request: \(error)")
//        }
//        
//        do {
//            
//            try context.save()
//            
//            print("synced offline data")
//            
//        } catch let error as NSError  {
//            
//            print("Could not save \(error), \(error.userInfo)")
//            
//        } catch {
//            
//        }
//        
//        getSavedData()
//        resetVars()
    }
    
    func getContext () -> NSManagedObjectContext {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        //        let paddingView = UIView(frame: CGRect.init(x: 0, y: 0, width: 25, height: self.searchTxt.frame.height))
        //
        //        searchTxt.leftView = paddingView
        //        searchTxt.leftViewMode = UITextFieldViewMode.always
    }
    
    func configurePageControl() {
        
        // The total number of pages that are available is based on how many available colors we have.
        
        self.pageControl?.numberOfPages = featuredList.count
        self.pageControl?.currentPage = 0
        //self.pageControl?.tintColor = UIColor.red
        self.pageControl?.pageIndicatorTintColor = appDelegate.crWarmGray
        self.pageControl?.currentPageIndicatorTintColor = appDelegate.crGreen
        //self.view.addSubview(pageControl!)
    }
    
    // MARK: - Notifications
    
    @objc func refreshHomeFeedNotification (notification: NSNotification) {
        
        getRecipes(param: "")
    }
    
    @objc func showMenuNotification (notification: NSNotification) {
        
        let tb : TabController = self.parent as! TabController
        tb.showMenu(sender: notification.object as! UIButton)
    }
    
    @objc func applyFiltersNotification (notification: NSNotification) {
        
        print("port: \(notification.object)")
        
        let dict = notification.object as! NSDictionary
        
        selectedMeals = dict.object(forKey: "selectedMeals") as! NSMutableArray
        selectedIngredients = dict.object(forKey: "selectedIngredients") as! NSMutableArray
        selectedCuisines = dict.object(forKey: "selectedCuisines") as! NSMutableArray
        selectedDiets = dict.object(forKey: "selectedDiets") as! NSMutableArray
        
        let filterCount = selectedMeals.count + selectedIngredients.count + selectedCuisines.count + selectedDiets.count
        
        if filterCount == 0
        {
            isFiltered = false
            filterCountLbl.isHidden = true
        }
        else
        {
            isFiltered = true
            filterCountLbl.text = String(filterCount)
            filterCountLbl.isHidden = false
            
            //completeQuery = "selectedMeals=\(selectedMeals.componentsJoined(by: ","))&selectedCuisines=\(selectedCuisines.componentsJoined(by: ","))&selectedDiets=\(selectedDiets.componentsJoined(by: ","))"
            
            //let completeQuery = "\(mealQuery)\(ingredientQuery)\(cuisineQuery)\(dietQuery)"
            
            //print("completeQuery: \(completeQuery)")
        }
        
        getRecipes(param: "")
        
        //self.navigationController?.popViewController(animated: true)
    }
    
    @objc func applyFiltersNotification1 (notification: NSNotification) {
        
        print("port: \(notification.object)")
        
        let dict = notification.object as! NSDictionary
        
        selectedMeals = dict.object(forKey: "selectedMeals") as! NSMutableArray
        selectedIngredients = dict.object(forKey: "selectedIngredients") as! NSMutableArray
        selectedCuisines = dict.object(forKey: "selectedCuisines") as! NSMutableArray
        selectedDiets = dict.object(forKey: "selectedDiets") as! NSMutableArray
        
        let filterCount = selectedMeals.count + selectedIngredients.count + selectedCuisines.count + selectedDiets.count
        
        if filterCount == 0
        {
            isFiltered = false
            filterCountLbl.isHidden = true
        }
        else
        {
            isFiltered = true
            filterCountLbl.text = String(filterCount)
            filterCountLbl.isHidden = false
            
            var mealQuery = ""
            var ingredientQuery = ""
            var cuisineQuery = ""
            var dietQuery = ""
            
            completeQuery = ""
            
            if selectedMeals.count > 0
            {
                var ind = 0
                
                for str in selectedMeals
                {
                    //print("str: \(str)")
                    
                    if ind == 0
                    {
                        mealQuery = "(meal = '\(str)'"
                    }
                    else
                    {
                        mealQuery = "\(mealQuery) OR meal = '\(str)'"
                    }
                    
                    ind += 1
                }
                
                mealQuery = "\(mealQuery))"
                
                print("mealQuery: \(mealQuery)")
                
                completeQuery = "\(completeQuery)\(mealQuery)"
            }
            
            if selectedIngredients.count > 0
            {
                var ind = 0
                
                if mealQuery != ""
                {
                    completeQuery = "\(completeQuery) AND "
                }
                
                for str in selectedIngredients
                {
                    //print("str: \(str)")
                    
                    if ind == 0
                    {
                        ingredientQuery = "(ingredient = '\(str)'"
                    }
                    else
                    {
                        ingredientQuery = "\(ingredientQuery) OR ingredient = '\(str)'"
                    }
                    
                    ind += 1
                }
                
                ingredientQuery = "\(ingredientQuery))"
                
                print("ingredientQuery: \(ingredientQuery)")
                
                completeQuery = "\(completeQuery)\(ingredientQuery)"
            }
            
            if selectedCuisines.count > 0
            {
                var ind = 0
                
                if mealQuery != "" || ingredientQuery != ""
                {
                    completeQuery = "\(completeQuery) AND "
                }
                
                for str in selectedCuisines
                {
                    //print("str: \(str)")
                    
                    if ind == 0
                    {
                        cuisineQuery = "(cuisine = '\(str)'"
                    }
                    else
                    {
                        cuisineQuery = "\(cuisineQuery) OR cuisine = '\(str)'"
                    }
                    
                    ind += 1
                }
                
                cuisineQuery = "\(cuisineQuery))"
                
                print("cuisineQuery: \(cuisineQuery)")
                
                completeQuery = "\(completeQuery)\(cuisineQuery)"
            }
            
            if selectedDiets.count > 0
            {
                var ind = 0
                
                if mealQuery != "" || ingredientQuery != "" || cuisineQuery != ""
                {
                    completeQuery = "\(completeQuery) AND "
                }
                
                for str in selectedDiets
                {
                    //print("str: \(str)")
                    
                    if ind == 0
                    {
                        dietQuery = "(diet = '\(str)'"
                    }
                    else
                    {
                        dietQuery = "\(dietQuery) OR diet = '\(str)'"
                    }
                    
                    ind += 1
                }
                
                dietQuery = "\(dietQuery))"
                
                print("dietQuery: \(dietQuery)")
                
                completeQuery = "\(completeQuery)\(dietQuery)"
            }
            
            //let completeQuery = "\(mealQuery)\(ingredientQuery)\(cuisineQuery)\(dietQuery)"
            
            print("completeQuery: \(completeQuery)")
        }
        
        getRecipes(param: "")
        
        //self.navigationController?.popViewController(animated: true)
    }
    
    func debug () {
        
        var dict : NSMutableDictionary = ["recipename" : "Pistachio-Crusted Chicken and Quinoa Salad", "category" : "","source" : "Hello Fresh","preptime" : "40 mins","calories" : "400","imagename" : "pistachio"]
        
        featuredList.append(dict)
        
        dict = ["recipename" : "Mixed Mushroom Risotto", "category" : "1","source" : "Blue Apron","preptime" : "60 mins","calories" : "300","imagename" : "risotto"]
        
        concernList1.append(dict)
        
        dict = ["recipename" : "Smoky Beluga Lentils", "category" : "2","source" : "Blue Apron","preptime" : "20 mins","calories" : "350","imagename" : "lentils"]
        
        concernList2.append(dict)
    }
    
    func debugRecentSearches (){
        
        recentSearches.add( ["type" : "User Input", "name" : "Tomato", "image" : nil, "image2" : nil])
        recentSearches.add( ["type" : "Diet", "name" : "Diabetic", "image" : UIImage.init(named: "noSugarBlue")!, "image2" : UIImage.init(named: "noSugarWhite")!])
        recentSearches.add( ["type" : "Diet", "name" : "Dairy Allergy", "image" : UIImage.init(named: "dairyAllergyBlue")!, "image2" : UIImage.init(named: "dairyAllergyWhite")!])
        
        recentSearchesTable.reloadData()
    }
    
    func clearScrollviews () {
        
        var subViews = scrollview1.subviews
        
        for subview in subViews{
            subview.removeFromSuperview()
        }
        
        subViews = scrollview2.subviews
        
        for subview in subViews{
            subview.removeFromSuperview()
        }
        
        subViews = scrollview3.subviews
        
        for subview in subViews{
            subview.removeFromSuperview()
        }
    }
    
    // MARK: - Webservice
    
    func getFamily() {
        
        familyList.removeAll()
        
        print("getFamily")
        
        let dataString = "familyData"
        
        let urlString = "\(appDelegate.serverDestination!)getFamilyMembers.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        let dict = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!, "usertype":"0"] as NSDictionary
        familyList.append(dict)
        
        self.familyCV.reloadData()
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("family jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
//                        self.familyCV.isHidden = true
//                        self.familyCV.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let members = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (members.count > 0)
                    {
                        hasData = true
                        
                        for dict in members
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.familyList.append(dict2)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if hasData == true
                            {
                                self.familyCV.isHidden = false
                                self.familyCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
//                                self.familyCV.isHidden = true
//                                self.familyCV.reloadData()
                            }
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getRecipes( param : String) {
        
        featuredList = [NSMutableDictionary]()
        concernList1 = [NSMutableDictionary]()
        concernList2 = [NSMutableDictionary]()
        
        clearScrollviews()
        
        print("getRecipes")
        
        let urlString = "\(appDelegate.serverDestination!)getRecipesHome.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var paramString  = ""
        
        if isFiltered == true
        {
            //paramString = "query=\(completeQuery)&filtered=true"
            
            if selectedMeals.count > 0
            {
                paramString = "\(paramString)selectedMeals=\(selectedMeals.componentsJoined(by: ","))"
            }
            
            if selectedCuisines.count > 0
            {
                paramString = "\(paramString)&selectedCuisines=\(selectedCuisines.componentsJoined(by: ","))"
            }
            
            if selectedDiets.count > 0
            {
                paramString = "\(paramString)&selectedDiets=\(selectedDiets.componentsJoined(by: ","))"
            }
            
            paramString = "\(paramString)&filtered=true"
            
            //print("paramString filtered: \(paramString)")
            print("param has filter")
        }
        
        if search.text != "" && isSearching == true
        {
            paramString = "\(paramString)&searching=\(search.text!)"
            
            print("param searching")
        }
        
        let usertype = selectedUser?.object(forKey: "usertype") as! String
        
        paramString = "\(paramString)&devStatus=\(appDelegate.devStage!)&usertype=\(usertype)"
        
        if selectedUser != nil
        {
            if usertype == "0"
            {
                paramString = "\(paramString)&userid=\(selectedUser?.object(forKey: "userid") as! String)&userfavoriteid=\(appDelegate.userid!)"
            }
            else
            {
                paramString = "\(paramString)&userid=\(selectedUser?.object(forKey: "familyid") as! String)&userfavoriteid=\(selectedUser?.object(forKey: "familyid") as! String)"
            }
        }
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict["recipeData"]! is NSNull {
                    
                    print("no recipes")
                    
                    DispatchQueue.main.sync(execute: {
                    
                        self.noResultsMain.isHidden = false
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict["recipeData"]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    if (uploadData.count > 0)
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.noResultsMain.isHidden = true
                        })
                        
                        
                        self.concernDicts = uploadData as! [NSDictionary]
                        
                        print("concernDicts: \(self.concernDicts)")
                        
                        var lastConcern = ""
                        var ind = 0
                        var catChangeCount = 0
                        
                        for dict in self.concernDicts!
                        {
                            let concernDict = dict.mutableCopy() as! NSMutableDictionary
                            self.recipes = concernDict.object(forKey: "recipes") as! [NSDictionary]
                            let concern = concernDict.object(forKey: "concernname") as! String
                            
//                            if ind == 0
//                            {
//                                lastConcern = concern
//                            }
                            
                            if self.isFiltered == false
                            {
                                for recipeDict in self.recipes
                                {
                                    let feat = recipeDict.object(forKey: "isfeatured") as! String
                                    let recipeDictC = recipeDict.mutableCopy() as! NSMutableDictionary
                                    
                                    if feat == "1"
                                    {
                                        self.featuredList.append(recipeDictC)
                                    }
                                    
                                    if catChangeCount == 0
                                    {
                                        self.concernList1.append(recipeDictC)
                                    }
                                    else if catChangeCount == 1
                                    {
                                        self.concernList2.append(recipeDictC)
                                    }
                                }
                                
                                if concern != lastConcern
                                {
                                    catChangeCount += 1
                                    lastConcern = concern
                                    print("catChangeCount \(catChangeCount)")
                                }
                                
                                ind += 1
                            }
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if self.isSearching == true || self.isFiltered == true
                            {
                                self.recipeCV.reloadData()
                                self.searchView.isHidden = false
                                
                                if self.recipes.count > 0
                                {
                                    self.noResultsView.isHidden = true
                                    self.recipeCV.isHidden = false
                                }
                                else
                                {
                                    self.noResultsView.isHidden = false
                                    self.recipeCV.isHidden = true
                                }
                                
                                //show results CV
                                //hide scrollviews
                                
                                self.scrollview1.isHidden = true
                                self.scrollview2.isHidden = true
                                self.scrollview3.isHidden = true
                            }
                            else
                            {
                                //hide results CV
                                //show scrollviews
                                
                                if (self.concernDicts?.count)! > 0
                                {
                                    let dict1 = self.concernDicts![0]
                                    
                                    self.categoryHeaderLbl1.text = dict1.object(forKey: "concernname") as? String
                                }
                                else
                                {
                                    self.categoryHeaderLbl1.text = ""
                                }
                                
                                if (self.concernDicts?.count)! > 1
                                {
                                    let dict2 = self.concernDicts![1]
                                    self.categoryHeaderLbl2.text = dict2.object(forKey: "concernname") as? String
                                }
                                else
                                {
                                    self.categoryHeaderLbl2.text = ""
                                }
                                
                                self.searchView.isHidden = true
                                
                                if self.featuredList.count > 0
                                {
                                    self.scrollview1.isHidden = false
                                    self.scrollview1Height!.constant = 256
                                    self.featuredHeaderLbl.isHidden = false
                                    self.header1Height!.constant = 18
                                    self.pageControl!.isHidden = false
                                    self.pageControlHeight!.constant = 37
                                    self.catHeader1Padding!.constant = 20
                                    self.featSVPadding!.constant = 20
                                    self.featLblPadding!.constant = 12
                                }
                                else
                                {
                                    self.scrollview1.isHidden = true
                                    self.scrollview1Height!.constant = 0
                                    self.featuredHeaderLbl.isHidden = true
                                    self.header1Height!.constant = 0
                                    self.pageControl!.isHidden = true
                                    self.pageControlHeight!.constant = 0
                                    self.catHeader1Padding!.constant = 0
                                    self.featSVPadding!.constant = 0
                                    self.featLblPadding!.constant = 8
                                }
                                
                                self.scrollview2.isHidden = false
                                self.scrollview3.isHidden = false
                                
                                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.loadScrollviews), userInfo: nil, repeats: false)
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @objc func connected(_ sender:Any){
        
        let id = sender as! UITapGestureRecognizer
        
        //print("you tap image number : \(id.view?.tag)")
        
        if id.view?.accessibilityIdentifier == "featured"
        {
            selectedRecipe = featuredList[(id.view?.tag)!]
        }
        else if id.view?.accessibilityIdentifier == "list1"
        {
            selectedRecipe = concernList1[(id.view?.tag)!]
        }
        else if id.view?.accessibilityIdentifier == "list2"
        {
            selectedRecipe = concernList2[(id.view?.tag)!]
        }
        
        performSegue(withIdentifier: "RecipeDetails", sender: nil)
    }
    
    @objc func loadScrollviews () {
        
        scrollview1?.delegate = self
        scrollview1?.isPagingEnabled = true
        
        for index in 0..<featuredList.count {
            
            frame.origin.x = (self.scrollview1?.frame.size.width)! * CGFloat(index)
            
            if index > 0
            {
                frame.origin.x = frame.origin.x + 10
            }
            
            //frame.size = (self.scrollview1?.frame.size)!
            frame.size = CGSize.init(width: self.view.frame.width, height: 256)
            
            let frv = FeaturedRecipeView.createMyClassView()
            frv.frame = frame
            
            let dict = featuredList[index]
            
            //frv.recipeIV.image = UIImage.init(named: (dict.object(forKey: "imagename") as? String)!)
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeScreen.connected(_:)))
            
            frv.recipeIV.isUserInteractionEnabled = true
            frv.recipeIV.tag = index
            frv.recipeIV.accessibilityIdentifier = "featured"
            frv.recipeIV.addGestureRecognizer(tapGestureRecognizer)
            
            let imagename = dict.object(forKey: "imagename") as? String
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
                
                if let img = statusImageCache[urlString]
                {
                    frv.recipeIV.image = img
                }
                else
                {
                    frv.recipeIV.image = UIImage(named: "profile-icon")
                    
                    self.downloadImage(imagename: imagename!, urlString: urlString, iv: frv.recipeIV)
                }
            }
            
            frv.headerLbl.text = dict.object(forKey: "recipename") as? String
            frv.infoLbl.text = "\(dict.object(forKey: "preptime") as! String) • \(dict.object(forKey: "calories") as! String)"
            
            let attributedString = NSMutableAttributedString(string: "Source: \((dict.object(forKey: "source") as? String)!)", attributes: [
                .font: UIFont(name: "Avenir-Book", size: 13.0)!,
                .foregroundColor: appDelegate.crGray
                ])
            attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 13.0)!, range: NSRange(location: 0, length: 7))
            
            frv.sourceLbl.attributedText = attributedString
            
            frv.favoriteBtn.restorationIdentifier = "\(index)"
            frv.favoriteBtn.accessibilityIdentifier = "featured"
            
            frv.favoriteBtn.addTarget(self, action: #selector(self.manageFavorite(_:)), for: UIControlEvents.touchUpInside)
            
            let isFavorite = dict.object(forKey: "isFavorite") as? String
            
            if isFavorite == "1"
            {
                frv.favoriteBtn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
            }
            else
            {
                frv.favoriteBtn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
            }
            
            self.scrollview1.addSubview(frv)
            frv.setNeedsDisplay()
        }
        
        //sv2
        
        for index in 0..<concernList1.count {
            
            let rv = RecipeView.createMyClassView()
            rv.frame = CGRect.init(x: 200 * CGFloat(index), y: 0, width: 200, height: 292)
            
            if index > 0
            {
                rv.frame = CGRect.init(x: (200 * CGFloat(index))+10, y: 0, width: 200, height: 292)
            }
            
            let dict = concernList1[index]
            
            rv.headerLbl.text = dict.object(forKey: "recipename") as? String
            rv.infoLbl.text = "\(dict.object(forKey: "preptime") as! String) • \(dict.object(forKey: "calories") as! String)"
            
            let attributedString = NSMutableAttributedString(string: "Source: \((dict.object(forKey: "source") as? String)!)", attributes: [
                .font: UIFont(name: "Avenir-Book", size: 13.0)!,
                .foregroundColor: appDelegate.crGray
                ])
            attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 13.0)!, range: NSRange(location: 0, length: 7))
            
            rv.sourceLbl.attributedText = attributedString
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeScreen.connected(_:)))
            
            rv.recipeIV.isUserInteractionEnabled = true
            rv.recipeIV.tag = index
            rv.recipeIV.accessibilityIdentifier = "list1"
            rv.recipeIV.addGestureRecognizer(tapGestureRecognizer)
            
            let imagename = dict.object(forKey: "imagename") as? String
            
            //rv.recipeIV.image = UIImage.init(named: imagename!)
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
                
                if let img = statusImageCache[urlString]
                {
                    rv.recipeIV.image = img
                }
                else
                {
                    rv.recipeIV.image = UIImage(named: "profile-icon")
                    
                    self.downloadImage(imagename: imagename!, urlString: urlString, iv: rv.recipeIV)
                }
            }
            
            rv.favoriteBtn.restorationIdentifier = "\(index)"
            rv.favoriteBtn.accessibilityIdentifier = "list1"
            
            rv.favoriteBtn.addTarget(self, action: #selector(self.manageFavorite(_:)), for: UIControlEvents.touchUpInside)
            
            let isFavorite = dict.object(forKey: "isFavorite") as? String
            
            if isFavorite == "1"
            {
                rv.favoriteBtn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
            }
            else
            {
                rv.favoriteBtn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
            }
            
            self.scrollview2.addSubview(rv)
            rv.setNeedsDisplay()
        }
        
        
        
        
        
        //sv3
        
        let width = CGFloat(200)
        
        
        for index in 0..<concernList2.count {
            
            let rv2 = RecipeView.createMyClassView()
            rv2.frame = CGRect.init(x: width * CGFloat(index), y: 0, width: width, height: 292)
            
            if index > 0
            {
                rv2.frame = CGRect.init(x: (width * CGFloat(index))+(CGFloat(index) * 10), y: 0, width: width, height: 292)
            }
                        
            print("index: \(index)")
            
            let dict = concernList2[index]
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeScreen.connected(_:)))
            
            rv2.recipeIV.isUserInteractionEnabled = true
            rv2.recipeIV.tag = index
            rv2.recipeIV.accessibilityIdentifier = "list2"
            rv2.recipeIV.addGestureRecognizer(tapGestureRecognizer)
            
            let imagename = dict.object(forKey: "imagename") as? String
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
                
                if let img = statusImageCache[urlString]
                {
                    rv2.recipeIV.image = img
                }
                else
                {
                    rv2.recipeIV.image = UIImage(named: "profile-icon")
                    
                    self.downloadImage(imagename: imagename!, urlString: urlString, iv: rv2.recipeIV)
                }
            }
            
            rv2.headerLbl.text = dict.object(forKey: "recipename") as? String
            rv2.infoLbl.text = "\(dict.object(forKey: "preptime") as! String) • \(dict.object(forKey: "calories") as! String)"
            
            let attributedString = NSMutableAttributedString(string: "Source: \((dict.object(forKey: "source") as? String)!)", attributes: [
                .font: UIFont(name: "Avenir-Book", size: 13.0)!,
                .foregroundColor: appDelegate.crGray
                ])
            attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 13.0)!, range: NSRange(location: 0, length: 7))
            
            rv2.sourceLbl.attributedText = attributedString
            
            rv2.favoriteBtn.restorationIdentifier = "\(index)"
            rv2.favoriteBtn.accessibilityIdentifier = "list2"
            
            rv2.favoriteBtn.addTarget(self, action: #selector(self.manageFavorite(_:)), for: UIControlEvents.touchUpInside)
            
            let isFavorite = dict.object(forKey: "isFavorite") as? String
            
            if isFavorite == "1"
            {
                rv2.favoriteBtn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
            }
            else
            {
                rv2.favoriteBtn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
            }
            
            self.scrollview3.addSubview(rv2)
            rv2.setNeedsDisplay()
        }
        
        self.scrollview1?.contentSize = CGSize(width:(self.scrollview1?.frame.size.width)! * CGFloat(featuredList.count),height: (self.scrollview1?.frame.size.height)!)
        pageControl?.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
        
        self.scrollview2?.contentSize = CGSize(width:width * CGFloat(concernList1.count) + (CGFloat(concernList1.count) * 10),height: (self.scrollview2?.frame.size.height)!)
        self.scrollview3?.contentSize = CGSize(width:width * CGFloat(concernList2.count) + (CGFloat(concernList2.count) * 10),height: (self.scrollview3?.frame.size.height)!)
        //self.scrollview2?.contentSize = CGSize(width:200 * CGFloat(concernList1.count),height: (self.scrollview2?.frame.size.height)!)
        
        self.contentScrollview?.contentSize = CGSize(width:(self.contentScrollview?.frame.size.width)!,height: ((self.contentScrollview?.frame.size.height)!+650))
        
        configurePageControl()
    }
    
    func manageFavorite( recipeID : String) {
        
        print("manageFavorite")
        
        let dataString = "favoriteData"
        
        let urlString = "\(appDelegate.serverDestination!)manageFavorites.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "recipeid=\(recipeID)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //                        self.activityView.stopAnimating()
                        //                        self.activityView.isHidden = true
                    })
                    
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "favorite item saved" || status == "favorite deleted"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            //self.getRecipes(param: "")
                            NotificationCenter.default.post(name: Notification.Name("refreshRecipes"), object: nil)
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func downloadImage (imagename : String, urlString : String, iv: UIImageView ) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache[urlString] = image
                        iv.image = image
                        
                        if urlString.contains(self.appDelegate.profileImg!)
                        {
                            self.appDelegate.userImage = image
                        }
                    }
                    
                    //println("test")
                })
                return nil
            }))
        }
        else
        {
            iv.image = UIImage.init(named: "profile-icon")
        }
    }
    
    // MARK: - Actions
    
    @IBAction func manageFavorite(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        var item : NSMutableDictionary?
        
        if btn.accessibilityIdentifier == "featured"
        {
            item = featuredList[Int(btn.restorationIdentifier!)!]
        }
        else if btn.accessibilityIdentifier == "list1"
        {
            item = concernList1[Int(btn.restorationIdentifier!)!]
        }
        else if btn.accessibilityIdentifier == "list2"
        {
            item = concernList2[Int(btn.restorationIdentifier!)!]
        }
        
        if item?.object(forKey: "isFavorite") as! String == "0"
        {
            item?.setValue("1", forKey: "isFavorite")
            btn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
        }
        else
        {
            item?.setValue("0", forKey: "isFavorite")
            btn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
        }
        
        recipeCV.reloadData()
        
        manageFavorite(recipeID: item?.object(forKey: "recipeid") as! String)
    }
    
    @IBAction func clearSearch(_ sender: Any) {
        
        search.text = ""
        //clearSearchBtn.isHidden = true
        recentSearchesView.isHidden = true
        
        if isSearching == true
        {
            isSearching = false
            getRecipes(param: "")
        }
    }
    
    @objc func doneClicked(sender: UIButton!) {
        
        let context = self.getContext()
        
        //isFiltered = false
        //filterCountLbl.isHidden = true
        
        recentSearchesView.isHidden = true
        //searchView.isHidden = true
        
        self.view.endEditing(true)
        
        if search.text != ""
        {
            isSearching = true
            isFiltered = true
            //getRecipes(param: "")
            
            let value = search.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            let entity =  NSEntityDescription.entity(forEntityName: "RecentSearches", in: context)
            
            let searchOb = NSManagedObject(entity: entity!, insertInto: context)
            
            searchOb.setValue(value, forKey: "value")
            searchOb.setValue(0, forKey: "type")
            searchOb.setValue(NSDate(), forKey: "date")
            searchOb.setValue(appDelegate.userid!, forKey: "userid")
            
            let searchPredicate: NSPredicate = NSPredicate(format: "SELF.name == [cd] %@", value)
            let filteredArray = self.recentSearches.filtered(using: searchPredicate)
            //print("here: \(filteredArray)")
            
            if filteredArray.count == 0
            {
                recentSearches.add( ["type" : "User Input", "name" : value, "image" : nil, "image2" : nil, "userid" : appDelegate.userid!])
                
                recentSearchesTable.reloadData()
                
                //getContext().save()
                
                do {
                    
                    try getContext().save()
                    
                    print("saved search term")
                    
                } catch let error as NSError  {
                    
                    print("Could not save \(error), \(error.userInfo)")
                    
                } catch {
                    
                }
            }
        }
        else if search.text == ""
        {
            isSearching = false
            
            let filterCount = selectedMeals.count + selectedIngredients.count + selectedCuisines.count + selectedDiets.count
            
            if filterCount == 0
            {
                isFiltered = false
            }
            //clearSearchBtn.isHidden = true
        }
        
        getRecipes(param: "")
    }
    
    @objc func doneClickedOrig(sender: UIButton!) {
        
        //isFiltered = false
        filterCountLbl.isHidden = true
        
        recentSearchesView.isHidden = true
        searchView.isHidden = true
        
        self.view.endEditing(true)
        
        if search.text != ""
        {
            isSearching = true
            //getRecipes(param: "")
        }
        else if search.text == ""
        {
            isSearching = false
            //clearSearchBtn.isHidden = true
        }
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    
    @objc func changePage(sender: AnyObject) -> () {
        
        let x = CGFloat((pageControl?.currentPage)!) * (scrollview1?.frame.size.width)!
        scrollview1?.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    @IBAction func playVideo2(_ sender: Any) {
        
        //https://www.raywenderlich.com/5191-video-streaming-tutorial-for-ios-getting-started
        
        print("play video2")
        
        let btn = sender as! UIButton
        
        //let video = videos[0]
        
        let url = "https://s3.us-east-2.amazonaws.com/shotgraph1224/chewsrite/\(btn.restorationIdentifier!)"
        
        selectedVideo = Video(url: URL.init(string: url)!, thumbURL: URL.init(string: url)!, title: "", subtitle: "")
        
        let videoURL = selectedVideo?.url
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        present(playerViewController, animated: true) {
            player.play()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView.restorationIdentifier == "sv1"
        {
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControl?.currentPage = Int(pageNumber)
        }
    }
    
    // MARK: - Textfield Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.restorationIdentifier == "Search" && textField.text == "" && recentSearches.count > 0
        {
            recentSearchesView.isHidden = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text != ""
        {
            //clearSearchBtn.isHidden = false
        }
        else if textField.text == "" && recentSearches.count > 0
        {
            //clearSearchBtn.isHidden = true
            recentSearchesView.isHidden = false
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        //        if textField.restorationIdentifier == "Search"
        //        {
        //            recentSearchesView.isHidden = true
        //        }
        
        recentSearchesView.isHidden = true
        searchView.isHidden = true
    }
    
    // MARK: - Searching
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText != "" && recentSearches.count > 0
        {
            //clearSearchBtn.isHidden = false
            
            recentSearchesView.isHidden = false
        }
        else if searchText == ""
        {
            //clearSearchBtn.isHidden = true
            recentSearchesView.isHidden = true
        }
        
        //make screen with all collectionviews
        
        //        currentItemArray = itemArray.filter({ item -> Bool in
        //
        //            if searchText != ""
        //            {
        //                //clearSearchBtn.isHidden = false
        //
        //                recentSearchesView.isHidden = false
        //            }
        //            else if searchText == ""
        //            {
        //                //clearSearchBtn.isHidden = true
        //                recentSearchesView.isHidden = true
        //            }
        //
        //            return true
        //        })
        
        //recipeCV.reloadData()
    }
    
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        
        //search.text = ""
        //clearSearchBtn.isHidden = true
        recentSearchesView.isHidden = true
        
        if isSearching == true
        {
            isSearching = false
            getRecipes(param: "")
        }
    }
    
    func debug2() {
        
        var dict : NSMutableDictionary = ["recipename" : "Pistachio-Crusted Chicken and Quinoa Salad", "category" : "","source" : "Hello Fresh","preptime" : "40 mins","calories" : "400","imagename" : "pistachio"]
        
        recipes.append(dict)
        
        dict = ["recipename" : "Mixed Mushroom Risotto", "category" : "1","source" : "Blue Apron","preptime" : "60 mins","calories" : "300","imagename" : "risotto"]
        
        recipes.append(dict)
        
        dict = ["recipename" : "Smoky Beluga Lentils", "category" : "2","source" : "Blue Apron","preptime" : "20 mins","calories" : "350","imagename" : "lentils"]
        
        recipes.append(dict)
        
        recipeCV.reloadData()
    }
    
    @IBAction func next(_ sender: Any) {}
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.restorationIdentifier == "family"
        {
            return CGSize.init(width: 42, height: 42)
        }
        else
        {
            let kWhateverHeightYouWant = 190
            
            return CGSize.init(width: 600, height: CGFloat(kWhateverHeightYouWant))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.restorationIdentifier == "family"
        {
            return self.familyList.count
        }
        else
        {
            return self.recipes.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.restorationIdentifier == "family"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FamilyCVCell
            
            let dict = familyList[indexPath.row]
            
            let imagename = dict.object(forKey: "userimage") as? String
            
            if selectedUser == dict
            {
                let img = UIImage.init(named: "selectWhite")
                cell?.si.image = img
            }
            else
            {
                cell?.si.image = nil
            }

            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                
                //let url2 = NSURL(string:urlString)!
                
                // If this image is already cached, don't re-download
                
                if let img = statusImageCache[urlString]
                {
                    //print("load cached status image")
                    
                    cell?.userImg.image = img
                }
                else
                {
                    //print("status image not
                    
                    let imagename = dict.object(forKey: "userimage") as? String
                    
                    if imagename != nil && imagename != ""
                    {
                        let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                        
                        print("img urlString: \(urlString)")
                        
                        if let img = statusImageCache[urlString]
                        {
                            cell?.userImg.image = img
                        }
                        else
                        {
                            cell?.userImg.image = UIImage(named: "profile-icon")
                            
                            self.downloadImage(imagename: imagename!, urlString: urlString, iv: ((cell?.userImg!)!))
                        }
                    }
                    else
                    {
                        cell?.userImg.image = UIImage(named: "profile-icon")
                    }
                }
            }
            
            return cell!
        }
        else
        {
            //chanage cell width to 40% of screen
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? RecipesCollectionViewCell
            
            let dict = recipes[indexPath.row]
            
            cell?.frame.size = CGSize.init(width: recipeCV.frame.width * 0.5, height: 292) //Randall. check this
            
            
            let video = dict.object(forKey: "videoname") as! String
            
            if video == ""
            {
                cell?.videoBtn.isHidden = true
                //cell?.pl?.isHidden = true
            }
            else
            {
                cell?.videoBtn.isHidden = false
                //cell?.pl?.isHidden = false
                cell?.videoURL = video
                
                cell?.videoBtn.addTarget(self, action: #selector(self.playVideo2(_:)), for: UIControlEvents.touchUpInside)
                
                cell?.videoBtn.restorationIdentifier = "\(video)"
            }
            
            
            
            
            if dict.object(forKey: "isFavorite") as! String == "0"
            {
                cell?.favoriteBtn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
            }
            else
            {
                cell?.favoriteBtn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
            }
            
            cell?.favoriteBtn.restorationIdentifier = "\(indexPath.row)"
            
            cell?.favoriteBtn.addTarget(self, action: #selector(self.manageFavorite(_:)), for: UIControlEvents.touchUpInside)
            
            let imagename = dict.object(forKey: "imagename") as? String
            
            cell?.recipeIV.image = UIImage.init(named: (dict.object(forKey: "imagename") as? String)!)
            
            cell?.headerLbl.text = dict.object(forKey: "recipename") as? String
            cell?.infoLbl.text = "\(dict.object(forKey: "preptime") as! String) • \(dict.object(forKey: "calories") as! String)"

            let attributedString = NSMutableAttributedString(string: "Source: \((dict.object(forKey: "source") as? String)!)", attributes: [
                .font: UIFont(name: "Avenir-Book", size: 13.0)!,
                .foregroundColor: appDelegate.crGray
                ])
            attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 13.0)!, range: NSRange(location: 0, length: 7))

            cell?.sourceLbl.attributedText = attributedString

            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"

                //let url2 = NSURL(string:urlString)!

                // If this image is already cached, don't re-download

                if let img = statusImageCache[urlString]
                {
                    //print("load cached status image")

                    cell?.recipeIV.image = img
                }
                else
                {
                    //print("status image not

                    let imagename = dict.object(forKey: "imagename") as? String
                    
                    if imagename != nil
                    {
                        let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
                        
                        if let img = statusImageCache[urlString]
                        {
                            cell?.recipeIV.image = img
                        }
                        else
                        {
                            cell?.recipeIV.image = UIImage(named: "profile-icon")
                            
                            self.downloadImage(imagename: imagename!, urlString: urlString, iv: (cell?.recipeIV)!)
                        }
                    }
                }
            }
            
            return cell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.restorationIdentifier == "family"
        {
            selectedUser = familyList[indexPath.row]
//
            print("selectedUser: \(selectedUser)")            
            
            getRecipes(param: "")
            
            familyCV.reloadData()
        }
        else
        {
            let recipe = recipes[indexPath.row]
            
            selectedRecipe = recipe.mutableCopy() as? NSMutableDictionary
            
            print("selectedRecipe: \(selectedRecipe!)")
            
            //print("selectedCuisines: \(selectedCuisines)")
            
            performSegue(withIdentifier: "RecipeDetails", sender: nil)
            
            recipeCV.reloadData()
        }
    }
    
    // MARK: - Tableview methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.recentSearches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : DietTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! DietTableViewCell
        
        let dict = self.recentSearches[(indexPath as NSIndexPath).row] as! NSDictionary
        
        let string = dict.object(forKey: "name") as! String
        let type = dict.object(forKey: "type") as! String
        
        cell.nameLbl?.text = string
        
        if type == "Diet"
        {
            if dict.object(forKey: "image") != nil
            {
                cell.iconIV.image = dict.object(forKey: "image") as? UIImage
            }
        }
        else
        {
            cell.ivWidthConstraint.constant = 0
            cell.iconIV.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = self.recentSearches[(indexPath as NSIndexPath).row] as! NSDictionary
        
        //let type = dict.object(forKey: "type") as! String
        let string = dict.object(forKey: "name") as! String
        
//        if type == "Diet"
//        {
//            paramString1 = "diet=\(string)"
//        }
//        else
//        {
//            paramString1 = "ingredient=\(string)"
//        }
        
        //paramString1 = "filter=\(string)"
        
        search.text = string
        //clearSearchBtn.isHidden = false
        
        //getRecipes(param: "")
        
         self.recentSearchesView.isHidden = true
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowFilter"
        {
            let destination = segue.destination as! FilterSearch
            
            destination.selectedCuisines = selectedCuisines
            destination.selectedIngredients = selectedIngredients
            destination.selectedMeals = selectedMeals
            destination.selectedDiets = selectedDiets
        }
        else if segue.identifier == "RecipeDetails"
        {
            let item = FeedItem(recipename: selectedRecipe!.object(forKey: "recipename") as! String,
                                preptime: selectedRecipe!.object(forKey: "preptime") as! String,
                                cooktime: selectedRecipe!.object(forKey: "cooktime") as! String,
                                imagename: selectedRecipe!.object(forKey: "imagename") as! String,
                                videoname: selectedRecipe!.object(forKey: "videoname") as! String,
                                calories: selectedRecipe!.object(forKey: "calories") as! String,
                                source: selectedRecipe!.object(forKey: "source") as! String, sourceurl: selectedRecipe!.object(forKey: "sourceurl") as! String,
                                isFavorite: selectedRecipe!.object(forKey: "isFavorite") as! String,
                                username: selectedRecipe!.object(forKey: "username") as! String,
                                recipeid: selectedRecipe!.object(forKey: "recipeid") as! String,
                                cuisinetags: selectedRecipe!.object(forKey: "cuisinetags") as! String,
                                mealplanid: "", mealplandate: "", mealid: "",
                                userimage: selectedRecipe!.object(forKey: "userimage") as! String,
                                firstname: selectedRecipe!.object(forKey: "firstname") as! String,
                                lastname: selectedRecipe!.object(forKey: "lastname") as! String,
                                description: selectedRecipe!.object(forKey: "recipedescription") as! String,
                                servings: selectedRecipe!.object(forKey: "servings") as! String,
                                userid: selectedRecipe!.object(forKey: "userid") as! String,
                                totaltime: selectedRecipe!.object(forKey: "totaltime") as! String,
                                rating: selectedRecipe!.object(forKey: "rating") as! String,
                                timeAdded: selectedRecipe!.object(forKey: "timeAdded") as! String)
            
            let destination = segue.destination as! RecipeDetails
            
            let imagename = selectedRecipe!.object(forKey: "imagename") as? String
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
                
                if let img = statusImageCache[urlString]
                {
                    destination.selectedImage = img
                }
            }
            
            destination.selectedItem = item
        }
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    var lastId = ""
    
    func openViewControllerBasedOnIdentifier1(_ strIdentifier:String){
        
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        let topViewController : UIViewController = self.navigationController!.topViewController!
        
        if (lastId == destViewController.restorationIdentifier!){
            
            print("Same VC")
            
        } else {
            
            lastId = strIdentifier
            
            self.navigationController!.pushViewController(destViewController, animated: true)
            
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        }
    }
    
    //play video in cell
    
//    else if (feedCell.post.postType == "2")
//    {
//    feedCell.statusImageView.hidden = true
//    feedCell.statusVideoView.view.hidden = false
//    feedCell.videoPlayView.hidden = false
//
//    //feedCell.statusImageView.removeFromSuperview()
//
//    let s3BucketName = "prospectdugout/videos"
//    let fileName = "\(feedCell.post.statusImageName!)"
//
//    print("fileName: \(fileName)")
//
//    //            let dr = AWSS3PreSignedURLBuilder .defaultS3PreSignedURLBuilder().getPreSignedURL(AWSS3GetPreSignedURLRequest())
//    //
//    //            let preSignedReq = AWSS3GetPreSignedURLRequest()
//    //            preSignedReq.bucket = s3BucketName
//    //            preSignedReq.key = fileName
//    //            preSignedReq.HTTPMethod = AWSHTTPMethod.GET                   // required
//    //            preSignedReq.contentType = "video/mp4"                       // required
//    //            preSignedReq.expires = NSDate(timeIntervalSinceNow: 60*60)    // required
//    //
//    //            //video/mpeg
//    //            //video/quicktime
//    //
//    //            // The defaultS3PreSignedURLBuilder uses the global config, as specified in the init method.
//    //            let urlBuilder = AWSS3PreSignedURLBuilder.defaultS3PreSignedURLBuilder()
//    //
//    //            // The new AWS SDK uses BFTasks to chain requests together:
//    //            urlBuilder.getPreSignedURL(preSignedReq).continueWithBlock { (task) -> AnyObject! in
//    //
//    //                if task.error != nil {
//    //                    print("getPreSignedURL error: %@", task.error)
//    //                    return nil
//    //                }
//    //
//    //                let preSignedUrl = task.result as! NSURL
//    //                NSLog("preSignedUrl: %@", preSignedUrl)
//    //
//    //                dispatch_async(dispatch_get_main_queue(), {
//    //
//    //                    //let url2 : NSURL = NSURL(fileURLWithPath:downloadFilePath)
//    //
//    //                    feedCell.statusVideoView.moviePlayer.contentURL = preSignedUrl
//    //                    feedCell.statusVideoView.moviePlayer.prepareToPlay()
//    //
//    //                })
//    //
//    //                return nil
//    //            }
//
//
//    //                        [[[AWSS3PreSignedURLBuilder defaultS3PreSignedURLBuilder] getPreSignedURL:request] continueWithBlock:^id(AWSTask *task) {
//    //
//    //                            if (task.error)
//    //                            { NSLog(@"Error: %@",task.error);
//    //                            } else
//    //                            {
//    //                            NSURL *presignedURL = task.result;
//    //                            NSLog(@"download presignedURL is: \n%@", presignedURL);
//    //                            } return nil;
//    //                            }];
//
//
//
//
//
//    let downloadFilePath = (NSTemporaryDirectory() as NSString).stringByAppendingPathComponent(fileName)
//    let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent(fileName)
//
//    // Set the logging to verbose so we can see in the debug console what is happening
//    AWSLogger.defaultLogger().logLevel = .None
//
//    let downloadRequest = AWSS3TransferManagerDownloadRequest()
//    downloadRequest.bucket = s3BucketName
//    downloadRequest.key  = fileName
//    downloadRequest.downloadingFileURL = downloadingFileURL
//
//
//    let transferManager = AWSS3TransferManager.defaultS3TransferManager()
//    transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock: ({
//    (task: AWSTask!) -> AWSTask! in
//    dispatch_async(dispatch_get_main_queue(), {
//
//    if task.error != nil
//    {
//    print("AWS Error downloading video")
//
//    print(task.error!.description)
//    }
//    else
//    {
//    print("AWS download video successful")
//
//    var downloadOutput = AWSS3TransferManagerDownloadOutput()
//
//    downloadOutput = task.result! as! AWSS3TransferManagerDownloadOutput
//
//    //                        print("downloadOutput video: \(downloadOutput)");
//    //                        print("downloadFilePath video: \(downloadFilePath)");
//
//    //                        NSLog(@"download outptut = %@", downloadOutput);
//    //                        NSString *body = [downloadOutput valueForKey:@"body"];
//    //                        NSLog(@"body = %@", body);
//
//
//    let url2 : NSURL = NSURL(fileURLWithPath:downloadFilePath)
//
//    feedCell.statusVideoView.moviePlayer.contentURL = url2
//    feedCell.statusVideoView.moviePlayer.prepareToPlay()
//    }
//    })
//    return nil
//    }))
//
//
//
//    tappedVideo = feedCell.statusVideoView
//    tappedVideoOverlay = feedCell.videoPlayView
//
//    tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
//
//    tap.delegate = self
//
//    feedCell.statusVideoView.view.addGestureRecognizer(tap)
//    }
    
    
//    func uploadS3Video (dict: NSMutableDictionary) {
//
//        //        createStatusLbl.text = "Uploading Video"
//        //        uploadView.isHidden = false
//        //        activityView.startAnimating()
//
//
//        print("upload dict: \(dict)")
//
//        //http://docs.aws.amazon.com/mobile/sdkforios/developerguide/s3transfermanager.html
//
//        let transferManager = AWSS3TransferManager.default()
//
//        let uploadRequest = AWSS3TransferManagerUploadRequest()
//        uploadRequest?.bucket = "brunchbunch/eventphotos"
//        uploadRequest!.key = (dict.object(forKey: "filename") as! String)
//        uploadRequest?.body = dict.object(forKey: "url") as! URL
//
//        //        let imageURL = Bundle.main.url(forResource: "ava", withExtension: "png")!
//        //
//        //        uploadRequest?.body = imageURL
//
//        uploadRequest?.acl = AWSS3ObjectCannedACL.publicReadWrite;
//
//        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
//
//            if let error = task.error as? NSError {
//
//                if error.domain == AWSS3TransferManagerErrorDomain,
//
//                    let code = AWSS3TransferManagerErrorType(rawValue: error.code)
//                {
//                    switch code
//                    {
//                    case .cancelled, .paused:
//                        break
//                    default:
//                        print("Error uploading 1: \(uploadRequest?.key!) Error: \(error)")
//                    }
//                }
//                else
//                {
//                    print("Error uploading: \(uploadRequest?.key!) Code: \(error.code) Error: \(error)")
//                }
//                return nil
//            }
//
//            //let uploadOutput = task.result
//
//            print("Upload complete for: \(uploadRequest!.key!)")
//
//            //Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.doneUploading), userInfo: nil, repeats: false)
//
//            return nil
//        })
//    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    
    func downloaded(from link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
    
}
