//
//  ShareRecipe.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/20/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class ShareRecipe: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var folderTable: UITableView!
    @IBOutlet weak var folderNameTxt: UITextField!
    @IBOutlet weak var recipeIV: UIImageView!
    
    var foldersList = NSMutableArray()
    var friendsList = NSMutableArray()
    var selectedList = [NSMutableDictionary]()
    var selectedItems = [NSMutableDictionary]()
    var selectedGroups = [NSMutableDictionary]()
    var selectedFriends = [NSMutableDictionary]()
    
    var isSharingFriends : Bool?
    var isSharingFolderMealPlan : Bool?
    var isSharingPrivateGroups : Bool?
    
    var shareDestination : String?
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var statusTV: UITextView!
    var recipeImage : UIImage?
    
    var selectedPhotoID : String?
    var recipeID : String?
    var newFolderDict : NSMutableDictionary?
    var isSharing = false
    @IBOutlet weak var textHeight : NSLayoutConstraint?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        recipeIV.image = recipeImage
        
        folderTable.delegate = self
        folderTable.dataSource = self
        folderTable.tableFooterView = UIView()
        
        activityView.isHidden = true
        statusTV.isHidden = true
        //debug()
        
        if isSharingFolderMealPlan == true
        {
            headerLbl.text = "Share Recipe To \(shareDestination!)"
            shareBtn.setTitle("Bookmark Recipe", for: .normal)
            statusTV.text = "You currently have no bookmarks"
        }
        else if isSharingFriends == true
        {
            headerLbl.text = "Share Recipe with Friends"
            statusTV.text = "You currently have no friends"
            
            textHeight?.constant = 0
        }
        else if isSharingPrivateGroups == true
        {
            headerLbl.text = "Share with Private Groups"
            statusTV.text = "You currently have no groups"
            
            textHeight?.constant = 0
        }
        
        getData()
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        keyboardDoneButtonView.items = [doneButton]
        
        folderNameTxt?.inputAccessoryView = keyboardDoneButtonView
    }
    
    func debug () {
        
        if isSharingFriends == true
        {
            var dict : NSMutableDictionary = ["name" : "Mary", "active" : false]
            selectedList.append(dict)
            
            dict  = ["name" : "Joe", "active" : false]
            selectedList.append(dict)
        }
        else
        {
            if isSharingFolderMealPlan == true
            {
                //headerLbl.text = "Share Recipe To \(shareDestination!)"
                
//                if shareDestination == "My Meal Plan"
//                {
//                    var dict : NSMutableDictionary = ["name" : "Heart Healthy", "active" : false]
//                    selectedList.append(dict)
//
//                    dict  = ["name" : "Vegan", "active" : false]
//                    selectedList.append(dict)
//                }
                
                var dict : NSMutableDictionary = ["name" : "Heart Healthy", "active" : false]
                selectedList.append(dict)
                
                dict  = ["name" : "Vegan", "active" : false]
                selectedList.append(dict)
            }
        }
        
        folderTable.reloadData()
    }
    
    @objc func doneClicked(sender: UIButton!) {
        
        if folderNameTxt.text != ""
        {
            debugfolderAdded()
            
            //uploadFolderData()
        }
        
        self.view.endEditing(true)
    }
    
    func debugfolderAdded () {
        
        newFolderDict = ["foldername" : folderNameTxt.text!, "active" : false]
        
        selectedList.append(newFolderDict!)
        
        folderTable.reloadData()
        
        folderNameTxt.text = ""
    }
    
    // MARK: Webservice
    
    func getData() {
        
        print("getData")
        
        var dataString = ""
        var urlString = ""
        
        if isSharingFriends == true
        {
            urlString = "\(appDelegate.serverDestination!)getUserFriends.php"
            dataString = "friendsData"
        }
        else if isSharingFolderMealPlan == true
        {
            urlString = "\(appDelegate.serverDestination!)getFolders.php"
            dataString = "folderData"
        }
        else if isSharingPrivateGroups == true
        {
            urlString = "\(appDelegate.serverDestination!)getUserCreatedGroups.php"
            dataString = "groupsData"
        }
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&recipeid=\(recipeID!)&devStatus=\(appDelegate.devStage!)&isSharing=\(isSharing)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("folders jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                        
                        self.statusTV.isHidden = false
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                
                    if (uploadData.count > 0)
                    {
                        if self.isSharingFolderMealPlan == true
                        {
                            let list = uploadData as! [NSDictionary]
                            
                            for dict in list
                            {
                                let d = dict.mutableCopy() as! NSMutableDictionary
                                
                                let dict1 : NSMutableDictionary = ["foldername" : d.object(forKey: "foldername")!,"folderid" : d.object(forKey: "folderid")!, "active" : d.object(forKey: "active")!]
                                
                                let isActive = d.object(forKey: "active") as! Bool
                                
                                if isActive == true
                                {
                                    self.selectedItems.append(dict1)
                                }
                                
                                self.selectedList.append(dict1)
                            }
                        }
                        else if self.isSharingPrivateGroups == true
                        {
                            let list = uploadData as! [NSDictionary]
                            
                            for dict in list
                            {
                                let d = dict.mutableCopy() as! NSMutableDictionary
                                
                                let dict1 : NSMutableDictionary = ["groupname" : d.object(forKey: "groupname")!,"groupid" : d.object(forKey: "groupid")!, "active" : d.object(forKey: "active")!]
                                
                                let isActive = d.object(forKey: "active") as! Bool
                                
                                if isActive == true
                                {
                                    self.selectedGroups.append(dict1)
                                }
                                
                                self.selectedList.append(dict1)
                            }
                        }
                        else if self.isSharingFriends == true
                        {
                            let list = uploadData as! [NSDictionary]
                            
                            for dict in list
                            {
                                let d = dict.mutableCopy() as! NSMutableDictionary
                                
                                let dict1 : NSMutableDictionary = ["username" : d.object(forKey: "username")!,"userid" : d.object(forKey: "userid")!, "userimage" : d.object(forKey: "userimage")!]
                                
//                                let isActive = d.object(forKey: "active") as! Bool
//
//                                if isActive == true
//                                {
//                                    self.selectedFriends.append(dict1)
//                                }
                                
                                self.selectedList.append(dict1)
                            }
                        }
                        
                        //self.selectedList = uploadData as! [NSMutableDictionary]
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.folderTable.reloadData()
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                            
                            self.statusTV.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func uploadFolderData() {
        
        print("uploadData")
        
        let dataString = "folderData"
        
        let urlString = "\(appDelegate.serverDestination!)addFolder.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var paramString = "foldername=\(folderNameTxt.text!)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.statusTV.isHidden = false
                        
//                        self.activityView.stopAnimating()
//                        self.activityView.isHidden = true
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "folder saved"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            print("folder saved")
                            
                            self.newFolderDict = ["foldername" : self.folderNameTxt.text!, "active" : false]
                            
                            self.selectedList.append(self.newFolderDict!)
                            self.folderTable.reloadData()
                            
                            self.folderNameTxt.text = ""
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                            
//                            self.activityView.stopAnimating()
//                            self.activityView.isHidden = true
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func uploadBookmarkData() {
        
        print("uploadBookmarkData")
        
        var foldersString = ""
        
        if selectedItems.count > 0
        {
            var folderids = ""
            var i = 0
            
            for a in selectedItems
            {
                if i == 0
                {
                    folderids = (a.object(forKey: "folderid") as? String)!
                }
                else
                {
                    folderids = "\(folderids),\(a.object(forKey: "folderid") as! String)"
                }
                
                i += 1
            }
            
            foldersString = "\(folderids)"
        }
        
        let dataString = "bookmarkData"
        
        let urlString = "\(appDelegate.serverDestination!)addBookmark.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "recipeid=\(recipeID!)&folderids=\(foldersString)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                        
                        //                        self.activityView.stopAnimating()
                        //                        self.activityView.isHidden = true
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "bookmark saved" || status == "bookmark updated"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            self.recipeBookmarked()
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                            
                            //                            self.activityView.stopAnimating()
                            //                            self.activityView.isHidden = true
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func shareGroups() {
        
        print("shareGroups")
        
        var groupsString = ""
        
        if selectedGroups.count > 0
        {
            var groupids = ""
            var i = 0
            
            for a in selectedGroups
            {
                if i == 0
                {
                    groupids = (a.object(forKey: "groupid") as? String)!
                }
                else
                {
                    groupids = "\(groupids),\(a.object(forKey: "groupid") as! String)"
                }
                
                i += 1
            }
            
            groupsString = "\(groupids)"
        }
        
        let dataString = "inviteData"
        
        let urlString = "\(appDelegate.serverDestination!)shareGroups.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "recipeid=\(recipeID!)&groupids=\(groupsString)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                        
                        //                        self.activityView.stopAnimating()
                        //                        self.activityView.isHidden = true
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status.contains("invites sent")
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            self.recipeBookmarked()
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                            
                            //                            self.activityView.stopAnimating()
                            //                            self.activityView.isHidden = true
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    //to do Randall 1-14-19 test this
    //add to notification strategy sheet
    
    func shareFriends() {
        
        print("shareFriends")
        
        var friendsString = ""
        
        if selectedFriends.count > 0
        {
            var friendids = ""
            var i = 0
            
            for a in selectedFriends
            {
                if i == 0
                {
                    friendids = (a.object(forKey: "userid") as? String)!
                }
                else
                {
                    friendids = "\(friendids),\(a.object(forKey: "userid") as! String)"
                }
                
                i += 1
            }
            
            friendsString = "\(friendids)"
        }
        
        let dataString = "inviteData"
        
        let urlString = "\(appDelegate.serverDestination!)shareFriends.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "recipeid=\(recipeID!)&friendids=\(friendsString)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                        
                        //                        self.activityView.stopAnimating()
                        //                        self.activityView.isHidden = true
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status.contains("invites sent")
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            self.recipeBookmarked()
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                            
                            //                            self.activityView.stopAnimating()
                            //                            self.activityView.isHidden = true
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func saveRecipe(_ sender: Any) {
        
        if isSharingPrivateGroups == true
        {
            if selectedGroups.count > 0
            {
                activityView.startAnimating()
                activityView.isHidden = false
                shareBtn.setTitle("", for: .normal)
                
                shareGroups()
            }
            else
            {
                showBasicAlert(string: "Please select a group.")
            }
        }
        else if isSharingFriends == true
        {
            if selectedFriends.count > 0
            {
                activityView.startAnimating()
                activityView.isHidden = false
                shareBtn.setTitle("", for: .normal)
                
                shareFriends()
            }
            else
            {
                showBasicAlert(string: "Please select a friend.")
            }
        }
        else if isSharingFolderMealPlan == true
        {
            if selectedItems.count > 0
            {
                activityView.startAnimating()
                activityView.isHidden = false
                shareBtn.setTitle("", for: .normal)
                
                uploadBookmarkData()
            }
            else
            {
                showBasicAlert(string: "Please select a folder.")
            }
        }
        
        //create addFolder.php
        //create bookmarkRecipe.php
        
        //Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.debugSave), userInfo: nil, repeats: false)
    }
    
    @objc func debugSave () {
        
        recipeBookmarked()
    }
    
    func recipeBookmarked () {
        
        //Alamofire to send dict
                
        NotificationCenter.default.post(name: Notification.Name("recipeBookmarked"), object: nil)
        
        activityView.stopAnimating()
        activityView.isHidden = true
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.selectedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : FolderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! FolderTableViewCell
        
       // let a = self.selectedList[indexPath.row]
        
        //print("a: \(type(of: self.selectedList[indexPath.row]))")
        
        
        var val = ""
        var dict : NSMutableDictionary?
        
        if isSharingFolderMealPlan == true
        {
            dict = self.selectedList[indexPath.row]
            val = dict!.object(forKey: "foldername") as! String
            
            print("val: \(val)")
        }
        else if isSharingPrivateGroups == true
        {
            dict =  self.selectedList[indexPath.row]
            val = dict!.object(forKey: "groupname") as! String
            
            print("val: \(val)")
        }
        else if isSharingFriends == true
        {
            dict =  self.selectedList[indexPath.row]
            val = dict!.object(forKey: "friendname") as! String
            
            print("val: \(val)")
        }
        
        
        cell.titleLbl?.text = val
        
        cell.selectionStyle = .none
        
        let active = dict!.object(forKey: "active") as! Bool
        
        if active == false
        {
            cell.startEditBtn.setBackgroundImage(#imageLiteral(resourceName: "plus-normal"), for: .normal)
        }
        else
        {
            cell.startEditBtn.setBackgroundImage(#imageLiteral(resourceName: "checkGreen"), for: .normal)
        }
        
        cell.startEditBtn.restorationIdentifier = String(indexPath.row)
        cell.startEditBtn.addTarget(self, action: #selector(self.saveToFolder(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    @objc func saveToFolder(_ sender : UIButton) {
        
        let btn = sender as UIButton
        let ind = Int(btn.restorationIdentifier!)
        
        if isSharingPrivateGroups == true
        {
            let dict = self.selectedList[ind!]
            
            let active = dict.object(forKey: "active") as! Bool
            
            if active == true
            {
                dict.setValue(false, forKey: "active")
                
                if selectedGroups.contains(dict)
                {
                    selectedGroups.remove(at: selectedGroups.index(of: dict)!)
                }
            }
            else
            {
                dict.setValue(true, forKey: "active")
                selectedGroups.append(dict)
            }
        }
        else if isSharingFolderMealPlan == true
        {
            let dict = self.selectedList[ind!]
            
            let active = dict.object(forKey: "active") as! Bool
            
            if active == true
            {
                dict.setValue(false, forKey: "active")
                
                if selectedItems.contains(dict)
                {
                    selectedItems.remove(at: selectedItems.index(of: dict)!)
                }
            }
            else
            {
                dict.setValue(true, forKey: "active")
                selectedItems.append(dict)
            }
            
            print("selectedItems: \(selectedItems)")
        }
        
        folderTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSharingFolderMealPlan == true
        {
            let selectedIem = self.selectedList[(indexPath as NSIndexPath).row]
            
            selectedPhotoID = selectedIem.object(forKey: "folderid") as? String
        }
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
