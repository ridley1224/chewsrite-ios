//
//  FamilyMemberRelationship.swift
//  ChewsRite
//
//  Created by DTO MacBook11 on 11/7/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class FamilyMemberRelationship: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var relationshipTable: UITableView!
    
    var foldersList = [NSMutableDictionary]()
    
    var selectedRelationship : NSMutableDictionary?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        relationshipTable.delegate = self
        relationshipTable.dataSource = self
        relationshipTable.tableFooterView = UIView()
        relationshipTable.isHidden = true
        
        getRelationships()
        
//        foldersList.append(["title":"None","id":0,"active":false])
//        foldersList.append(["title":"Mom","id":1,"active":false])
//        foldersList.append(["title":"Dad","id":2,"active":false])
//
//        relationshipTable.reloadData()

        // Do any additional setup after loading the view.
    }
    
    func getRelationships() {
        
        foldersList.removeAll()
        
        print("getRelationships")
        
        let dataString = "relationshipData"
        
        let urlString = "\(appDelegate.serverDestination!)getFamilyRelationships.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("relationships jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.relationshipTable.isHidden = true
                        self.relationshipTable.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let members = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (members.count > 0)
                    {
                        hasData = true
                        
                        for dict in members
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.foldersList.append(dict2)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if hasData == true
                            {
                                self.relationshipTable.reloadData()
                                self.relationshipTable.isHidden = false
                            }
                            else
                            {
                                print("no data")
                                
                                self.relationshipTable.isHidden = true
                            }
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func save(_ sender: Any) {
        
        if selectedRelationship != nil
        {
            NotificationCenter.default.post(name: Notification.Name("selectedRelationship"), object: selectedRelationship)
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }        
    }
    // MARK: Tableview
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        selectedRelationship = self.foldersList[indexPath.row]
//
//        relationshipTable.reloadData()
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.foldersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //update check green and selected list
        
        let cell : FolderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! FolderTableViewCell
        
        let dict = self.foldersList[indexPath.row]
        cell.selectionStyle = .none
        
        cell.titleLbl?.text = dict.object(forKey: "title") as? String
        
        cell.startEditBtn.restorationIdentifier = String(indexPath.row)
        cell.startEditBtn.addTarget(self, action: #selector(self.saveToFolder(_:)), for: UIControlEvents.touchUpInside)
        cell.startEditBtn.restorationIdentifier = "\(indexPath.row)"
        
        if dict == selectedRelationship
        {
            cell.startEditBtn.setBackgroundImage(UIImage.init(named: "checkGreen"), for: .normal)
        }
        else
        {
            cell.startEditBtn.setBackgroundImage(UIImage.init(named: "unselected-1"), for: .normal)
        }
        
        return cell
    }
    
    @IBAction func saveToFolder(_ sender : UIButton) {
        
        let ind = Int(sender.restorationIdentifier!)
        
        //let dict = self.foldersList[ind!] as NSMutableDictionary
        
        selectedRelationship = self.foldersList[ind!]
        
        relationshipTable.reloadData()
    }
    
    @IBAction func cancel() {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
