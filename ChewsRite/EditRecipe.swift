//
//  EditRecipe.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/21/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import Alamofire

class EditRecipe: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var folderTable: UITableView!
    @IBOutlet weak var folderNameTxt: UITextField!
    @IBOutlet weak var recipeIV: UIImageView!

    var selectedList = [NSMutableDictionary]()
    var substituteList = [String]()
    var selectedItems = [NSMutableDictionary]()
    var ingredientOptions = [NSMutableDictionary]()
    var newIngredientOptions = [NSMutableDictionary]()
    
    var isSharingFriends : Bool?
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    var recipeImage : UIImage?
    var isUpdating : Bool?
    var recipeName : String?
    
    var selectedIngredientItem : NSMutableDictionary?
    var selectedIngredient : String?
    
    @IBOutlet weak var recipeNameLbl: UILabel!
    var currentTxt : UITextField?
    
    @IBOutlet weak var servingsLbl: UILabel!
    var servingSize : Float!
    
    var units = [NSMutableDictionary]()
    
    @IBOutlet weak var substituteView: UIView!
    @IBOutlet weak var substituteLbl: UILabel!
    
    @IBOutlet weak var substituteTable: UITableView!
    @IBOutlet weak var editQuantityTxt: UITextField!
    
    var removeObject : NSMutableDictionary?
    var recipeID : String?
    
    var addCurrentIngredients : Bool?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        recipeIV.image = recipeImage
        recipeNameLbl.text = recipeName
        
        folderTable.delegate = self
        folderTable.dataSource = self
        folderTable.tableFooterView = UIView()
        
        substituteTable.delegate = self
        substituteTable.dataSource = self
        substituteTable.tableFooterView = UIView()
        
        substituteTable.estimatedRowHeight = UITableViewAutomaticDimension
        substituteTable.rowHeight = UITableViewAutomaticDimension
        
        activityView.isHidden = true
        substituteView.isHidden = true
        
//        shareBtn.backgroundColor = self.appDelegate.crWarmGray
//        shareBtn.isEnabled = false
        
        folderNameTxt.isHidden = true
        
        if servingSize == nil
        {
            servingSize = 0
        }
        
        servingsLbl.text = "\(servingSize!) Servings"
        
        let attributedString = NSMutableAttributedString(string: "If you don\'t have chicken breast you can substitute  with (please select one): ", attributes: [
            .font: UIFont(name: "Avenir-Light", size: 14.0)!,
            .foregroundColor: UIColor(white: 74.0 / 255.0, alpha: 1.0)
            ])
        attributedString.addAttribute(.foregroundColor, value: appDelegate.gray51, range: NSRange(location: 58, length: 19))
        
        substituteLbl.attributedText = attributedString
        
        for dict in ingredientOptions
        {
            newIngredientOptions.append(dict.mutableCopy() as! NSMutableDictionary)
        }
        
        //print("newIngredientOptions: \(newIngredientOptions)")
        
        populateMeasurements()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        newIngredientOptions.removeAll()
        ingredientOptions.removeAll()
    }
    
    func populateMeasurements () {
        
        let dict : NSMutableDictionary = ["name" : "add new ingredient", "active" : false, "unit" : "oz","quantity" : 1,"ingredient" : "0"]
        
        newIngredientOptions.insert(dict, at: 0)
        
        var dict2 : NSMutableDictionary = ["name" : "Miligram", "abbr" : "mil"]
        
        units.append(dict2)
        
        dict2 = ["name" : "Ounce", "abbr" : "oz"]
        
        units.append(dict2)
        
        dict2 = ["name" : "Pound", "abbr" : "lb"]
        
        units.append(dict2)
        
        dict2 = ["name" : "Count", "abbr" : ""]
        
        units.append(dict2)
        
        dict2 = ["name" : "Teaspoon", "abbr" : "tsp"]
        
        units.append(dict2)
        
        dict2 = ["name" : "Tablespoon", "abbr" : "tbsp"]
        
        units.append(dict2)
        
        dict2 = ["name" : "Fluid Ounce", "abbr" : "fl oz"]
        
        units.append(dict2)
    }
    
    func debugIngredient () {
        
        let debugDict = NSMutableDictionary()
        
        debugDict.setValue("Vodka", forKey: "ingredientname")
        debugDict.setValue(false, forKey: "active")
        debugDict.setValue("", forKey: "name")
        debugDict.setValue("1", forKey: "quantity")
        debugDict.setValue("oz", forKey: "unit")
        debugDict.setValue(self.ingredientOptions.count, forKey: "recipeorder")
        
        newIngredientOptions.append(debugDict)
        ingredientOptions.append(debugDict)
        folderTable.reloadData()
    }
    
    func debug () {
        
        if isSharingFriends == true
        {
            var dict : NSMutableDictionary = ["name" : "Mary", "active" : false]
            selectedList.append(dict)
            
            dict  = ["name" : "Joe", "active" : false]
            selectedList.append(dict)
        }
        else
        {
            var dict : NSMutableDictionary = ["name" : "Heart Healthy", "active" : false]
            selectedList.append(dict)
            
            dict  = ["name" : "Vegan", "active" : false]
            selectedList.append(dict)
        }
        
        folderTable.reloadData()
    }
    
    @IBAction func closeSubstituteView(_ sender: Any) {
        
        self.substituteTable.isHidden = true
        substituteView.isHidden = true
    }
    
    // MARK: Webservice
    
    func getFolders() {
        
        print("getFolders")
        
        var dataString = "folderData"
        
        var urlString = "\(appDelegate.serverDestination!)getFolders.php"
        
        if isSharingFriends == true
        {
            urlString = "\(appDelegate.serverDestination!)getUserFriends.php"
            dataString = "friendsData"
        }
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    if (uploadData.count > 0)
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = true
                        })
                        
                        self.selectedList = uploadData as! [NSMutableDictionary]
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.folderTable.reloadData()
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func removeRecipeItem () {
        
        print("removeRecipeItem")

        let parameters: Parameters = [
            "ingredientid": removeObject?.object(forKey: "ingredientid") as! String,
            "recipeid": recipeID!,
            "delete": true
        ]
        
        print("remove parameters: \(parameters)")
        
        //https://github.com/Alamofire/Alamofire
        
        Alamofire.request("\(appDelegate.serverDestination!)updateRecipeAF.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            print("request: \(response.request!)")
            print("result: \(response.result)")
            
            if let JSON = response.result.value {
                
                print("JSON: \(JSON)")
                
                let dataDict : NSDictionary = JSON as! NSDictionary
                
                //print("dataDict: \(dataDict)")
                
                if dataDict.object(forKey: "status") is NSNull
                {
                    print("nil status")
                }
                else
                {
                    let status = dataDict.object(forKey: "status") as! String
                    
                    print("status: \(status)")
                    
                    if status.contains("ingredients deleted")
                    {
                        //post notification refresh update recipes
                        
                        let dict2 : NSMutableDictionary = ["options" : self.newIngredientOptions, "servings" : self.servingSize!]
                    
                        NotificationCenter.default.post(name: Notification.Name("updateIngredients"), object: dict2)
                        
                        self.newIngredientOptions.removeAll()
                    }
                }
            }
        }
    }
    
    @IBAction func saveRecipe(_ sender: Any) {
        
        activityView.startAnimating()
        activityView.isHidden = false
        shareBtn.setTitle("", for: .normal)
        
        let debug = false
        
        if debug == true
        {
            ingredientOptions.remove(at: 0)
            
            NotificationCenter.default.post(name: Notification.Name("updateIngredients"), object: self.ingredientOptions)
            dismiss()
        }
        else
        {
            updateRecipe()
        }
        
        //Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.debugSave), userInfo: nil, repeats: false)
    }
    
    @objc func debugSave () {
        
        saveRecipe()
    }
    
    func saveRecipe () {
        
        //Alamofire to send dict
        
        //remove add ingredient option
        
        ingredientOptions.remove(at: 0)
        
        NotificationCenter.default.post(name: Notification.Name("modifyRecipe"), object: ingredientOptions)
        
        activityView.stopAnimating()
        activityView.isHidden = true
        
        dismiss()
    }
    
    func dismiss() {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        dismiss()
    }
    
    // MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.restorationIdentifier == "ingredients"
        {
            return self.newIngredientOptions.count
        }
        else
        {
            return self.substituteList.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //var selectedIem = self.foldersList[(indexPath as NSIndexPath).row]
        
        if tableView.restorationIdentifier == "ingredients"
        {
            if indexPath.row > 0
            {
                selectedIngredientItem = newIngredientOptions[indexPath.row]
                selectedIngredient = selectedIngredientItem?.object(forKey: "ingredientname") as? String
                substituteView.isHidden = false
            }
            
            getSubstitution()
        }
        else
        {
            selectedIngredientItem?.setValue(substituteList[indexPath.row], forKey: "ingredient")
            folderTable.reloadData()
            substituteView.isHidden = true
            self.substituteTable.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if indexPath.row > 0
        {
           return true
        }
        else
        {
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            
            // handle delete (by removing the data from your array and updating the tableview)
            
            removeObject = self.newIngredientOptions[indexPath.row]
            
            if removeObject?.object(forKey: "ingredientid") as? String != nil {
                
                removeRecipeItem()
            }
            
            self.newIngredientOptions.remove(at: indexPath.row)
            
//            if newIngredientOptions.contains(removeObject!)
//            {
//                newIngredientOptions.removeAll(where: { $0.object(forKey: "ingredientname") as! String == removeObject!.object(forKey: "ingredientname") as! String })
//            }
            
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            //delete where ingredientid AND recipeid AND userid
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.restorationIdentifier == "substitutes"
        {
            let cell : AddIngredientTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! AddIngredientTableViewCell
            
            
            cell.addIngredientLbl.text = substituteList[indexPath.row]
            
            return cell
        }
        else //if tableView.restorationIdentifier == "ingredients"
        {
            let cell : AddIngredientTableViewCell = tableView.dequeueReusableCell(withIdentifier: "add-ingredient-cell")! as! AddIngredientTableViewCell
            
            let dict = self.newIngredientOptions[(indexPath as NSIndexPath).row]
            
            let val = dict.object(forKey: "name") as! String
            
            if indexPath.row == 0
            {
                cell.selectionStyle = .none
            }
            else
            {
                cell.selectionStyle = .default
            }
            
            cell.quantityTxt.text = ""
            cell.unitTxt.text = ""
            cell.ingredientNameTxt.text = ""
            cell.unitTxt.keyboardType = .numberPad
            
            let pickerView = UIPickerView()
            pickerView.delegate = self
            pickerView.dataSource = self
            
            cell.unitTxt.inputView = pickerView
            
            let active = dict.object(forKey: "active") as! Bool            
            
            if val == "add new ingredient"
            {
                cell.startEditBtn.setTitle("", for: .normal)
                //cell.selectionStyle = .none
                
                if active == true
                {
                    cell.startEditBtn.isHidden = true
                    cell.addIngredientLbl.isHidden = true
                    
                    cell.quantityTxt.isHidden = false
                    cell.unitTxt.isHidden = false
                    cell.ingredientNameTxt.isHidden = false
                    
                    cell.quantityTxt.restorationIdentifier = String(indexPath.row)
                    cell.unitTxt.restorationIdentifier = String(indexPath.row)
                    cell.ingredientNameTxt.restorationIdentifier = String(indexPath.row)
                    
                    cell.quantityTxt.accessibilityHint = "quantity"
                    cell.unitTxt.accessibilityHint = "unit"
                    cell.ingredientNameTxt.accessibilityHint = "ingredient"
                    
                    cell.quantityTxt.delegate = self
                    cell.unitTxt.delegate = self
                    cell.ingredientNameTxt.delegate = self
                    
                    cell.quantityTxt.placeholder = "qty"
                    cell.unitTxt.placeholder = "unit"
                    cell.ingredientNameTxt.placeholder = "ingredient name"
                    
                    let keyboardDoneButtonView = UIToolbar()
                    keyboardDoneButtonView.sizeToFit()
                    
                    //quantity
                    
                    let customViewQuantity = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
                    customViewQuantity.backgroundColor = appDelegate.crLightBlue
                    
                    let viewLbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
                    viewLbl.text = "Quantity"
                    viewLbl.textColor = .white
                    viewLbl.textAlignment = .center
                    viewLbl.font = UIFont(name: "Roboto-Regular", size: 16.0)
                    
                    customViewQuantity.addSubview(viewLbl)
                    
                    //unit
                    
                    let customViewunit = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
                    customViewunit.backgroundColor = appDelegate.crLightBlue
                    
                    let viewLblUnit = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
                    viewLblUnit.text = "Unit"
                    viewLblUnit.textColor = .white
                    viewLblUnit.textAlignment = .center
                    viewLblUnit.font = UIFont(name: "Roboto-Regular", size: 16.0)
                    
                    customViewunit.addSubview(viewLblUnit)
                    
                    let doneButton4 = UIButton(frame: CGRect(x: customViewunit.frame.width - 60, y: 0, width: 50, height: 44))
                    doneButton4.setTitle("Done", for: .normal)
                    doneButton4.addTarget(self, action: #selector(self.doneClicked1(_:)), for: UIControlEvents.touchUpInside)
                    doneButton4.restorationIdentifier = String(indexPath.row)
                    customViewunit.addSubview(doneButton4)
                    
                    //ingredient
                    
                    let customViewIngredient = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
                    customViewIngredient.backgroundColor = appDelegate.crLightBlue
                    
                    let viewLbl3 = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
                    viewLbl3.text = "Ingredient"
                    viewLbl3.textColor = .white
                    viewLbl3.textAlignment = .center
                    viewLbl3.font = UIFont(name: "Roboto-Regular", size: 16.0)
                    
                    customViewIngredient.addSubview(viewLbl3)
                    
                    let doneButton3 = UIButton(frame: CGRect(x: customViewIngredient.frame.width - 60, y: 0, width: 50, height: 44))
                    doneButton3.setTitle("Done", for: .normal)
                    doneButton3.addTarget(self, action: #selector(self.doneClicked(_:)), for: UIControlEvents.touchUpInside)
                    doneButton3.restorationIdentifier = String(indexPath.row)
                    customViewIngredient.addSubview(doneButton3)
                    
                    cell.quantityTxt?.inputAccessoryView = customViewQuantity
                    cell.unitTxt?.inputAccessoryView = customViewunit
                    cell.ingredientNameTxt?.inputAccessoryView = customViewIngredient
                }
                else
                {
                    cell.startEditBtn.isHidden = false
                    cell.addIngredientLbl.isHidden = false
                    
                    cell.quantityTxt.isHidden = true
                    cell.unitTxt.isHidden = true
                    cell.ingredientNameTxt.isHidden = true
                    
                    cell.startEditBtn.restorationIdentifier = String(indexPath.row)
                    cell.startEditBtn.addTarget(self, action: #selector(self.startEditing(_:)), for: UIControlEvents.touchUpInside)
                }
            }
            else //if val == ""
            {
                cell.startEditBtn.setTitle("", for: .normal)
                cell.selectionStyle = .none
                
                //cell.startEditBtn.isHidden = false
                cell.startEditBtn.isHidden = true
                cell.addIngredientLbl.isHidden = false
                
                cell.quantityTxt.isHidden = true
                cell.unitTxt.isHidden = true
                cell.ingredientNameTxt.isHidden = true
                
                let qty = dict.object(forKey: "quantity") as! String
                
                var qtyStr = ""
                
                if qty == ""
                {
                    qtyStr = "0"
                }
                else
                {
                    qtyStr = String(format:"%.1f", Float(qty)!)
                }
                
                let unit = dict.object(forKey: "unit") as! String
                let ingredient = dict.object(forKey: "ingredientname") as! String
                
                cell.addIngredientLbl.text = "\(qtyStr) \(unit) \(ingredient)"
                cell.addIngredientLbl.textColor = appDelegate.gray51
                
                //randall to do disable since selected in grocery list on recipedetails
                
                if active == true && 0==1
                {
                    cell.addIngredientLbl.isHidden = true
                    
                    cell.quantityTxt.text = qty
                    cell.unitTxt.text = unit
                    cell.ingredientNameTxt.text = ingredient
                    
                    cell.quantityTxt.isHidden = false
                    cell.unitTxt.isHidden = false
                    cell.ingredientNameTxt.isHidden = false
                }
            }
            
            //print("val: \(val)")
            
            return cell
        }
    }
    
    // MARK: Pickerview

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let dict = units[row] as NSMutableDictionary
        
        //let name = dict.object(forKey: "name") as! String
        let abbr = dict.object(forKey: "abbr") as! String
        
        //currentTxt?.text = "\(name) \(abbr)"
        currentTxt?.text = abbr
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return units.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let dict = units[row] as NSMutableDictionary
        
        let name = dict.object(forKey: "name") as! String
        let abbr = dict.object(forKey: "abbr") as! String
        
        return "\(name) \(abbr)"
    }
    
    @objc func doneClicked1(_ sender : UIButton) {
        
        self.view.endEditing(true)
    }
    
    @objc func doneClicked(_ sender : UIButton) {
        
        if currentTxt?.text != ""
        {
            let btn = sender as UIButton
            let ind = Int(btn.restorationIdentifier!)
            
            let dict = self.ingredientOptions[ind!] as NSMutableDictionary
            
            dict.setValue(currentTxt?.text, forKey: "ingredientname")
            dict.setValue(false, forKey: "active")
            dict.setValue("", forKey: "name")
            dict.setValue(nil, forKey: "ingredientid")
            
            //loop through and make all dicts false
            
            newIngredientOptions.append(dict)
            //ingredientOptions.append(dict)
            
//            for dict1 in newIngredientOptions
//            {
//                dict1.setValue(false, forKey: "active")
//            }
            
            
            
            
//            let dict2 : NSMutableDictionary = ["name" : "add new ingredient", "active" : false]
//
//            //ingredientOptions.append(dict2)
//            ingredientOptions.insert(dict2, at: 0)
            
            if isUpdating != true
            {
                
            }
            
            shareBtn.backgroundColor = self.appDelegate.crOrange
            shareBtn.isEnabled = true
            
            //isUpdating = false
            
            folderTable.reloadData()
            
            //print("first: \(self.ingredientOptions[0])")
        }
        
        self.view.endEditing(true)
    }
    
    func getSubstitution () {
        
        print("getSubstitution")
        
        //var ingredient = "butter"
        
        let headers = [
            "X-Mashape-Key": "A6jIcNophWmshQEMCrJucq0gQW1xp1oyts4jsnqkBJhn4r3Iu3",
            "X-Mashape-Host": "spoonacular-recipe-food-nutrition-v1.p.mashape.com",
            "Cache-Control": "no-cache",
            "Postman-Token": "a29c5037-371f-4de0-846a-5410e183b0d6"
        ]
        
        let a = selectedIngredient?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        //print(selectedIngredient!)
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/food/ingredients/substitutes?ingredientName=\(a!)")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        print("sub request \(request)")
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if (error != nil) {
                print(error)
            } else {
                
//                let httpResponse = response as? HTTPURLResponse
//                print(httpResponse)
                
                do {
                    
                    let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    
                    print("substitutes jsonResult: \(jsonResult)")
                    
                    //let dataDict : NSDictionary = jsonResult.object(forKey: "ingredientData") as! NSDictionary
                    //let status : String = jsonResult.object(forKey: "status") as! String
                    
                    if jsonResult["status"]! is NSNull || jsonResult["message"] as! String == "Could not find any substitutes for that ingredient."  {
                        
                        print("no data")
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.substituteList.removeAll()
                            self.substituteTable.reloadData()
                            self.substituteTable.isHidden = true
                        })
                    }
                    else
                    {
                        let substituteData : NSMutableArray = (jsonResult["substitutes"]! as! NSArray).mutableCopy() as! NSMutableArray
                        
                        if (substituteData.count > 0)
                        {
                            DispatchQueue.main.sync(execute: {
                                
                                //self.noResultsMain.isHidden = true
                            })
                            
                            //self.selectedList = substituteData as! [NSMutableDictionary]
                            self.substituteList = substituteData as! [String]

                            DispatchQueue.main.sync(execute: {
                                
                                self.substituteTable.reloadData()
                                self.substituteTable.isHidden = false
                            })
                        }
                        else
                        {
                            DispatchQueue.main.sync(execute: {
                                
                                //self.noResultsMain.isHidden = false
                            })
                        }
                    }
                }
                catch let err as NSError
                {
                    print("error: \(err.description)")
                }                
            }
        })
        
        dataTask.resume()
    }
    
    @IBAction func increaseServing(_ sender: Any) {
        
        addCurrentIngredients = true
        
        servingSize += 0.5
        servingsLbl.text = "\(servingSize!) Servings"
        
        //newIngredientOptions.removeAll()
        
        var i = 0
        
        for dict in newIngredientOptions
        {
            if i > 0
            {
                var qty = Float(dict.object(forKey: "quantity") as! String)

                print("qty: \(qty)")

                qty = qty! + 0.5

                let str = String(format:"%.1f", qty!)

                dict.setValue("\(str)", forKey: "quantity")
            }
            
//            var qty = Float(dict.object(forKey: "quantity") as! String)
//
//            //print("qty: \(qty)")
//
//            qty = qty! + 0.5
//
//            let str = String(format:"%.1f", qty!)
//
//            dict.setValue("\(str)", forKey: "quantity")
            
            i += 1
            
            //newIngredientOptions.append(dict)
        }
        
        
        folderTable.reloadData()
    }
    
    @IBAction func decreaseServing(_ sender: Any) {
        
        if servingSize > 0.5
        {
            addCurrentIngredients = true
            
            //newIngredientOptions.removeAll()
            
            servingSize -= 0.5
            servingsLbl.text = "\(servingSize!) Servings"
            
            var i = 0
            
            for dict in newIngredientOptions
            {
                if i > 0
                {
                    var qty = Float(dict.object(forKey: "quantity") as! String)

                     qty = qty! - 0.5

                    let str = String(format:"%.1f", qty!)

                    dict.setValue("\(str)", forKey: "quantity")
                }

                i += 1
                
                
//                var qty = Float(dict.object(forKey: "quantity") as! String)
//
//                qty = qty! - 0.5
//
//                let str = String(format:"%.1f", qty!)
//
//                dict.setValue("\(str)", forKey: "quantity")
                
                //newIngredientOptions.append(dict)
            }
        }
        
        folderTable.reloadData()
    }
    
    @objc func startEditing(_ sender : UIButton) {
        
        substituteView.isHidden = true
        
        let btn = sender as UIButton
        let ind = Int(btn.restorationIdentifier!)
        
        let dict = self.newIngredientOptions[ind!] as NSDictionary
        
        dict.setValue(true, forKey: "active")
        
        if dict.object(forKey: "unit") != nil || dict.object(forKey: "quantity") != nil || dict.object(forKey: "ingredient") != nil
        {
            isUpdating = true
        }
        
        folderTable.reloadData()
    }
    
    @objc func saveToFolder(_ sender : UIButton) {
        
        let btn = sender as UIButton
        let ind = Int(btn.restorationIdentifier!)
        
        let dict = self.ingredientOptions[ind!]
        
        let active = dict.object(forKey: "active") as! Bool
        
        if active == true
        {
            dict.setValue(false, forKey: "active")
            
            if ingredientOptions.contains(dict)
            {
                ingredientOptions.remove(at: ingredientOptions.index(of: dict)!)
            }
        }
        else
        {
            dict.setValue(true, forKey: "active")
            ingredientOptions.append(dict)
            newIngredientOptions.append(dict)
        }
        
        folderTable.reloadData()
        
        print("selectedItems: \(selectedItems)")
    }
    
    // MARK: Textfield Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.restorationIdentifier != "editQuantity"
        {
            currentTxt = textField
            substituteView.isHidden = true
        }
        else
        {
            //substituteView.isHidden = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
        //func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.restorationIdentifier == "RecipeName"
        {
            //            recipeNameBtn.isHidden = false
            //            recipeNameLbl.isHidden = false
            //            recipeNameTxt.isHidden = true
        }
        else
        {
            
            let ind = Int(textField.restorationIdentifier!)
            
            let dict = self.ingredientOptions[ind!] as NSMutableDictionary
            
            dict.setValue(textField.text, forKey: textField.accessibilityHint!)
            
            if textField.accessibilityHint! == "unit" && textField.text == ""
            {
                dict.setValue("mil", forKey: textField.accessibilityHint!)
                textField.text = "mil"
            }
            else if textField.accessibilityHint! == "quantity" && textField.text == ""
            {
                dict.setValue("1", forKey: textField.accessibilityHint!)
                textField.text = "1"
            }
            
            print("dict vals: \(dict)")
        }
        
//        active = 0;
//        ingredient = rice;
//        name = "";
//        quantity = 1;
//        recipeorder = 0;
//        unit = oz;
        
        //testAFJSON.php to update
        
        //
    }
    
    func updateRecipe () {
        
        print("updateRecipe")
        
        self.newIngredientOptions.remove(at: 0)
        
        var parameters: Parameters = [
            "ingredients": newIngredientOptions,
            "userid": appDelegate.userid!,
            "recipeid": recipeID!,
            "servings": servingSize!
        ]
        
        if addCurrentIngredients == true
        {
            parameters.updateValue(self.ingredientOptions, forKey: "currentIngredients")
        }
        
        print("recipe parameters: \(parameters)")
        
        //https://github.com/Alamofire/Alamofire
        
        Alamofire.request("\(appDelegate.serverDestination!)updateRecipeAF.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            print("request: \(response.request!)")  // original URL request
            //print("response: \(response.response!)") // HTTP URL response
            //print("data: \(response.data)")     // server data
            print("result: \(response.result)")   // result of response serialization
            
            if let JSON = response.result.value {
                
                print("JSON: \(JSON)")
                
                let dataDict : NSDictionary = JSON as! NSDictionary
                
                //print("dataDict: \(dataDict)")
                
                if dataDict.object(forKey: "status") is NSNull
                {
                    print("nil status")
                }
                else
                {
                    let status = dataDict.object(forKey: "status") as! String
                    
                    print("status: \(status)")
                    
                    if status.contains("ingredients saved")
                    {
                        //post notification refresh update recipes
                        
                        self.shareBtn.backgroundColor = self.appDelegate.crWarmGray
                        self.shareBtn.isEnabled = false
                        
                        
                        let dict2 : NSMutableDictionary = ["options" : self.newIngredientOptions, "servings" : self.servingSize!]
                        
                        NotificationCenter.default.post(name: Notification.Name("updateIngredients"), object: dict2)
                        
                         self.newIngredientOptions.removeAll()
                        
                        
                        let alert = UIAlertController(title: nil, message: "Recipe Updated", preferredStyle: UIAlertControllerStyle.alert)
                        
                        self.present(alert, animated: true, completion: nil)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            switch action.style {
                                
                            case .default:
                                print("default")
                                
                                self.dismiss()
                                
                            case .cancel:
                                print("cancel")
                                
                            case .destructive:
                                print("destructive")
                            }
                        }))
                    }
                }
            }
        }
        
        activityView.stopAnimating()
        activityView.isHidden = true
        
        shareBtn.setTitle("Save Modified Recipe", for: .normal)
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
