//
//  EditRecipeGroceryList.swift
//  ChewsRite
//
//  Created by Randall Ridley on 8/23/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class EditRecipeGroceryList: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var groceryTable: UITableView!
    @IBOutlet weak var substituteLbl: UILabel!
    
    @IBOutlet weak var headerLbl: UILabel!
    var groceryList = [GroceryItem]()
    var showingGrocery : Bool?
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var ingredientIV: UIImageView!
    @IBOutlet weak var selectedImage: UIImage!
    
    @IBOutlet weak var servingsLbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    var selectedRecipeItem : RecipeItem?
    
    //var selectedItems = [GroceryItem]()
    var item : GroceryItem?
    
    var selectedItems : [String]?
    
    var selectedGroceryItems : [GroceryItem]?
    var removedSubstitutes : [GroceryItem]?
    
    var servingsCount : Int = 0
    var recipeID : String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //fix view recipe button crash
        
        //selectedItems = [String]()
        
//        for dict in groceryList
//        {
//            print("item: \(dict.ingredientid)")
//        }
        
        servingsCount = 1
        servingsLbl.text = "\(servingsCount) Servings"
        
        activityView.isHidden = true
        
        showingGrocery = false
        
        //headerLbl.text = groceryList[0].name
        ingredientIV.image = selectedImage
        
        groceryTable.delegate = self
        groceryTable.dataSource = self
        groceryTable.tableFooterView = UIView()
                
        //getAddedItems()
        getAllRecipeItems()
        
        //debug()
    }
    
    func debug () {
        
//        servingsCount = 1
//
//        headerLbl.text = "Quinoa"
//        ingredientIV.image = #imageLiteral(resourceName: "risotto")
//
//        servingsLbl.text = "\(servingsCount) Servings"
//
//        var dict : NSMutableDictionary = ["name" : "Millet","serving" : "1 oz","reviewdate" : "1-12", "active" : false]
//        groceryList.append(dict)
//
//        dict  = ["name" : "quinoa","serving" : "2 oz","reviewdate" : "2-11", "active" : false]
//        groceryList.append(dict)
//
//        groceryTable.reloadData()
    }
    
    // MARK: Webservice
    
    func getAllRecipeItems() {
        
        print("getMyRecipes")
        
        let dataString = "itemsData"
        
        let urlString = "\(appDelegate.serverDestination!)getRecipeIngredients.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&recipeid=\(recipeID!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("recipe items jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.recipeStatusTV.isHidden = false
                    })
                }
                else
                {
                    //let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let hasData = true
                    
                    let ingredientList = uploadData as! [NSDictionary]
                    //let ingredientid = uploadData.object(forKey: "ingredientids") as! String
                    
                    for dict in ingredientList
                    {
                        let item = GroceryItem(
                            ingredientname: dict.object(forKey: "ingredientname") as! String,
                            serving: "\(dict.object(forKey: "quantity") as! String) \(dict.object(forKey: "unit") as! String)",
                            active: false,
                            recipeid: self.recipeID!,
                            ingredientid: dict.object(forKey: "ingredientid") as! String,
                            ingredientsid: "", isSubstitute: dict.object(forKey: "isSubstitute") as! Bool,
                            imagename: dict.object(forKey: "imagename") as! String)
                        
                        self.groceryList.append(item)
                    }

                    DispatchQueue.main.sync(execute: {
                        
                        if hasData == true
                        {
                            self.groceryTable.reloadData()
                        }
                        else
                        {
                            print("no data")
                            
                            //self.noResultsMain.isHidden = false
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getAddedItems() {
        
        //displayItems.removeAll()
        
        print("getMyRecipes")
        
        let dataString = "itemsData"
        
        let urlString = "\(appDelegate.serverDestination!)getAddedGroceryItems.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&recipeid=\(groceryList[0].recipeid)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("recipe items jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.recipeStatusTV.isHidden = false
                    })
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    let hasData = true
                    
                    let ingredientid = uploadData.object(forKey: "ingredientids") as! String
                    
                    let arr = ingredientid.split(separator: ",")
                    
                    for a in arr
                    {
                        let b = String(a)
                        print("b: \(b)")
                        self.selectedItems?.append(String(a))
                    }
                    
//                        self.currentRecipesItemArrayGrocery = self.itemArrayGrocery
//                        self.currentRecipesItemArray = self.itemArrayRecipes
//
                    DispatchQueue.main.sync(execute: {
                        
                        //                            self.activityView.stopAnimating()
                        //                            self.activityView.isHidden = true
                        
                        if hasData == true
                        {
                            self.groceryTable.reloadData()
                        }
                        else
                        {
                            print("no data")
                            
                            //self.noResultsMain.isHidden = false
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func saveItems() {
        
        self.activityView.isHidden = false
        self.activityView.startAnimating()
        
        submitBtn.isEnabled = false
        submitBtn.backgroundColor = appDelegate.crWarmGray
        
        print("saveItems")
        
        var selectedIds = [String]()
        var subIds = [String]()
        //var removedSubIds = [String]()
        
        for dict in selectedGroceryItems!
        {
            if dict.isSubstitute == true
            {
                subIds.append(dict.ingredientid)
            }
            else
            {
                selectedIds.append(dict.ingredientid)
            }
        }
        
        var selectedRemovedIds = [String]()
        
        for dict in removedSubstitutes!
        {
            selectedRemovedIds.append(dict.ingredientid)
        }
        
        var removedItemslist = ""
        
        let itemslist = selectedIds.joined(separator: ",")
        removedItemslist = selectedRemovedIds.joined(separator: ",")
        let subIDlist = subIds.joined(separator: ",")
        
        //let itemslist = (itemsString.map{String($0)}).joined(separator: ",")
        
        let dataString = "groceryItemsData"
        
        let urlString = "\(appDelegate.serverDestination!)manageRecipeGroceryItems.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&ingredientids=\(itemslist)&recipeid=\(groceryList[0].recipeid)&devStatus=\(appDelegate.devStage!)&removedSubIDs=\(removedItemslist)&subIDs=\(subIDlist)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("added recipe items jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.showBasicAlert(string: "No data error")
                    })
                }
                else
                {
                    let dict : NSDictionary = dataDict["groceryItemsData"] as! NSDictionary
                    let status : String = dict["status"] as! String
                    
                    //print("status: \(status)")
                    
                    if (status == "grocery items saved" || status == "grocery items updated")
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.backgroundColor = self.appDelegate.crOrange
                            
                            //update previous screen selections
                            
                            NotificationCenter.default.post(name: Notification.Name("updateSavedGroceryItems"), object: self.selectedGroceryItems)
                            
                            //show alert
                            
                            let alert = UIAlertController(title: nil, message: "Items Saved Successfully", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style {
                                    
                                case .default:
                                    print("default")
                                    
                                    self.navigationController?.popViewController(animated: true)
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                            
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.backgroundColor = self.appDelegate.crOrange
                            
                            self.showBasicAlert(string: status)
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    // MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.groceryList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 65
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : GroceryListCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! GroceryListCell
        
        let dict = self.groceryList[(indexPath as NSIndexPath).row]
        
        let reviewdate = dict.ingredientname
        let username = dict.serving
        
        cell.selectionStyle = .none
        
        cell.ingredientLbl.text = reviewdate
        cell.servingLbl.text = username
        
        cell.selectBtn.restorationIdentifier = "\(indexPath.row)"
        cell.selectBtn.addTarget(self, action: #selector(self.selectItem(_:)), for: UIControlEvents.touchUpInside)
        
        if (selectedGroceryItems?.contains(dict))! {
            
            // found
            
            cell.selectBtn.setBackgroundImage(#imageLiteral(resourceName: "selected"), for: .normal)
            
        } else {
            
            // not found
            
            cell.selectBtn.setBackgroundImage(#imageLiteral(resourceName: "unselected"), for: .normal)
        }
        
        return cell
    }
    
    // MARK: Actions
    
    @IBAction func selectItem(_ sender: Any) {
        
        //update in db on click?
        
        let btn = sender as! UIButton
        let ind = Int(btn.restorationIdentifier!)
        
        item = groceryList[ind!]
        
        if (selectedGroceryItems?.contains((item)!))! {
            
            // found
            
            if (item?.isSubstitute)!
            {
                removedSubstitutes?.append(item!)
            }
            
            btn.setBackgroundImage(#imageLiteral(resourceName: "unselected"), for: .normal)
            
            selectedGroceryItems?.removeAll(where: {$0.ingredientid == item?.ingredientid})
            
        } else {
            
            btn.setBackgroundImage(#imageLiteral(resourceName: "selected"), for: .normal)
            
            selectedGroceryItems?.append((item)!)
        }
    }
    
    @IBAction func saveIngredient(_ sender: Any) {
        
        saveItems()
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    @IBAction func changeServings(_ sender: Any) {
        
        let btn = sender as? UIButton
        
        if btn?.restorationIdentifier == "increase"
        {
            servingsCount += 1
            
            servingsLbl.text = "\(servingsCount) Servings"
        }
        else
        {
            if servingsCount > 1
            {
                servingsCount -= 1
                
                servingsLbl.text = "\(servingsCount) Servings"
            }
        }
    }
    @IBAction func viewRecipe(_ sender: Any) {
        
        performSegue(withIdentifier: "ViewRecipe", sender: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func dismissKeyboard () {
        
        self.view.endEditing(true)
    }
    
     // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "ViewRecipe"
        {
            let destination = segue.destination as! RecipeDetails
            destination.selectedImage = selectedImage
            destination.selectedRecipeItem = selectedRecipeItem
        }
    }
     
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension GroceryItem: Equatable {}

func ==(left: GroceryItem, right: GroceryItem) -> Bool {
    
    return left.ingredientid == right.ingredientid
}
