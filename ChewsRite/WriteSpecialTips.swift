//
//  WriteSpecialTips.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/13/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class WriteSpecialTips: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var tipsTV: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tipsTV.delegate = self
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        keyboardDoneButtonView.items = [doneButton]
        
        tipsTV?.inputAccessoryView = keyboardDoneButtonView
        
        tipsTV.text = "Enter tip"
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Enter tip"
        {
            textView.text = ""
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""
        {
            textView.text = "Enter tip"
        }
    }

    @IBAction func save(_ sender: Any) {
        
        let dict : NSDictionary = ["name" : "","tips" : tipsTV.text!]
        
        NotificationCenter.default.post(name: Notification.Name("tipsSaved"), object: dict)
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func doneClicked(sender: UIButton!) {
        
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
