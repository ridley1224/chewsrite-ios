//
//  RecipeDetails.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/13/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import Social
import MessageUI
import AVFoundation
import AVKit
import AWSS3

class RecipeDetails: UIViewController, UITableViewDelegate, UITableViewDataSource,UICollectionViewDataSource, UICollectionViewDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate  {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var statusImageCache = [String:UIImage]()

    @IBOutlet weak var headerLbl: UILabel!
    
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var recipeIV: UIImageView!
    
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var ratingIV: UIImageView!
    
    @IBOutlet weak var thankYouView: UIView!
    @IBOutlet weak var modifyBtn: UIButton!
    
    @IBOutlet weak var groceryListBtn: UIButton!
    @IBOutlet weak var ratingBtn: UIButton!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var optionsSegment: UISegmentedControl!
    
    @IBOutlet weak var viewReviewsBtn: UIButton!
    @IBOutlet weak var ratingTV: UITextView!
    @IBOutlet weak var sourceLbl: UILabel!
    @IBOutlet weak var prepTimeLbl: UILabel!
    @IBOutlet weak var cookTimeLbl: UILabel!
    @IBOutlet weak var totalTimeLbl: UILabel!
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var sourceIV: UIImageView!
    
    @IBOutlet weak var servingsLbl: UILabel!
    
    @IBOutlet weak var tagsCV: UICollectionView!
    @IBOutlet weak var relatedRecipesCV: UICollectionView!
    
    @IBOutlet weak var ratingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentScrollview: UIScrollView!
    
    @IBOutlet weak var reviewView: UIView!
    @IBOutlet weak var reviewviewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var buttonViewHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var addRatingsBtnHeight: NSLayoutConstraint!
    
    @IBOutlet weak var relatedRecipesTopMargin: NSLayoutConstraint!
    @IBOutlet weak var star5: UIButton!
    @IBOutlet weak var star4: UIButton!
    @IBOutlet weak var star3: UIButton!
    @IBOutlet weak var star2: UIButton!
    @IBOutlet weak var star1: UIButton!
    
    var ingredientsList = [NSMutableDictionary]()
    var directionsList = [NSDictionary]()
    var nutritionValues = [String]()
    var tipsList = [NSDictionary]()
    var reviewsList = [NSDictionary]()
    var tagsList = [String]()
    var cartItems = [NSMutableDictionary]()
    var recipeList = [NSDictionary]()
    
    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    
    var selectedRating : String?
    var recipeID : String?
    
    @IBOutlet weak var cartItemsLbl: UILabel!
    
    @IBOutlet weak var cartIV: UIImageView!
    @IBOutlet weak var infoTable: UITableView!
    var isFavorite : Bool?
    
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var shareView: UIView!
    
    var isSharingFriends : Bool?
    
    @IBOutlet weak var toastView: UIView!
    @IBOutlet weak var toastStatusLbl: UILabel!
    @IBOutlet weak var cancelShareBtn: UIButton!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    var recipeName : String?
    
    @IBOutlet weak var shareSocialView: UIView!
    @IBOutlet weak var shareToView: UIView!
    var recipeDict : NSMutableDictionary?
    
    var shareDestination : String?
    
    var selectedItem : FeedItem?
    var selectedRecipeItem : RecipeItem?
    var selectedImage : UIImage?
    
    var recipename = ""
    var calories = ""
    var source = ""
    var cooktime = ""
    var preptime = ""
    
    var nutritionLabelList = ["Calories","Carbohydrates","Protein","Fat","     Saturated Fat","     Unsaturated Fat","     Trans Fat","Cholestrol","Sodium","Potassium","Fiber","Sugars","Vitamin A","Vitamin C","Calcium","Iron"]
        
    @IBOutlet weak var tableStatusLbl: UILabel!
    
    @IBOutlet weak var imagesWidth: NSLayoutConstraint!
    
    @IBOutlet weak var imagesLeading: NSLayoutConstraint!
    
    @IBOutlet weak var tagsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imagesBtn: UIButton!
    
    
    @IBOutlet weak var buttonTopSpace: NSLayoutConstraint!
    
    @IBOutlet weak var loadingView: UIView!
    
    @IBOutlet weak var sourceIVLeading: NSLayoutConstraint!
    
    var scrollHeight : CGFloat?
    
    var selectedIngredient : NSMutableDictionary?
    
    
    // MARK: viewDidLoad
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        relatedRecipesCV.delegate = self
        relatedRecipesCV.dataSource = self
        
        modifyBtn.isHidden = true
        playBtn.isHidden = true
        cartItemsLbl.isHidden = true
        tableStatusLbl.isHidden = true
        imagesBtn.isHidden = true
        cartIV.isHidden = true
        imagesWidth.constant = 0
        imagesLeading.constant = 0
        
        shareView.layer.cornerRadius = 20
        shareView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        shareToView.layer.cornerRadius = 20
        shareToView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        //default new review value
        
        selectedRating = "5"
        
        star1.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
        star2.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
        star3.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
        star4.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
        star5.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
        
        for val in nutritionLabelList
        {
            if val == "Calories"
            {
                nutritionValues.append("0")
            }
            else
            {
                nutritionValues.append("0.0")
            }
        }
        
        if selectedItem != nil
        {
            recipename = (selectedItem?.recipename)!
            calories = (selectedItem?.calories)!
            source = (selectedItem?.source)!
            cooktime = (selectedItem?.cooktime)!
            preptime = (selectedItem?.preptime)!
            
            recipeID = selectedItem!.recipeid
            
            sourceIV.downloaded(from: "\(selectedItem!.sourceurl)/favicon.ico")

            let desc = selectedItem?.description.replacingOccurrences(of: "\"", with: "")
            
            descriptionLbl.text = desc
            servingsLbl.text = selectedItem?.servings
            
            if selectedItem?.videoname != ""
            {
                playBtn.isHidden = false
            }
            
            let attributedString = NSMutableAttributedString(string: "\(recipename) (\(calories) calories)", attributes: [
                .font: UIFont(name: "Avenir-Black", size: 18.0)!,
                .foregroundColor: UIColor.white
                ])
            
            let c1 = recipename.count
            let c2 = calories.count
            
            let len = c1 + 1
            let last = c2 + 10
            
            attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Black", size: 14.0)!, range: NSRange(location: len, length: last))
            
            headerLbl.attributedText = attributedString
            sourceLbl.text = "Source: \(source)"
            
//            let textCount = sourceLbl.text?.count
//            
//            if textCount! <= 20
//            {
//                sourceIVLeading.constant = 50
//            }
//            else if textCount! > 20 && textCount! <= 40
//            {
//                sourceIVLeading.constant = 25
//            }
//            else
//            {
//                sourceIVLeading.constant = 15
//            }
            
            //sourceIVLeading.constant = CGFloat(()!)
            
            prepTimeLbl.text = "\(preptime) mins"
            cookTimeLbl.text =  "\(cooktime) mins"
            totalTimeLbl.text = "\(selectedItem!.totaltime) mins"
            
            if selectedItem?.isFavorite == "1"
            {
                self.favoriteBtn?.setBackgroundImage(#imageLiteral(resourceName: "addToFavoritesRecipe"), for: .normal)
            }
            
            if selectedItem?.userid == appDelegate.userid
            {
                modifyBtn.isHidden = false
            }
            
            let rating = selectedItem?.rating
            
            if rating == "1"
            {
                ratingIV.image = #imageLiteral(resourceName: "a1star")
            }
            else if rating == "2"
            {
                ratingIV.image = #imageLiteral(resourceName: "a2star")
            }
            else if rating == "3"
            {
                ratingIV.image = #imageLiteral(resourceName: "a3star")
            }
            else if rating == "4"
            {
                ratingIV.image = #imageLiteral(resourceName: "a4star")
            }
            else if rating == "5"
            {
                ratingIV.image = #imageLiteral(resourceName: "a5star")
            }
            
            getRecipeIngredients()
        }
        else if selectedRecipeItem != nil
        {
            recipeID = selectedRecipeItem!.recipeid
            getRecipeDetails()
            //getRecipeIngredients()
        }
        
        if selectedImage != nil
        {
            recipeIV.image = selectedImage
        }
        else
        {
            recipeIV.image =  UIImage(named: "profile-icon")
        }
        
        reviewviewHeight.constant = 0
        reviewView.isHidden = true
        ratingBtn.isHidden = true
        viewReviewsBtn.isHidden = true
        thankYouView.isHidden = true
        shareSocialView.isHidden = true
        
        infoTable.delegate = self
        infoTable.dataSource = self
        infoTable.tableFooterView = UIView()
        
        tagsCV.delegate = self
        tagsCV.dataSource = self
        
        //optionsSegment.tintColor = .white
        
        let titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        optionsSegment.setTitleTextAttributes(titleTextAttributes, for: .normal)
        optionsSegment.setTitleTextAttributes(titleTextAttributes, for: .selected)
        
        shareView.isHidden = true
        toastView.isHidden = true
        shareToView.isHidden = true
        cancelShareBtn.layer.cornerRadius = 4.0
        groceryListBtn.layer.cornerRadius = 4.0
        viewReviewsBtn.layer.cornerRadius = 4.0
        //viewReviewsBtn.layer.cornerRadius = 5.0
        
        shareView.layer.cornerRadius = 20.0
        //toastView.clipsToBounds = true
        
        toastView.layer.shadowOffset = .zero        
        
        let btnItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(dismissKeyboard))
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width:  self.view.frame.size.width, height: 50))
        numberToolbar.backgroundColor = UIColor.darkGray
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.tintColor = UIColor.black
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            btnItem]
        
        numberToolbar.sizeToFit()
        
        ratingTV.inputAccessoryView = numberToolbar
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(sharedToFriendsGroupsNotification),
            name: NSNotification.Name(rawValue: "sharedToFriendsGroups"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(recipeBookmarkedNotification),
            name: NSNotification.Name(rawValue: "recipeBookmarked"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(modifyRecipeNotification),
            name: NSNotification.Name(rawValue: "modifyRecipe"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(savedMealPlanNotification),
            name: NSNotification.Name(rawValue: "savedMealPlan"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(updateIngredientsNotification),
            name: NSNotification.Name(rawValue: "updateIngredients"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(refreshDetailsNotification),
            name: NSNotification.Name(rawValue: "refreshDetails"),
            object: nil)
        
    }
    
    @objc func refreshDetailsNotification (notification: NSNotification) {
        
        print("reload table")
        
        infoTable.reloadData()
    }
    
    func initUI () {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        contentScrollview.contentSize = CGSize.init(width: contentScrollview.frame.width, height: 1500)
    }
    
    // MARK: Webservice
    
    func getRecipeDetails () {
        
        let urlString = "\(appDelegate.serverDestination!)getRecipeDetails.php"
        
        print("recipe details urlString: \(urlString)")
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
                
        let paramString = "recipeid=\(recipeID!)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        print("recipe details param: \(paramString)")
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            //print("Entered the completionHandler: \(response)")
            
            //var err: NSError?
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("loginData: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                print("dataDict: \(dataDict)")
                
                // || dataDict["relatedRecipeData"] == nil
                if dataDict["relatedRecipeData"] is NSNull
                {
                    print("no related recipes data")
                }
                else
                {
                    let relatedRecipeData = (dataDict["relatedRecipeData"]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    var hasRelatedRecipeData = false
                    
                    if (relatedRecipeData != nil)
                    {
                        self.recipeList = relatedRecipeData as! [NSDictionary]
                        
                        if (self.recipeList.count > 0)
                        {
                            hasRelatedRecipeData = true
                            
    //                        for dict in recipes
    //                        {
    //                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
    //
    //                            self.cuisines.add(dict2)
    //                        }
                        }
                    }
                    else
                    {
                        print("no related recipes")
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if hasRelatedRecipeData
                        {
                            self.relatedRecipesCV.reloadData()
                        }
                        else
                        {
                            self.relatedRecipesCV.isHidden = true
                        }
                    })
                }
                
                
                let recipeData : NSDictionary = dataDict.object(forKey: "recipeData") as! NSDictionary
                
                if (recipeData != nil)
                {
                    self.recipename = recipeData.object(forKey: "recipename") as! String
                    self.calories = recipeData.object(forKey: "calories") as! String
                    self.source = recipeData.object(forKey: "source") as! String
                    self.cooktime = recipeData.object(forKey: "cooktime") as! String
                    self.preptime = recipeData.object(forKey: "preptime") as! String
                    
                    let c1 = self.recipename.count
                    let c2 = self.calories.count
                    
                    let len = c1 + 1
                    let last = c2 + 10
                    
                    let attributedString = NSMutableAttributedString(string: "\(self.recipename) (\(self.calories) calories)", attributes: [
                        .font: UIFont(name: "Avenir-Black", size: 18.0)!,
                        .foregroundColor: UIColor.white
                        ])
                    
                    attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Black", size: 14.0)!, range: NSRange(location: len, length: last))
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if recipeData.object(forKey: "userid") as? String == self.appDelegate.userid
                        {
                            self.modifyBtn.isHidden = false
                        }
                        
                        if recipeData.object(forKey: "videoname") as? String != ""
                        {
                            self.playBtn.isHidden = false
                        }
                        
                        var desc = recipeData.object(forKey: "recipedescription") as? String
                        desc = desc!.replacingOccurrences(of: "\"", with: "")
                        
                        self.descriptionLbl.text = desc
                        
                        self.servingsLbl.text = recipeData.object(forKey: "servings") as? String
                        
                        self.headerLbl.attributedText = attributedString
                        self.sourceLbl.text = "Source: \(self.source)"
                        
                        let downloadString = "\(recipeData.object(forKey: "sourceurl") as! String)/favicon.ico"
                        
                        print("downloadString: \(downloadString)")
                        
                        self.sourceIV.downloaded(from: downloadString)
                        
                        self.prepTimeLbl.text = self.preptime
                        self.cookTimeLbl.text =  self.cooktime
                        self.totalTimeLbl.text =  recipeData.object(forKey: "totaltime") as? String
                        
                        if recipeData.object(forKey: "isFavorite") as! String == "1"
                        {
                            self.favoriteBtn?.setBackgroundImage(#imageLiteral(resourceName: "addToFavoritesRecipe"), for: .normal)
                        }
                        else
                        {
                            self.favoriteBtn?.setBackgroundImage(#imageLiteral(resourceName: "favorite-recipe-details"), for: .normal)
                        }
                        
                        let rating = recipeData.object(forKey: "rating") as? String
                        
                        if rating == "1"
                        {
                            self.ratingIV.image = #imageLiteral(resourceName: "a1star")
                        }
                        else if rating == "2"
                        {
                            self.ratingIV.image = #imageLiteral(resourceName: "a2star")
                        }
                        else if rating == "3"
                        {
                            self.ratingIV.image = #imageLiteral(resourceName: "a3star")
                        }
                        else if rating == "4"
                        {
                            self.ratingIV.image = #imageLiteral(resourceName: "a4star")
                        }
                        else if rating == "5"
                        {
                            self.ratingIV.image = #imageLiteral(resourceName: "a5star")
                        }
                        
                        self.getRecipeIngredients()
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
                
                DispatchQueue.main.sync(execute: {
                    
                })
            }
            
        }.resume()
    }
    
    func getRecipeIngredients() {
        
        print("getRecipeIngredients")
        
        let dataString = "recipeData"
        
        let urlString = "\(appDelegate.serverDestination!)getRecipeInfo.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        //let userid = "22"
        
        let paramString = "recipeid=\(recipeID!)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("recipe ingredients jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no ingredients")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.tableHeightConstraint.constant = 10
                        self.relatedRecipesTopMargin.constant = 0
                        self.cartIV.isHidden = true
                        self.tableStatusLbl.isHidden = false
                        self.groceryListBtn.isHidden = true
                        //self.tableStatusLbl.text = "There are currently no ingredients for this recipe."
                        self.tableStatusLbl.text = ""
                    })
                }
                else
                {
                    let ingredientData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = ingredientData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        //self.tableStatusLbl.isHidden = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            let sub = dict2.object(forKey: "isSubstitute") as! Bool
                            
                            let dict : NSMutableDictionary = ["name" : "","quantity" : dict2.object(forKey: "quantity") as! String,"unit" : dict2.object(forKey: "unit") as! String ,"ingredientname" : dict2.object(forKey: "ingredientname") as! String, "ingredientid" : dict2.object(forKey: "ingredientid") as! String, "active" : dict2.object(forKey: "active") as! Bool, "recipeorder" : dict2.object(forKey: "recipeorder") as! String,"isSubstitute" : sub, "recipeid" : self.recipeID!]
                            
                            if sub == true
                            {
                                dict.setValue(dict2.object(forKey: "replacedobjectid") as! String, forKey: "replacedobjectid")
                            }
                            
                            self.ingredientsList.append(dict)
                            
                            if dict2.object(forKey: "active") as! Bool == true
                            {
                                self.cartItems.append(dict)
                            }
                        }
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if hasData == true
                        {
                            self.cartIV.isHidden = false
                            
                            if self.cartItems.count > 0
                            {
                                self.cartItemsLbl.isHidden = false
                                self.cartItemsLbl.text = "\(self.cartItems.count)"
                            }                            
                        }
                        else
                        {
                            print("no ingredient data")
                            
                            self.cartIV.isHidden = true
                            self.tableStatusLbl.isHidden = false
                            //self.tableStatusLbl.text = "There are currently no ingredients for this recipe."
                            self.tableStatusLbl.text = ""
                            self.relatedRecipesTopMargin.constant = 0
                        }
                    })
                }
                
                if dataDict["directionsData"]! is NSNull {
                    
                    print("no directions")
                }
                else
                {
                    let directionsData : NSMutableArray = (dataDict["directionsData"]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let directions = directionsData as! [NSDictionary]
                    
                    var hasDirectionsData = false
                    
                    if (directions.count > 0)
                    {
                        hasDirectionsData = true
                        
                        for dict in directions
                        {
                            let dict3 = dict.mutableCopy() as! NSMutableDictionary
                            
                            var str1 = dict3.object(forKey: "directions") as! String
                            str1 = str1.replacingOccurrences(of: "\"", with: "")
                            
                            let dict : NSMutableDictionary = ["title" : dict3.object(forKey: "title") as! String, "directions" : str1.utf8DecodedString()]
                            
                            self.directionsList.append(dict)
                        }
                    }
                    else
                    {
                        print("no ingredient data")
                    }
                }
                
                if dataDict["reviewsData"]! is NSNull {
                    
                    print("no reviews")
                }
                else
                {
                    let reviewsData : NSMutableArray = (dataDict["reviewsData"]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let reviews = reviewsData as! [NSDictionary]
                    
                    var hasReviewsData = false
                    
                    if (reviews.count > 0)
                    {
                        hasReviewsData = true
                        
                        for dict in reviews
                        {
                            let dict3 = dict.mutableCopy() as! NSMutableDictionary
                            
                            let dict : NSMutableDictionary = ["review" : dict3.object(forKey: "review") as! String, "reviewdate" : dict3.object(forKey: "reviewdate") as! String, "username" : dict3.object(forKey: "username") as! String, "rating" : dict3.object(forKey: "rating") as! String]
                            
                            self.reviewsList.append(dict)
                        }
                    }
                    else
                    {
                        print("no reviews data")
                    }
                }
                
                
                if dataDict["tipsData"]! is NSNull {
                    
                    print("no tips")
                }
                else
                {
                    let tipsData : NSMutableArray = (dataDict["tipsData"]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let tips = tipsData as! [NSDictionary]
                    
                    var hasTipsData = false
                    
                    if (tips.count > 0)
                    {
                        hasTipsData = true
                        
                        for dict in tips
                        {
                            let dict3 = dict.mutableCopy() as! NSMutableDictionary
                            
                            let dict : NSMutableDictionary = ["tip" : dict3.object(forKey: "tip") as! String, "username" : dict3.object(forKey: "username") as! String, "title" : dict3.object(forKey: "title") as! String]
                            
                            self.tipsList.append(dict)
                        }
                    }
                    else
                    {
                        print("no tips data")
                    }
                }
                
//                if dataDict["tagsData"]! is NSNull {
//
//                    print("no tags")
//
//                    DispatchQueue.main.sync(execute: {
//
//                        self.tagsHeight.constant = 22
//                    })
//                }
//                else
//                {
//                    let tagsData : NSMutableArray = (dataDict["tagsData"]! as! NSArray).mutableCopy() as! NSMutableArray
//
//                    let tags = tagsData as! [NSDictionary]
//
//                    var hasTagsData = false
//
//                    if (tags.count > 0)
//                    {
//                        hasTagsData = true
//
//                        for dict in tags
//                        {
//                            let dict3 = dict.mutableCopy() as! NSMutableDictionary
//
//                            self.tagsList.append(dict3.object(forKey: "cuisinename") as! String)
//                        }
//
//                        if (tags.count < 4)
//                        {
//                            DispatchQueue.main.sync(execute: {
//
//                                self.tagsHeight.constant = 22
//                            })
//                        }
//                    }
//                    else
//                    {
//                        print("no tags data")
//
//                        DispatchQueue.main.sync(execute: {
//
//                            self.tagsHeight.constant = 22
//                        })
//                    }
//                }
                
                
                if dataDict["concernsData"]! is NSNull {
                    
                    print("no concerns")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.tagsHeight.constant = 22
                    })
                }
                else
                {
                    let tagsData : NSMutableArray = (dataDict["concernsData"]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let tags = tagsData as! [NSDictionary]
                    
                    var hasTagsData = false
                    
                    if (tags.count > 0)
                    {
                        hasTagsData = true
                        
                        for dict in tags
                        {
                            let dict3 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.tagsList.append(dict3.object(forKey: "concernname") as! String)
                        }
                        
                        if (tags.count < 4)
                        {
                            DispatchQueue.main.sync(execute: {
                                
                                self.tagsHeight.constant = 22
                            })
                        }
                    }
                    else
                    {
                        print("no concerns data")
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.tagsHeight.constant = 22
                        })
                    }
                }
                
                if dataDict["nutritionData"]! is NSNull {
                    
                    print("no nutrition")
                }
                else
                {
                    let nutritionData = dataDict["nutritionData"]! as! NSDictionary
                    
                    self.nutritionValues[0] = nutritionData.object(forKey: "Calories") as! String
                    self.nutritionValues[1] = nutritionData.object(forKey: "Carbohydrates") as! String
                    self.nutritionValues[2] = nutritionData.object(forKey: "Protein") as! String
                    self.nutritionValues[3] = nutritionData.object(forKey: "Fat") as! String
                    self.nutritionValues[4] = nutritionData.object(forKey: "SaturatedFat") as! String
                    self.nutritionValues[5] = nutritionData.object(forKey: "UnsaturatedFat") as! String
                    self.nutritionValues[6] = nutritionData.object(forKey: "TransFat") as! String
                    self.nutritionValues[7] = nutritionData.object(forKey: "Cholestrol") as! String
                    self.nutritionValues[8] = nutritionData.object(forKey: "Sodium") as! String
                    self.nutritionValues[9] = nutritionData.object(forKey: "Potassium") as! String
                    self.nutritionValues[10] = nutritionData.object(forKey: "Fiber") as! String
                    self.nutritionValues[11] = nutritionData.object(forKey: "Sugars") as! String
                    self.nutritionValues[12] = nutritionData.object(forKey: "VitaminA") as! String
                    self.nutritionValues[13] = nutritionData.object(forKey: "VitaminC") as! String
                    self.nutritionValues[14] = nutritionData.object(forKey: "Calcium") as! String
                    self.nutritionValues[15] = nutritionData.object(forKey: "Iron") as! String
                }
                
                var hasRelatedRecipeData = false

                
                if dataDict["relatedRecipeData"]! is NSNull {
                
                    print("no related recipes")
                }
                else
                {
                    let relatedRecipeData = (dataDict["relatedRecipeData"]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    if (relatedRecipeData != nil)
                    {
                        if (relatedRecipeData.count > 0)
                        {
                            hasRelatedRecipeData = true
                            
                            for dict in relatedRecipeData
                            {
                                //let dict2 = dict.mutableCopy() as! NSMutableDictionary
    
                                self.recipeList.append(dict as! NSDictionary)
                            }
                        }
                    }
                }
                
                
                DispatchQueue.main.sync(execute: {
                    
                    if self.ingredientsList.count > 0
                    {
                        self.tableHeightConstraint.constant = 300
                    }
                    else
                    {
                        self.tableHeightConstraint.constant = 10
                    }
                    
                    self.infoTable.reloadData()
                    self.tagsCV.reloadData()
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        
                        self.loadingView.alpha = 0
                    })
                    
                    if hasRelatedRecipeData == true
                    {
                        self.relatedRecipesCV.reloadData()
                    }
                    else
                    {
                        self.relatedRecipesCV.isHidden = true
                    }
                })
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func updateGroceryItem() {
        
        print("updateGroceryItem")
        
        let dataString = "groceryItemsData"
        
        let urlString = "\(appDelegate.serverDestination!)manageSingleGroceryItem.php"
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let ingredientid = selectedIngredient?.object(forKey: "ingredientid") as! String
        let active = selectedIngredient?.object(forKey: "active") as! Bool
        let isSubstitute = selectedIngredient?.object(forKey: "isSubstitute") as! Bool
        
        
        var paramString = "recipeid=\(recipeID!)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)&active=\(active.intValue)"
        
        if isSubstitute == true
        {
            paramString = "\(paramString)&isSubstitute=true&ingredientid=\(selectedIngredient?.object(forKey: "replacedobjectid") as! String)&subid=\(ingredientid)"
        }
        else
        {
            paramString = "\(paramString)&ingredientid=\(selectedIngredient?.object(forKey: "ingredientid") as! String)"
        }
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("grocery jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                    })
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "grocery items updated" || status == "grocery item added" || status == "substitution added" || status == "substitution removed" || status == "substitution updated"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            //update grocery items tab
                            
                            NotificationCenter.default.post(name: Notification.Name("updateGroceryItemsTab"), object: self.selectedIngredient)
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func updateGroceryItemOrig() {
        
        print("updateGroceryItem")
        
        let dataString = "groceryData"
        
        let urlString = "\(appDelegate.serverDestination!)manageGroceryItem.php"
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "recipeid=\(recipeID!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("grocery jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                    })
                }
                else
                {
                    DispatchQueue.main.sync(execute: {
                        
                        self.showBasicAlert(string: "Please check your internet connection.")
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
            }.resume()
    }
    
    func debug () {
        
//        var dict : NSMutableDictionary = ["name" : "","quantity" : "1/2","unit" : "cup ","ingredient" : "Quinoa", "active" : false]
//
//        ingredientsList.append(dict)
//
//        dict = ["name" : "","quantity" : "12","unit" : "oz ","ingredient" : "vegetable stock concentrate", "active" : false]
//
//        ingredientsList.append(dict)
//
//        dict  = ["title" : "Step 1: Preheat oven and cook quinoa", "directions" : "Preheat oven to 400° F. Place vegetable stock concentrate, quinoa, 1 cup water, and a pinch of salt in a small pot. Bring to a boil, cover, and reduce to a simmer until tender (15-20 minutes)"]
//
//        directionsList.append(dict)
//
//        dict  = ["title" : "Step 2: Prepare produce", "directions" : "Wash and dry all produce. Shell and roughly chop pistachios. Dice cucumber and tomato. Finely chop mint leaves. Halve, peel and finely dice shallot. Zest and halve lime. Dice jalapeño, removing ribs and seeds if you prefer less heat."]
//
//        directionsList.append(dict)
//
//
//        dict  = ["title" : "Calories", "value" : "400"]
//
//        nutritionList.append(dict)
//
//        dict  = ["title" : "     Carbohydrates", "value" : "0.0g"]
//
//        nutritionList.append(dict)
//
//        dict  = ["title" : "Tip 1 Title", "tip" : "Tip description"]
//
//        tipsList.append(dict)
//
//        dict  = ["username" : "Marguerite_Mullins", "reviewdate" : "05/09/2017", "review" : "This delicious recipe tops the good-for-you quinoa grain with pistachio-crusted chicken, a refreshing chopped salad, and a bit of jalapeno for good measure.", "rating" : "4"]
//
//        reviewsList.append(dict)
//
//        recipeDict = ["preptime" : "10 minutes", "cooktime" : "20 minutes", "totaltime" : "30 minutes", "source" : "Source: Hello Fresh", "recipename" : "Pistachio-Crusted Chicken and Quinoa Salad", "imagename" : "risotto", "calories" : "400", "recipeid" : "1"]
//
//        prepTimeLbl.text = recipeDict?.object(forKey: "preptime") as? String
//        cookTimeLbl.text =  recipeDict?.object(forKey: "cooktime") as? String
//        totalTimeLbl.text =  recipeDict?.object(forKey: "totaltime") as? String
//
//        sourceLbl.text = recipeDict?.object(forKey: "source") as? String
//
//        tagsList.append("Heart Healthy")
//        tagsList.append("Dairy Free")
//        tagsList.append("Wheat Free")
//        tagsList.append("No Peanuts")
//        tagsList.append("Kosher")
//        tagsList.append("Something Else")
//
//        descriptionLbl.text = "This delicious recipe tops the good-for-you quinoa grain with pistachio-crusted chicken, a refreshing chopped salad, and a bit of jalapeno…"
//
//        servingsLbl.text = "3"
//
//        let rating = "4"
//
//        if rating == "1"
//        {
//            ratingIV.image = #imageLiteral(resourceName: "a1star")
//        }
//        else if rating == "2"
//        {
//            ratingIV.image = #imageLiteral(resourceName: "a2star")
//        }
//        else if rating == "3"
//        {
//            ratingIV.image = #imageLiteral(resourceName: "a3star")
//        }
//        else if rating == "4"
//        {
//            ratingIV.image = #imageLiteral(resourceName: "a4star")
//        }
//        else if rating == "5"
//        {
//            ratingIV.image = #imageLiteral(resourceName: "a5star")
//        }
//
//        if isFavorite == true
//        {
//            favoriteBtn?.setBackgroundImage(#imageLiteral(resourceName: "addToFavoritesRecipe"), for: .normal)
//        }
//        else
//        {
//            favoriteBtn?.setBackgroundImage(#imageLiteral(resourceName: "favorite-recipe-details"), for: .normal)
//        }
//
//        if ingredientsList.count > 0
//        {
//            tableHeightConstraint.constant = 400
//        }
//        else
//        {
//            tableHeightConstraint.constant = 10
//        }
//
//        recipeName = "Name 1"
//
//        infoTable.reloadData()
//        tagsCV.reloadData()
    }
    
    @objc func dismissKeyboard () {
        
        self.view.endEditing(true)
    }
    
    // MARK: Notifications
    
    
    
    @objc func updateIngredientsNotification (notification: NSNotification) {
        
        let ob = notification.object as! NSMutableDictionary
        
        ingredientsList = ob.object(forKey: "options") as! [NSMutableDictionary]
        
        let servings = ob.object(forKey: "servings") as! Float
        servingsLbl.text = "\(servings)"
        
        infoTable.reloadData()
    }
    
    
    @objc func modifyRecipeNotification (notification: NSNotification) {
        
        ingredientsList = notification.object as! [NSMutableDictionary]
        
        infoTable.reloadData()
        
        toastStatusLbl.text = "Recipe has been successfully saved."
        
        toastView.isHidden = false
        
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.fadeOut), userInfo: nil, repeats: false)
    }
    
    @objc func savedMealPlanNotification (notification: NSNotification) {
        
        //print("port: \(notification.object)")
        
        toastStatusLbl.text = "Recipe has been saved to your meal plan."
        toastView.isHidden = false
        
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.fadeOut), userInfo: nil, repeats: false)
    }
    
    @objc func sharedToFriendsGroupsNotification (notification: NSNotification) {
        
        //print("port: \(notification.object)")        
        
        if isSharingFriends == true
        {
            toastStatusLbl.text = "Recipe has been shared with your friends."
        }
        else
        {
            toastStatusLbl.text = "Recipe has been shared with your groups."
        }
        
        toastView.isHidden = false
        
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.fadeOut), userInfo: nil, repeats: false)
    }
    
    @objc func recipeBookmarkedNotification (notification: NSNotification) {
        
        //print("port: \(notification.object)")
        
        toastStatusLbl.text = "Recipe has been saved to your bookmarks."
        
        toastView.isHidden = false
        
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.fadeOut), userInfo: nil, repeats: false)
    }
    
    @objc func fadeOut() {
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
      
            self.toastView.alpha = 0
            
        }, completion: { (finished) -> Void in

            self.toastView.isHidden = true
            self.toastView.alpha = 1
        })
    }
    
    // MARK: Actions
    
    @IBAction func shareTo(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        shareDestination = btn.restorationIdentifier
        
        if shareDestination == "My Folder"
        {
            performSegue(withIdentifier: "SaveFolderMealPlan", sender: nil)
        }
        else
        {
            performSegue(withIdentifier: "AddMealPlan", sender: nil)
        }
        
        shareToView.isHidden = true
    }
    
    @IBAction func cancelShareTo(_ sender: Any) {
        
        shareToView.isHidden = true
    }
    
    @IBAction func segmentChange(_ sender: Any) {
        
        if optionsSegment.selectedSegmentIndex == 0
        {
            groceryListBtn.isHidden = false
            ratingBtn.isHidden = true
            viewReviewsBtn.isHidden = true
            
            reviewviewHeight.constant = 0
            reviewView.isHidden = true
            
            buttonTopSpace.constant = 37
            buttonViewHeight.constant = 130
            
            if cartItems.count > 0
            {
                cartItemsLbl.isHidden = false
            }
            
            if ingredientsList.count > 0
            {
                cartIV.isHidden = false
                
                if ingredientsList.count < 5
                {
                    tableHeightConstraint.constant = 200
                }
                else
                {
                    tableHeightConstraint.constant = 300
                }
                
                tableStatusLbl.isHidden = true
                groceryListBtn.isHidden = false
                
                relatedRecipesTopMargin.constant = 15
            }
            else
            {
                cartIV.isHidden = true
                tableHeightConstraint.constant = 10
                tableStatusLbl.isHidden = false
                groceryListBtn.isHidden = true
                //tableStatusLbl.text = "There are currently no ingredients for this recipe."
                tableStatusLbl.text = ""
                relatedRecipesTopMargin.constant = 0
            }
            
            contentScrollview.contentSize = CGSize.init(width: contentScrollview.frame.width, height: 1500)
        }
        else if optionsSegment.selectedSegmentIndex == 1
        {
            groceryListBtn.isHidden = true
            ratingBtn.isHidden = true
            viewReviewsBtn.isHidden = true
            
            reviewviewHeight.constant = 0
            reviewView.isHidden = true
            
            cartItemsLbl.isHidden = true
            cartIV.isHidden = true
            
            if directionsList.count > 0
            {
                buttonTopSpace.constant = 0
                buttonViewHeight.constant = 0
                
                tableHeightConstraint.constant = 300
                tableStatusLbl.isHidden = true
                relatedRecipesTopMargin.constant = 15
            }
            else
            {
                buttonTopSpace.constant = 37
                buttonViewHeight.constant = 130
                
                tableHeightConstraint.constant = 10
                tableStatusLbl.isHidden = false
                //tableStatusLbl.text = "There are currently no directions for this recipe."
                tableStatusLbl.text = ""
                relatedRecipesTopMargin.constant = 0
            }
            
            contentScrollview.contentSize = CGSize.init(width: contentScrollview.frame.width, height: 1500)
        }
        else if optionsSegment.selectedSegmentIndex == 2
        {
            groceryListBtn.isHidden = true
            ratingBtn.isHidden = true
            viewReviewsBtn.isHidden = true
            reviewviewHeight.constant = 0
            reviewView.isHidden = true
            
            cartItemsLbl.isHidden = true
            cartIV.isHidden = true
            
            let tableHeight = CGFloat(nutritionLabelList.count*54)

            if nutritionValues.count > 0
            {
                tableHeightConstraint.constant = tableHeight
                tableStatusLbl.isHidden = true
                relatedRecipesTopMargin.constant = 15
                
                buttonTopSpace.constant = 0
                buttonViewHeight.constant = 0
            }
            else
            {
                buttonTopSpace.constant = 37
                buttonViewHeight.constant = 130
                
                tableHeightConstraint.constant = 10
                tableStatusLbl.isHidden = false
                tableStatusLbl.text = "There is currently no nutrition information for this recipe."
                relatedRecipesTopMargin.constant = 100
            }
            
            contentScrollview.contentSize = CGSize.init(width: contentScrollview.frame.width, height: 1500+tableHeight)
        }
        else if optionsSegment.selectedSegmentIndex == 3
        {
            groceryListBtn.isHidden = true
            ratingBtn.isHidden = true
            viewReviewsBtn.isHidden = true
            reviewviewHeight.constant = 0
            reviewView.isHidden = true
            
            cartItemsLbl.isHidden = true
            cartIV.isHidden = true
            
            if tipsList.count > 0
            {
                buttonTopSpace.constant = 0
                buttonViewHeight.constant = 0
                
                tableHeightConstraint.constant = 300
                tableStatusLbl.isHidden = true
                relatedRecipesTopMargin.constant = 15
            }
            else
            {
                buttonTopSpace.constant = 37
                buttonViewHeight.constant = 130
                
                tableHeightConstraint.constant = 10
                tableStatusLbl.isHidden = false
                //tableStatusLbl.text = "There are currently no tips for this recipe."
                tableStatusLbl.text = ""
                relatedRecipesTopMargin.constant = 0
            }
            
            contentScrollview.contentSize = CGSize.init(width: contentScrollview.frame.width, height: 1500)
        }
        else if optionsSegment.selectedSegmentIndex == 4
        {
            groceryListBtn.isHidden = true
            ratingBtn.isHidden = false
            viewReviewsBtn.isHidden = false
            
            cartItemsLbl.isHidden = true
            cartIV.isHidden = true
            
            buttonViewHeight.constant = 130
            buttonTopSpace.constant = 37
            
            if reviewsList.count > 0
            {
                tableHeightConstraint.constant = 300
                
                tableStatusLbl.isHidden = true
                
                //viewReviewsBtn.constant = 0 //in case button height needs to be collapsed
                viewReviewsBtn.isHidden = false
                relatedRecipesTopMargin.constant = 15
            }
            else
            {
                tableHeightConstraint.constant = 10
                tableStatusLbl.isHidden = false
                //tableStatusLbl.text = "There are currently no reviews for this recipe."
                tableStatusLbl.text = ""
                
                //viewReviewsBtn.constant = 0
                viewReviewsBtn.isHidden = true
                relatedRecipesTopMargin.constant = 0
            }
            
            contentScrollview.contentSize = CGSize.init(width: contentScrollview.frame.width, height: 1500)
        }
        
        infoTable.reloadData()
    }
    
    @IBAction func shareAppFacebook(_ sender: Any) {
        
        let isInstalled = UIApplication.shared.canOpenURL(URL.init(string: "fb://")!)
        
        if (isInstalled) {
            
            let vc = SLComposeViewController(forServiceType:SLServiceTypeFacebook)
            //vc.add(imageView.image!)
            vc?.add(URL(string: "http://chewsrite.com"))
            //vc?.setInitialText("Use my signup code: \(self.appDelegate.referralCode!)...")
            
            UIPasteboard.general.string = "Download Chewsrite at http://chewsrite.com and use my signup code: \(self.appDelegate.referralCode!)..."
            
            self.present(vc!, animated: true, completion: nil)
        }
        else
        {
            print("no fb")
            
            let alert = UIAlertController(title: nil, message: "Facebook is not installed on this device.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func shareAppTwitter(_ sender: Any) {
        
        let isInstalled = UIApplication.shared.canOpenURL(URL.init(string: "twitter://")!)
        
        if (isInstalled) {
            
            let vc = SLComposeViewController(forServiceType:SLServiceTypeTwitter)
            //vc.add(imageView.image!)
            vc?.add(URL(string: "http://chewsrite.com"))
            vc?.setInitialText("Use my signup code: \(self.appDelegate.referralCode!)...")
            self.present(vc!, animated: true, completion: nil)
        }
        else
        {
            print("no twitter")
            
            let alert = UIAlertController(title: nil, message: "Twitter is not installed on this device.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func closeShareApp(_ sender: Any) {
        
        shareSocialView.isHidden = true
    }
    @IBAction func shareAction(_ sender: Any) {
        
        let btn = sender as? UIButton
        
        if btn?.restorationIdentifier == "twitter"
        {
            let isInstalled = UIApplication.shared.canOpenURL(URL.init(string: "twitter://")!)
            
            if (isInstalled) {
                
                let vc = SLComposeViewController(forServiceType:SLServiceTypeTwitter)
                //vc.add(imageView.image!)
                vc?.add(URL(string: "http://chewsrite.com"))
                vc?.setInitialText("Use my signup code: \(self.appDelegate.referralCode!)...")
                self.present(vc!, animated: true, completion: nil)
            }
            else
            {
                print("no twitter")
                
                let alert = UIAlertController(title: nil, message: "Twitter is not installed on this device.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else if btn?.restorationIdentifier == "facebook"
        {
            let isInstalled = UIApplication.shared.canOpenURL(URL.init(string: "fb://")!)
            
            if (isInstalled) {
                
                let vc = SLComposeViewController(forServiceType:SLServiceTypeFacebook)
                //vc.add(imageView.image!)
                vc?.add(URL(string: "http://chewsrite.com"))
                //vc?.setInitialText("Use my signup code: \(self.appDelegate.referralCode!)...")
                
                UIPasteboard.general.string = "Download Chewsrite at http://chewsrite.com and use my signup code: \(self.appDelegate.referralCode!)..."
                
                self.present(vc!, animated: true, completion: nil)
            }
            else
            {
                print("no fb")
                
                let alert = UIAlertController(title: nil, message: "Facebook is not installed on this device.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else if btn?.restorationIdentifier == "friends"
        {
            performSegue(withIdentifier: "ShareFriends", sender: self)
            isSharingFriends = true
            shareView.isHidden = true
        }
        else if btn?.restorationIdentifier == "groups"
        {
            performSegue(withIdentifier: "ShareGroups", sender: self)
            isSharingFriends = false
            shareView.isHidden = true
        }
        else if btn?.restorationIdentifier == "email"
        {
            if MFMailComposeViewController.canSendMail() {
                
                let composeVC = MFMailComposeViewController()
                composeVC.mailComposeDelegate = self
                
                // Configure the fields of the interface.
                //composeVC.setToRecipients(["ctr89josh@gmail.com"])
                composeVC.setSubject("Chewsrite Invitation")
                
                composeVC.setMessageBody("Download Chewsrite at http://chewsrite.com and use my signup code: \(self.appDelegate.referralCode!)", isHTML: false)
                
                // Present the view controller modally.
                self.present(composeVC, animated: true, completion: nil)
                
            } else {
                // show failure alert
                
                let alert = UIAlertController(title: nil, message: "An email client is not installed on this device.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else if btn?.restorationIdentifier == "sms"
        {
            if (MFMessageComposeViewController.canSendText()) {
                
                let controller = MFMessageComposeViewController()
                controller.body = "Download Chewsrite at http://chewsrite.com and use my signup code: \(self.appDelegate.referralCode!)"
                //controller.recipients = [phoneNumber.text]
                controller.messageComposeDelegate = self
                
                self.present(controller, animated: true, completion: nil)
                
            } else {
                // show failure alert
                
                let alert = UIAlertController(title: nil, message: "Texting is not allowed on this device.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
        
    @IBAction func cancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelShare(_ sender: Any) {
        
        shareView.isHidden = true
    }
    
    @IBAction func addFavorite(_ sender: Any) {
        
        let btn = sender as? UIButton
        
        manageFavorite(recipeID: recipeID!)
    }
    
    func manageFavorite( recipeID : String) {
        
        print("manageFavorite")
        
        let dataString = "favoriteData"
        
        let urlString = "\(appDelegate.serverDestination!)manageFavorites.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "recipeid=\(recipeID)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //                        self.activityView.stopAnimating()
                        //                        self.activityView.isHidden = true
                    })
                    
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "favorite item saved" || status == "favorite deleted"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {                            
                            NotificationCenter.default.post(name: Notification.Name("refreshHomeFeed"), object: nil)
                            
                            if status == "favorite item saved"
                            {
                                self.favoriteBtn.setBackgroundImage(#imageLiteral(resourceName: "addToFavoritesRecipe"), for: .normal)
                            }
                            else
                            {
                                self.favoriteBtn.setBackgroundImage(#imageLiteral(resourceName: "favorite-recipe-details"), for: .normal)
                            }
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func shareRecipe(_ sender: Any) {
        
        shareView.isHidden = false
    }
    
    @IBAction func bookmark(_ sender: Any) {
        
        shareToView.isHidden = false
    }
    
    @IBAction func viewPhotos(_ sender: Any) {
    }
    @IBAction func modifyRecipe(_ sender: Any) {
        
        performSegue(withIdentifier: "EditRecipe", sender: self)
    }
    
    @IBAction func showMoreLess(_ sender: Any) {
        
        if descriptionHeight.constant == 60
        {
            descriptionHeight.constant = 120
            
            moreBtn.setTitle("LESS", for: .normal)
        }
        else
        {
            descriptionHeight.constant = 60
            
            moreBtn.setTitle("MORE", for: .normal)
        }
    }
    
    @IBAction func addAllToGrocery(_ sender: Any) {
        
        //let btn = sender as? UIButton
        
        if groceryListBtn.titleLabel?.text == "Add all to Grocery List"
        {
            addAll()
            groceryListBtn?.setTitle("Remove all from Grocery List", for: .normal)
        }
        else
        {
            groceryListBtn?.setTitle("Add all to Grocery List", for: .normal)
            removeAll()
        }
    }
    
    func removeAll () {
        
        for dict in ingredientsList
        {
            dict.setValue(false, forKey: "active")
        }
        
        cartItems.removeAll()
        cartItemsLbl.isHidden = true
        
        infoTable.reloadData()
    }
    
    func addAll () {
        
        for dict in ingredientsList
        {
            dict.setValue(true, forKey: "active")
            //update image
            
            if !cartItems.contains(dict)
            {
                cartItems.append(dict)
            }
        }
        
        cartItemsLbl.isHidden = false
        cartItemsLbl.text = "\(cartItems.count)"
        
        infoTable.reloadData()
    }
    
    @IBAction func showRatingView(_ sender: Any) {
        
        if reviewviewHeight.constant == 0
        {
            reviewviewHeight.constant = 264
            reviewView.isHidden = false
            
            buttonViewHeight.constant = 90
        }
        else
        {
            reviewviewHeight.constant = 0
            reviewView.isHidden = true
        }
    }
    
    @IBAction func selectRating(_ sender: Any) {
        
        let btn = sender as? UIButton
        
        selectedRating = btn?.restorationIdentifier
        
        if selectedRating == "1"
        {
            star1.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star2.setBackgroundImage(UIImage.init(named: "green-star"), for: .normal)
            star3.setBackgroundImage(UIImage.init(named: "green-star"), for: .normal)
            star4.setBackgroundImage(UIImage.init(named: "green-star"), for: .normal)
            star5.setBackgroundImage(UIImage.init(named: "green-star"), for: .normal)
        }
        else if selectedRating == "2"
        {
            star1.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star2.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star3.setBackgroundImage(UIImage.init(named: "green-star"), for: .normal)
            star4.setBackgroundImage(UIImage.init(named: "green-star"), for: .normal)
            star5.setBackgroundImage(UIImage.init(named: "green-star"), for: .normal)
        }
        else if selectedRating == "3"
        {
            star1.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star2.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star3.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star4.setBackgroundImage(UIImage.init(named: "green-star"), for: .normal)
            star5.setBackgroundImage(UIImage.init(named: "green-star"), for: .normal)
        }
        else if selectedRating == "4"
        {
            star1.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star2.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star3.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star4.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star5.setBackgroundImage(UIImage.init(named: "green-star"), for: .normal)
        }
        else
        {
            star1.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star2.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star3.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star4.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
            star5.setBackgroundImage(UIImage.init(named: "green-selected"), for: .normal)
        }
    }
    
    @IBAction func saveRating(_ sender: Any) {
        
        if ratingTV.text == ""
        {
            showBasicAlert(string: "Please enter your comments.")
        }
        else
        {
            uploadRating()
        }
    }
    
    func uploadRating() {
        
        let urlString = "\(appDelegate.serverDestination!)addRating.php"
        
        print("urlString: \(urlString)")
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&review=\(ratingTV.text!)&rating=\(self.selectedRating!)&recipeid=\(self.recipeID!)&devStatus=\(appDelegate.devStage!)"
        
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("confirmation check jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict["reviewData"]! is NSNull
                {
                    print("no review")
                }
                else
                {
                    let userDict : NSDictionary = dataDict["reviewData"] as! NSDictionary
                    
                    let status : String = userDict["status"] as! String
                    
                    print("status: \(status)")
                    
                    if (status == "rating saved")
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.getRecipeIngredients()
                            
                            let dict : NSMutableDictionary  = ["username" : self.appDelegate.username, "reviewdate" : self.appDelegate.convertDateToSQLDate(date: Date()), "review" : self.ratingTV.text, "rating" : self.selectedRating!]
                            
                            self.reviewsList.append(dict)
                            
                            self.ratingTV.text = ""
                            
                            //self.buttonViewHeight.constant = 130

                            self.reviewView.isHidden = true
                            
                            self.tableStatusLbl.isHidden = true
                            
                            //self.tableHeightConstraint.constant = 400
                            self.tableStatusLbl.isHidden = true
                            
                            //viewReviewsBtn.constant = 0 //in case button height needs to be collapsed
                            
                            self.viewReviewsBtn.isHidden = false
                            
                            self.ratingView.isHidden = true //verify bitbucket change
                            
                            self.infoTable.reloadData()
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.showBasicAlert(string: "Review not saved. Please check your internet connection.")
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func cancelRating(_ sender: Any) {
        
        buttonViewHeight.constant = 130
        reviewviewHeight.constant = 0
        reviewView.isHidden = true
    }
    
    @IBAction func viewReviews(_ sender: Any) {
        
        performSegue(withIdentifier: "AllReviews", sender: self)
    }
    
    @IBAction func playVideo(_ sender: Any) {
        
        //https://www.raywenderlich.com/5191-video-streaming-tutorial-for-ios-getting-started
        
        print("play video2")
        
        let btn = sender as! UIButton
        
        //let video = videos[0]
        
        let url = "https://s3.us-east-2.amazonaws.com/shotgraph1224/chewsrite/\(selectedItem!.videoname)"
        
        print("url: \(url)")
        
        let selectedVideo = Video(url: URL.init(string: url)!, thumbURL: URL.init(string: url)!, title: "", subtitle: "")
        
        let videoURL = selectedVideo.url
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        present(playerViewController, animated: true) {
            player.play()
        }
    }
    
    // MARK: Tableview Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if optionsSegment.selectedSegmentIndex == 0
        {
            return self.ingredientsList.count
        }
        else if optionsSegment.selectedSegmentIndex == 1
        {
            return self.directionsList.count
        }
        else if optionsSegment.selectedSegmentIndex == 2
        {
            return self.nutritionValues.count
        }
        else if optionsSegment.selectedSegmentIndex == 3
        {
            return self.tipsList.count
        }
        else if optionsSegment.selectedSegmentIndex == 4
        {
            return self.reviewsList.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if optionsSegment.selectedSegmentIndex == 0 || optionsSegment.selectedSegmentIndex == 2
        {
            return 54
        }
        else
        {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView,heightForRowAt indexPath:IndexPath) -> CGFloat
    {
        
        if optionsSegment.selectedSegmentIndex == 0 || optionsSegment.selectedSegmentIndex == 2
        {
            return 54
        }
        else
        {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        if optionsSegment.selectedSegmentIndex == 0
        {
            let cell : AddIngredientTableViewCell = tableView.dequeueReusableCell(withIdentifier: "add-ingredient-cell")! as! AddIngredientTableViewCell
            
            let dict = self.ingredientsList[(indexPath as NSIndexPath).row] as NSDictionary
            
            let quantity = dict.object(forKey: "quantity") as! String
            let unit = dict.object(forKey: "unit") as! String
            let ingredient = dict.object(forKey: "ingredientname") as! String
            
            cell.selectionStyle = .none
            
            let active = dict.object(forKey: "active") as! Bool
            
            cell.addIngredientLbl.text = "\(quantity) \(unit) \(ingredient)"
            
            if active == false
            {
                cell.startEditBtn.setBackgroundImage(#imageLiteral(resourceName: "plus-normal"), for: .normal)
            }
            else
            {                
                cell.startEditBtn.setBackgroundImage(#imageLiteral(resourceName: "checkGreen"), for: .normal)
            }
                        
            cell.startEditBtn.restorationIdentifier = String(indexPath.row)
            cell.startEditBtn.addTarget(self, action: #selector(self.manageIngredient(_:)), for: UIControlEvents.touchUpInside)
            
            //print("val: \(val)")
            
            return cell
        }
        else if optionsSegment.selectedSegmentIndex == 1
        {
            let dict = self.directionsList[(indexPath as NSIndexPath).row] as NSDictionary
            
            //let val = dict.object(forKey: "name") as! String
            
            let cell : DirectionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "directions-cell")! as! DirectionsTableViewCell
            
            cell.selectionStyle = .none
            
            let title = dict.object(forKey: "title") as! String
            let directions = dict.object(forKey: "directions") as! String
            
            cell.titleLbl.isHidden = false
            cell.directionsLbl.isHidden = false
            
            cell.titleLbl.text = title
            cell.directionsLbl.text = directions
            
//            cell.titleLbl.font = UIFont(name: "Avenir-Medium", size: 14.0)
//            cell.directionsLbl.font = UIFont(name: "Avenir-Book", size: 14.0)
            
            return cell
            
            //print("val: \(val)")
        }
        else if optionsSegment.selectedSegmentIndex == 2
        {
            let dict = self.nutritionLabelList[(indexPath as NSIndexPath).row]
            
            //let val = dict.object(forKey: "name") as! String
            
            let cell : NutritionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "nutrition-cell")! as! NutritionTableViewCell
            
            cell.selectionStyle = .none
            
//            let title = dict.object(forKey: "title") as! String
//            let value = dict.object(forKey: "value") as! String
            
            let title = nutritionLabelList[indexPath.row]
            let value = nutritionValues[indexPath.row]
            
            cell.titleLbl.text = title
            cell.valueLbl.text = value
            
            return cell
            
            //print("val: \(val)")
        }
        else if optionsSegment.selectedSegmentIndex == 3
        {
            let cell : AddTipsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "tips-cell")! as! AddTipsTableViewCell
            
            let dict = self.tipsList[(indexPath as NSIndexPath).row] as NSDictionary
            
            //let val = dict.object(forKey: "name") as! String
            
            cell.selectionStyle = .none
            
            let title = dict.object(forKey: "title") as! String
            let tip = dict.object(forKey: "tip") as! String
            
            cell.titleLbl.text = title
            cell.tipLbl.text = tip
            
            //cell.titleLbl.font = UIFont(name: "Avenir-Medium", size: 14.0)
            
            return cell
            
            //print("val: \(val)")
        }
        else if optionsSegment.selectedSegmentIndex == 4
        {
            let cell : ReviewsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "review-cell")! as! ReviewsTableViewCell
            
            let dict = self.reviewsList[(indexPath as NSIndexPath).row] as NSDictionary
            
            let reviewdate = dict.object(forKey: "reviewdate") as! String
            let username = dict.object(forKey: "username") as! String
            let review = dict.object(forKey: "review") as! String
            let rating = dict.object(forKey: "rating") as! String
                        
            if rating == "1"
            {
                cell.ratingIV.image = #imageLiteral(resourceName: "b1star")
            }
            else if rating == "2"
            {
                cell.ratingIV.image = #imageLiteral(resourceName: "b2star")
            }
            else if rating == "3"
            {
                cell.ratingIV.image = #imageLiteral(resourceName: "b3star")
            }
            else if rating == "4"
            {
                cell.ratingIV.image = #imageLiteral(resourceName: "b4star")
            }
            else if rating == "5"
            {
                cell.ratingIV.image = #imageLiteral(resourceName: "b5star")
            }
            
            cell.selectionStyle = .none
            
            cell.dateLbl.text = reviewdate
            cell.usernameLbl.text = username
            cell.reviewLbl.text = review
            
            //cell.titleLbl.font = UIFont(name: "Avenir-Medium", size: 14.0)
            
            return cell
            
            //print("val: \(val)")
        }
        
        return cell
    }
    
    @objc func manageIngredient(_ sender : UIButton) {
        
        let btn = sender as UIButton
        let ind = Int(btn.restorationIdentifier!)
        
        if optionsSegment.selectedSegmentIndex == 0
        {
            selectedIngredient = self.ingredientsList[ind!] as NSMutableDictionary
            
            let active = selectedIngredient!.object(forKey: "active") as! Bool
            
            if active == true
            {
                selectedIngredient!.setValue(false, forKey: "active")
                //update image
                
                if cartItems.contains(selectedIngredient!)
                {
                    cartItems.remove(at: cartItems.index(of: selectedIngredient!)!)
                }
                
                if cartItems.count == 0
                {
                    cartItemsLbl.isHidden = true
                }
            }
            else
            {
                selectedIngredient!.setValue(true, forKey: "active")
                cartItems.append(selectedIngredient!)
                
                cartItemsLbl.isHidden = false
            }
            
            cartItemsLbl.text = "\(cartItems.count)"
            
            print("cartItems: \(cartItems)")
            
            updateGroceryItem()
        }
        
        if cartItems.count == ingredientsList.count
        {
            groceryListBtn?.setTitle("Remove all from Grocery List", for: .normal)
        }
        else if cartItems.count == 0 || cartItems.count < ingredientsList.count
        {
            groceryListBtn?.setTitle("Add all to Grocery List", for: .normal)
        }
        
        infoTable.reloadData()
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let kWhateverHeightYouWant = 190
        
        return CGSize.init(width: 600, height: CGFloat(kWhateverHeightYouWant))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.restorationIdentifier == "recipes"
        {
            return recipeList.count
        }
        else if tagsList.count < 8
        {
            return self.tagsList.count
        }
        else
        {
            return 7
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.restorationIdentifier == "recipes"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? RecipesCollectionViewCell
            
            let dict = recipeList[indexPath.row]
            
            cell?.frame.size = CGSize.init(width: relatedRecipesCV.frame.width * 0.5, height: 292) //Randall. check this
            
            //cell?.recipeIV.image = UIImage.init(named: (dict.object(forKey: "imagename") as? String)!)
            
            //recipe photo
            
            let recipeimagename = dict.object(forKey: "imagename") as! String
            
            if recipeimagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(recipeimagename)"
                
                if let img = statusImageCache[urlString]
                {
                    cell?.recipeIV.image = img
                }
                else
                {
                    cell?.recipeIV.image = UIImage(named: "profile-icon")
                    
                    self.downloadImage(imagename: recipeimagename, urlString: urlString, iv: (cell?.recipeIV!)!)
                }
            }
            
            cell?.headerLbl.text = dict.object(forKey: "recipename") as? String
            cell?.infoLbl.text = "\(dict.object(forKey: "preptime") as! String) • \(dict.object(forKey: "calories") as! String)"
            
            let attributedString = NSMutableAttributedString(string: "Source: \((dict.object(forKey: "source") as? String)!)", attributes: [
                .font: UIFont(name: "Avenir-Book", size: 13.0)!,
                .foregroundColor: appDelegate.crGray
                ])
            attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 13.0)!, range: NSRange(location: 0, length: 7))
            
            cell?.sourceLbl.attributedText = attributedString
            
            return cell!
        }
        else
        {
            //chanage cell width to 40% of screen
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? TagsCollectionViewCell
            
            let tag = tagsList[indexPath.row]
            
            //cell?.frame.size = CGSize.init(width: tagsCV.frame.width * 0.5, height: 292) //Randall. check this
            
            //cell?.recipeIV.image = UIImage.init(named: (dict.object(forKey: "imagename") as? String)!)
            
            if indexPath.row < 8
            {
                cell?.tagLbl.text = tag
            }
            else
            {
                cell?.tagLbl.text = "..."
            }
            
            cell?.tagLbl.layer.borderWidth = 0.5
            cell?.tagLbl.layer.borderColor = appDelegate.gray51.cgColor
            cell?.tagLbl.layer.cornerRadius = 10.0
            
            return cell!
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        let recipe = tagsList[indexPath.row]
//
//        print("recipe: \(recipe)")
//
//        tagsCV.reloadData()
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Text Delegate

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        controller.dismiss(animated: true)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        controller.dismiss(animated: true)
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    func downloadImage (imagename : String, urlString : String, iv: UIImageView ) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache[urlString] = image
                        iv.image = image
                        
                        if urlString.contains(self.appDelegate.profileImg!)
                        {
                            self.appDelegate.userImage = image
                        }
                    }
                    
                    //println("test")
                })
                return nil
            }))
        }
        else
        {
            iv.image = UIImage.init(named: "profile-icon")
        }
    }
    
    // MARK: - Navigation

     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
        if segue.identifier == "ShareFriends"
        {
            let destination = segue.destination as! ShareRecipe
            destination.isSharingFriends = true
            destination.recipeImage = recipeIV.image
            destination.recipeID = recipeID
        }
        if segue.identifier == "ShareGroups"
        {
            let destination = segue.destination as! ShareRecipe
            destination.isSharingPrivateGroups = true
            destination.recipeImage = recipeIV.image
            destination.recipeID = recipeID
        }
        else if segue.identifier == "EditRecipe"
        {
            let destination = segue.destination as! EditRecipe
            destination.recipeImage = recipeIV.image
            destination.recipeName = recipeName
            destination.ingredientOptions = ingredientsList
            destination.servingSize = (selectedItem?.servings as! NSString).floatValue
            destination.recipeID = recipeID
        }
        else if segue.identifier == "AllReviews"
        {
            let destination = segue.destination as! AllReviews
            destination.recipeID = recipeID
            destination.recipeName = recipename
        }
        else if segue.identifier == "SaveFolderMealPlan"
        {
            let destination = segue.destination as! ShareRecipe
            destination.recipeImage = recipeIV.image
            destination.isSharingFolderMealPlan = true
            destination.shareDestination = shareDestination
            destination.recipeID = recipeID
        }
        else if segue.identifier == "AddMealPlan"
        {
            if recipeDict == nil
            {
                recipeDict = ["active":"false"]
                recipeDict?.setValue(recipename, forKey: "recipename")
                recipeDict?.setValue(recipeID, forKey: "recipeid")
            }
            
            let destination = segue.destination as! AddRecipeMealPlan
            destination.recipeImage = recipeIV.image
            destination.recipeDict = recipeDict
            //destination.isSharingFolderMealPlan = true
            //destination.shareDestination = shareDestination
        }
     }
}

@IBDesignable
class DesignableView: UIView {
}

@IBDesignable
class DesignableButton: UIButton {
}

@IBDesignable
class DesignableLabel: UILabel {
}

extension String {
    
    func utf8DecodedString()-> String {
        
        let data = self.data(using: .utf8)
        
        if let message = String(data: data!, encoding: .nonLossyASCII){
            return message
        }
        
        return ""
    }
    
    func utf8EncodedString()-> String {
        
        let messageData = self.data(using: .nonLossyASCII)
        let text = String(data: messageData!, encoding: .utf8)
        return text!
    }
}

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}

extension Bool {
    var intValue: Int {
        return self ? 1 : 0
    }
}
