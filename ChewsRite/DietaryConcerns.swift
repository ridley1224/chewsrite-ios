//
//  DietaryConcerns.swift
//  ChewsRite
//
//  Created by Randall Ridley on 5/19/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class DietaryConcerns: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var cuisineCV: UICollectionView!
    
    var concerns = [NSDictionary]()
    var selectedConcerns = [NSDictionary]()
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var submitBtn: UIButton!
    var newFamilyMemberID : Int?
    
    var firstname : String?
    
    @IBOutlet weak var otherTxt: UITextField!
    
    @IBOutlet weak var progressView: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let pg = UIView.init(frame: CGRect.init(x: 0, y: 0, width: progressView.frame.width * 0.50, height: progressView.frame.height))
        
        pg.backgroundColor = appDelegate.crLightBlue
        progressView.addSubview(pg)
        
        cuisineCV.delegate = self
        cuisineCV.dataSource = self
        
        otherTxt.isHidden = true
        activityView.isHidden = true
        
        //appDelegate.userid = "1"
        
        //populate()
        
        getAllConcerns()
        
        
        //query user added concerns
        
        //print("frame size: \(self.view.frame.width)")
        
        var fontSize : CGFloat = 16.0
        
        if self.view.frame.width <= 320
        {
            fontSize = 14.0
        }
        
        var header = "Step 2: Let us know your dietary concerns"
        
        if firstname != nil
        {
            header = "Step 2: \(firstname!)'s dietary concerns"
        }
        
        if isEditing == true
        {
            header = "Dietary Concerns"
        }
        
        let attributedString = NSMutableAttributedString(string: header, attributes: [
            .font: UIFont(name: "Avenir-Medium", size: fontSize)!,
            .foregroundColor: UIColor.white
            ])
        attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Book", size: fontSize)!, range: NSRange(location: 0, length: 7))
        
        headerLbl.attributedText = attributedString
        
        
        let btnItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(dismissKeyboard))
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width:  self.view.frame.size.width, height: 50))
        numberToolbar.backgroundColor = UIColor.darkGray
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.tintColor = UIColor.black
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            btnItem]
        
        numberToolbar.sizeToFit()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refeshConcerns),
                                               name: NSNotification.Name(rawValue: "refeshConcerns"),
                                               object: nil)
    }
    
    func getAllConcerns() {
        
        //displayItems.removeAll()
        
        print("getAllConcerns")
        
        let dataString = "concernsData"
        
        let urlString = "\(appDelegate.serverDestination!)getAllConcerns.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        //appDelegate.userid = "22"
        
        let paramString = "devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("concerns jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.recipeStatusTV.isHidden = false
                        
                        self.cuisineCV.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict2 in recipes1
                        {
                            let name = dict2.object(forKey: "concernname" as String)!
                            let image = dict2.object(forKey: "imageblue" as String)!
                            let image2 = dict2.object(forKey: "imagewhite" as String)!
                            let concernid = dict2.object(forKey: "concernid" as String)!
                            
                            self.concerns.append( ["name" : name, "image" : UIImage.init(named: image as! String)!, "image2" : UIImage.init(named: image2 as! String)!,"concernid": concernid])
                        }
                        
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if hasData == true
                            {
                                self.cuisineCV.reloadData()
                                
                                if self.newFamilyMemberID == nil && self.appDelegate.signingUp != true
                                {
                                    self.getUserConcerns()
                                }
                            }
                            else
                            {
                                print("no data")
                                
                                //self.noResultsMain.isHidden = false
                                
                                self.cuisineCV.reloadData()
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.cuisineCV.reloadData()
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getUserConcerns() {
        
        //displayItems.removeAll()
        
        print("getUserConcerns")
        
        let dataString = "concernsData"
        
        let urlString = "\(appDelegate.serverDestination!)getDietaryConcerns.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        //appDelegate.userid = "22"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("concerns jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.recipeStatusTV.isHidden = false
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [String]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        var ind = 0
                        
                        for dict2 in self.concerns
                        {
                            let dict3 = self.concerns[ind] as NSDictionary
                            
                            let concernid = dict3.object(forKey: "concernid") as! String
                            
                            if recipes1.contains(concernid)
                            {
                                self.selectedConcerns.append(dict3)
                            }
                            
                            ind += 1
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if hasData == true
                            {
                                self.cuisineCV.reloadData()
                                //self.recipeStatusTV.text = "You currently have no recipes."
                                
                                //self.updateDisplayList()
                                
                                //self.cuisineCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
                                //self.noResultsMain.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func populate () {
        
        concerns.append( ["name" : "Diabetic", "image" : UIImage.init(named: "noSugarBlue")!, "image2" : UIImage.init(named: "noSugarWhite")!,"concernid": "1"])
        concerns.append( ["name" : "Dairy Allergy", "image" : UIImage.init(named: "dairyAllergyBlue")!, "image2" : UIImage.init(named: "dairyAllergyWhite")!,"concernid": "2"])
        concerns.append( ["name" : "Egg Allergy", "image" : UIImage.init(named: "eggAllergyBlue")!, "image2" : UIImage.init(named: "eggAllergyWhite")!,"concernid": "3"])
        concerns.append( ["name" : "Fish Allergy", "image" : UIImage.init(named: "fishAllergyBlue")!, "image2" : UIImage.init(named: "fishAllergyWhite")!,"concernid": "4"])
        concerns.append( ["name" : "Flexitarian", "image" : UIImage.init(named: "flexitarianBlue")!, "image2" : UIImage.init(named: "flexitarianWhite")!,"concernid": "5"])
        concerns.append( ["name" : "Gluten Intolerance", "image" : UIImage.init(named: "glutenIntoleranceBlue")!, "image2" : UIImage.init(named: "glutenIntoleranceWhite")!,"concernid": "6"])
        concerns.append( ["name" : "Halal", "image" : UIImage.init(named: "halalBlue")!, "image2" : UIImage.init(named: "halalWhite")!,"concernid": "7"])
        concerns.append( ["name" : "Heart Healthy", "image" : UIImage.init(named: "heartHealthyBlue")!, "image2" : UIImage.init(named: "heartHealthyWhite")!,"concernid": "8"])
        concerns.append( ["name" : "Keto", "image" : UIImage.init(named: "ketoBlue")!, "image2" : UIImage.init(named: "ketoWhite")!,"concernid": "9"])
        concerns.append( ["name" : "Kosher", "image" : UIImage.init(named: "kosherBlue")!, "image2" : UIImage.init(named: "kosherWhite")!,"concernid": "10"])
        concerns.append( ["name" : "Lacto Vegitarian", "image" : UIImage.init(named: "lactoVegetarianBlue")!, "image2" : UIImage.init(named: "lactoVegetarianWhite")!,"concernid": "11"])
        concerns.append( ["name" : "Lactose Intolerant", "image" : UIImage.init(named: "lactoseIntoleranceBlue")!, "image2" : UIImage.init(named: "lactoseIntoleranceWhite")!,"concernid": "12"])
        concerns.append( ["name" : "Low Carb", "image" : UIImage.init(named: "lowCarbBlue")!, "image2" : UIImage.init(named: "lowCarbWhite")!,"concernid": "13"])
        concerns.append( ["name" : "Low Fat", "image" : UIImage.init(named: "lowFatBlue")!, "image2" : UIImage.init(named: "lowFatWhite")!,"concernid": "14"])
        concerns.append( ["name" : "Low Sodium", "image" : UIImage.init(named: "lowSodiumBlue")!, "image2" : UIImage.init(named: "lowSodiumWhite")!,"concernid": "15"])
        concerns.append( ["name" : "Low Sugar", "image" : UIImage.init(named: "lowSugarBlue")!, "image2" : UIImage.init(named: "lowSugarWhite")!,"concernid": "16"])
        concerns.append( ["name" : "Ovo Vegetarian", "image" : UIImage.init(named: "ovoVegetarianBlue")!, "image2" : UIImage.init(named: "ovoVegetarianWhite")!,"concernid": "17"])
        concerns.append( ["name" : "Paleo", "image" : UIImage.init(named: "paleoBlue")!, "image2" : UIImage.init(named: "paleoWhite")!,"concernid": ""])
        concerns.append( ["name" : "Peanut Allergy", "image" : UIImage.init(named: "peanutAllergyBlue")!, "image2" : UIImage.init(named: "peanutAllergyWhite")!,"concernid": "18"])
        concerns.append( ["name" : "Pescetarian", "image" : UIImage.init(named: "pescetarianBlue")!, "image2" : UIImage.init(named: "pescetarianWhite")!,"concernid": "19"])
        concerns.append( ["name" : "Shellfish Allergy", "image" : UIImage.init(named: "shellfishAllergyBlue")!, "image2" : UIImage.init(named: "shellfishAllergyWhite")!,"concernid": "20"])
        concerns.append( ["name" : "Soy Allergy", "image" : UIImage.init(named: "soyAllergyBlue")!, "image2" : UIImage.init(named: "soyAllergyWhite")!,"concernid": "21"])
        concerns.append( ["name" : "Vegan", "image" : UIImage.init(named: "veganBlue")!, "image2" : UIImage.init(named: "veganWhite")!,"concernid": ""])
        concerns.append( ["name" : "Vegetarian", "image" : UIImage.init(named: "vegetarianBlue")!, "image2" : UIImage.init(named: "vegetarianWhite")!,"concernid": "22"])
        concerns.append( ["name" : "Wheat Allergy", "image" : UIImage.init(named: "wheatAllergyBlue")!, "image2" : UIImage.init(named: "wheatAllergyWhite")!,"concernid": "23"])
        concerns.append( ["name" : "Other", "image" : UIImage.init(named: "add")!, "image2" : UIImage.init(named: "add")!,"concernid": "24"])
    }
    
    @objc func refeshConcerns (notification: NSNotification) {
        
        let newConcerns = notification.object as! [NSDictionary]
        
        //add new cats
        
        var ind = concerns.count-1
        
        concerns.remove(at: ind)
        
        for dict in newConcerns
        {
            let dict1 = ["name" : dict.object(forKey: "name") as! String, "image" : UIImage.init(named: "customWhite")!, "image2" : UIImage.init(named: "customWhite")!,"concernid": dict.object(forKey: "concernid") as! String] as [String : Any]
            
            concerns.append(dict1 as NSDictionary)
            selectedConcerns.append(dict1 as NSDictionary)
            
            ind += 1
        }
        
        concerns.append( ["name" : "Other", "image" : UIImage.init(named: "add")!, "image2" : UIImage.init(named: "add")!,"concernid": ind])
        
        cuisineCV.reloadData()
    }
    
    @objc func dismissKeyboard () {
        
        self.view.endEditing(true)
    }
    
    @IBAction func next(_ sender: Any) {
        
        uploadConcerns()
    }
    
    func uploadConcerns() {
        
        submitBtn.isEnabled = false
        
        activityView.isHidden = false
        activityView.startAnimating()
        
        print("uploadUserData")
        
        var concernString = ""
        
        if selectedConcerns.count > 0
        {
            var concernids = ""
            var i = 0
            
            for a in selectedConcerns
            {
                if i == 0
                {
                    concernids = (a.object(forKey: "concernid") as? String)!
                }
                else
                {
                    concernids = "\(concernids),\(a.object(forKey: "concernid") as! String)"
                }
                
                i += 1
            }
            
            concernString = "\(concernids)"
        }
        
        if concernString == ""
        {
            concernString = "8,23"
        }
        
        let urlString = "\(appDelegate.serverDestination!)manageDietaryConcerns.php"
        
        print("urlString: \(urlString)")
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var paramString = ""
        
        if newFamilyMemberID != nil
        {
            paramString = "selections=\(concernString)&familyid=\(newFamilyMemberID!)&devStatus=\(appDelegate.devStage!)&usertype=1"
        }
        else
        {
            paramString = "selections=\(concernString)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)&usertype=0"
        }
        
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("concerns jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict["favoriteData"]! is NSNull
                {
                    print("no user")
                    
                    self.activityView.isHidden = true
                    self.activityView.stopAnimating()
                    
                    self.submitBtn.isEnabled = true
                    self.submitBtn.alpha = 1
                    
                    self.showBasicAlert(string: "no data")
                }
                else
                {
                    let userDict : NSDictionary = dataDict["favoriteData"] as! NSDictionary
                    
                    let status : String = userDict["status"] as! String
                    
                    print("status: \(status)")
                    
                    if status == "user dietary concerns saved" || status == "user dietary concerns saved" || status == "user dietary concerns updated"
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.alpha = 1
                            
                            if (self.appDelegate.signingUp == true || self.newFamilyMemberID != nil)
                            {
                                self.performSegue(withIdentifier: "LikesDislikes", sender: self)
                            }
                            else
                            {
                                self.showBasicAlert(string: "Dietary Concerns Updated")
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.alpha = 1
                            
                            //self.showBasicAlert(string: status)
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let kWhateverHeightYouWant = 190
        
        return CGSize.init(width: 600, height: CGFloat(kWhateverHeightYouWant))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.concerns.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let dict = self.concerns[indexPath.row]
        
        let string = dict.object(forKey: "name") as! String
        let img = dict.object(forKey: "image") as! UIImage
        let img2 = dict.object(forKey: "image2") as! UIImage
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? CuisinesCollectionviewCell
        
        cell?.cuisineLbl.text = string
        cell?.iconIV.image = img
        
        if selectedConcerns.contains(dict)
        {
            cell?.backgroundColor = appDelegate.crLightBlue
            cell?.cuisineLbl.textColor = UIColor.white
            cell?.iconIV.image = img2
        }
        else
        {
            cell?.backgroundColor = UIColor.white
            cell?.cuisineLbl.textColor = appDelegate.crLightBlue
        }
        
        cell?.layer.borderWidth = 1.0
        cell?.layer.borderColor = appDelegate.crLightBlue.cgColor
        cell?.layer.cornerRadius = 4.0
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == concerns.count-1
        {
            performSegue(withIdentifier: "AddConcern", sender: nil)
        }
        else
        {
            let dict = self.concerns[indexPath.row]
            
            //let id = dict.object(forKey: "cuisineid") as! String
            //        let name = dict.object(forKey: "cuisinename") as! String
            //
            //        print("cuisinename: \(name)")
            
            if selectedConcerns.contains(dict)
            {
                let ind = selectedConcerns.firstIndex(of: dict)
                selectedConcerns.remove(at: ind!)
            }
            else
            {
                selectedConcerns.append(dict)
            }
            
            print("selectedConcerns: \(selectedConcerns)")
            
            cuisineCV.reloadData()
        }
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "LikesDislikes"
        {
            let destination = segue.destination as! LikesDislikes
            destination.newFamilyMemberID = newFamilyMemberID
            destination.firstname = firstname
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
