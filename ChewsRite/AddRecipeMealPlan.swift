//
//  AddRecipeMealPlan.swift
//  ChewsRite
//
//  Created by Randall Ridley on 7/15/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class AddRecipeMealPlan: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var recipeCV: UICollectionView!
    
    @IBOutlet weak var folderTable: UITableView!
    @IBOutlet weak var folderNameTxt: UITextField!
    @IBOutlet weak var recipeIV: UIImageView!
    
    var breakfastList = [NSMutableDictionary]()
    var lunchList = [NSMutableDictionary]()
    var snackList = [NSMutableDictionary]()
    var dinnerList = [NSMutableDictionary]()
    var selectedItems = [NSMutableDictionary]()
    var selectedList = [NSMutableDictionary]()
    var displayItems = [NSMutableDictionary]()
    var mealBtns = [UIButton]()
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var recipeDict: NSMutableDictionary!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var recipeLbl: UILabel!
    var recipeImage : UIImage?
    
    var mealIndex : Int = 0
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var mealPlanStatusLbl: UILabel!
    var currentDate : Date?
    var todayDate : Date?
    var todayStr : String?
    var todayDateStr : String?
    var currentDateSQLStr : String?
    var originalRecipeID : String?
    
    @IBOutlet weak var recipesStatusLbl: UILabel!
    
    @IBOutlet weak var buttonScollview: UIScrollView!
    
    @IBOutlet weak var familyView: UIView!
    var addedToFamily = false
    var showedFamilyBox = false
    
    var currentDateOffset = 0
    
    @IBOutlet weak var breakfastBtn: DesignableButton!
    @IBOutlet weak var lunchBtn: DesignableButton!
    @IBOutlet weak var snackBtn: DesignableButton!
    
    @IBOutlet weak var dinnerBtn: DesignableButton!
    
    var itemReplaced : Bool?
    
    @IBOutlet weak var mealPlanStatusLblHeight: NSLayoutConstraint!
    
    var statusImageCache = [String:UIImage]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        mealBtns = [breakfastBtn,lunchBtn,snackBtn,dinnerBtn]
        
        recipeCV.delegate = self
        recipeCV.dataSource = self
        
        recipeIV.image = recipeImage
        recipeLbl.text = recipeDict.object(forKey: "recipename") as? String
        
        activityView.isHidden = true
        familyView.isHidden = true
        
        todayDate = Date()
        currentDate = todayDate
        
        mealIndex = 0
        
        //print("currentDate: \(currentDate)")
        
        let dateStr = appDelegate.formatMessageDate1(date: Date(), format: "dd MMMM")
        todayDateStr = appDelegate.formatMessageDate1(date: currentDate!, format: "yyyy, dd MMMM")
        
        todayStr = "Today, \(dateStr)"
        
        dateLbl.text = todayStr
        
        currentDateSQLStr = appDelegate.formatMessageDate1(date: currentDate!, format: "yyyy-MM-dd")
        
        //recipesStatusLbl.text = ""
        //mealPlanStatusLbl.text = ""
        
        recipesStatusLbl.isHidden = true
        mealPlanStatusLbl.isHidden = true
        
        getMealPlanRecipes()
        
        //getFolders()
        //debug()
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        keyboardDoneButtonView.items = [doneButton]
        
        folderNameTxt?.inputAccessoryView = keyboardDoneButtonView
        
        buttonScollview.contentSize = CGSize.init(width: 138*4, height: buttonScollview.frame.height)
    }
    
    func debug () {
        
        recipesStatusLbl.isHidden = true
        
        let dateStr = appDelegate.formatMessageDate1(date: Date(), format: "dd MMMM")
        todayDateStr = appDelegate.formatMessageDate1(date: currentDate!, format: "yyyy, dd MMMM")
        
        todayStr = "Today, \(dateStr)"
        
        dateLbl.text = todayStr
        
        mealPlanStatusLbl.text = "You have 2 recipes for breakfast in your meal plan"
        
        var dict : NSMutableDictionary = ["recipename" : "Mushroom and Asparagus Risotto", "category" : "","source" : "Hello Fresh","preptime" : "40 mins","calories" : "400","imagename" : "pistachio", "mealplandate" :  "2018-08-04", "mealIndex": 0 , "replaced": false, "recipeid": "2"]
        
        breakfastList.append(dict)
        
        dict = ["recipename" : "Creamy Shrimp Risotto", "category" : "1","source" : "Blue Apron","preptime" : "60 mins","calories" : "300","imagename" : "risotto", "mealplandate" :  "2018-08-05",  "mealIndex": 0, "replaced": false, "recipeid": "3"]
        
        breakfastList.append(dict)
        
        selectedList = breakfastList
        
        updateDisplayList()
    }
    
    func updateDisplayList () {
        
        currentDateSQLStr = appDelegate.formatMessageDate1(date: currentDate!, format: "yyyy-MM-dd")
        
        displayItems.removeAll()
        
        print("currentDateSQLStr: \(currentDateSQLStr!)")
        
        for dict in selectedList {
            
            let mealplandate = dict.object(forKey: "mealplandate") as! String
            
            print("mealplandate: \(mealplandate)")
            
            if mealplandate == currentDateSQLStr
            {
                print("show recipe")
                
                displayItems.append(dict)
            }
        }
        
        //var status = ""
        var plan = ""
        
        if mealIndex == 0
        {
            plan = "breakfast"
        }
        else if mealIndex == 1
        {
            plan = "lunch"
        }
        else if mealIndex == 2
        {
            plan = "dinner"
        }
        
        if displayItems.count > 0
        {
            mealPlanStatusLbl.text = "You have \(displayItems.count) recipes in your \(plan) meal plan"
            mealPlanStatusLbl.isHidden = false
            recipesStatusLbl.isHidden = true
            
            self.recipeCV.isHidden = false
        }
        else
        {
            mealPlanStatusLbl.text = ""
            mealPlanStatusLbl.isHidden = true
            recipesStatusLbl.isHidden = false
            
            self.recipeCV.isHidden = true
        }
        
        recipeCV.reloadData()
    }
    
    // MARK: Actions
    
    @IBAction func learnMore(_ sender: Any) {
        
//        if !showedFamilyBox
//        {
//            showedFamilyBox = true
//            familyView.isHidden = false
//        }
        
        familyView.isHidden = false
    }
    
    @IBAction func changeMeal(_ sender: Any) {
        
        let btn1 = sender as? UIButton
        
        for btn in mealBtns
        {
            if btn != btn1
            {
                btn.backgroundColor = .clear
                btn.setTitleColor(appDelegate.crLightBlue, for: .normal)
            }
            else
            {
                btn.backgroundColor = appDelegate.crLightBlue
                btn.setTitleColor(.white, for: .normal)
            }
        }
        
        mealIndex = Int((btn1?.restorationIdentifier)!)!
        
        if mealIndex == 0
        {
            selectedList = breakfastList
        }
        else if mealIndex == 1
        {
            selectedList = lunchList
        }
        else if mealIndex == 2
        {
            selectedList = snackList
        }
        else if mealIndex == 3
        {
            selectedList = dinnerList
        }
        
        updateDisplayList()
    }
    
    @IBAction func replaceRecipe(_ sender: Any) {
        
        //randall to do
    }
    
    @IBAction func previousDate(_ sender: Any) {
        
        currentDate = (currentDate?.yesterday)!
        
        var dateStr = appDelegate.formatMessageDate1(date: currentDate!, format: "yyyy, dd MMMM")
        
        if todayDateStr == dateStr
        {
            dateStr = todayStr!
        }
        
        dateLbl.text = dateStr
        
        updateDisplayList()
    }
    
    @IBAction func nextDate(_ sender: Any) {
        
        currentDate = (currentDate?.tomorrow)!
        
        var dateStr = appDelegate.formatMessageDate1(date: currentDate!, format: "yyyy, dd MMMM")
        
        if todayDateStr == dateStr
        {
            dateStr = todayStr!
        }
        
        dateLbl.text = dateStr
        
        updateDisplayList()
    }
    
    @IBAction func addToFamily(_ sender: Any) {
        
        let btn = sender as? UIButton
        
        if !addedToFamily
        {
            btn?.setBackgroundImage(#imageLiteral(resourceName: "selected"), for: .normal)
            addedToFamily = true            
        }
        else
        {
            btn?.setBackgroundImage(#imageLiteral(resourceName: "unselected"), for: .normal)
            addedToFamily = false
        }
    }
    
    @IBAction func closeFamilyView(_ sender: Any) {
        
        familyView.isHidden = true
    }
    
    @objc func doneClicked(sender: UIButton!) {
        
        //selectedList.append(folderNameTxt.text!)
        folderTable.reloadData()
        
        self.view.endEditing(true)
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func saveToFolder(_ sender : UIButton) {
        
        //let btn = sender as UIButton
        //let ind = Int(btn.restorationIdentifier!)
        
        //let dict = self.selectedList[ind!]
        
        //print("selectedItems: \(selectedItems)")
    }
    
    @IBAction func saveRecipe(_ sender: Any) {
        
        activityView.startAnimating()
        activityView.isHidden = false
        shareBtn.isEnabled = false
        shareBtn.setTitle("", for: .normal)
        
        
        if displayItems.count < 4
        //if selectedList.count < 4
        {
            uploadData()
        }
        else
        {
            self.mealPlanStatusLbl.text = "You have already reached the limit of 4 recipes for breakfast in your meal plan. Please replace an existing recipe."
            
            mealPlanStatusLblHeight.constant = 65
            mealPlanStatusLbl.isHidden = false
            
            activityView.stopAnimating()
            activityView.isHidden = true
        }
        
        //Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.debugSave), userInfo: nil, repeats: false)
    }
    
    @objc func debugSave () {
        
        recipeSaved()
    }
    
    func recipeSaved () {
        
        //Alamofire to send dict
        
        print("recipe meal plan saved")
        
        NotificationCenter.default.post(name: Notification.Name("refreshMealPlans"), object: nil)
        NotificationCenter.default.post(name: Notification.Name("refreshMealPlan"), object: nil)
        NotificationCenter.default.post(name: Notification.Name("savedMealPlan"), object: nil)
        
        activityView.stopAnimating()
        activityView.isHidden = true
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Webservice
    
    func getFolders() {
        
        print("getFolders")
        
        let dataString = "folderData"
        
        let urlString = "\(appDelegate.serverDestination!)getFolders.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    if (uploadData.count > 0)
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = true
                        })
                        
                        //self.selectedList = uploadData as! [NSMutableDictionary]
                        
                        DispatchQueue.main.sync(execute: {
                            
                            //self.folderTable.reloadData()
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getMealPlanRecipes() {
        
        breakfastList.removeAll()
        lunchList.removeAll()
        snackList.removeAll()
        dinnerList.removeAll()
        selectedItems.removeAll()
        
        print("getMealPlanRecipes")
        
        let dataString = "recipeData"
        
        let urlString = "\(appDelegate.serverDestination!)getMealPlanRecipes.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            let mealid = dict2.object(forKey: "mealid") as! String
                            
                            if mealid == "0"
                            {
                                self.breakfastList.append(dict2)
                            }
                            else if mealid == "1"
                            {
                                self.lunchList.append(dict2)
                            }
                            else if mealid == "2"
                            {
                                self.snackList.append(dict2)
                            }
                            else if mealid == "3"
                            {
                                self.dinnerList.append(dict2)
                            }
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.stopAnimating()
                            self.activityView.isHidden = true
                            
                            if hasData == true
                            {
                                self.recipesStatusLbl.isHidden = true
                                
                                self.mealPlanStatusLbl.isHidden = false
                                
                                self.mealPlanStatusLbl.text = "You have \(self.breakfastList) recipes for breakfast in your meal plan"
                                
                                self.selectedList = self.breakfastList
                                
                                self.updateDisplayList()
                            }
                            else
                            {
                                print("no data")
                                
                                //self.noResultsMain.isHidden = false
                            }
                            
                            //self.noResultsMain.isHidden = true
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func uploadData() {
        
        print("uploadData")
        
        let dataString = "mealPlanData"
        
        let urlString = "\(appDelegate.serverDestination!)manageRecipeMealPlan.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var paramString = "recipeid=\(recipeDict.object(forKey: "recipeid") as! String)&mealid=\(mealIndex)&mealdate=\(currentDateSQLStr!)&userid=\(appDelegate.userid!)&family=\(addedToFamily)&devStatus=\(appDelegate.devStage!)"
        
        if itemReplaced == true
        {
            paramString = "\(paramString)&originalrecipeid=\(originalRecipeID!)&replace=true"
        }
        else
        {
            paramString = "\(paramString)&insert=true"
        }
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                        
                        self.activityView.stopAnimating()
                        self.activityView.isHidden = true
                        self.shareBtn.isEnabled = true
                        self.shareBtn.setTitle("Add to Meal Plan", for: .normal)
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "meal plan item saved" || status == "meal plan recipe replaced"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            self.recipeSaved()
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                            
                            self.activityView.stopAnimating()
                            self.activityView.isHidden = true
                            self.shareBtn.isEnabled = true
                            self.shareBtn.setTitle("Add to Meal Plan", for: .normal)
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let kWhateverHeightYouWant = 190
        
        return CGSize.init(width: 600, height: CGFloat(kWhateverHeightYouWant))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
//        if mealIndex == 0
//        {
//            return self.breakfastList.count
//        }
//        else if mealIndex == 1
//        {
//            return self.lunchList.count
//        }
//        else
//        {
//            return self.dinnerList.count
//        }
        
        return self.displayItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //chanage cell width to 40% of screen
        
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? MealPlanCollectionViewCell
        
        var dict: NSMutableDictionary?
        
        //dict = breakfastList[indexPath.row]
        
//        if mealIndex == 0
//        {
//            dict = breakfastList[indexPath.row]
//        }
//        else if mealIndex == 1
//        {
//            dict = lunchList[indexPath.row]
//        }
//        else if mealIndex == 2
//        {
//            dict = dinnerList[indexPath.row]
//        }
        
        dict = displayItems[indexPath.row]
        
        //print("recipename: \(dict?.object(forKey: "recipename") as? String)")
        
        //cell?.frame.size = CGSize.init(width: recipeCV.frame.width * 0.5, height: 292) //Randall. check this
        
        let b = dict?.object(forKey: "replaced") as? Bool
        
        if b == true
        {
            cell?.statusIV.image = #imageLiteral(resourceName: "checkGreen")
        }
        else
        {
            cell?.statusIV.image = #imageLiteral(resourceName: "replace")
        }
        
        //cell?.headerIV.image = UIImage.init(named: (dict?.object(forKey: "imagename") as? String)!)
        
        let imagename = dict!.object(forKey: "imagename") as? String
        
        //rv.recipeIV.image = UIImage.init(named: imagename!)
        
        if imagename != nil
        {
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
            
            if let img = statusImageCache[urlString]
            {
                cell?.headerIV.image = img
            }
            else
            {
                cell?.headerIV.image = UIImage(named: "profile-icon")
                
                self.downloadImage(imagename: imagename!, urlString: urlString, iv: (cell?.headerIV)!)
            }
        }
        
        cell?.headerLbl.text = dict?.object(forKey: "recipename") as? String
        cell?.infoLbl.text = "\(dict?.object(forKey: "preptime") as! String) • \(dict?.object(forKey: "calories") as! String)"
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //let cell = collectionView.cellForItem(at: indexPath) as! MealPlanCollectionViewCell
        
        let selectedDict = displayItems[indexPath.row]
        
        let replaced = selectedDict.object(forKey: "replaced") as? Bool
        
        let newRecipeDict = recipeDict.mutableCopy()
        
        //print("b: \(b)")
        //..
        if replaced != true
        {
            (newRecipeDict as AnyObject).setValue(true, forKey: "replaced")
            
            (newRecipeDict as AnyObject).setValue(appDelegate.formatMessageDate1(date: currentDate!, format: "yyyy-MM-dd"), forKey: "mealplandate")
            
            if mealIndex == 0
            {
                let ind = breakfastList.index(of: selectedDict)
                originalRecipeID = breakfastList[ind!].object(forKey: "recipeid") as? String
                breakfastList[ind!] = newRecipeDict as! NSMutableDictionary
            }
            else if mealIndex == 1
            {
                let ind = lunchList.index(of: selectedDict)
                originalRecipeID = lunchList[ind!].object(forKey: "recipeid") as? String
                
                lunchList[ind!] = newRecipeDict as! NSMutableDictionary
            }
            else if mealIndex == 2
            {
                let ind = snackList.index(of: selectedDict)
                originalRecipeID = snackList[ind!].object(forKey: "recipeid") as? String
                snackList[ind!] = newRecipeDict as! NSMutableDictionary
            }
            else if mealIndex == 3
            {
                let ind = dinnerList.index(of: selectedDict)
                originalRecipeID = dinnerList[ind!].object(forKey: "recipeid") as? String
                dinnerList[ind!] = newRecipeDict as! NSMutableDictionary
            }
            
            print("originalRecipeID: \(originalRecipeID!)")
            
            let ind = selectedList.index(of: selectedDict)
            
            itemReplaced = true
            
            selectedList[ind!] = newRecipeDict as! NSMutableDictionary
            
            displayItems[indexPath.row] = newRecipeDict as! NSMutableDictionary
            
            recipeCV.reloadData()
        }
    }
    
    func downloadImage (imagename : String, urlString : String, iv: UIImageView ) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache[urlString] = image
                        iv.image = image
                        
                        if urlString.contains(self.appDelegate.profileImg!)
                        {
                            self.appDelegate.userImage = image
                        }
                    }
                    
                    //println("test")
                })
                return nil
            }))
        }
        else
        {
            iv.image = UIImage.init(named: "profile-icon")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension Date {
    
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
}
