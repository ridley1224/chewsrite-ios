//
//  AddPersonGroup.swift
//  ChewsRite
//
//  Created by Randall Ridley on 9/15/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class AddPersonGroup: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var feedList = [NSMutableDictionary]()
    var filteredList = [NSMutableDictionary]()
    var selectedInviteList = [NSMutableDictionary]()
    var isFiltered :Bool?
    
    var userids :String?
    var groupId :String?
    var groupName :String?
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    @IBOutlet weak var friendTable: UITableView!
    @IBOutlet weak var addBtn: UIButton!
    
    var statusImageCache : [String:UIImage]?
    
    @IBOutlet weak var statusTV: UITextView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        friendTable.delegate = self
        friendTable.dataSource = self
        friendTable.tableFooterView = UIView()
        
        activityView.isHidden = true
        statusTV.isHidden = true
        
        checkBtn()
        getFriends()
        
        //debug()
    }
    
    func debug () {
        
        var dict : NSMutableDictionary?
        
        groupId = "2"
        
        dict  = ["friendname" : "Friend 1", "userid" : "22", "imagename" : "1"]
        feedList.append(dict!)
        
        dict  = ["friendname" : "Friend 2", "userid" : "23", "imagename" : "1"]
        feedList.append(dict!)
        
        friendTable.reloadData()
    }
    
    func getFriends() {
        
        feedList.removeAll()
        
        print("getFriends")
        
        let dataString = "friendsData"
        
        let urlString = "\(appDelegate.serverDestination!)getUserFriendsJSON.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&groupid=\(groupId!)&groupfilter=true&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("friends jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.statusTV.isHidden = false
                        
                        //self.friendsCV.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            dict2.setValue("0", forKey: "isSelected")
                            
                            self.feedList.append(dict2)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            //                            self.activityView.stopAnimating()
                            //                            self.activityView.isHidden = true
                            
                            if hasData == true
                            {
                                //self.recipeStatusTV.text = "You currently have no friend requests."
                                
                                self.friendTable.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
                                self.statusTV.isHidden = false
                                
                                //self.noResultsMain.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.statusTV.isHidden = false
                            
                            //self.friendsCV.reloadData()
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        cancel()
    }
    
    func cancel() {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltered == true
        {
            return self.filteredList.count
        }
        else
        {
            return self.feedList.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 48
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! FriendRequestCell
        
        var dict : NSMutableDictionary?
        
        if isFiltered == true
        {
            dict = filteredList[indexPath.row]
        }
        else
        {
            dict =  feedList[indexPath.row]
        }
        
        cell.selectionStyle = .none
        
        //cell.nameLbl.text = dict?.object(forKey: "friendname") as? String
        
        let fn = dict?.object(forKey: "firstname") as? String
        let ln = dict?.object(forKey: "lastname") as? String
        
        cell.nameLbl.text = "\(fn!) \(ln!)"
        
        if selectedInviteList.contains(dict!)
        {
            cell.requestIV.image = #imageLiteral(resourceName: "checkGreen")
        }
        else
        {
            cell.requestIV.image = #imageLiteral(resourceName: "unselected-1")
        }
        
        let imagename = dict?.object(forKey: "userimage") as? String
        
        print("imagename: \(imagename)")
        
        //cell.friendIV.image = UIImage.init(named: imagename!)
        
        if imagename != nil
        {
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
            
            if let img = statusImageCache![urlString]
            {
                cell.friendIV.image = img
            }
            else
            {
                cell.friendIV.image = UIImage(named: "profile-icon")
                
                self.downloadImage(imagename: imagename!, urlString: urlString, rv: cell)
            }
        }
        
        //cell.requestBtn.restorationIdentifier = "\(indexPath.row)"
        //cell.requestBtn.addTarget(self, action: #selector(self.manageButton(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! FriendRequestCell
        
        var item : NSMutableDictionary?
        
        if isFiltered == true
        {
            item = filteredList[indexPath.row]
        }
        else
        {
            item =  feedList[indexPath.row]
        }
        
        if selectedInviteList.contains(item!)
        {
            let ind = selectedInviteList.index(of: item!)
            
            cell.requestIV.image = #imageLiteral(resourceName: "unselected-1")
            
            //cell.requestBtn.setBackgroundImage(#imageLiteral(resourceName: "unselected-1"), for: .normal) //replace with check
            selectedInviteList.remove(at: ind!)
        }
        else
        {
            cell.requestIV.image = #imageLiteral(resourceName: "checkGreen")
            
            //cell.requestBtn.setBackgroundImage(#imageLiteral(resourceName: "checkGreen"), for: .normal)
            selectedInviteList.append(item!)
        }
        
        checkBtn ()
        tableView.reloadData()
    }
    
    @objc func manageButton (_ sender: Any)  {
        
        let btn = sender as! UIButton
        
        let ind = Int(btn.restorationIdentifier!)
        var item : NSMutableDictionary?
        
        if isFiltered == true
        {
            item = filteredList[ind!]
        }
        else
        {
            item =  feedList[ind!]
        }
        
        if selectedInviteList.contains(item!)
        {
            btn.setBackgroundImage(#imageLiteral(resourceName: "unselected-1"), for: .normal) //replace with check
            selectedInviteList.remove(at: ind!)
        }
        else
        {
            btn.setBackgroundImage(#imageLiteral(resourceName: "checkGreen"), for: .normal)
            selectedInviteList.append(item!)
        }        
    }
    
    func checkBtn () {
        
        if selectedInviteList.count == 0
        {
            self.addBtn.isEnabled = false
            self.addBtn.alpha = 0.5
        }
        else
        {
            self.addBtn.isEnabled = true
            self.addBtn.alpha = 1
        }
    }
    
    @IBAction func addMembers(_ sender: Any) {
        
        activityView.isHidden = false
        activityView.startAnimating()
        
        userids = ""
        
        var i = 0
        
        for user in selectedInviteList
        {
            let userid = user.object(forKey: "userid") as! String
            
            if i > 0
            {
                userids = "\(userids!),\(userid)"
            }
            else
            {
                userids = "\(userid)"
            }
            
            i += 1
        }
        
        print("userids: \(userids!)")
        
        sendInvites()
    }
    
    func sendInvites() {
        
        print("sendInvites")
        
        let dataString = "inviteData"
        
        let urlString = "\(appDelegate.serverDestination!)sendInvites.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&groupid=\(groupId!)&userids=\(userids!)&groupinvites=true&devStatus=\(appDelegate.devStage!)&groupname=\(groupName!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.activityView.stopAnimating()
                        self.activityView.isHidden = true
                        
                        self.showBasicAlert(string: "No Data")
                    })
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status.contains("invites sent")
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            let alert = UIAlertController(title: nil, message: "Invites Sent", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                    self.cancel()
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                        else
                        {
                            print("save error")
                            
                            self.showBasicAlert(string: "Save Error")
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    func downloadImage (imagename : String, urlString : String, rv: FriendRequestCell ) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache![urlString] = image
                        rv.friendIV.image = image
                    }
                    
                    //println("test")
                })
                return nil
            }))
        }
        else
        {
            rv.friendIV.image = UIImage.init(named: "profile-icon")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
