//
//  AddIngredients.swift
//  ChewsRite
//
//  Created by Randall Ridley on 5/19/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class AddIngredients: UIViewController, UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    @IBOutlet weak var ingredientTxt: UITextField!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var ingredientsTable: UITableView!
    
    var ingredients : NSMutableArray?
    var newIngredients = [String]()
    var category : String?
    var newIngredientsDict = [NSDictionary]()
    
    @IBOutlet weak var submitBtn: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        activityView.isHidden = true
        
        ingredientTxt.delegate = self
        ingredientsTable.delegate = self
        ingredientsTable.dataSource = self
        
        if ingredients == nil
        {
            ingredients = NSMutableArray()
        }

        ingredientsTable.isHidden = true
        statusLbl.isHidden = true
        
        let btnItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(dismissKeyboard))
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width:  self.view.frame.size.width, height: 50))
        numberToolbar.backgroundColor = UIColor.darkGray
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.tintColor = UIColor.black
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            btnItem]
        
        numberToolbar.sizeToFit()
        
        ingredientTxt.inputAccessoryView = numberToolbar
        
        ingredientsTable.tableFooterView = UIView()
    }
    
    @objc func dismissKeyboard () {
        
        if ingredientTxt.text != ""
        {
            addIngredient ()
        }
        
        self.view.endEditing(true)
    }
    
    @IBAction func addIngredient(_ sender: Any) {
        
        if ingredientTxt.text != ""
        {
            addIngredient ()
        }
    }
    
    func addIngredient () {
        
        if (ingredients?.contains(ingredientTxt.text!))! ||  (newIngredients.contains(ingredientTxt.text!))
        {
            statusLbl.isHidden = false
        }
        else
        {
            //uploadConcernData()
            
            statusLbl.isHidden = true
            
            self.newIngredients.append(self.ingredientTxt.text!)
            //self.newIngredientsDict.append(dict1 as NSDictionary)
            self.ingredientTxt.text = ""
            self.ingredientsTable.reloadData()
            self.ingredientsTable.isHidden = false
            
        }
    }
    
    @IBAction func submitIngredients(_ sender: Any) {
        
        if newIngredients.count > 0
        {
            uploadIngredientData()
        }
    }
    
    func uploadIngredientData() {
        
        submitBtn.isEnabled = false
        activityView.isHidden = false
        activityView.startAnimating()
        
        print("uploadIngredientData")
        
        let urlString = "\(appDelegate.serverDestination!)addIngredient.php"
        
        print("urlString: \(urlString)")
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let ingredientsStr = newIngredients.joined(separator: ",")
        
        let paramString = "ingredients=\(ingredientsStr)&userid=\(appDelegate.userid!)&category=\(category!)&devStatus=\(appDelegate.devStage!)"
        
        //let paramString = "ingredient=\(ingredientTxt.text!)&category=\(category!)&userid=\(appDelegate.userid!)"
        
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("add user jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict["ingredientData"]! is NSNull
                {
                    print("no data")
                    
                    self.activityView.isHidden = true
                    self.activityView.stopAnimating()
                    
                    self.submitBtn.isEnabled = true
                    self.submitBtn.alpha = 1
                }
                else
                {
                    let userDict : NSDictionary = dataDict["ingredientData"] as! NSDictionary
                    
                    let status : String = userDict["status"] as! String
                    
                    print("status: \(status)")
                    
                    if (status == "ingredient info saved")
                    {
                        let ingredientIDS : String = userDict["ingredientids"] as! String
                        
                        let ids = ingredientIDS.split(separator: ",")
                        
                        var i = 0
                        
                        for id in ids
                        {
                            let dict1 = ["ingredient" : self.newIngredients[i], "ingredientid" : id, "categoryid" : self.category!] as [String : Any]
                            self.newIngredientsDict.append(dict1 as NSDictionary)
                            
                            i += 1
                        }

                        DispatchQueue.main.sync(execute: {
                            
                            NotificationCenter.default.post(name: Notification.Name("refreshIngredients"), object: self.newIngredientsDict)
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.goBack()
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.alpha = 1
                                                        
                            //self.showBasicAlert(string: status)
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
                
                self.activityView.isHidden = true
                self.activityView.stopAnimating()
                
                self.submitBtn.isEnabled = true
                self.submitBtn.alpha = 1
            }
            
        }.resume()
    }
    
    func goBack () {
        
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.newIngredients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : LikesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! LikesTableViewCell
        
        cell.itemLbl?.text = self.newIngredients[(indexPath as NSIndexPath).row] as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //var selectedIem = self.newIngredients![(indexPath as NSIndexPath).row]
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
