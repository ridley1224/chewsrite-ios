//
//  EditNutritionGoals.swift
//  ChewsRite
//
//  Created by Randall Ridley on 1/2/19.
//  Copyright © 2019 RT. All rights reserved.
//

import UIKit

class EditNutritionGoals: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    //nutritional goals
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    var categoryNutrition : String?
    
    @IBOutlet weak var nutritionTable: UITableView!
    
    @IBOutlet weak var v1Nutrition: UIView!
    @IBOutlet weak var v2Nutrition: UIView!
    
    @IBOutlet weak var b1Nutrition: UIButton!
    @IBOutlet weak var b2Nutrition: UIButton!
    
    var selectedListNutrition : NSMutableArray?
    var currentButtonsNutrition = [UIButton]()
    var isUpdating: Bool?
    
    var nutritionLabelList = ["Calories","Carbohydrates","Protein","Fat","     Saturated Fat","     Unsaturated Fat","     Trans Fat","Cholestrol","Sodium","Potassium","Fiber","Sugars","Vitamin A","Vitamin C","Calcium","Iron"]
    
    var dailyList = NSMutableArray()
    var weeklyList = NSMutableArray()
    var savedVals = NSMutableArray()
    var fields = [UITextField]()
    
    var numberToolbar : UIToolbar?
    
    var currentTextfieldValue = ""
    
    @IBOutlet weak var submitBtn: UIButton!   
    
    @IBOutlet weak var nutritionView: UIView!
    var selectedList : NSMutableArray?
    
    var newFamilyMemberID : Int?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //nutritional goals
        
        categoryNutrition = "0"
        selectedList = dailyList
        
        for val in nutritionLabelList
        {
            if val == "Calories"
            {
                dailyList.add("0")
                weeklyList.add("0")
            }
            else
            {
                dailyList.add("0.0")
                weeklyList.add("0.0")
            }
        }
        
        nutritionTable.delegate = self
        nutritionTable.dataSource = self
        
        activityView.isHidden = true
        v2Nutrition.isHidden = true
        
        //print("frame size: \(self.view.frame.width)")
        
        nutritionTable.tableFooterView = UIView()
        nutritionTable.reloadData()
        
        scrollToBottom()
        scrollToTop()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.nutritionLabelList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell : GoalsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! GoalsTableViewCell
        
        let val = self.nutritionLabelList[(indexPath as NSIndexPath).row]
        
        cell.itemLbl?.text = val
        cell.selectionStyle = .none
        cell.tf.text = selectedListNutrition![indexPath.row] as? String
        
        if val == "Cholestrol" || val == "Sodium" || val == "Potassium"
        {
            //cell.tf.text = "0.00"
            cell.unitLbl.text = "mg"
        }
        else if val == "Vitamin A" || val == "Vitamin C" || val == "Calcium" || val == "Iron"
        {
            //cell.tf.text = "0.00"
            cell.unitLbl.text = "%"
        
        }
        else if val == "Calories"
        {
            cell.unitLbl.text = ""
            cell.tf.keyboardType = .numberPad
        }
        else
        {
            //cell.tf.text = "0.00"
            cell.unitLbl.text = "g"
        }
        
        cell.tf.inputAccessoryView = numberToolbar
        cell.tf.restorationIdentifier = "\(indexPath.row)"
        cell.tf.delegate = self
        cell.tf.keyboardType = .decimalPad
        
        if !fields.contains(cell.tf)
        {
            fields.append(cell.tf)
        }
        
        return cell
    }
    
    //nutritional goals
    
    @IBAction func showOptionsNutrition(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        if btn.restorationIdentifier == "b1"
        {
            v1Nutrition.isHidden = false
            v2Nutrition.isHidden = true
            
            selectedListNutrition = dailyList
            
            categoryNutrition = "0"
        }
        else if btn.restorationIdentifier == "b2"
        {
            v1Nutrition.isHidden = true
            v2Nutrition.isHidden = false
            
            selectedListNutrition = weeklyList
            
            categoryNutrition = "1"
        }
        
        fields.removeAll()
        nutritionTable.reloadData()
    }
    
    func scrollToBottom() {
        
        DispatchQueue.main.async {
            
            let indexPath = IndexPath(row: self.fields.count-1, section: 0)
            self.nutritionTable.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
    func scrollToTop() {
        
        DispatchQueue.main.async {
            
            let indexPath = IndexPath(row: 0, section: 0)
            self.nutritionTable.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
    //    func getGoals() {
    //
    //        //displayItems.removeAll()
    //
    //        print("getGoals")
    //
    //        let dataString = "concernsData"
    //
    //        let urlString = "\(appDelegate.serverDestination!)getDietaryConcerns.php"
    //
    //        let url = URL(string: urlString)
    //
    //        var request = URLRequest(url: url!)
    //
    //        request.httpMethod = "POST"
    //
    //        let paramString = "userid=\(appDelegate.userid!)"
    //
    //        print("urlString: \(urlString)")
    //        print("paramString: \(paramString)")
    //
    //        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
    //
    //        let session = URLSession.shared
    //
    //        session.dataTask(with: request) {data, response, err in
    //
    //            do {
    //
    //                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
    //
    //                print("cuisines jsonResult: \(jsonResult)")
    //
    //                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
    //
    //                if dataDict[dataString]! is NSNull {
    //
    //                    print("no data")
    //
    //                    DispatchQueue.main.sync(execute: {
    //
    //                        //self.recipeStatusTV.isHidden = false
    //                    })
    //                }
    //                else
    //                {
    //                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
    //
    //                    let recipes1 = uploadData as! [String]
    //
    //                    var hasData = false
    //
    //                    if (recipes1.count > 0)
    //                    {
    //                        if dataDict["nutritionData"]! is NSNull {
    //
    //                            print("no nutrition")
    //                        }
    //                        else
    //                        {
    //                            let nutritionData = dataDict["nutritionData"]! as! NSDictionary
    //
    //                            self.nutritionValues[0] = nutritionData.object(forKey: "Calories") as! String
    //                            self.nutritionValues[1] = nutritionData.object(forKey: "Carbohydrates") as! String
    //                            self.nutritionValues[2] = nutritionData.object(forKey: "Protein") as! String
    //                            self.nutritionValues[3] = nutritionData.object(forKey: "Fat") as! String
    //                            self.nutritionValues[4] = nutritionData.object(forKey: "SaturatedFat") as! String
    //                            self.nutritionValues[5] = nutritionData.object(forKey: "UnsaturatedFat") as! String
    //                            self.nutritionValues[6] = nutritionData.object(forKey: "TransFat") as! String
    //                            self.nutritionValues[7] = nutritionData.object(forKey: "Cholestrol") as! String
    //                            self.nutritionValues[8] = nutritionData.object(forKey: "Sodium") as! String
    //                            self.nutritionValues[9] = nutritionData.object(forKey: "Potassium") as! String
    //                            self.nutritionValues[10] = nutritionData.object(forKey: "Fiber") as! String
    //                            self.nutritionValues[11] = nutritionData.object(forKey: "Sugars") as! String
    //                            self.nutritionValues[12] = nutritionData.object(forKey: "VitaminA") as! String
    //                            self.nutritionValues[13] = nutritionData.object(forKey: "VitaminC") as! String
    //                            self.nutritionValues[14] = nutritionData.object(forKey: "Calcium") as! String
    //                            self.nutritionValues[15] = nutritionData.object(forKey: "Iron") as! String
    //                        }
    //                    }
    //                    else
    //                    {
    //                        DispatchQueue.main.sync(execute: {
    //
    //                            //self.noResultsMain.isHidden = false
    //                        })
    //                    }
    //                }
    //            }
    //            catch let err as NSError
    //            {
    //                print("error: \(err.description)")
    //            }
    //
    //        }.resume()
    //    }
    
    func uploadGoals() {
        
        submitBtn.isEnabled = false
        self.submitBtn.alpha = 0.5
        
        var paramString = ""
        
        if newFamilyMemberID != nil
        {
            paramString = "familyid=\(newFamilyMemberID!)&insert=true&devStatus=\(appDelegate.devStage!)"
        }
        else
        {
            paramString = "userid=\(appDelegate.userid!)&insert=true&devStatus=\(appDelegate.devStage!)"
        }
        
        //week params
        
        var i = 0
        
        for field in nutritionLabelList
        {
            let val = field.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
            
            //to do strip all white space from param name
            paramString = "\(paramString)&w\(val)=\(weeklyList[i] as! String)&devStatus=\(appDelegate.devStage!)"
            i += 1
        }
        
        //daily params
        
        i = 0
        
        for field in nutritionLabelList
        {
            let val = field.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
            //to do strip all white space from param name
            paramString = "\(paramString)&d\(val)=\(dailyList[i] as! String)&devStatus=\(appDelegate.devStage!)"
            i += 1
        }
        
        print("paramString: \(paramString)")
        
        activityView.isHidden = false
        activityView.startAnimating()
        
        let urlString = "\(appDelegate.serverDestination!)addNutritionGoals.php"
        
        print("urlString: \(urlString)")
        
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            //print("Entered the completionHandler: \(response)")
            
            //var err: NSError?
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("goalsData: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                //print("dataDict: \(dataDict)")
                
                let uploadData : NSDictionary = dataDict.object(forKey: "goalsData") as! NSDictionary
                
                if (uploadData != nil)
                {
                    let status = uploadData.object(forKey: "status") as? String
                    
                    if (status == "goals info saved")
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.alpha = 1
                            
                            if self.newFamilyMemberID != nil
                            {
                                NotificationCenter.default.post(name: Notification.Name("refreshFamily"), object: nil)
                                
                                for controller in self.navigationController!.viewControllers as Array {
                                    
                                    if controller.isKind(of: ManageFamilyMembers.self) {
                                        
                                        self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                            }
                            else
                            {
                                if (self.self.appDelegate.signingUp == true)
                                {
                                    self.performSegue(withIdentifier: "login", sender: self)
                                }
                                else
                                {
                                    self.showBasicAlert(string: "Nutrition Goals Updated")
                                }
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.alpha = 1
                            
                            self.showBasicAlert(string: status!)
                        })
                    }
                }
                else
                {
                    //data not returned
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        currentTextfieldValue = textField.text!
        
        print("currentTextfieldValue: \(currentTextfieldValue)")
        
        textField.text = ""
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
        print("tf ind: \(textField.restorationIdentifier!) \(textField.text!)")
        
        if appDelegate.isStringAnInt(string: textField.text!)
        {
            let ind = Int(textField.restorationIdentifier!)
            
            if categoryNutrition == "1"
            {
                weeklyList[ind!] = textField.text!
            }
            else
            {
                dailyList[ind!] = textField.text!
            }
        }
        else
        {
            textField.text = currentTextfieldValue
            showBasicAlert(string: "Invalid format. Please enter numberic value.")
        }
        
        nutritionTable.reloadData()
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
