//
//  ChangeMealPlan.swift
//  ChewsRite
//
//  Created by Randall Ridley on 8/14/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class ChangeMealPlan: UIViewController, FSCalendarDelegate, FSCalendarDataSource, UICollectionViewDataSource, UICollectionViewDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var recipeCV: UICollectionView!
    @IBOutlet weak var shareBtn: UIButton!
    
    //var displayItems = [NSMutableDictionary]()
    var displayItems : [NSMutableDictionary]?
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    @IBOutlet weak var mealPlanStatusLbl: UILabel!
    @IBOutlet weak var recipeLbl: UILabel!
    @IBOutlet weak var recipeIV: UIImageView!
    @IBOutlet weak var selectedImage: UIImage?
    var mealIndex = 0
    var currentDateSQLStr : String?
    
    @IBOutlet weak var calenderView: DesignableView!
    
    @IBOutlet weak var calenderPopup: UIView!
    @IBOutlet weak var calendar : FSCalendar?
    
    var eventDates = ["2018-08-19","2018-08-20"]
    
    
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    
    @IBOutlet weak var mealSegment: UISegmentedControl!
    
    var statusImageCache : [String:UIImage]?
    var selectedFeedItem: FeedItem?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        activityView.isHidden = true
        mealSegment.selectedSegmentIndex = Int(selectedFeedItem!.mealid)!
        
        let a = selectedFeedItem!.recipename
        let b = selectedFeedItem!.calories
        
        //recipeLbl.text = "\(a!) (\(b!) calories)"
        
        //let imagename = selectedFeedItem!.imagename
        
        //let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
        
        if selectedImage != nil
        {
            print("changemealplan image: \(selectedImage)")
            
            recipeIV.image = selectedImage
        }
        else
        {
            print("no")
        }
        
        let attributedString = NSMutableAttributedString(string: "\(a) (\(b) calories)", attributes: [
            .font: UIFont(name: "Avenir-Black", size: 14.0)!,
            .foregroundColor: UIColor.white,
            .kern: 0.19
            ])
        
        attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Black", size: 18.0)!, range: NSRange(location: 0, length: a.characters.count))
        
        recipeLbl.attributedText = attributedString
        
        calendar?.dataSource = self
        calendar?.delegate = self
        
        recipeCV.delegate = self
        recipeCV.dataSource = self
        
        calenderView.layer.cornerRadius = 20
        calenderView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        calendar?.calendarWeekdayView.backgroundColor = appDelegate.crOrange2
        calendar?.calendarHeaderView.backgroundColor = .white
        
        let lbls = calendar?.calendarWeekdayView.weekdayLabels
        
        let dateFormat = "yyyy-MM-dd"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        let mealplandate = dateFormatter.date(from: selectedFeedItem!.mealplandate)
        
        currentDateSQLStr = appDelegate.convertDateToSQLTime(date: mealplandate!)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "MMMM E dd"
        let selectedDate = dateFormatter2.string(from: mealplandate!)
                
        let str = selectedDate.components(separatedBy: " ")
        
        monthLbl.text = str[0]
        dayLbl.text = "\(str[1]) \(str[2])"
        
        for lbl in lbls!
        {
            let s = lbl.text?.character(at: 0)
            lbl.text = String(describing: s!)
        }
        
        //getMealPlanRecipes()
        
        //let a = calendar.dat
        
        calenderPopup.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        //print("date: \(appDelegate.convertDateToSQLDate(date: date))")
        
        let cd = appDelegate.convertDateToSQLDate(date: date)
        
        if eventDates.contains(cd)
        {
            return 1
        }
        else
        {
           return 0
        }
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        print("date: \(date)")
        
        calenderPopup.isHidden = true
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM E dd"
        let selectedDate = dateFormatter.string(from: date)
        
        print("day: \(selectedDate)")
        
        let str = selectedDate.components(separatedBy: " ")
        
        monthLbl.text = str[0]
        dayLbl.text = "\(str[1]) \(str[2])"
        
        currentDateSQLStr = appDelegate.convertDateToSQLTime(date: date)
        
        print("currentDateSQLStr: \(currentDateSQLStr!)")
    }

    func getMealPlanRecipes() {
        
        displayItems?.removeAll()
        
        print("getMealPlanRecipes")
        
        let dataString = "recipeData"
        
        let urlString = "\(appDelegate.serverDestination!)getMealPlanRecipes.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("meal plan recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.mealPlanStatusLbl.isHidden = false
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            //let mealid = dict2.object(forKey: "mealid") as! String
                            
                            self.displayItems?.append(dict2)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.stopAnimating()
                            self.activityView.isHidden = true
                            
                            if hasData == true
                            {
                                self.mealPlanStatusLbl.text = "You have \(self.displayItems?.count) recipes for breakfast in your meal plan"
                                
                                self.updateDisplayList()
                            }
                            else
                            {
                                print("no data")
                                
                                //self.noResultsMain.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func updateDisplayList () {
        
        recipeCV.reloadData()
    }
    
    func recipeSaved () {
        
        NotificationCenter.default.post(name: Notification.Name("recipeUpdated"), object: nil)
        
        activityView.stopAnimating()
        activityView.isHidden = true
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func uploadData() {
        
        print("uploadData")
        
        let dataString = "mealPlanData"
        
        let urlString = "\(appDelegate.serverDestination!)manageRecipeMealPlan.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "mealplanid=\(selectedFeedItem!.mealplanid) as! String)&mealid=\(mealIndex)&mealdate=\(currentDateSQLStr!)&userid=\(appDelegate.userid!)&update=true&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                        
                        self.activityView.stopAnimating()
                        self.activityView.isHidden = true
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "meal plan item updated"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            self.recipeSaved()
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                            
                            self.activityView.stopAnimating()
                            self.activityView.isHidden = true
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func saveRecipe(_ sender: Any) {
        
        uploadData()
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let kWhateverHeightYouWant = 190
        
        return CGSize.init(width: 600, height: CGFloat(kWhateverHeightYouWant))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (self.displayItems?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //chanage cell width to 40% of screen
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? MealPlanCollectionViewCell
        
        let dict = displayItems?[indexPath.row]
        
        cell?.headerLbl.text = dict?.object(forKey: "recipename") as? String
        cell?.infoLbl.text = "\(dict?.object(forKey: "preptime") as! String) • \(dict?.object(forKey: "calories") as! String)"
        
        if selectedImage != nil
        {
            print("image is there")
            cell?.headerIV.image = selectedImage!
        }
        else
        {
            let imagename = dict?.object(forKey: "imagename") as? String
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
            
            print("no image")
            cell?.headerIV.image = UIImage(named: "profile-icon")
            
            self.downloadImage(imagename: imagename!, urlString: urlString, rv: cell!)
        }
        
        return cell!
    }
    
    func downloadImage (imagename : String, urlString : String, rv: MealPlanCollectionViewCell ) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache![urlString] = image
                        rv.headerIV.image = image
                    }
                    
                    //println("test")
                })
                return nil
            }))
        }
        else
        {
            rv.headerIV.image = UIImage.init(named: "profile-icon")
        }
    }
    
    @IBAction func cancelChange(_ sender: Any) {
        
        calenderPopup.isHidden = true
    }
    
    @IBAction func selectMeal(_ sender: Any) {
        
        let seg = sender as! UISegmentedControl
        
        mealIndex = seg.selectedSegmentIndex
    }
    
    @IBAction func selectDayDate(_ sender: Any) {
        
        print("")
        
        calenderPopup.isHidden = false
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    
    func index(at position: Int, from start: Index? = nil) -> Index? {
        let startingIndex = start ?? startIndex
        return index(startingIndex, offsetBy: position, limitedBy: endIndex)
    }
    
    func character(at position: Int) -> Character? {
        guard position >= 0, let indexPosition = index(at: position) else {
            return nil
        }
        return self[indexPosition]
    }
}
