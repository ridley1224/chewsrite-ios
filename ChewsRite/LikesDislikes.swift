//
//  LikesDislikes.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/2/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class LikesDislikes : UIViewController,UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var headerLbl: UILabel!
    
    var likedItems = NSMutableArray()
    var dislikedItems = NSMutableArray()
    
    @IBOutlet weak var otherTxt: UITextField!
    
    var category : String?
    
    var firstname : String?
    
    @IBOutlet weak var v1: UIView!
    @IBOutlet weak var v2: UIView!
    @IBOutlet weak var v3: UIView!
    @IBOutlet weak var v4: UIView!
    @IBOutlet weak var v5: UIView!
    @IBOutlet weak var v6: UIView!
    
    @IBOutlet weak var b1: UIButton!
    @IBOutlet weak var b2: UIButton!
    @IBOutlet weak var b3: UIButton!
    @IBOutlet weak var b4: UIButton!
    @IBOutlet weak var b5: UIButton!
    @IBOutlet weak var b6: UIButton!
    
    var selectedList = NSMutableArray()
    var currentButtons = [UIButton]()
    var scrollButtons = [UIButton]()
    
    var buttonUnderscores = [UIView]()
    
//    var fruitsList : NSMutableArray = ["Apple"]
//    var vegList : NSMutableArray = ["Artichoke","Artichoke Heart"]
//    var meatList : NSMutableArray = ["Beef"]
//    var seafoodList : NSMutableArray = ["Shrimp"]
//    var dairyList : NSMutableArray = ["Milk"]
//    var nutsList : NSMutableArray = ["Cashews"]
    
    var fruitsList = NSMutableArray()
    var vegList = NSMutableArray()
    var meatList = NSMutableArray()
    var seafoodList = NSMutableArray()
    var dairyList = NSMutableArray()
    var nutsList = NSMutableArray()
    
    @IBOutlet weak var likesTable: UITableView!
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var submitBtn: UIButton!    
    var newFamilyMemberID : Int?
    
    @IBOutlet weak var buttonScrollview: UIScrollView!
    
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var progressView: UIView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        let pg = UIView.init(frame: CGRect.init(x: 0, y: 0, width: progressView.frame.width * 0.73, height: progressView.frame.height))
        
        pg.backgroundColor = appDelegate.crLightBlue
        progressView.addSubview(pg)
        
        //appDelegate.userid = "1"
        
        getIngredients()
        
        //selectedList = fruitsList
        
        category = "0"
        
        activityView.isHidden = true
        
        likesTable.delegate = self
        likesTable.dataSource = self
        likesTable.tableFooterView = UIView()
        
        v2.isHidden = true
        v3.isHidden = true
        v4.isHidden = true
        v5.isHidden = true
        v6.isHidden = true
        
        //print("frame size: \(self.view.frame.width)")
        
        var fontSize : CGFloat = 16.0
        
        if self.view.frame.width <= 320
        {
            fontSize = 14.0
        }
        
        var header = "Step 3: Let us know your likes & dislikes"
        
        if firstname != nil
        {
            header = "Step 3: \(firstname!)'s likes & dislikes"
        }
        
        let attributedString = NSMutableAttributedString(string: header, attributes: [
            .font: UIFont(name: "Avenir-Medium", size: fontSize)!,
            .foregroundColor: UIColor.white
            ])
        
        attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Light", size: fontSize)!, range: NSRange(location: 0, length: 7))
        
        headerLbl.attributedText = attributedString
        
        let btnItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(dismissKeyboard))
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width:  self.view.frame.size.width, height: 50))
        numberToolbar.backgroundColor = UIColor.darkGray
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.tintColor = UIColor.black
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            btnItem]
        
        numberToolbar.sizeToFit()
        
        buttonView.isHidden = true
        
        populateButtonView()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refreshIngredients),
                                               name: NSNotification.Name(rawValue: "refreshIngredients"),
                                               object: nil)
        
        //otherTxt.inputAccessoryView = numberToolbar
    }
    
    @objc func refreshIngredients (notification: NSNotification) {
        
        let newItems = notification.object as! [NSDictionary]
        
        //add new cats
        
        for dict in newItems
        {
            let ingredientID = dict.object(forKey: "ingredientid") as! String
            
            let dict = ["ingredient" : dict.object(forKey: "ingredient") as! String,"ingredientid": ingredientID,"categoryid": dict.object(forKey: "categoryid") as! String] as NSDictionary
            
            likedItems.add("\(ingredientID)-up")
            
            if dict.object(forKey: "categoryid") as! String == "1"
            {
                self.fruitsList.add(dict)
            }
            else if dict.object(forKey: "categoryid") as! String == "2"
            {
                self.vegList.add(dict)
            }
            else if dict.object(forKey: "categoryid") as! String == "3"
            {
                self.meatList.add(dict)
            }
            else if dict.object(forKey: "categoryid") as! String == "4"
            {
                self.seafoodList.add(dict)
            }
            else if dict.object(forKey: "categoryid") as! String == "5"
            {
                self.dairyList.add(dict)
            }
            else if dict.object(forKey: "categoryid") as! String == "6"
            {
                self.nutsList.add(dict)
            }
            
            //ind += 1
            
            self.selectedList.add(dict)
        }
        
        
        self.likesTable.reloadData()
    }
    
    func populateButtonView () {
        
        var xPos = 0
        let lbls = ["Fruits","Vegetables","Meat","Seafood","Dairy","Nuts, Beans &\nOther Protein"]
        
        var width = 65
        
        for i : Int in 0 ..< lbls.count
        {
            
            if i == 5
            {
                width = 80
            }
            else
            {
                width = 65
            }
            
            let doneButton3 = UIButton(frame: CGRect(x: xPos, y: 0, width: width, height: 34))
            doneButton3.setTitle(lbls[i], for: .normal)
            doneButton3.addTarget(self, action: #selector(self.showOptions(_:)), for: UIControlEvents.touchUpInside)
            doneButton3.restorationIdentifier = "b\(i+1)"
            doneButton3.accessibilityIdentifier = String(i)
            buttonScrollview.addSubview(doneButton3)
            
            let f = UIFont(name: "Avenir-Book", size: 12.0)
            
            doneButton3.titleLabel?.font = f
            doneButton3.titleLabel?.numberOfLines = 0
            doneButton3.setTitleColor(appDelegate.crLightBlue, for: .normal)
            
            let view = UIButton(frame: CGRect(x: xPos, y: 36, width: width, height: 2))
            view.backgroundColor = appDelegate.crLightBlue
            buttonScrollview.addSubview(view)
            buttonUnderscores.append(view)
            
            if i > 0
            {
               view.isHidden = true
            }
            
            xPos = xPos + width
        }
        
        buttonScrollview.contentSize = CGSize.init(width: lbls.count*75, height: 42)
    }
    
    @objc func dismissKeyboard () {
        
        self.view.endEditing(true)
    }
    
    @IBAction func next(_ sender: Any) {
        
        uploadUserData()
    }
    
    func uploadUserData() {
        
        submitBtn.isEnabled = false
        
        activityView.isHidden = false
        activityView.startAnimating()
        
        print("uploadUserData")
        
        var likesString = ""
        var dislikesString = ""
        
        if likedItems.count > 0
        {
            var cuisineids = ""
            var i = 0
            
            for a in likedItems
            {
                if i == 0
                {
                    //cuisineids = (a.object(forKey: "concernid") as? String)!
                    
                    cuisineids = a as! String
                }
                else
                {
                    //cuisineids = "\(cuisineids),\(a.object(forKey: "concernid") as! String)"
                    cuisineids = "\(cuisineids),\(a as! String)"
                }
                
                i += 1
            }
            
            likesString = "\(cuisineids)"
        }
        
        if dislikedItems.count > 0
        {
            var cuisineids = ""
            var i = 0
            
            for a in dislikedItems
            {
                if i == 0
                {
                    //cuisineids = (a.object(forKey: "concernid") as? String)!
                    
                    cuisineids = a as! String
                }
                else
                {
                    //cuisineids = "\(cuisineids),\(a.object(forKey: "concernid") as! String)"
                    cuisineids = "\(cuisineids),\(a as! String)"
                }
                
                i += 1
            }
            
            dislikesString = "\(cuisineids)"
        }
        
        let urlString = "\(appDelegate.serverDestination!)manageUserFoodLikes.php"
        
        print("urlString: \(urlString)")
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        likesString = likesString.replacingOccurrences(of: "-up", with: "", options: .literal, range: nil)
        dislikesString = dislikesString.replacingOccurrences(of: "-down", with: "", options: .literal, range: nil)
        
        print("likesString: \(likesString)")
        print("dislikesString: \(dislikesString)")
        
        var paramString = ""
        
        if newFamilyMemberID != nil
        {
            paramString = "likes=\(likesString)&dislikes=\(dislikesString)&familyid=\(newFamilyMemberID!)&devStatus=\(appDelegate.devStage!)"
        }
        else
        {
            paramString = "likes=\(likesString)&dislikes=\(dislikesString)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        }
        
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("likes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict["likesData"]! is NSNull
                {
                    print("no user")
                    
                    self.activityView.isHidden = true
                    self.activityView.stopAnimating()
                    
                    self.submitBtn.isEnabled = true
                    self.submitBtn.alpha = 1
                    
                    self.showBasicAlert(string: "no data")
                }
                else
                {
                    let userDict : NSDictionary = dataDict["likesData"] as! NSDictionary
                    
                    let status : String = userDict["status"] as! String
                    
                    print("status: \(status)")
                    
                    if status == "likes saved" || status == "likes updated"
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.alpha = 1
                            
                            if (self.appDelegate.signingUp == true || self.newFamilyMemberID != nil)
                            {
                                self.performSegue(withIdentifier: "NutritionalGoals", sender: self)
                            }
                            else
                            {
                                self.showBasicAlert(string: "Likes/Dislikes Updated")
                            }                            
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.alpha = 1
                            
                            self.showBasicAlert(string: status)
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getIngredients() {
        
        print("getIngredients")
        
        let dataString = "ingredientData"
        
        let urlString = "\(appDelegate.serverDestination!)getAllIngredients.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = ""
        
        print("urlString: \(urlString)")
        //print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("ingredients jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    if (uploadData.count > 0)
                    {
                        
                        let list = uploadData as! NSMutableArray
                        
                        for ob in list {
                            
                            let dict = ob as! NSDictionary
                            
//                            var fruitsList = NSMutableArray()
//                            var vegList = NSMutableArray()
//                            var meatList = NSMutableArray()
//                            var seafoodList = NSMutableArray()
//                            var dairyList = NSMutableArray()
//                            var nutsList = NSMutableArray()
                            
                            if dict.object(forKey: "categoryid") as! String == "1"
                            {
                                self.fruitsList.add(dict)
                            }
                            else if dict.object(forKey: "categoryid") as! String == "2"
                            {
                                self.vegList.add(dict)
                            }
                            else if dict.object(forKey: "categoryid") as! String == "3"
                            {
                                self.meatList.add(dict)
                            }
                            else if dict.object(forKey: "categoryid") as! String == "4"
                            {
                                self.seafoodList.add(dict)
                            }
                            else if dict.object(forKey: "categoryid") as! String == "5"
                            {
                                self.dairyList.add(dict)
                            }
                            else if dict.object(forKey: "categoryid") as! String == "6"
                            {
                                self.nutsList.add(dict)
                            }
//                            else if dict.object(forKey: "categoryid") as! String == "7"
//                            {
//                                fruitsList.add(dict)
//                            }
//                            else if dict.object(forKey: "categoryid") as! String == "8"
//                            {
//                                fruitsList.add(dict)
//                            }
//                            else if dict.object(forKey: "categoryid") as! String == "9"
//                            {
//                                fruitsList.add(dict)
//                            }
//                            else if dict.object(forKey: "categoryid") as! String == "10"
//                            {
//                                fruitsList.add(dict)
//                            }

                        }
                        
                        self.selectedList = self.fruitsList
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.likesTable.reloadData()
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func showOptions(_ sender: Any) {
        
        let btn = sender as! UIButton
        let ind = Int(btn.accessibilityIdentifier!)
        
        var i = 0
        
        for view in buttonUnderscores
        {
            if i == ind
            {
               view.isHidden = false
            }
            else
            {
                view.isHidden = true
            }
            
            i += 1
        }
        
        if btn.restorationIdentifier == "b1"
        {
            v1.isHidden = false
            v2.isHidden = true
            v3.isHidden = true
            v4.isHidden = true
            v5.isHidden = true
            v6.isHidden = true
            
            selectedList = fruitsList
            
            category = "0"
        }
        else if btn.restorationIdentifier == "b2"
        {
            v1.isHidden = true
            v2.isHidden = false
            v3.isHidden = true
            v4.isHidden = true
            v5.isHidden = true
            v6.isHidden = true
            
            selectedList = vegList
            
            category = "1"
        }
        else if btn.restorationIdentifier == "b3"
        {
            v1.isHidden = true
            v2.isHidden = true
            v3.isHidden = false
            v4.isHidden = true
            v5.isHidden = true
            v6.isHidden = true
            
            selectedList = meatList
            
            category = "2"
        }
        else if btn.restorationIdentifier == "b4"
        {
            v1.isHidden = true
            v2.isHidden = true
            v3.isHidden = true
            v4.isHidden = false
            v5.isHidden = true
            v6.isHidden = true
            
            selectedList = seafoodList
            
            category = "3"
        }
        else if btn.restorationIdentifier == "b5"
        {
            v1.isHidden = true
            v2.isHidden = true
            v3.isHidden = true
            v4.isHidden = true
            v5.isHidden = false
            v6.isHidden = true
            
            selectedList = dairyList
            
            category = "4"
        }
        else if btn.restorationIdentifier == "b6"
        {
            v1.isHidden = true
            v2.isHidden = true
            v3.isHidden = true
            v4.isHidden = true
            v5.isHidden = true
            v6.isHidden = false
            
            selectedList = nutsList
            
            category = "5"
        }
        
        currentButtons.removeAll()
        
        likesTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.selectedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : LikesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! LikesTableViewCell
        
        let dict = self.selectedList[(indexPath as NSIndexPath).row] as! NSDictionary
        
        let id = dict.object(forKey: "ingredientid") as? String
        var val = dict.object(forKey: "ingredient") as? String
        
        val = val!.replacingOccurrences(of: "\"", with: "", options: .literal, range: nil)
        val = val!.replacingOccurrences(of: "/", with: "", options: .literal, range: nil)
        val = val!.replacingOccurrences(of: "\\u2019", with: "'", options: .literal, range: nil)
                
        cell.likeBtn?.setBackgroundImage(UIImage.init(named: "gray-up"), for: .normal)
        cell.dislikeBtn.setBackgroundImage(UIImage.init(named: "gray"), for: .normal)
        
        cell.itemLbl?.text = val!
        cell.selectionStyle = .none
        
        let likeRestVal = "\(id!)-up"
        let dislikeRestVal = "\(id!)-down"
        
        cell.likeBtn?.restorationIdentifier = likeRestVal
        cell.dislikeBtn?.restorationIdentifier = dislikeRestVal
        
        if likedItems.contains(likeRestVal)
        {
            cell.likeBtn?.setBackgroundImage(UIImage.init(named: "green"), for: .normal)
        }
        else if dislikedItems.contains(dislikeRestVal)
        {
            cell.dislikeBtn?.setBackgroundImage(UIImage.init(named: "red"), for: .normal)
        }
        
        cell.likeBtn?.addTarget(self, action: #selector(self.likeItem(_:)), for: UIControlEvents.touchUpInside)
        cell.dislikeBtn?.addTarget(self, action: #selector(self.dislikeItem(_:)), for: UIControlEvents.touchUpInside)
        
        currentButtons.append(cell.likeBtn)
        currentButtons.append(cell.dislikeBtn)
        
        return cell
    }
    
    @objc func likeItem(_ sender : UIButton) {
        
        let btn = sender as UIButton
        let val = btn.restorationIdentifier
        
        if likedItems.contains(val!)
        {
            likedItems.remove(val!)
            btn.setBackgroundImage(UIImage.init(named: "gray-up"), for: .normal)
        }
        else
        {
            likedItems.add(val!)
            btn.setBackgroundImage(UIImage.init(named: "green"), for: .normal)
            
            if dislikedItems.contains(val!)
            {
                dislikedItems.remove(val!)
            }
            
            let prefix = val?.split(separator: "-")
            
            for btn1 in self.currentButtons {
                
                if btn1.restorationIdentifier == "\(prefix![0])-down"
                {
                    //print("set to gray")
                    dislikedItems.remove("\(prefix![0])-down")
                    btn1.setBackgroundImage(UIImage.init(named: "gray"), for: .normal)
                }
            }
        }
        
        print("likedItems: \(likedItems)")
        print("dislikedItems: \(dislikedItems)")
    }
    
    @objc func dislikeItem(_ sender : UIButton) {
        
        let btn = sender as UIButton
        let val = btn.restorationIdentifier
        
        if dislikedItems.contains(val!)
        {
            dislikedItems.remove(val!)
            btn.setBackgroundImage(UIImage.init(named: "gray"), for: .normal)
        }
        else
        {
            dislikedItems.add(val!)
            btn.setBackgroundImage(UIImage.init(named: "red"), for: .normal)
            
            if likedItems.contains(val!)
            {
                likedItems.remove(val!)
            }
            
            let prefix = val?.split(separator: "-")
            
            for  btn1 in self.currentButtons {
                
                if btn1.restorationIdentifier == "\(prefix![0])-up"
                {
                    likedItems.remove("\(prefix![0])-up")
                    //print("set to gray")
                    btn1.setBackgroundImage(UIImage.init(named: "gray-up"), for: .normal)
                }
            }
        }
        
        print("likedItems: \(likedItems)")
        print("dislikedItems: \(dislikedItems)")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //println("You selected cell #\(indexPath.row)!")
        
        //selectedIem = self.items[(indexPath as NSIndexPath).row]
        
        //self.performSegue(withIdentifier: "GoToCategoryCards", sender: self)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        //        if textField.restorationIdentifier == "other" && textField.text != ""
        //        {
        //            cuisines.add(otherTxt.text!)
        //            otherTxt.text = ""
        //            cuisineCV.reloadData()
        //        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "AddIngredients"
        {
            let destination = segue.destination as! AddIngredients
            destination.category = category
        }
        else if segue.identifier == "NutritionalGoals"
        {
            let destination = segue.destination as! NutritionalGoals
            destination.newFamilyMemberID = newFamilyMemberID
            destination.firstname = firstname
        }
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
