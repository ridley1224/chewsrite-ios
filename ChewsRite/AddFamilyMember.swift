//
//  AddFamilyMember.swift
//  ChewsRite
//
//  Created by DTO MacBook11 on 11/6/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class AddFamilyMember: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var pregnantView: UIView!
    @IBOutlet weak var instructionsLbl: UILabel!
    @IBOutlet weak var genderSegment: UISegmentedControl!
    
    var isPregnant : Bool?
    var isBreastfeeding : Bool?
    
    @IBOutlet weak var fullnameLbl: UILabel!
    @IBOutlet weak var fullnameTxt: UITextField!
    @IBOutlet weak var headerLbl: UILabel!
    
    @IBOutlet weak var dobLbl: UILabel!
    @IBOutlet weak var dobTxt: UITextField!
    @IBOutlet weak var heightTxt: UITextField!
    @IBOutlet weak var weightTxt: UITextField!
    
    @IBOutlet weak var dobTxtTopPadding: NSLayoutConstraint!
    @IBOutlet weak var fullnameTxtTopPadding: NSLayoutConstraint!
    
    @IBOutlet weak var dobPicker: UIDatePicker!
    
    var selectedDateString : String?
    var selectedDate : Date?
    
    var footList = NSMutableArray()
    var inchList = NSMutableArray()
    var weightList = NSMutableArray()
    
    var editingIndex : Int?
    
    var selectedFoot : Int?
    var selectedInch : Int?
    var selectedWeight : Int?
    
    @IBOutlet weak var heightWeightPicker: UIPickerView!
    
    @IBOutlet weak var heightView: UIView!
    
    @IBOutlet weak var heightViewHeight: NSLayoutConstraint!
    
    var selectedRelationship : NSMutableDictionary?
    
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var relationshipTxt: UITextField!
    
    var imagePicker : UIImagePickerController!
    var selectedImage : UIImage?
    
    var statusImageCache = [String:UIImage]()
    
    var uploadImageName : String?
    
    @IBOutlet weak var imageIV : UIImageView!
    @IBOutlet weak var pickersView: UIView!
    
    @IBOutlet weak var pickerHeaderLbl: UILabel!
    @IBOutlet weak var relationshipLbl: UILabel!
    
    var newFamilyMemberID: Int?
    var relationshipid : Int?
    
    var selectedMember : NSMutableDictionary?
    
    var isNewPhoto : Bool?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        activityView.isHidden = true
        
        heightWeightPicker.delegate = self
        heightWeightPicker.dataSource = self
        
        selectedFoot = 5
        selectedInch = 5
        selectedWeight = 125
        
        //debugUser()
        
        for i in 4 ..< 8
        {
            let val = "\(i)"
            footList.add(val)
        }
        
        for i in 0 ..< 12
        {
            let val = "\(i)"
            inchList.add(val)
        }
        
        for i in 0 ..< 501
        {
            let val = "\(i)"
            weightList.add(val)
        }
    
        heightWeightPicker.reloadAllComponents()
        pickersView.isHidden = true
        
        heightView.isHidden = true
        heightViewHeight.constant = 0
        
        //activityView.isHidden = true
        
        dobTxtTopPadding.constant = 0
        //fullnameTxtTopPadding.constant = 0
        
        //years in days * seconds in a day
        
        selectedDate = Date(timeInterval: -6570*86400, since: NSDate() as Date)
        
        //dobPicker.setDate(NSDate() as Date, animated: false)
        dobPicker.setDate(selectedDate!, animated: false)
        
        //pregnantView.isHidden = true
        fullnameLbl.isHidden = true
        dobLbl.isHidden = true
        relationshipLbl.isHidden = true
        
        let attributedString = NSMutableAttributedString(string: "* You can change this from your Account Settings", attributes: [
            .font: UIFont(name: "Avenir-Book", size: 12.0)!,
            .foregroundColor: appDelegate.crWarmGray
            ])
        attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 12.0)!, range: NSRange(location: 32, length: 16))
        
        //instructionsLbl.attributedText = attributedString
        
        let btnItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(dismissKeyboard))
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width:  self.view.frame.size.width, height: 50))
        numberToolbar.backgroundColor = UIColor.darkGray
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.tintColor = UIColor.black
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            btnItem]
        
        numberToolbar.sizeToFit()
        
        fullnameTxt.inputAccessoryView = numberToolbar
        dobTxt.inputAccessoryView = numberToolbar
        
        //debug()
        
        if selectedMember != nil
        {
            headerLbl.text = "Edit Family Member"
            fullnameTxt.text = selectedMember?.object(forKey: "fullname") as? String
            
            selectedDateString = appDelegate.convertDateToSQLDate(date: dobPicker.date)
            selectedDate = dobPicker.date
            
            let df = DateFormatter()
            df.dateFormat = "MM/d/yy"
            
            let displayString = df.string(from: dobPicker.date)
            
            dobTxt.text = displayString
            
            heightViewHeight.constant = 72
            heightView.isHidden = false
            
            selectedWeight = 200
            selectedFoot = 6
            selectedInch = 0
            
            let height = selectedMember?.object(forKey: "height") as? String
            let heightArr = height?.split(separator: " ")
            
            if (heightArr?.count)! > 0
            {
                heightTxt.text = "\(heightArr![0])ft \(heightArr![1])in"
            }
            
            weightTxt.text = selectedMember?.object(forKey: "weight") as? String
            
            relationshipid = Int((selectedMember?.object(forKey: "relationshipid") as? String)!)
            relationshipTxt.text = selectedMember?.object(forKey: "title") as? String
            
            newFamilyMemberID = Int((selectedMember?.object(forKey: "userid") as? String)!)
            
            uploadImageName = selectedMember?.object(forKey: "userimage") as? String
            
            let gender = selectedMember?.object(forKey: "gender") as? String
            
            genderSegment.selectedSegmentIndex = Int(gender!)!
            
            relationshipLbl.isHidden = false
            fullnameLbl.isHidden = false
            dobLbl.isHidden = false
            
            if selectedImage != nil
            {
                imageIV.image = selectedImage
            }
        }
        else
        {
            uploadImageName = "\(appDelegate.randomString(length: 10)).jpeg"
        }
        
        let v = UIView.init(frame: CGRect.init(x: 0, y: profileBtn.frame.height-25, width: 300, height: 50))
        v.backgroundColor = .white
        v.isUserInteractionEnabled = false
        
        profileBtn.addSubview(v)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(selectedRelationshipNotification),
            name: NSNotification.Name(rawValue: "selectedRelationship"),
            object: nil)
    }
    
    func debugUser () {
        
        fullnameTxt.text = "RR"
        
        selectedDateString = appDelegate.convertDateToSQLDate(date: dobPicker.date)
        selectedDate = dobPicker.date
        
        let df = DateFormatter()
        df.dateFormat = "MM/d/yy"
        
        let displayString = df.string(from: dobPicker.date)
        
        dobTxt.text = displayString
        
        heightViewHeight.constant = 72
        heightView.isHidden = false
        
        selectedWeight = 200
        selectedFoot = 6
        selectedInch = 0
        
        heightTxt.text = "6ft 0in"
        weightTxt.text = "200 lbs"
        
        relationshipTxt.text = "Father"
        relationshipid = 1
        
        uploadImageName = "\(appDelegate.randomString(length: 10)).jpeg"
    }
    
    @objc func selectedRelationshipNotification (notification: NSNotification) {
        
        selectedRelationship = notification.object as? NSMutableDictionary
        relationshipid = Int((selectedRelationship?.object(forKey: "id") as? String)!)
        
        relationshipLbl.isHidden = false
        
        relationshipTxt.text = selectedRelationship?.object(forKey: "title") as? String
        heightViewHeight.constant = 72
        heightView.isHidden = false
    }
    
    @objc func dismissKeyboard () {
        
        self.view.endEditing(true)
        
        if fullnameTxt.text != ""
        {
            fullnameLbl.isHidden = false
        }
    }
    
    @IBAction func showDatePopup(_ sender: Any) {
        
        dismissKeyboard()
        
        let btn = sender as! UIButton
        
        editingIndex = Int(btn.restorationIdentifier!)
        
        if editingIndex == 0 || editingIndex == 1
        {
            heightWeightPicker.reloadAllComponents()
            dobPicker.isHidden = true
            
            if editingIndex == 0
            {
                pickerHeaderLbl.text = "Height"
                
                heightWeightPicker.selectRow(selectedFoot!-1, inComponent: 0, animated: false)
                heightWeightPicker.selectRow(selectedInch!-1, inComponent: 1, animated: false)
            }
            else
            {
                pickerHeaderLbl.text = "Weight"
                
                heightWeightPicker.selectRow(selectedWeight!-1, inComponent: 0, animated: false)
            }
            
            heightWeightPicker.isHidden = false
        }
        else
        {
            pickerHeaderLbl.text = "Date of Birth"
            
            //dobPicker.reloadInputViews()
            heightWeightPicker.isHidden = true
            dobPicker.isHidden = false
            
            dobPicker.setDate(selectedDate!, animated: false)
        }
        
        pickersView.isHidden = false
    }
    
    @IBAction func selectCusines(_ sender: Any) {
        
        if fullnameTxt.text == ""
        {
            self.showBasicAlert(string: "Please Enter Full Name")
            return
        }

        if dobTxt.text == ""
        {
            self.showBasicAlert(string: "Please Enter Date of Birth")
            return
        }
        
        if heightTxt.text == ""
        {
            self.showBasicAlert(string: "Please Enter Height")
            return
        }
        
        if weightTxt.text == ""
        {
            self.showBasicAlert(string: "Please Enter Weight")
            return
        }
        
        if selectedImage == nil
        {
            self.showBasicAlert(string: "Please select image")
            return
        }
        
        if selectedMember != nil
        {
            updateFamilyMember()
        }
        else
        {
            uploadFamilyData()
        }
    }
    
    func showBasicAlert(string:String) {
        
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    @IBAction func selectPicker(_ sender: Any) {
        
        pickersView.isHidden = true
        
        if editingIndex == 0
        {
            heightTxt.text = "\(selectedFoot!) ft, \(selectedInch!) in"
        }
        else if editingIndex == 1
        {           heightWeightPicker.isHidden  = false
            
            weightTxt.text = "\(selectedWeight!) lbs"
        }
        else
        {
            dobLbl.isHidden = false

            selectedDateString = appDelegate.convertDateToSQLDate(date: dobPicker.date)
            selectedDate = dobPicker.date
            
            let df = DateFormatter()
            df.dateFormat = "MM/d/yy"
            
            let displayString = df.string(from: dobPicker.date)
            
            dobTxt.text = displayString
            pickersView.isHidden = true
        }
    }
    
    @IBAction func selectDate(_ sender: Any) {
        
        dobLbl.isHidden = false
        
        selectedDateString = appDelegate.convertDateToSQLDate(date: dobPicker.date)
        selectedDate = dobPicker.date
        
        let df = DateFormatter()
        df.dateFormat = "MM/d/yy"
        
        let displayString = df.string(from: dobPicker.date)
        
        dobTxt.text = displayString
        pickersView.isHidden = true
    }
    
    @IBAction func manageGender(_ sender: Any) {
        
        if genderSegment.selectedSegmentIndex != 0
        {
            pregnantView.isHidden = false
        }
        else
        {
            pregnantView.isHidden = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.restorationIdentifier == "fullname" && textField.text != ""
        {
            fullnameLbl.isHidden = false
            //usernameLblHeight.constant = 18
            
            //fullnameTxtTopPadding.constant = 0
        }
        else if textField.restorationIdentifier == "dob" && textField.text != ""
        {
            dobLbl.isHidden = false
            //emailLblHeight.constant = 18
            dobTxtTopPadding.constant = 5
        }
    }
    
    func updateFamilyMember () {
        
        submitBtn.isEnabled = false
        
        activityView.isHidden = false
        activityView.startAnimating()
        
        print("updateFamilyMember")
        
        let urlString = "\(appDelegate.serverDestination!)addFamilyMember.php"
        
        print("urlString: \(urlString)")
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var paramString = "fullname=\(fullnameTxt.text!)&relationshipid=\(relationshipid!)&dob=\(selectedDateString!)&gender=\(genderSegment.selectedSegmentIndex)&weight=\(selectedWeight!)&height=\(selectedFoot!) \(selectedInch!)&dob=\(appDelegate.convertDateToSQLDate(date: selectedDate!))&userimage=\(uploadImageName!)"
        
        paramString = "\(paramString)&updateprofile=true&familyid=\(newFamilyMemberID!)&devStatus=\(appDelegate.devStage!)"
        
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("update user jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict["userData"]! is NSNull
                {
                    print("no user")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.submitBtn.isEnabled = true
                    })
                }
                else
                {
                    let userDict : NSDictionary = dataDict["userData"] as! NSDictionary
                    
                    let status : String = userDict["status"] as! String
                    
                    print("status: \(status)")
                    
                    if (status == "family member updated")
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            if self.isNewPhoto == true
                            {
                                self.uploadS3()
                            }
                            else
                            {
                                NotificationCenter.default.post(name: Notification.Name("refreshFamily"), object: nil)
                                self.navigationController?.popViewController(animated: true)
                            }
                            //self.performSegue(withIdentifier: "SelectCuisines", sender: self)
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            
                            self.showBasicAlert(string: "Please check internet connection.")
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func uploadFamilyData() {
        
        submitBtn.isEnabled = false
        submitBtn.backgroundColor = appDelegate.crWarmGray
        
        activityView.isHidden = false
        activityView.startAnimating()
        
        print("uploadUserData")
        
        let urlString = "\(appDelegate.serverDestination!)addFamilyMember.php"

        print("urlString: \(urlString)")
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var paramString = "fullname=\(fullnameTxt.text!)&relationshipid=\(relationshipid!)&dob=\(selectedDateString!)&gender=\(genderSegment.selectedSegmentIndex)&weight=\(selectedWeight!)&height=\(selectedFoot!) \(selectedInch!)&dob=\(appDelegate.convertDateToSQLDate(date: selectedDate!))&userimage=\(uploadImageName!)"
        
        paramString = "\(paramString)&newprofile=true&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("add user jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict["userData"]! is NSNull
                {
                    print("no user")
                    
                    self.activityView.isHidden = true
                    self.activityView.stopAnimating()
                    
                    self.submitBtn.isEnabled = true
                    self.submitBtn.backgroundColor = self.appDelegate.crOrange
                }
                else
                {
                    let userDict : NSDictionary = dataDict["userData"] as! NSDictionary
                    
                    let status : String = userDict["status"] as! String
                    
                    self.newFamilyMemberID = userDict["newuserid"] as? Int
                    
                    print("status: \(status)")
                    
                    if (status == "family member saved")
                    {
                        DispatchQueue.main.sync(execute: {
                            
//                            self.activityView.isHidden = true
//                            self.activityView.stopAnimating()
                            
                            self.uploadS3()
                            
                            //self.performSegue(withIdentifier: "SelectCuisines", sender: self)
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            
                            self.submitBtn.isEnabled = true
                            self.submitBtn.backgroundColor = self.appDelegate.crOrange
                            
                            self.showBasicAlert(string: "Please check internet connection.")
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func cancel() {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Pickerview
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        if editingIndex == 0
        {
            return 2
        }
        else
        {
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if editingIndex == 0
        {
            if component == 0
            {
                return footList.count
            }
            else
            {
                return inchList.count
            }
        }
        else
        {
            return weightList.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if editingIndex == 0
        {
            if component == 0
            {
                let str = footList[row] as! String
                var comp = str.components(separatedBy: " ")
                selectedFoot = Int(comp[0])!
            }
            else
            {
                let str = inchList[row] as! String
                var comp = str.components(separatedBy: " ")
                selectedInch = Int(comp[0])!
            }
        }
        else
        {
            if component == 0
            {
                let str = weightList[row] as! String
                var comp = str.components(separatedBy: " ")
                selectedWeight = Int(comp[0])!
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var str = ""
        
        if editingIndex == 0
        {
            if component == 0
            {
                str = "\(footList[row]) ft"
            }
            else
            {
                str = "\(inchList[row]) in"
            }
        }
        else
        {
            if component == 0
            {
                if Int(weightList[row] as! String) == 501
                {
                    str = "\(weightList[row])+ lbs"
                }
                else
                {
                    str = "\(weightList[row]) lbs"
                }
            }
        }
        
        return str
    }
    
    @IBAction func selectPhoto(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "Change your profile picture", preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Open Photo Library", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
                self.choosePhoto()
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
                self.takePhoto()
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    // MARK: Image Picker
    
    func takePhoto () {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func choosePhoto () {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        print("cancel")
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        print("did pick")
        
        selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        imageIV.image = selectedImage
        
        isNewPhoto = true
        
        self.dismiss(animated: true, completion: {
            
            //self.uploadToServer(name:"registration-\(self.appDelegate.userid!)")
        });
    }
    
    func uploadS3 () {
                
        let image = selectedImage!
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(uploadImageName!)
        let imageData = UIImageJPEGRepresentation(image, 0)
        fileManager.createFile(atPath: path as String, contents: imageData, attributes: nil)
        
        let fileUrl = NSURL(fileURLWithPath: path)
        
        print("upload")
        
        let transferManager = AWSS3TransferManager.default()
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.bucket = "chewsrite/images"
        uploadRequest!.key = uploadImageName
        uploadRequest?.body = fileUrl as URL
        uploadRequest?.contentType = "image/jpeg"
        
        uploadRequest?.acl = AWSS3ObjectCannedACL.publicReadWrite;
        
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            
            if let error = task.error as NSError? {
                
                if error.domain == AWSS3TransferManagerErrorDomain,
                    
                    let code = AWSS3TransferManagerErrorType(rawValue: error.code)
                {
                    switch code
                    {
                    case .cancelled, .paused:
                        break
                    default:
                        print("Error uploading 1: \(uploadRequest?.key!) Error: \(error)")
                    }
                }
                else
                {
                    print("Error uploading: \(uploadRequest?.key!) Code: \(error.code) Error: \(error)")
                }
                
                return nil
            }
            
            //let uploadOutput = task.result
            
            self.activityView.isHidden = true
            self.activityView.stopAnimating()
            
            print("Upload complete for: \(uploadRequest!.key!)")
            
            if self.selectedMember != nil
            {
                let urlString = "\(self.appDelegate.serverDestination!)userimages/\(self.uploadImageName!)"
                
                NotificationCenter.default.post(name: Notification.Name("refreshFamily"), object: urlString)
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                self.performSegue(withIdentifier: "FavoriteCuisines", sender: self)
            }
            
            //self.updateUserImage()
            
            return nil
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "FavoriteCuisines"
        {
            //update newFamilyMemberID for favorites, dietary concerns, likes, nutrition on server, add db table for family members
            
            let destination = segue.destination as! FavoriteCuisines
            destination.newFamilyMemberID = newFamilyMemberID
            
            let str = fullnameTxt.text?.split(separator: " ")
            destination.firstname = "\(str![0])"
            
            destination.isEditing = true
        }
    }
}
