//
//  MyRecipes.swift
//  ChewsRite
//
//  Created by Randall Ridley on 8/20/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

import AVFoundation
import AVKit

class MyRecipes: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var recipeCV: UICollectionView!
    
    var displayItems = [NSMutableDictionary]()
    var cuisineList = [NSMutableDictionary]()
    var filteredList = [NSMutableDictionary]()
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var recipeStatusTV: UITextView!
    
    @IBOutlet weak var cuisineCV: UICollectionView!
    @IBOutlet weak var buttonsScrollview: UIScrollView!
    @IBOutlet weak var search: UISearchBar!

    var selectedCuisine = 0
    var mealBtns = [UIButton]()
    var isFiltered : Bool?
    
    var itemArray = [FeedItem]()
    var currentItemArray = [FeedItem]()
    
    var statusImageCache = [String:UIImage]()
    var selectedItem : FeedItem?
    
    @IBOutlet weak var menuBtn: UIButton!
    
    @IBOutlet weak var familyCV: UICollectionView!
    var selectedUser : NSDictionary?
    var familyList = [NSDictionary]()
    
    var selectedVideo : Video?
    
    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        selectedUser = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!, "usertype":"0"] as NSDictionary
        familyList.append(selectedUser!)
        
        if appDelegate.userImage != nil
        {
            let urlString = "\(appDelegate.serverDestination!)userimages/\(appDelegate.profileImg!)"
            self.statusImageCache[urlString] = appDelegate.userImage
        }
        
        familyCV.delegate = self
        familyCV.dataSource = self
        
        recipeStatusTV.isHidden = true
        activityView.isHidden = true
        
        recipeCV.delegate = self
        recipeCV.dataSource = self
        
        search.delegate = self
        search.backgroundImage = UIImage()
        
        search.placeholder = "Search Recipes"
        
        UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        
        let tb : TabController = self.parent as! TabController
        
        menuBtn.addTarget(self, action: #selector(tb.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
        getMyCuisines()
        getMyRecipes()
        getFamily()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reloadCuisinesNotificationName),
            name: NSNotification.Name(rawValue: "reloadCuisines"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(manageFavoriteNotificationName),
            name: NSNotification.Name(rawValue: "manageFavorite"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(refreshRecipeNotification),
            name: NSNotification.Name(rawValue: "refreshRecipes"),
            object: nil)
    }
    
    @objc func refreshRecipeNotification (notification: NSNotification) {
        
        //manageFavorite(recipeID: notification.object as! NSDictionary)
        
        getMyRecipes()
    }
    
    @objc func manageFavoriteNotificationName (notification: NSNotification) {
        
        //manageFavorite(recipeID: notification.object as! NSDictionary)
    }
    
    @objc func reloadCuisinesNotificationName (notification: NSNotification) {
        
        selectedCuisine = Int(notification.object as! String)!
    }
    
    @IBAction func changeCuisine(_ sender: Any) {
        
        let btn1 = sender as? UIButton
        
        for btn in mealBtns
        {
            if btn != btn1
            {
                btn.backgroundColor = .clear
                btn.setTitleColor(appDelegate.crLightBlue, for: .normal)
            }
            else
            {
                btn.backgroundColor = appDelegate.crLightBlue
                btn.setTitleColor(.white, for: .normal)
            }
        }
        
        if btn1?.titleLabel?.text == "Favorite"
        {
            currentItemArray = itemArray.filter({ item -> Bool in
                
                return item.isFavorite == "1"
            })
        }
        else
        {
            selectedCuisine = Int((btn1?.restorationIdentifier)!)!
            
            print("selectedCuisine: \(selectedCuisine)")
            
            if selectedCuisine == 0
            {
                isFiltered = false
                currentItemArray = itemArray
            }
            else
            {
                //change filter
                
                currentItemArray = itemArray.filter({ item -> Bool in
                    
                    let tags = item.cuisinetags.components(separatedBy: ",")
                    
                    print("tags: \(tags)")
                    
                    return tags.contains("\(selectedCuisine)".lowercased())
                    
                    //return item.cuisinetags.lowercased().contains("\(selectedCuisine)".lowercased())
                })
            }
        }
        
        recipeCV.reloadData()
    }
    
    // MARK: Webservice
    
    func getFamily() {
        
        familyList.removeAll()
        
        print("getFamily")
        
        let dataString = "familyData"
        
        let urlString = "\(appDelegate.serverDestination!)getFamilyMembers.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        selectedUser = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!, "usertype":"0"] as NSDictionary
        familyList.append(selectedUser!)
        
        self.familyCV.reloadData()
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("family jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
//                        self.familyCV.isHidden = true
//                        self.familyCV.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let members = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (members.count > 0)
                    {
                        hasData = true
                        
                        for dict in members
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.familyList.append(dict2)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if hasData == true
                            {
                                self.familyCV.isHidden = false
                                self.familyCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
//                                self.familyCV.isHidden = true
//                                self.familyCV.reloadData()
                            }
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
            }.resume()
    }
    
    func getMyCuisines() {
        
        displayItems.removeAll()
        
        print("getMyCuisines")
        
        let dataString = "cuisineData"
        
        let urlString = "\(appDelegate.serverDestination!)getCuisines.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("cuisines jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.recipeStatusTV.isHidden = false
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        var ind = 0
                        
                        DispatchQueue.main.sync(execute: {
                            
                            for dict in recipes1
                            {
                                let dict2 = dict.mutableCopy() as! NSMutableDictionary
                                
                                self.cuisineList.append(dict2)
                                
                                let str = dict2.object(forKey: "cuisinename") as? String
                                
//                                let ct = str?.characters.count
//
//                                var width = ct!*11
//
//                                print("ct: \(ct)")
//
//                                if width < 140
//                                {
//                                    width = 140
//                                }
                                
                                let width = 150
                                
                                let btn = DesignableButton.init(frame: CGRect.init(x: ind*(width+5), y: 0, width: width, height: 36))
                                
                                btn.addTarget(self, action: #selector(self.changeCuisine(_:)), for: UIControlEvents.touchUpInside)
                                
                                btn.cornerRadius = 18
                                btn.borderWidth = 1
                                btn.borderColor = self.appDelegate.crLightBlue
                                
                                if(ind == 0)
                                {
                                    btn.backgroundColor = self.appDelegate.crLightBlue
                                    btn.setTitleColor(.white, for: .normal)
                                }
                                else
                                {
                                    btn.backgroundColor = .clear
                                    btn.setTitleColor(self.appDelegate.crLightBlue, for: .normal)
                                }
                                
                                btn.setTitle(str, for: .normal)
                                btn.titleLabel?.font =  UIFont(name: "Avenir-Book", size: 16)
                                
                                //.font: UIFont(name: "Avenir-Book", size: 13.0)!,
                                
                                let cuisineid = Int(dict2.object(forKey: "cuisineid") as! String)
                                
                                if (cuisineid != nil)
                                {
                                    btn.restorationIdentifier = "\(cuisineid!)"
                                }
                                
                                self.buttonsScrollview.addSubview(btn)
                                
                                self.mealBtns.append(btn)
                                
                                ind += 1
                            }
                            
                            self.buttonsScrollview.contentSize = CGSize.init(width: (150*ind)+10, height: Int(self.buttonsScrollview.frame.height))
                            
                            self.activityView.stopAnimating()
                            self.activityView.isHidden = true
                            
                            if hasData == true
                            {
                                //self.recipeStatusTV.text = "You currently have no recipes."
                                
                                //self.updateDisplayList()
                                
                                //self.cuisineCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
                                //self.noResultsMain.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getMyRecipes() {
        
        displayItems.removeAll()
        itemArray.removeAll()
        currentItemArray.removeAll()
        
        //displayItems.removeAll()
        
        print("getMyRecipes")
        
        let dataString = "recipeData"
        
        let urlString = "\(appDelegate.serverDestination!)getRecipes.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var paramString = "allfavorites=true&devStatus=\(appDelegate.devStage!)"
                
        if selectedUser != nil
        {
            let usertype = selectedUser?.object(forKey: "usertype") as! String
            
            if usertype == "0"
            {
                paramString = "\(paramString)&userid=\(selectedUser?.object(forKey: "userid") as! String)"
            }
            else
            {
                paramString = "\(paramString)&userid=\(selectedUser?.object(forKey: "familyid") as! String)"
            }
        }
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("my recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.recipeCV.reloadData()
                        self.recipeStatusTV.isHidden = false
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.displayItems.append(dict2)
                            
                            self.itemArray.append(FeedItem(recipename: dict2.object(forKey: "recipename") as! String,
                                                           preptime: dict2.object(forKey: "preptime") as! String,
                                                           cooktime: dict2.object(forKey: "cooktime") as! String,
                                                           imagename: dict2.object(forKey: "imagename") as! String,
                                                           videoname: dict2.object(forKey: "videoname") as! String,
                                                           calories: dict2.object(forKey: "calories") as! String,
                                                           source: dict2.object(forKey: "source") as! String, sourceurl: dict2.object(forKey: "sourceurl") as! String,
                                                           isFavorite: dict2.object(forKey: "isFavorite") as! String,
                                                           username: dict2.object(forKey: "username") as! String,
                                                           recipeid: dict2.object(forKey: "recipeid") as! String,
                                                           cuisinetags: dict2.object(forKey: "cuisinetags") as! String,
                                                           mealplanid: "", mealplandate: "", mealid: "",
                                                           userimage: dict2.object(forKey: "userimage") as! String,
                                                           firstname: dict2.object(forKey: "firstname") as! String,
                                                           lastname: dict2.object(forKey: "lastname") as! String,
                                                           description: dict2.object(forKey: "recipedescription") as! String,
                                                           servings: dict2.object(forKey: "servings") as! String,
                                                           userid: dict2.object(forKey: "userid") as! String,
                                                           totaltime: dict2.object(forKey: "totaltime") as! String,
                                                           rating: dict2.object(forKey: "rating") as! String, timeAdded: dict2.object(forKey: "timeAdded") as! String))
                        }
                        
                        self.currentItemArray = self.itemArray
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.stopAnimating()
                            self.activityView.isHidden = true
                            self.recipeStatusTV.isHidden = true
                            
                            if hasData == true
                            {
                                self.recipeStatusTV.text = "You currently have no recipes."
                                
                                self.updateDisplayList()
                            }
                            else
                            {
                                print("no data")
                                
                                //self.noResultsMain.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func updateDisplayList () {
        
        recipeCV.reloadData()
    }
    
    // MARK: Search
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        currentItemArray = itemArray.filter({ item -> Bool in
            
            switch searchBar.selectedScopeButtonIndex {
            case 0:
                if searchText.isEmpty { return true }
                return item.recipename.lowercased().contains(searchText.lowercased())
            default:
                return false
            }
        })
        
        recipeCV.reloadData()
    }
    
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        
        //print("clear")
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.restorationIdentifier == "family"
        {
            return CGSize.init(width: 42, height: 42)
        }
        else
        {
            let kWhateverHeightYouWant = 190
            
            return CGSize.init(width: 600, height: CGFloat(kWhateverHeightYouWant))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.restorationIdentifier == "family"
        {
            return self.familyList.count
        }
        else
        {
            return currentItemArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.restorationIdentifier == "family"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FamilyCVCell
            
            let dict = familyList[indexPath.row]
            
            let imagename = dict.object(forKey: "userimage") as? String
            
            if selectedUser == dict
            {
                let img = UIImage.init(named: "selectWhite")
                cell?.si.image = img
            }
            else
            {
                cell?.si.image = nil
            }
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                
                //let url2 = NSURL(string:urlString)!
                
                // If this image is already cached, don't re-download
                
                if let img = statusImageCache[urlString]
                {
                    //print("load cached status image")
                    
                    cell?.userImg.image = img
                }
                else
                {
                    //print("status image not
                    
                    let imagename = dict.object(forKey: "userimage") as? String
                    
                    if imagename != nil && imagename != ""
                    {
                        let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                        
                        print("img urlString: \(urlString)")
                        
                        if let img = statusImageCache[urlString]
                        {
                            cell?.userImg.image = img
                        }
                        else
                        {
                            cell?.userImg.image = UIImage(named: "profile-icon")
                            
                            self.downloadImage(imagename: imagename!, urlString: urlString, iv: (cell?.userImg!)!)
                        }
                    }
                    else
                    {
                        cell?.userImg.image = UIImage(named: "profile-icon")
                    }
                }
            }
            
            return cell!
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FeaturedRecipeCell
            
            let item = currentItemArray[indexPath.row]
            
            //cell?.recipeIV.image = UIImage.init(named: item.imagename)
            
            let imagename = item.imagename
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
                
                if let img = statusImageCache[urlString]
                {
                    cell?.recipeIV.image = img
                }
                else
                {
                    cell?.recipeIV.image = UIImage(named: "profile-icon")
                    
                    self.downloadImage(imagename: imagename, urlString: urlString, iv: (cell?.recipeIV!)!)
                }
            }
            
            cell?.headerLbl.text = item.recipename
            cell?.infoLbl.text = "\(item.preptime) • \(item.calories)"
            
            let attributedString = NSMutableAttributedString(string: "Source: \(item.source)", attributes: [
                .font: UIFont(name: "Avenir-Book", size: 13.0)!,
                .foregroundColor: appDelegate.crGray
                ])
            attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 13.0)!, range: NSRange(location: 0, length: 7))
            
            cell?.sourceLbl.attributedText = attributedString
            
            cell?.favoriteBtn.restorationIdentifier = item.recipeid
            cell?.favoriteBtn.accessibilityIdentifier = "\(indexPath.row)"
            
            let video = item.videoname
            
            if video == ""
            {
                cell?.videoBtn.isHidden = true
                //cell?.pl?.isHidden = true
            }
            else
            {
                cell?.videoBtn.isHidden = false
                //cell?.pl?.isHidden = false
                cell?.videoURL = video
                
                cell?.videoBtn.addTarget(self, action: #selector(self.playVideo2(_:)), for: UIControlEvents.touchUpInside)
                
                cell?.videoBtn.restorationIdentifier = "\(video)"
            }            
            
            let isFavorite = item.isFavorite
            
            if isFavorite == "1"
            {
                cell?.favoriteBtn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
                cell?.isFavorite = true
                //dict?.setValue(true, forKey: "isFavorite")
            }
            else
            {
                cell?.favoriteBtn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
                cell?.isFavorite = false
                //dict?.setValue(false, forKey: "isFavorite")
            }
            
            cell?.favoriteBtn.addTarget(self, action: #selector(self.manageFavorite(_:)), for: UIControlEvents.touchUpInside)
            
            return cell!
        }
    }
    
    //var videos = Video.allVideos()
 
    @IBAction func playVideo2(_ sender: Any) {
        
        //https://www.raywenderlich.com/5191-video-streaming-tutorial-for-ios-getting-started
        
        print("play video2")

        let btn = sender as! UIButton
        
        //let video = videos[0]
        
        let url = "https://s3.us-east-2.amazonaws.com/shotgraph1224/chewsrite/\(btn.restorationIdentifier!)"
        
        selectedVideo = Video(url: URL.init(string: url)!, thumbURL: URL.init(string: url)!, title: "", subtitle: "")
        
        let videoURL = selectedVideo?.url
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        present(playerViewController, animated: true) {
            player.play()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.restorationIdentifier == "family"
        {
            selectedUser = familyList[indexPath.row]
            
            getMyRecipes()
            
            //
            //            print("selectedUser: \(selectedUser)")
            
            familyCV.reloadData()
        }
        else
        {
            selectedItem = currentItemArray[indexPath.row]
            
            performSegue(withIdentifier: "ViewRecipe", sender: nil)
        }
    }
    
    func downloadImage (imagename : String, urlString : String, iv: UIImageView ) {
        
        if appDelegate.downloadImages == true
        {
            
        let s3BucketName = "chewsrite/images"
        
        let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
        let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
        
        // Set the logging to verbose so we can see in the debug console what is happening
        //AWSLogger.default().logLevel = .none
        
        let downloadRequest = AWSS3TransferManagerDownloadRequest()
        downloadRequest?.bucket = s3BucketName
        downloadRequest?.key = imagename
        downloadRequest?.downloadingFileURL = downloadingFileURL
        
        let transferManager = AWSS3TransferManager.default()
        
        //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
        
        transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
            (task: AWSTask!) -> AWSTask<AnyObject>? in
            
            DispatchQueue.main.async(execute: {
                
                if task.error != nil
                {
                    print("AWS Error downloading image")
                    
                    print(task.error.debugDescription)
                }
                else
                {
                    //print("AWS download successful")
                    
                    var downloadOutput = AWSS3TransferManagerDownloadOutput()
                    
                    downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                    
                    print("downloadOutput photo: \(downloadOutput)");
                    print("downloadFilePath photo: \(downloadFilePath)");
                    
                    let image = UIImage(contentsOfFile: downloadFilePath)
                    
                    self.statusImageCache[urlString] = image
                    iv.image = image
                    
                    if urlString.contains(self.appDelegate.profileImg!)
                    {
                        self.appDelegate.userImage = image
                    }
                }
                
                //println("test")
            })
            return nil
        }))
        }
        else
        {
            iv.image = UIImage.init(named: "profile-icon")
        }
    }
    
    // MARK: Actions
    
    @IBAction func manageFavorite(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        let ind = Int(btn.accessibilityIdentifier!)!
        
        let item = currentItemArray[ind]
        
        if item.isFavorite == "0"
        {
            item.isFavorite = "1"
        }
        else
        {
            item.isFavorite = "0"
            
            var ind1 = 0
            
            for it in itemArray
            {
                if it.recipeid == item.recipeid
                {
                    itemArray.remove(at: ind1)
                }
                
                ind1 += 1
            }
            
            //let it = itemArray.inde
            
            currentItemArray.remove(at: ind)
        }
        
        recipeCV.reloadData()
        
        manageFavorite(recipeID: btn.restorationIdentifier!)
    }
    
    func manageFavorite( recipeID : String) {
        
        print("manageFavorite")
        
        let dataString = "favoriteData"
        
        let urlString = "\(appDelegate.serverDestination!)manageFavorites.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "recipeid=\(recipeID)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
//                        self.activityView.stopAnimating()
//                        self.activityView.isHidden = true
                    })
                    
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "favorite item saved" || status == "favorite deleted"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            NotificationCenter.default.post(name: Notification.Name("refreshHomeFeed"), object: nil)
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
     // MARK: - Navigation
     
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ViewRecipe"
        {
            let destination = segue.destination as! RecipeDetails
            
            destination.selectedItem = selectedItem
            
            let imagename = selectedItem?.imagename
            
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
            
            var selectedImage : UIImage?
            
            if statusImageCache[urlString] != nil
            {
                selectedImage = statusImageCache[urlString]
            }
            else
            {
                print("no")
            }
            
            destination.selectedImage = selectedImage
        }
    }
}



//func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FeaturedRecipeCell
//
//    var dict: NSMutableDictionary?
//
//    if isFiltered == true
//    {
//        dict = filteredList[indexPath.row]
//    }
//    else
//    {
//        dict = displayItems[indexPath.row]
//    }
//
//    cell?.recipeIV.image = UIImage.init(named: (dict?.object(forKey: "imagename") as? String)!)
//    cell?.headerLbl.text = dict?.object(forKey: "recipename") as? String
//    cell?.infoLbl.text = "\(dict?.object(forKey: "preptime") as! String) • \(dict?.object(forKey: "calories") as! String)"
//
//    let attributedString = NSMutableAttributedString(string: "Source: \((dict?.object(forKey: "source") as? String)!)", attributes: [
//        .font: UIFont(name: "Avenir-Book", size: 13.0)!,
//        .foregroundColor: appDelegate.crGray
//        ])
//    attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 13.0)!, range: NSRange(location: 0, length: 7))
//
//    cell?.sourceLbl.attributedText = attributedString
//
//    cell?.favoriteBtn.restorationIdentifier = dict?.object(forKey: "recipeid") as? String
//    cell?.favoriteBtn.accessibilityIdentifier = "\(indexPath.row)"
//
//    let video = dict?.object(forKey: "videoname") as! String
//
//    if video == ""
//    {
//        cell?.videoBtn.isHidden = true
//    }
//
//    let isFavorite = dict?.object(forKey: "isFavorite") as? String
//
//    if isFavorite == "1"
//    {
//        cell?.favoriteBtn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
//        cell?.isFavorite = true
//        //dict?.setValue(true, forKey: "isFavorite")
//    }
//    else
//    {
//        cell?.favoriteBtn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
//        cell?.isFavorite = false
//        //dict?.setValue(false, forKey: "isFavorite")
//    }
//
//    cell?.favoriteBtn.addTarget(self, action: #selector(self.manageFavorite(_:)), for: UIControlEvents.touchUpInside)
//
//    return cell!
//}
