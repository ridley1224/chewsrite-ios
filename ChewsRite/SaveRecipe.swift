//
//  SaveRecipe.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/13/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import Alamofire
import AWSS3

class SaveRecipe: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var folderTable: UITableView!
    @IBOutlet weak var folderNameTxt: UITextField!
    @IBOutlet weak var recipeIV: UIImageView!
    
    var ingredientOptions : [NSDictionary]?
    var directionList : [NSDictionary]?
    var nutritionList : [NSDictionary]?
    var tipsList : [NSDictionary]?
    var selectedImage :UIImage?
    var foldersList = [NSMutableDictionary]()
    
    var selectedItems = [NSMutableDictionary]()
    //var newFolderItems = [NSMutableDictionary]()
    
    var recipeDict : NSMutableDictionary?
    
    var nutritionValues : [String]?
    var nutritionValLabels : [String]?
    var selectedVideoURL : URL?
    var filename : String?
    var filetype : Int?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        filetype = 0
        
        var i = 0
        
        for dict in ingredientOptions!
        {
            if dict.object(forKey: "name") as! String == "add ingredient"
            {
                ingredientOptions?.remove(at: i)
                break
            }
            
            i += 1
        }
        
        i = 0
        
        for dict in directionList!
        {
            if dict.object(forKey: "name") as! String == "add direction"
            {
                directionList?.remove(at: i)
                break
            }
            
            i += 1
        }
        
        i = 0
        
        for dict in tipsList!
        {
            if dict.object(forKey: "name") as! String == "add special tips"
            {
                tipsList?.remove(at: i)
                break
            }
            
            i += 1
        }
        
        print("ingredientOptions: \(ingredientOptions)")
        print("directionList: \(directionList)")
        print("tipsList: \(tipsList)")
        
        recipeIV.image = selectedImage
        
        folderTable.delegate = self
        folderTable.dataSource = self
        folderTable.tableFooterView = UIView()

//        foldersList.add("Heart Healthy")
//        foldersList.add("Vegan")
        
        activityView.isHidden = true
        
        getFolders()
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        keyboardDoneButtonView.items = [doneButton]
        
        folderNameTxt?.inputAccessoryView = keyboardDoneButtonView
    }
    
    @objc func doneClicked(sender: UIButton!) {
        
        //let dict : NSMutableDictionary = ["foldername": folderNameTxt.text!, "active": true]
        
        //foldersList.append(dict)
        //selectedItems.append(dict)
        
        addFolder(folder: folderNameTxt.text!)
        
        folderNameTxt.text = ""
        folderTable.reloadData()
        
        self.view.endEditing(true)
    }
    
    // MARK: Webservice
    
    func getFolders() {
        
        print("getFolders")
        
        let urlString = "\(appDelegate.serverDestination!)getFolders.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("paramString: \(urlString)?\(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                let dataString = "folderData"
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.folderTable.isHidden = true
                        
                        //                        self.familyCV.isHidden = true
                        //                        self.familyCV.reloadData()
                    })
                }
                else
                {
                    let uploadData = dataDict[dataString]! as! [NSDictionary]
                    
                    for dict in uploadData
                    {
                        let dict1 = dict.mutableCopy() as! NSMutableDictionary
                        self.foldersList.append(dict1)
                    }
                    
                    //self.foldersList = uploadData
                    
                    print("foldersList: \(self.foldersList)")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.folderTable.reloadData()
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func addFolder(folder:String) {
        
        print("saveFolder")
        
        let urlString = "\(appDelegate.serverDestination!)addFolder.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&foldername=\(folder)&devStatus=\(appDelegate.devStage!)"
        
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict["folderData"]! is NSNull {
                    
                    print("no folder data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSDictionary = dataDict["folderData"]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    let folderid = uploadData.object(forKey: "folderid") as! String
                    
                    var isSaved = false
                    
                    if status == "folder saved"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            let dict : NSMutableDictionary = ["foldername": folder, "folderid": folderid, "active": true]
                            
                            self.foldersList.append(dict)
                            self.folderTable.reloadData()
                        }
                        else
                        {
                            //to do randall save locally in queue
                            
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func saveRecipe(_ sender: Any) {
        
        //saveRecipe()
        
        //let debug = false
        
        filename = appDelegate.randomString(length: 10)
        
        //filename = "a23kfls"

//        if debug == false
//        {
//            if selectedVideoURL != nil
//            {
//                filetype = 1
//            }
//
//            uploadS3()
//        }
//        else
//        {
//            saveRecipe()
//        }
        
        if selectedVideoURL != nil
        {
            filetype = 1
        }

        uploadS3()
    }
    
    func uploadS3 () {
        
        saveBtn.isEnabled = false
        activityView.startAnimating()
        activityView.isHidden = false
        
        let image = selectedImage!
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(filename!)
        let imageData = UIImageJPEGRepresentation(image, 0)
        fileManager.createFile(atPath: path as String, contents: imageData, attributes: nil)
        
        let fileUrl = NSURL(fileURLWithPath: path)
        
        print("upload")
        
        let transferManager = AWSS3TransferManager.default()
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.bucket = "chewsrite/images"
        uploadRequest!.key = "\(filename!).jpg"
        uploadRequest?.body = fileUrl as URL
        uploadRequest?.contentType = "image/jpeg"
        
        uploadRequest?.acl = AWSS3ObjectCannedACL.publicReadWrite;
        
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            
            if let error = task.error as NSError? {
                
                if error.domain == AWSS3TransferManagerErrorDomain,
                    
                    let code = AWSS3TransferManagerErrorType(rawValue: error.code)
                {
                    switch code
                    {
                    case .cancelled, .paused:
                        break
                    default:
                        print("Error uploading 1: \(uploadRequest?.key!) Error: \(error)")
                    }
                }
                else
                {
                    print("Error uploading: \(uploadRequest?.key!) Code: \(error.code) Error: \(error)")
                }
                
                return nil
            }
            
            //let uploadOutput = task.result
            
            print("Upload complete for: \(uploadRequest!.key!)")
            
            if self.filetype == 0
            {
                self.saveRecipe()
            }
            else if self.selectedVideoURL != nil
            {
                self.uploadS3Video()
            }
            
            //Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.doneUploading), userInfo: nil, repeats: false)
            
            return nil
        })
    }
    
    func uploadS3Video () {
        
        //        createStatusLbl.text = "Uploading Video"
        //        uploadView.isHidden = false
        //        activityView.startAnimating()
        
        //print("upload dict: \(dict)")
        
        print("uploadS3Video")
        
        //http://docs.aws.amazon.com/mobile/sdkforios/developerguide/s3transfermanager.html
        
        let transferManager = AWSS3TransferManager.default()
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.bucket = "chewsrite/video"
        uploadRequest!.key = "\(filename!).mov"
        uploadRequest?.body = selectedVideoURL!
        
        //        let imageURL = Bundle.main.url(forResource: "ava", withExtension: "png")!
        //
        //        uploadRequest?.body = imageURL
        
        uploadRequest?.acl = AWSS3ObjectCannedACL.publicReadWrite;
        
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            
            if let error = task.error as? NSError {
                
                if error.domain == AWSS3TransferManagerErrorDomain,
                    
                    let code = AWSS3TransferManagerErrorType(rawValue: error.code)
                {
                    switch code
                    {
                    case .cancelled, .paused:
                        break
                    default:
                        print("Error uploading 1: \(uploadRequest?.key!) Error: \(error)")
                    }
                }
                else
                {
                    print("Error uploading: \(uploadRequest?.key!) Code: \(error.code) Error: \(error)")
                }
                return nil
            }
            
            //let uploadOutput = task.result
            
            print("Upload complete for: \(uploadRequest!.key!)")
            
            self.saveRecipe()
            
            //Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.doneUploading), userInfo: nil, repeats: false)
            
            return nil
        })
    }
    
    func saveRecipe () {
        
        print("saveRecipe")
        
        var foldersString = ""
        
        if foldersList.count > 0
        {
            var folderids = ""
            var i = 0
            
            for a in foldersList
            {
                let active = a.object(forKey: "active") as? Bool
                
                if active == true
                {
                    if i == 0
                    {
                        folderids = (a.object(forKey: "folderid") as? String)!
                    }
                    else
                    {
                        folderids = "\(folderids),\(a.object(forKey: "folderid") as! String)"
                    }
                    
                    i += 1
                }
            }
            
            foldersString = "\(folderids)"
        }
        
        let nutritionValuesStr = nutritionValues?.joined(separator: ",")
        let nutritionValLabelsStr = nutritionValLabels?.joined(separator: ",")
        
        print("nutritionValuesStr: \(nutritionValuesStr!)")
        print("nutritionValLabelsStr: \(nutritionValLabelsStr!)")
        
        //            "nutrition": nutritionList!,
        
        let parameters: Parameters = [
            "ingredients": ingredientOptions!,
            "directions": directionList!,
            "nutritionValues": nutritionValuesStr!,
            "nutritionValLabels": nutritionValLabelsStr!,
            "tips" : tipsList!,
            "userid": appDelegate.userid!,
            "recipename": recipeDict?.object(forKey: "recipename") as! String,
            "description": recipeDict?.object(forKey: "description") as! String,
            "source": recipeDict?.object(forKey: "source") as! String,
            "sourceurl": recipeDict?.object(forKey: "sourceurl") as! String,
            "servings": recipeDict?.object(forKey: "servings") as! String,
            "preptime": recipeDict?.object(forKey: "preptime") as! String,
            "cooktime": recipeDict?.object(forKey: "cooktime") as! String,
            "totaltime": recipeDict?.object(forKey: "totaltime") as! String,
            "calories": recipeDict?.object(forKey: "calories") as! String,
            "imagename": filename!,
            "folderids": foldersString,
            "cuisinetags": recipeDict?.object(forKey: "cuisinetags") as! String,
            "filetype": filetype!
        ]
        
        print("recipe parameters: \(parameters)")
        
        //https://github.com/Alamofire/Alamofire
        
        Alamofire.request("\(appDelegate.serverDestination!)addRecipeAF.php", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            print("request: \(response.request!)")  // original URL request
            //print("response: \(response.response!)") // HTTP URL response
            //print("data: \(response.data)")     // server data
            print("result: \(response.result)")   // result of response serialization
            
            if let JSON = response.result.value {
                
                print("JSON: \(JSON)")
                
                let dataDict : NSDictionary = JSON as! NSDictionary

                //print("dataDict: \(dataDict)")

                let status = dataDict.object(forKey: "status") as! String

                print("status: \(status)")
                
                if status.contains("recipe saved")
                {
                    //post notification refresh update recipes
                    
                    
                    self.activityView.isHidden = true
                    self.activityView.stopAnimating()
                    
                    NotificationCenter.default.post(name: Notification.Name("goToRecipes"), object: nil)
                    
                    
                    
                    self.dismiss(animated: false, completion: nil)
                    self.navigationController?.popViewController(animated: false)
                }
            }
        }
    }
    
    func saveRecipe1 () {
        
        print("saveRecipe")
        
        var foldersString = ""
        
        if foldersList.count > 0
        {
            var folderids = ""
            var i = 0
            
            for a in foldersList
            {
                if i == 0
                {
                    folderids = (a.object(forKey: "folderid") as? String)!
                }
                else
                {
                    folderids = "\(folderids),\(a.object(forKey: "folderid") as! String)"
                }
                
                i += 1
            }
            
            foldersString = "folderids=\(folderids)"
        }
        
        let urlString = "\(appDelegate.serverDestination!)saveRecipe.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&folderids=\(foldersString)&devStatus=\(appDelegate.devStage!)"
        
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict["recipeData"]! is NSNull {
                    
                    print("no folder data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSDictionary = dataDict["recipeData"]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    //let folderid = uploadData.object(forKey: "recipeid") as! String
                    
                    var isSaved = false
                    
                    if status == "recipe saved"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            
                        }
                        else
                        {
                            //to do randall save locally in queue
                            
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.foldersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : FolderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! FolderTableViewCell
        
        let dict = self.foldersList[indexPath.row]
        cell.selectionStyle = .none
        
        cell.titleLbl?.text = dict.object(forKey: "foldername") as? String
        
        cell.startEditBtn.restorationIdentifier = String(indexPath.row)
        cell.startEditBtn.addTarget(self, action: #selector(self.saveToFolder(_:)), for: UIControlEvents.touchUpInside)
        cell.startEditBtn.restorationIdentifier = "\(indexPath.row)"
        
        let active = dict.object(forKey: "active") as! Bool
        
        if active == true
        {
            cell.startEditBtn.setBackgroundImage(UIImage.init(named: "selected"), for: .normal)
        }
        else
        {
            cell.startEditBtn.setBackgroundImage(UIImage.init(named: "unselected"), for: .normal)
        }
        
        return cell
    }
    
    @objc func saveToFolder(_ sender : UIButton) {
        
        let ind = Int(sender.restorationIdentifier!)
        
        let dict = self.foldersList[ind!] as NSMutableDictionary
        
        let active = dict.object(forKey: "active") as! Bool
        
        if active == true
        {
            dict.setValue(false, forKey: "active")
            
//            if selectedItems.contains(dict)
//            {
//                selectedItems.remove(at: selectedItems.index(of: dict)!)
//            }
        }
        else
        {
            dict.setValue(true, forKey: "active")
            //selectedItems.append(dict)
        }
        
        folderTable.reloadData()
        
        //print("selectedItems: \(selectedItems)")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //var selectedIem = self.foldersList[(indexPath as NSIndexPath).row]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
