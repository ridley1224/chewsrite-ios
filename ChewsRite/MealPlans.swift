//
//  MealPlans.swift
//  ChewsRite
//
//  Created by Randall Ridley on 8/13/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class MealPlans: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var recipeCV: UICollectionView!
    
    @IBOutlet weak var folderTable: UITableView!
    @IBOutlet weak var folderNameTxt: UITextField!
    @IBOutlet weak var recipeIV: UIImageView!
    
    var breakfastList = [NSMutableDictionary]()
    var lunchList = [NSMutableDictionary]()
    var snackList = [NSMutableDictionary]()
    var dinnerList = [NSMutableDictionary]()
    var selectedItems = [NSMutableDictionary]()
    var selectedList = [NSMutableDictionary]()
    var displayItems = [NSMutableDictionary]()
    var mealBtns = [UIButton]()
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var recipeDict: NSMutableDictionary!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var recipeLbl: UILabel!
    var recipeImage : UIImage?
    
    var mealIndex : Int = 0
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var mealPlanStatusLbl: UILabel!
    @IBOutlet weak var mealPlanStatusTV: UITextView!
    var currentDate : Date?
    var todayDate : Date?
    var todayStr : String?
    var todayDateStr : String?
    var currentDateSQLStr : String?
    var originalRecipeID : String?
        
    @IBOutlet weak var buttonScollview: UIScrollView!
    
    @IBOutlet weak var familyView: UIView!
    var addedToFamily = false
    var showedFamilyBox = false
    
    var currentDateOffset = 0
    
    @IBOutlet weak var breakfastBtn: DesignableButton!
    @IBOutlet weak var lunchBtn: DesignableButton!
    @IBOutlet weak var snackBtn: DesignableButton!
    
    @IBOutlet weak var dinnerBtn: DesignableButton!
    
    var itemReplaced : Bool?
    var selectedMealItem: NSDictionary?
    
    @IBOutlet weak var mealPlanStatusLblHeight: NSLayoutConstraint!
    @IBOutlet weak var toastView: UIView!
    
    @IBOutlet weak var toastStatusLbl: UILabel!
    
    @IBOutlet weak var search: UISearchBar!
    var itemArray = [FeedItem]()
    var currentItemArray = [FeedItem]()
    var selectedFeedItem : FeedItem?
    var allRecipes = [NSDictionary]()
    
    var statusImageCache = [String:UIImage]()
    
    var plan = ""
    
    @IBOutlet weak var menuBtn: UIButton!
    
    @IBOutlet weak var familyCV: UICollectionView!
    var selectedUser : NSDictionary?
    var familyList = [NSDictionary]()
    
    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        selectedUser = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!, "usertype":"0"] as NSDictionary
        familyList.append(selectedUser!)
        
        if appDelegate.userImage != nil
        {
            let urlString = "\(appDelegate.serverDestination!)userimages/\(appDelegate.profileImg!)"
            self.statusImageCache[urlString] = appDelegate.userImage
        }
        
        familyCV.delegate = self
        familyCV.dataSource = self
        
        mealBtns = [breakfastBtn,lunchBtn,snackBtn,dinnerBtn]
        
        //menuBtn.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
        let tb : TabController = self.parent as! TabController
        
        menuBtn.addTarget(self, action: #selector(tb.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
        recipeCV.delegate = self
        recipeCV.dataSource = self
        recipeCV.canCancelContentTouches = false
        
        search.delegate = self
        search.backgroundImage = UIImage()
        
        search.placeholder = "Search Recipes"
        
        UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        
//        recipeIV.image = recipeImage
//        recipeLbl.text = recipeDict.object(forKey: "recipename") as? String
        
        toastView.isHidden = true
        mealPlanStatusTV.isHidden = true
        self.mealPlanStatusLbl.text = ""
        activityView.isHidden = true
        //familyView.isHidden = true
        
        todayDate = Date()
        currentDate = todayDate
        
        mealIndex = 0
        
        //print("currentDate: \(currentDate)")
        
        let dateStr = appDelegate.formatMessageDate1(date: Date(), format: "dd MMMM")
        todayDateStr = appDelegate.formatMessageDate1(date: currentDate!, format: "yyyy, dd MMMM")
        
        todayStr = "Today, \(dateStr)"
        
        dateLbl.text = todayStr
        
        getMealPlanRecipes()
        getFamily()
        //getFolders()
        //debug()
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        keyboardDoneButtonView.items = [doneButton]
        
        folderNameTxt?.inputAccessoryView = keyboardDoneButtonView
        
        buttonScollview.contentSize = CGSize.init(width: 138*4, height: buttonScollview.frame.height)
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(recipeUpdatedNotificationName),
            name: NSNotification.Name(rawValue: "recipeUpdated"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(refreshMealPlansNotification),
            name: NSNotification.Name(rawValue: "refreshMealPlans"),
            object: nil)        
    }
    
    @objc func refreshMealPlansNotification (notification: NSNotification) {
        
        //print("port: \(notification.object)")
        
        print("refresh meal plans")
        
        getMealPlanRecipes()
    }
    
    @objc func recipeUpdatedNotificationName (notification: NSNotification) {
        
        toastStatusLbl.text = "Recipe has been successfully saved."
        
        toastView.isHidden = false
        
        getMealPlanRecipes()
        
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.fadeOut), userInfo: nil, repeats: false)
    }
    
    @objc func fadeOut() {
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            
            self.toastView.alpha = 0
            
        }, completion: { (finished) -> Void in
            
            self.toastView.isHidden = true
            self.toastView.alpha = 1
        })
    }
    
    func debug () {
        
        mealPlanStatusTV.isHidden = true
        
        let dateStr = appDelegate.formatMessageDate1(date: Date(), format: "dd MMMM")
        todayDateStr = appDelegate.formatMessageDate1(date: currentDate!, format: "yyyy, dd MMMM")
        
        todayStr = "Today, \(dateStr)"
        
        dateLbl.text = todayStr
        
        mealPlanStatusLbl.text = "You have 2 recipes for breakfast in your meal plan"
        
        var dict : NSMutableDictionary = ["recipename" : "Mushroom and Asparagus Risotto", "category" : "","source" : "Hello Fresh","preptime" : "40 mins","calories" : "400","imagename" : "pistachio", "mealplandate" :  "2018-08-04", "mealIndex": 0 , "replaced": false, "recipeid": "2"]
        
        breakfastList.append(dict)
        
        dict = ["recipename" : "Creamy Shrimp Risotto", "category" : "1","source" : "Blue Apron","preptime" : "60 mins","calories" : "300","imagename" : "risotto", "mealplandate" :  "2018-08-05",  "mealIndex": 0, "replaced": false, "recipeid": "3"]
        
        breakfastList.append(dict)
        
        selectedList = breakfastList
        
        updateDisplayList()
    }
    
    func updateDisplayList () {
        
        currentDateSQLStr = appDelegate.formatMessageDate1(date: currentDate!, format: "yyyy-MM-dd")
        
        displayItems.removeAll()
        
        print("currentDateSQLStr: \(currentDateSQLStr)")
        
//        for dict in selectedList {
//
//            let mealplandate = dict.object(forKey: "mealplandate") as! String
//
//            print("mealplandate: \(mealplandate)")
//
//            if mealplandate == currentDateSQLStr
//            {
//                print("show recipe")
//
//                displayItems.append(dict)
//            }
//        }
        
        currentItemArray = itemArray.filter({ item -> Bool in
            
            return item.mealid == "\(mealIndex)" && item.mealplandate == currentDateSQLStr
        })
        
        //var status = ""
        
        
        if mealIndex == 0
        {
            plan = "breakfast"
        }
        else if mealIndex == 1
        {
            plan = "lunch"
        }
        else if mealIndex == 2
        {
            plan = "snack"
        }
        else if mealIndex == 3
        {
            plan = "dinner"
        }
        else if mealIndex == 4
        {
            plan = "dessert"
        }
        
        if currentItemArray.count > 0
        {
            mealPlanStatusLbl.text = "You have \(currentItemArray.count) recipes in your \(plan) meal plan"
            mealPlanStatusLbl.isHidden = false
            mealPlanStatusTV.isHidden = true
            
            recipeCV.isHidden = false
        }
        else
        {
            mealPlanStatusLbl.text = ""
            mealPlanStatusLbl.isHidden = true
            mealPlanStatusTV.isHidden = false
            
            recipeCV.isHidden = true
        }
        
        recipeCV.reloadData()
    }
    
    // MARK: Actions
    
    @IBAction func learnMore(_ sender: Any) {
        
        //        if !showedFamilyBox
        //        {
        //            showedFamilyBox = true
        //            familyView.isHidden = false
        //        }
        
        familyView.isHidden = false
    }
    
    @IBAction func changeMeal(_ sender: Any) {
        
        let btn1 = sender as? UIButton
        
        
        for btn in mealBtns
        {
            if btn != btn1
            {
                btn.backgroundColor = .clear
                btn.setTitleColor(appDelegate.crLightBlue, for: .normal)
            }
            else
            {
                btn.backgroundColor = appDelegate.crLightBlue
                btn.setTitleColor(.white, for: .normal)
            }
        }
        
        mealIndex = Int((btn1?.restorationIdentifier)!)!
        
        currentItemArray = itemArray.filter({ item -> Bool in
            
            return item.mealid == "\(mealIndex)" && item.mealplandate == currentDateSQLStr
            
            //return tags.contains("\(selectedCuisine)".lowercased())
            
            //return item.cuisinetags.lowercased().contains("\(selectedCuisine)".lowercased())
        })
        
        recipeCV.reloadData()
        
        
//        mealIndex = Int((btn1?.restorationIdentifier)!)!
//
        if mealIndex == 0
        {
            selectedList = breakfastList
        }
        else if mealIndex == 1
        {
            selectedList = lunchList
        }
        else if mealIndex == 2
        {
            selectedList = snackList
        }
        else if mealIndex == 3
        {
            selectedList = dinnerList
        }
        

        updateDisplayList()
    }
    
    @IBAction func replaceRecipe(_ sender: Any) {}
    
    @IBAction func previousDate(_ sender: Any) {
        
        currentDate = (currentDate?.yesterday)!
        
        var dateStr = appDelegate.formatMessageDate1(date: currentDate!, format: "yyyy, dd MMMM")
        
        if todayDateStr == dateStr
        {
            dateStr = todayStr!
        }
        
        dateLbl.text = dateStr
        
        updateDisplayList()
    }
    
    @IBAction func nextDate(_ sender: Any) {
        
        currentDate = (currentDate?.tomorrow)!
        
        var dateStr = appDelegate.formatMessageDate1(date: currentDate!, format: "yyyy, dd MMMM")
        
        if todayDateStr == dateStr
        {
            dateStr = todayStr!
        }
        
        dateLbl.text = dateStr
        
        updateDisplayList()
    }
    
    @IBAction func addToFamily(_ sender: Any) {
        
        let btn = sender as? UIButton
        
        if !addedToFamily
        {
            btn?.setBackgroundImage(#imageLiteral(resourceName: "checkGreen"), for: .normal)
            addedToFamily = true
        }
        else
        {
            btn?.setBackgroundImage(nil, for: .normal)
            addedToFamily = false
        }
    }
    
    @IBAction func closeFamilyView(_ sender: Any) {
        
        familyView.isHidden = true
    }
    
    @objc func doneClicked(sender: UIButton!) {
        
        //selectedList.append(folderNameTxt.text!)
        folderTable.reloadData()
        
        self.view.endEditing(true)
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func saveToFolder(_ sender : UIButton) {
        
        //let btn = sender as UIButton
        //let ind = Int(btn.restorationIdentifier!)
        
        //let dict = self.selectedList[ind!]
        
        //print("selectedItems: \(selectedItems)")
    }
    
    @IBAction func saveRecipe(_ sender: Any) {
        
        activityView.startAnimating()
        activityView.isHidden = false
        shareBtn.isEnabled = false
        shareBtn.setTitle("", for: .normal)
        
        if selectedList.count < 4
        {
            uploadData()
        }
        else
        {
            self.mealPlanStatusLbl.text = "You have already reached the limit of 4 recipes for breakfast in your meal plan. Please replace an existing recipe."
            
            mealPlanStatusLblHeight.constant = 57
        }
        
        //Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.debugSave), userInfo: nil, repeats: false)
    }
    
    @objc func removeRecipe(_ sender : UIButton) {
        
        let btn = sender as UIButton
        let ind = Int(btn.restorationIdentifier!)
        
        selectedFeedItem = currentItemArray[ind!]
        
        let alert = UIAlertController(title: nil, message: "Remove recipe from meal plan?", preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
                self.deleteRecipeData()
                
                //debug delete
                
//                self.currentItemArray.removeAll(where: {$0.mealplanid == self.selectedFeedItem!.mealplanid})
//                self.recipeCV.reloadData()
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    @objc func changeRecipe(_ sender : UIButton) {
        
        let btn = sender as UIButton
        let ind = Int(btn.restorationIdentifier!)
        
        //selectedMealItem = displayItems[ind!]
        
        selectedFeedItem = currentItemArray[ind!]
        
        performSegue(withIdentifier: "EditMealPlanItem", sender: nil)
    }
    
    @IBAction func showMenu(_ sender: Any) {
        
        //NotificationCenter.default.post(name: Notification.Name("showMenu"), object: sender)
    }
    
    @objc func debugSave () {
        
        recipeSaved()
    }
    
    func recipeSaved () {
        
        //Alamofire to send dict
        
        NotificationCenter.default.post(name: Notification.Name("savedMealPlan"), object: nil)
        
        activityView.stopAnimating()
        activityView.isHidden = true
        
        getMealPlanRecipes()
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Webservice
    
    func getFamily() {
        
        familyList.removeAll()
        
        print("getFamily")
        
        let dataString = "familyData"
        
        let urlString = "\(appDelegate.serverDestination!)getFamilyMembers.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        selectedUser = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!, "usertype":"0"] as NSDictionary
        familyList.append(selectedUser!)
        
        self.familyCV.reloadData()
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("family jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
//                        self.familyCV.isHidden = true
//                        self.familyCV.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let members = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (members.count > 0)
                    {
                        hasData = true
                        
                        for dict in members
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.familyList.append(dict2)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if hasData == true
                            {
                                self.familyCV.isHidden = false
                                self.familyCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
//                                self.familyCV.isHidden = true
//                                self.familyCV.reloadData()
                            }
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
            }.resume()
    }
    
    func getFolders() {
        
        print("getFolders")
        
        let dataString = "folderData"
        
        let urlString = "\(appDelegate.serverDestination!)getFolders.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    if (uploadData.count > 0)
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = true
                        
                        //self.selectedList = uploadData as! [NSMutableDictionary]
                            
                            //self.folderTable.reloadData()
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getMealPlanRecipes() {
        
        allRecipes.removeAll()
        currentItemArray.removeAll()
        itemArray.removeAll()
        breakfastList.removeAll()
        lunchList.removeAll()
        snackList.removeAll()
        dinnerList.removeAll()
        selectedItems.removeAll()
        
        print("getMealPlanRecipes")
        
        let dataString = "recipeData"
        
        let urlString = "\(appDelegate.serverDestination!)getMealPlanRecipes.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var paramString = ""
        
        if selectedUser != nil
        {
            let usertype = selectedUser?.object(forKey: "usertype") as! String
            
            if usertype == "0"
            {
                paramString = "\(paramString)&userid=\(selectedUser?.object(forKey: "userid") as! String)"
            }
            else
            {
                paramString = "\(paramString)&userid=\(selectedUser?.object(forKey: "familyid") as! String)"
            }
        }
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("meal plan recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.mealPlanStatusTV.isHidden = false
                        self.recipeCV.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    self.allRecipes = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (self.allRecipes.count > 0)
                    {
                        hasData = true
                        
                        for dict in self.allRecipes
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            let mealid = dict2.object(forKey: "mealid") as! String
                            
                            if mealid == "0"
                            {
                                self.breakfastList.append(dict2)
                            }
                            else if mealid == "1"
                            {
                                self.lunchList.append(dict2)
                            }
                            else if mealid == "2"
                            {
                                self.snackList.append(dict2)
                            }
                            else if mealid == "3"
                            {
                                self.dinnerList.append(dict2)
                            }
                            
                            self.itemArray.append(FeedItem(recipename: dict2.object(forKey: "recipename") as! String,
                                                           preptime: dict2.object(forKey: "preptime") as! String,
                                                           cooktime: dict2.object(forKey: "cooktime") as! String,
                                                           imagename: dict2.object(forKey: "imagename") as! String,
                                                           videoname: "", calories: dict2.object(forKey: "calories") as! String,
                                                           source: dict2.object(forKey: "source") as! String, sourceurl: dict2.object(forKey: "sourceurl") as! String,
                                                           isFavorite: "", username: "",
                                                           recipeid: dict2.object(forKey: "recipeid") as! String,
                                                           cuisinetags: "",
                                                           mealplanid: dict2.object(forKey: "mealplanid") as! String,
                                                           mealplandate: dict2.object(forKey: "mealplandate") as! String,
                                                           mealid: dict2.object(forKey: "mealid") as! String,
                                                           userimage: "",
                                                           firstname: "",
                                                           lastname: "",
                                                           description: dict2.object(forKey: "recipedescription") as! String,
                                                           servings: dict2.object(forKey: "servings") as! String, userid: dict2.object(forKey: "userid") as! String, totaltime: dict2.object(forKey: "totaltime") as! String, rating: dict2.object(forKey: "rating") as! String, timeAdded: dict2.object(forKey: "timeAdded") as! String))
                        }
                        
                        //self.currentItemArray = self.itemArray
                        
                        self.currentItemArray = self.itemArray.filter({ item -> Bool in
                            
                            return item.mealid == "\(self.mealIndex)"
                            
                            //return tags.contains("\(selectedCuisine)".lowercased())
                            
                            //return item.cuisinetags.lowercased().contains("\(selectedCuisine)".lowercased())
                        })
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.activityView.stopAnimating()
                            self.activityView.isHidden = true
                            
                            if hasData == true
                            {
                                self.mealPlanStatusTV.isHidden = true
                                
                                self.mealPlanStatusLbl.text = "You have \(self.displayItems.count) recipes in your \(self.plan) meal plan"
                                
                                self.selectedList = self.breakfastList
                                
                                self.updateDisplayList()
                                self.recipeCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                self.recipeCV.reloadData()
                                //self.noResultsMain.isHidden = false
                            }
                            
                            //self.noResultsMain.isHidden = true
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            self.recipeCV.reloadData()
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func uploadData() {
        
        print("uploadData")
        
        let dataString = "mealPlanData"
        
        let urlString = "\(appDelegate.serverDestination!)manageRecipeMealPlan.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var paramString = "recipeid=\(recipeDict.object(forKey: "recipeid") as! String)&mealid=\(mealIndex)&mealdate=\(currentDateSQLStr!)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        if itemReplaced == true
        {
            paramString = "\(paramString)&originalrecipeid=\(originalRecipeID!)&replace=true"
        }
        else
        {
            paramString = "\(paramString)&insert=true"
        }
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                        
                        self.activityView.stopAnimating()
                        self.activityView.isHidden = true
                        self.shareBtn.isEnabled = true
                        self.shareBtn.setTitle("Add to Meal Plan", for: .normal)
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "meal plan item saved" || status == "meal plan recipe replaced"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            self.recipeSaved()
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                            
                            self.activityView.stopAnimating()
                            self.activityView.isHidden = true
                            self.shareBtn.isEnabled = true
                            self.shareBtn.setTitle("Add to Meal Plan", for: .normal)
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func deleteRecipeData() {
        
        print("deleteRecipeData")
        
        let dataString = "mealPlanData"
        let urlString = "\(appDelegate.serverDestination!)manageRecipeMealPlan.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "mealplanid=\(selectedFeedItem!.mealplanid)&delete=true&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                        
//                        self.activityView.stopAnimating()
//                        self.activityView.isHidden = true
//                        self.shareBtn.isEnabled = true
//                        self.shareBtn.setTitle("Add to Meal Plan", for: .normal)
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "meal plan recipe deleted"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
//                            let ind = self.selectedList.index(of: self.selectedMealItem!)
//                            self.selectedList.remove(at: ind!)
//
//                            self.reloadMeals()
                            
                            self.currentItemArray.removeAll(where: {$0.mealplanid == self.selectedFeedItem!.mealplanid})
                            self.recipeCV.reloadData()
                            
                            //self.getMealPlanRecipes()
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                            
                            self.activityView.stopAnimating()
                            self.activityView.isHidden = true
                            self.shareBtn.isEnabled = true
                            self.shareBtn.setTitle("Add to Meal Plan", for: .normal)
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    // MARK: Search
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        currentItemArray = itemArray.filter({ item -> Bool in
            
            if searchText.isEmpty
            {
                return item.mealid == "\(mealIndex)"
            }
            else
            {
                return item.mealid == "\(mealIndex)" && item.recipename.lowercased().contains(searchText.lowercased())
            }
            
            //return tags.contains("\(selectedCuisine)".lowercased())
            //return item.cuisinetags.lowercased().contains("\(selectedCuisine)".lowercased())
        })
        
        //        currentItemArray = itemArray.filter({ item -> Bool in
        //
        //            switch searchBar.selectedScopeButtonIndex {
        //            case 0:
        //                if searchText.isEmpty { return true }
        //                return item.recipename.lowercased().contains(searchText.lowercased())
        //            default:
        //                return false
        //            }
        //        })
        
        recipeCV.reloadData()
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.restorationIdentifier == "family"
        {
            return CGSize.init(width: 42, height: 42)
        }
        else
        {
            let kWhateverHeightYouWant = 190
            
            return CGSize.init(width: 600, height: CGFloat(kWhateverHeightYouWant))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.restorationIdentifier == "family"
        {
            return self.familyList.count
        }
        else
        {
            return self.currentItemArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.restorationIdentifier == "family"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FamilyCVCell
            
            let dict = familyList[indexPath.row]
            
            let imagename = dict.object(forKey: "userimage") as? String
            
            if selectedUser == dict
            {
                let img = UIImage.init(named: "selectWhite")
                cell?.si.image = img
            }
            else
            {
                cell?.si.image = nil
            }
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                
                //let url2 = NSURL(string:urlString)!
                
                // If this image is already cached, don't re-download
                
                if let img = statusImageCache[urlString]
                {
                    //print("load cached status image")
                    
                    cell?.userImg.image = img
                }
                else
                {
                    //print("status image not
                    
                    let imagename = dict.object(forKey: "userimage") as? String
                    
                    if imagename != nil && imagename != ""
                    {
                        let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                        
                        print("img urlString: \(urlString)")
                        
                        if let img = statusImageCache[urlString]
                        {
                            cell?.userImg.image = img
                        }
                        else
                        {
                            cell?.userImg.image = UIImage(named: "profile-icon")
                            
                            self.downloadImage(imagename: imagename!, urlString: urlString, rv: ((cell?.userImg!)!))
                        }
                    }
                    else
                    {
                        cell?.userImg.image = UIImage(named: "profile-icon")
                    }
                }
            }
            
            return cell!
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? MealPlanCollectionViewCell
            
            let item = currentItemArray[indexPath.row]
            
            //cell?.headerIV.image = UIImage.init(named: item.imagename)
            
            let imagename = item.imagename
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
                
                if let img = statusImageCache[urlString]
                {
                    cell?.headerIV.image = img
                }
                else
                {
                    cell?.headerIV.image = UIImage(named: "profile-icon")
                    
                    self.downloadImage(imagename: imagename, urlString: urlString, rv: (cell?.headerIV!)!)
                }
            }
            
            cell?.headerLbl.text = item.recipename
            cell?.infoLbl.text = "\(item.preptime) • \(item.calories)"
            
            cell?.removeBtn.restorationIdentifier = "\(indexPath.row)" //dict?.object(forKey: "mealplanid") as? String
            cell?.changeBtn.restorationIdentifier = "\(indexPath.row)"
            
            //add selector for removeBtn and changeBtn
            
            cell?.removeBtn.addTarget(self, action: #selector(self.removeRecipe(_:)), for: UIControlEvents.touchUpInside)
            cell?.changeBtn.addTarget(self, action: #selector(self.changeRecipe(_:)), for: UIControlEvents.touchUpInside)
            
            return cell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if collectionView.restorationIdentifier == "family"
        {
            selectedUser = familyList[indexPath.row]
            
            getMealPlanRecipes()
            
            //
            //            print("selectedUser: \(selectedUser)")
            
            familyCV.reloadData()
        }
    }
    
    func downloadImage (imagename : String, urlString : String, rv: UIImageView ) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache[urlString] = image
                        rv.image = image
                    }
                    
                    //println("test")
                })
                return nil
            }))
        }
        else
        {
            rv.image = UIImage.init(named: "profile-icon")
        }
    }
    
    func reloadMeals () {
        
        recipeCV.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "EditMealPlanItem"
        {
            let destination = segue.destination as! ChangeMealPlan
            destination.displayItems = selectedList
            destination.selectedFeedItem = selectedFeedItem
            
            
            let urlString = "\(appDelegate.serverDestination!)userimages/\(selectedFeedItem!.imagename)"
            destination.selectedImage = self.statusImageCache[urlString]
            
//            let imagename = selectedMealItem?.object(forKey: "imagename") as! String
//            destination.selectedImage = self.statusImageCache[urlString]
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
