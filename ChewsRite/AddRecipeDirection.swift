//
//  AddRecipeDirection.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/12/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class AddRecipeDirection: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var directionTitleTxt: UITextField!
    @IBOutlet weak var directionTxt: UITextField!
    
    @IBOutlet weak var directionTV: UITextView!
    var previousRect = CGRect.zero
    
    @IBOutlet weak var directionTVHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        directionTitleTxt.delegate = self
        directionTV.delegate = self
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        keyboardDoneButtonView.items = [doneButton]
        
        directionTitleTxt?.inputAccessoryView = keyboardDoneButtonView
        //directionTxt?.inputAccessoryView = keyboardDoneButtonView
        
        directionTV?.inputAccessoryView = keyboardDoneButtonView
        
        directionTV.text = "enter recipe direction"
        
        //debug()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "enter recipe direction"
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""
        {
            textView.text = "enter recipe direction"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        
        return count <= 50
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard let text1 = textView.text else { return true }
        let count = text1.count + text.count - range.length
        
        return count <= 300
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        let pos = textView.endOfDocument
        let currentRect = textView.caretRect(for: pos)
        self.previousRect = self.previousRect.origin.y == 0.0 ? currentRect : previousRect
        
        if(currentRect.origin.y > previousRect.origin.y)
        {
            print("Started New Line")
            
            directionTVHeight.constant += 17
        }
        
        previousRect = currentRect
    }
    
//    func textViewDidChange(_ textView: UITextView) {
//
//        //print("change")
//
//        let pos = textView.endOfDocument
//        let currentRect = textView.caretRect(for: pos)
//
//        if (currentRect.origin.y == -1 || currentRect.origin.y == CGFloat.infinity){
//
//            print("Yeah!, I've gone to a new line")
//            //-1 for new line with a char, infinity is new line with a space
//        }
//    }
    
    func debug() {
        
        directionTitleTxt.text = "Prepare the ingredients and preheat the oven."
        directionTV.text = "Preheat the oven to 350° F. Chop the apple into small bite-sized pieces."
    }
    
    @IBAction func save(_ sender: Any) {
        
        let dict : NSDictionary = ["title" : directionTitleTxt.text!, "directions" : directionTV.text!]
        
        NotificationCenter.default.post(name: Notification.Name("directionSaved"), object: dict)
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func doneClicked(sender: UIButton!) {
        
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
