//
//  NutritionTableViewCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/14/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class NutritionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
