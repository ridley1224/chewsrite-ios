//
//  PendingRequestsCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 8/23/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class PendingRequestsCell: UITableViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var requestBtn: UIButton!
    @IBOutlet weak var friendIV: UIImageView!
}
