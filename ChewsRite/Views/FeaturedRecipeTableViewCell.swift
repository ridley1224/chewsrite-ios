//
//  FeaturedRecipeTableViewCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 9/2/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class FeaturedRecipeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userIV: UIImageView!
    @IBOutlet weak var recipeIV: UIImageView!
    @IBOutlet weak var starsIV: UIImageView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var sourceLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var videoBtn: UIButton!
    
    var isFavorite : Bool?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
