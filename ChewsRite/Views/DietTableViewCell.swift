//
//  DietTableViewCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/9/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class DietTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!

    @IBOutlet weak var ivWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var ivLeadingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
