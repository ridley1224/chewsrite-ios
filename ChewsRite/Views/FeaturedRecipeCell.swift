//
//  FeaturedRecipeCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 8/20/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class FeaturedRecipeCell: UICollectionViewCell {
    
    @IBOutlet weak var recipeIV: UIImageView!
    @IBOutlet weak var starsIV: UIImageView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var sourceLbl: UILabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var videoBtn: UIButton!
    var videoURL: String!
    
    var isFavorite : Bool?
//    var player : AVPlayer?
//    var pl : AVPlayerLayer?
    
    
    @IBAction func manageFavorite(_ sender: Any) {
        
//        if isFavorite == true
//        {
//            favoriteBtn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
//            isFavorite = false
//        }
//        else
//        {
//            favoriteBtn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
//            isFavorite = true
//        }
//        
//        let btn1 = sender as? UIButton
//        
//        let dict : NSDictionary = ["recipeid" : btn1?.restorationIdentifier!, "isFavorite" : isFavorite]
//        
//        NotificationCenter.default.post(name: Notification.Name("manageFavorite"), object: dict)
    }
    
    
    @IBAction func playVideo(_ sender: Any) {
        
        //print("play video")        
        
//        let s3BucketName = "chewsrite/images"
//        let fileName = "\(videoURL!)"
//
//        print("fileName: \(fileName)")
//
//        let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(fileName)
//        let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
//
//        // Set the logging to verbose so we can see in the debug console what is happening
//        AWSLogger.default().logLevel = .none
//
//        let downloadRequest = AWSS3TransferManagerDownloadRequest()
//        downloadRequest!.bucket = s3BucketName
//        downloadRequest!.key  = fileName
//        downloadRequest!.downloadingFileURL = downloadingFileURL
//
//        let transferManager = AWSS3TransferManager.default()
//
//
//        transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
//        (task: AWSTask!) -> AWSTask<AnyObject>? in
//
//        DispatchQueue.main.async(execute: {
//
//            if task.error != nil
//            {
//                print("AWS Error downloading image")
//
//                print(task.error.debugDescription)
//            }
//            else
//            {
//                print("AWS download video successful")
//
//                var downloadOutput = AWSS3TransferManagerDownloadOutput()
//
//                downloadOutput = task.result! as! AWSS3TransferManagerDownloadOutput
//
//                //                        print("downloadOutput video: \(downloadOutput)");
//                //                        print("downloadFilePath video: \(downloadFilePath)");
//
//                //                        NSLog(@"download outptut = %@", downloadOutput);
//                //                        NSString *body = [downloadOutput valueForKey:@"body"];
//                //                        NSLog(@"body = %@", body);
//
//
//                let url2 : NSURL = NSURL(fileURLWithPath:downloadFilePath)
//
//                self.player = AVPlayer.init(url: url2 as URL)
//                self.pl = AVPlayerLayer.init(player: self.player)
//                self.pl?.frame = self.recipeIV.bounds
//                self.contentView.layer.addSublayer(self.pl!)
//                self.recipeIV.isHidden = true
//
//                self.player?.play()
//            }
//        })
//        return nil
//        }))
}
    
}
