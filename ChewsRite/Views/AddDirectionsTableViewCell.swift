//
//  AddDirectionsTableViewCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/12/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class AddDirectionsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var startEditBtn: UIButton!
    @IBOutlet weak var addLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
