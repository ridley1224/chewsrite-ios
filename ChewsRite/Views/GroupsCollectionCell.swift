//
//  GroupsCollectionCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 9/14/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class GroupsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var selectedBtn: UIButton!
    @IBOutlet weak var groupIV: UIImageView!
    
}
