//
//  ReloadTable.swift
//  ChewsRite
//
//  Created by Randall Ridley on 9/2/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class ReloadTable: UITableView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func reloadData() {
        let offset = contentOffset
        super.reloadData()
        contentOffset = offset
    }

}
