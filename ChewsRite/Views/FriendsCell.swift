//
//  FriendsCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 8/23/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class FriendsCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var friendIV: UIImageView!
}
