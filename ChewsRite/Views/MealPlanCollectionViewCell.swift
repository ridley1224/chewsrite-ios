//
//  MealPlanCollectionViewCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 7/19/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class MealPlanCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var headerIV: UIImageView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var statusIV: UIImageView!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var changeBtn: UIButton!
    
}
