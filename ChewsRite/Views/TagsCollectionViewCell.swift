//
//  TagsCollectionViewCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/15/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class TagsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tagLbl: UILabel!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
}
