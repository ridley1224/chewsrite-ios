//
//  GroceryIngredientCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 8/22/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class GroceryIngredientCell: UITableViewCell {
    
    @IBOutlet weak var ingredientLbl: UILabel!
    @IBOutlet weak var selectBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
