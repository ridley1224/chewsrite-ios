//
//  ReviewsTableViewCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/15/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class ReviewsTableViewCell: UITableViewCell {
    
    //@IBOutlet weak var startEditBtn: UIButton!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var reviewLbl: UILabel!
    @IBOutlet weak var ratingIV: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
