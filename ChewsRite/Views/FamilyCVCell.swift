//
//  FamilyCVCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 1/12/19.
//  Copyright © 2019 RT. All rights reserved.
//

import UIKit

class FamilyCVCell: UICollectionViewCell {
    
    @IBOutlet weak var userImg : UIImageView!
    @IBOutlet weak var selectedImg : UIImageView!
    @IBOutlet weak var si : UIImageView!
    
}
