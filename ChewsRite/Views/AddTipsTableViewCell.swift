//
//  AddTipsTableViewCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/13/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class AddTipsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var startEditBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tipLbl: UILabel!
    //@IBOutlet weak var directionsLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
