//
//  CuisineCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 8/21/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class CuisineCell: UICollectionViewCell {
    
    @IBOutlet weak var cuisineBtn: UIButton!
    
    @IBAction func selectCuisine(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        print("selectCuisine")
        
        NotificationCenter.default.post(name: Notification.Name("reloadCuisines"), object: btn.restorationIdentifier)
    }
    
}
