//
//  AddIngredientTableViewCell.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/10/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class AddIngredientTableViewCell: UITableViewCell {
    
    @IBOutlet weak var startEditBtn: UIButton!
    @IBOutlet weak var addIngredientLbl: UILabel!

    @IBOutlet weak var quantityTxt: UITextField!
    
    @IBOutlet weak var unitTxt: UITextField!
    @IBOutlet weak var ingredientNameTxt: UITextField!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
