//
//  MyFriends.swift
//  ChewsRite
//
//  Created by Randall Ridley on 8/23/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import Social
import AWSS3

class MyFriends: BaseViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var friendList = [NSMutableDictionary]()
    var friendRequests = [NSMutableDictionary]()
    var pendingInvites = [NSMutableDictionary]()
    var filteredList = [NSMutableDictionary]()
    var selectedInviteList = [NSDictionary]()
    var friendFeedList = [NSMutableDictionary]()
    var tagsList = [String]()
    var selectedFriend : NSMutableDictionary?
    var selectedFriendItem : FriendItem?
    var isFiltered : Bool?
    var viewingRequests : Bool = true

    @IBOutlet weak var requestsTable: UITableView!
    @IBOutlet weak var friendsCV: UICollectionView!
    @IBOutlet weak var requestLbl: UILabel!
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var resendBtn: UIButton!
    
    @IBOutlet weak var invitePopup: DesignableView!
    
    var item : NSDictionary?    
    var offset : CGPoint?
    
    @IBOutlet weak var friendIV: UIImageView!
    
    @IBOutlet weak var friendDescriptionLbl: UILabel!
    @IBOutlet weak var friendHeaderLbl: UILabel!
    
    @IBOutlet weak var friendFeedTable: UITableView!
    
    @IBOutlet weak var friendPopup: UIView!
    
    @IBOutlet weak var tagsCollection: UICollectionView!

    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var search: UISearchBar!
    
    var itemArray = [FriendItem]()
    var currentItemArray = [FriendItem]()
    
    var friendItemArray = [FeedItem]()
    var currentFriendItemArray = [FeedItem]()
    
    var statusImageCache : [String:UIImage]?
    
    var selectedCuisine : Int?
    var inviteMode : Int?
    
    var emailsStr : String?
    var phonesStr : String?
    
    var pendingEmailInvites = [String]()
    var selectedEmailInviteList = [String]()
    var selectedRecipe : FeedItem?
    var selectedImage : UIImage?
    
    @IBOutlet weak var tagsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var familyCV: UICollectionView!
    var selectedUser : NSDictionary?
    var familyList = [NSDictionary]()
    
    @IBOutlet weak var unfriendBtn: UIButton!
    @IBOutlet weak var unfriendBtnHeight: NSLayoutConstraint!

    
    // MARK: - viewDidLoad
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        selectedUser = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!, "usertype":"0"] as NSDictionary
        familyList.append(selectedUser!)
        
        familyCV.delegate = self
        familyCV.dataSource = self
        
        familyCV.reloadData()
        
        statusImageCache = [String:UIImage]()
        
        menuBtn.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
//        let tb : TabController = self.parent as! TabController
//        
//        menuBtn.addTarget(self, action: #selector(tb.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
        requestsTable.dataSource = self
        requestsTable.delegate = self
        requestsTable.tableFooterView = UIView()
        
        friendFeedTable.dataSource = self
        friendFeedTable.delegate = self
        friendFeedTable.tableFooterView = UIView()
        
        friendsCV.dataSource = self
        friendsCV.delegate = self
        
        search.delegate = self
        search.backgroundImage = UIImage()
        
        search.placeholder = "Search in Friends"
        
        UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        
        tagsCollection.dataSource = self
        tagsCollection.delegate = self
        
        statusView.isHidden = true
        invitePopup.isHidden = true
        resendBtn.isHidden = true
        friendPopup.isHidden = true
        resendBtn.backgroundColor = appDelegate.crWarmGray
        resendBtn.isEnabled = false
        
        getFamily()
        getMyCuisines()
        //debugTags()
        //debug()
        
        getFriends()
        getFriendRequests()
        getInvites()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(invitesSentNotification),
            name: NSNotification.Name(rawValue: "invitesSentNotification"),
            object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: debug
    
    func debugTags () {
        
        //let dict : NSMutableDictionary = ["tagname" : "Vegan", "tagid" : "1"]
        
        tagsList.append("Vegan")
        tagsList.append("Vegan")
        tagsList.append("Vegan")
        tagsList.append("Vegan")
        tagsList.append("Vegan")
        tagsList.append("Vegan")
        
        tagsCollection.reloadData()
    }
    
    func debug () {
        
        var dict : NSMutableDictionary = ["username" : "placeholder","serving" : "1 oz", "active" : false]
        friendList.append(dict)
        
        //        dict  = ["username" : "Jane Ho","serving" : "2 oz", "active" : false]
        //        friendRequests.append(dict)
        
        dict  = ["username" : "RR", "userid" : "1","serving" : "2 oz", "active" : false]
        friendList.append(dict)
        
        requestsTable.reloadData()
        friendsCV.reloadData()
    }
    
    func debugPending () {
        
        requestLbl.text = "PENDING INVITES"
        
        var dict : NSMutableDictionary = ["username" : "Joe Blow 2","serving" : "1 oz", "active" : false]
        pendingInvites.append(dict)
        
        dict  = ["username" : "Jane Ho 2","serving" : "2 oz", "active" : false]
        pendingInvites.append(dict)
        
        requestsTable.reloadData()
        friendsCV.reloadData()
    }
    
    // MARK: Notifications
    
    @objc func invitesSentNotification (notification: NSNotification) {
        
        //print("port: \(notification.object)")
        
        let dict = notification.object as! NSDictionary
        
        emailsStr = dict.object(forKey: "emailsStr") as? String
        phonesStr = dict.object(forKey: "phonesStr") as? String
        inviteMode = Int((dict.object(forKey: "inviteMode") as? String)!)
        
        viewingRequests = false
        resendBtn.isHidden = false
        resendBtn.isEnabled = false
        
        if inviteMode == 2 //email list
        {
            viewingRequests = false
            requestLbl.text = "PENDING INVITES"
            
            let arr = emailsStr?.split(separator: ",")
            
            for a in arr!
            {
                let b = String(a)
                print("b: \(b)")
                self.pendingEmailInvites.append(String(a))
            }
        }
        
        //debugPending()
        
        requestsTable.reloadData()
        
        invitePopup.isHidden = false
        resendBtn.backgroundColor = appDelegate.crWarmGray
    }
    
    // MARK: Webservice
    
    func getFamily() {
        
        familyList.removeAll()
        
        print("getFamily")
        
        let dataString = "familyData"
        
        let urlString = "\(appDelegate.serverDestination!)getFamilyMembers.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let dict = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!,"usertype":"0"] as NSDictionary
        familyList.append(dict)
        
        self.familyCV.reloadData()
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("family jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
//                        self.familyCV.isHidden = true
//                        self.familyCV.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let members = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (members.count > 0)
                    {
                        hasData = true
                        
                        for dict in members
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.familyList.append(dict2)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if hasData == true
                            {
                                self.familyCV.isHidden = false
                                self.familyCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
//                                self.familyCV.isHidden = true
//                                self.familyCV.reloadData()
                            }
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getInvites() {
        
        pendingEmailInvites.removeAll()
        
        print("getInvites")
        
        let dataString = "inviteData"
        
        let urlString = "\(appDelegate.serverDestination!)getAppInvites.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var userid = ""
        
        if selectedUser?.object(forKey: "usertype") as! String == "0"
        {
            userid = selectedUser?.object(forKey: "userid") as! String
        }
        else
        {
            userid = selectedUser?.object(forKey: "familyid") as! String
        }
        
        let paramString = "userid=\(userid)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("invite jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
//                        self.familyCV.isHidden = true
//                        self.familyCV.reloadData()
                        
                        self.requestsTable.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let invites = uploadData as! [String]
                    
                    var hasData = false
                    
                    if (invites.count > 0)
                    {
                        hasData = true
                        
                        for dict in invites
                        {
                            self.pendingEmailInvites.append(dict)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if hasData == true
                            {
//                                self.familyCV.isHidden = false
//                                self.familyCV.reloadData()
                                
                                self.requestsTable.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
//                                self.familyCV.isHidden = true
//                                self.familyCV.reloadData()
                                
                                self.requestsTable.reloadData()
                            }
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getFriends() {
        
        friendList.removeAll()
        self.itemArray.removeAll()
        
        let dict : NSMutableDictionary = ["username" : "placeholder","serving" : "1 oz", "active" : false]
        friendList.append(dict)
        friendsCV.reloadData()
        
        self.itemArray.append(FriendItem(friendname: "placeholder", userid: "", userimage: "", story: ""))
        
        print("getFriends")
        
        let dataString = "friendsData"
        
        let urlString = "\(appDelegate.serverDestination!)getUserFriendsJSON.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var userid = ""
        
        if selectedUser?.object(forKey: "usertype") as! String == "0"
        {
            userid = selectedUser?.object(forKey: "userid") as! String
        }
        else
        {
            userid = selectedUser?.object(forKey: "familyid") as! String
        }
        
        let paramString = "userid=\(userid)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("friends jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    self.currentItemArray = self.itemArray
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.friendsCV.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.friendList.append(dict2)
                            
                            self.itemArray.append(FriendItem(friendname: "\(dict2.object(forKey: "firstname") as! String) \(dict2.object(forKey: "lastname") as! String)", userid: dict2.object(forKey: "userid") as! String, userimage: dict2.object(forKey: "userimage") as! String, story: dict2.object(forKey: "story") as! String))
                        }
                        
                        self.currentItemArray = self.itemArray
                        
                        DispatchQueue.main.sync(execute: {
                            
                            //                            self.activityView.stopAnimating()
                            //                            self.activityView.isHidden = true
                            
                            if hasData == true
                            {
                                //self.recipeStatusTV.text = "You currently have no friend requests."
                                
                                self.friendsCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
                                self.friendsCV.reloadData()
                                //self.noResultsMain.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        self.currentItemArray = self.itemArray
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.friendsCV.reloadData()
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getFriendRequests() {
        
        friendRequests.removeAll()
        
        print("getFriendRequests")
        
        let dataString = "friendsData"
        
        let urlString = "\(appDelegate.serverDestination!)manageFriendRequests.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        var userid = ""
        
        if selectedUser?.object(forKey: "usertype") as! String == "0"
        {
            userid = selectedUser?.object(forKey: "userid") as! String
        }
        else
        {
            userid = selectedUser?.object(forKey: "familyid") as! String
        }
        
        let paramString = "userid=\(userid)&query=true&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("friend requests jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.requestsTable.reloadData()
                        
                        //self.recipeStatusTV.isHidden = false
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.friendRequests.append(dict2)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
//                            self.activityView.stopAnimating()
//                            self.activityView.isHidden = true
                            
                            if hasData == true
                            {
                                //self.recipeStatusTV.text = "You currently have no friend requests."
                                
                                self.requestsTable.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
                                self.requestsTable.reloadData()
                                
                                //self.noResultsMain.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getFriendRecipes() {
        
        friendFeedList.removeAll()
        
        print("getMyRecipes")
        
        let imagename = selectedFriendItem?.userimage
        
        if imagename != ""
        {
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
            
            if let img = statusImageCache?[urlString]
            {
                friendIV.image = img
            }
        }
        
        let dataString = "recipeData"
        
        let urlString = "\(appDelegate.serverDestination!)getRecipes.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        //let userid = selectedFriend?.object(forKey: "userid") as! String
        let userid = selectedFriendItem?.userid
        
        let paramString = "userid=\(userid!)&friendid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("friend recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        

                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.friendFeedList.append(dict2)
                            
                            self.friendItemArray.append(FeedItem(recipename: dict2.object(forKey: "recipename") as! String, preptime: dict2.object(forKey: "preptime") as! String, cooktime: dict2.object(forKey: "cooktime") as! String, imagename: dict2.object(forKey: "imagename") as! String, videoname: "", calories: dict2.object(forKey: "calories") as! String, source: dict2.object(forKey: "source") as! String, sourceurl: dict2.object(forKey: "sourceurl") as! String, isFavorite: dict2.object(forKey: "isFavorite") as! String, username: dict2.object(forKey: "username") as! String, recipeid: dict2.object(forKey: "recipeid") as! String, cuisinetags: dict2.object(forKey: "cuisinetags") as! String, mealplanid: "", mealplandate: "", mealid: "", userimage: dict2.object(forKey: "userimage") as! String, firstname: dict2.object(forKey: "firstname") as! String, lastname: dict2.object(forKey: "lastname") as! String, description: dict2.object(forKey: "recipedescription") as! String, servings: dict2.object(forKey: "servings") as! String, userid: dict2.object(forKey: "userid") as! String, totaltime: dict2.object(forKey: "totaltime") as! String, rating: dict2.object(forKey: "rating") as! String, timeAdded: dict2.object(forKey: "timeAdded") as! String))
                        }
                        
                        self.currentFriendItemArray = self.friendItemArray
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.friendFeedTable.reloadData()
                                                        
                            if userid == self.appDelegate.userid
                            {
                                self.unfriendBtn.isHidden = true
                                self.unfriendBtnHeight.constant = 0
                            }
                            else
                            {
                                self.unfriendBtn.isHidden = false
                                self.unfriendBtnHeight.constant = 30
                            }
                            
                            self.friendPopup.isHidden = false
                            
//                            self.activityView.stopAnimating()
//                            self.activityView.isHidden = true
//
//                            if hasData == true
//                            {
//                                self.recipeStatusTV.text = "You currently have no recipes."
//
//                                self.updateDisplayList()
//                            }
//                            else
//                            {
//                                print("no data")
//
//                                //self.noResultsMain.isHidden = false
//                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getMyCuisines() {
        
        //displayItems.removeAll()
        
        print("getMyCuisines")
        
        let dataString = "cuisineData"
        
        let urlString = "\(appDelegate.serverDestination!)getCuisines.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("cuisines jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.recipeStatusTV.isHidden = false
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let tags = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (tags.count > 0)
                    {
                        hasData = true
                        
                        var ind = 0
                        
                        for dict in tags
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.tagsList.append(dict2.object(forKey: "cuisinename") as! String)

                            ind += 1
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if self.tagsList.count > 3
                            {
                                self.tagsHeight.constant = 100
                            }
                            else
                            {
                                self.tagsHeight.constant = 30
                            }
                            
                            if hasData == true
                            {
                                self.tagsCollection.reloadData()
                                
                                //self.recipeStatusTV.text = "You currently have no recipes."
                                
                                //self.updateDisplayList()
                                
                                //self.cuisineCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
                                //self.noResultsMain.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func manageFriend () {
        
        print("managementFriend")
        
        let dataString = "friendsData"
        
        let urlString = "\(appDelegate.serverDestination!)manageFriendRequests.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "friendid=\(selectedFriendItem!.userid)&userid=\(appDelegate.userid!)&remove=true&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //                        self.activityView.stopAnimating()
                        //                        self.activityView.isHidden = true
                    })
                    
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "friend removed"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            self.friendPopup.isHidden = true
                            self.getFriends()
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func resendInvite () {
        
        print("resendInvite")
        
        let dataString = "inviteData"
        
        let urlString = "\(appDelegate.serverDestination!)inviteEmail.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let emailsStr = selectedEmailInviteList.joined(separator: ",")
        
        let paramString = "emails=\(emailsStr)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //                        self.activityView.stopAnimating()
                        //                        self.activityView.isHidden = true
                    })
                    
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "invites sent"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            self.selectedInviteList.removeAll()
                            self.requestsTable.reloadData()
                            self.resendBtn.backgroundColor = self.appDelegate.crWarmGray
                            self.resendBtn.isEnabled = false
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
            }.resume()
    }
    
    // MARK: - Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.restorationIdentifier == "friendTable"
        {
            return self.currentFriendItemArray.count
        }
        else
        {
            if viewingRequests == true
            {
                return self.friendRequests.count
            }
            else
            {
                if inviteMode == 2 //email list
                {
                    return self.pendingEmailInvites.count
                }
                else
                {
                    return self.pendingInvites.count
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView.restorationIdentifier == "friendTable"
        {
            return 263
        }
        else
        {
            return 38
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.restorationIdentifier == "friendTable"
        {
            selectedRecipe = currentFriendItemArray[indexPath.row]
            
            let imagename = selectedRecipe?.imagename
            
            if imagename != ""
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                
                if let img = statusImageCache?[urlString]
                {
                    selectedImage = img
                }
            }
            
            performSegue(withIdentifier: "ViewRecipe", sender: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var dict : NSDictionary?
        
        if tableView.restorationIdentifier == "friendTable"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2")! as? FeaturedRecipeTableViewCell
            
            let item = currentFriendItemArray[indexPath.row]
            
            //cell?.recipeIV.image = UIImage.init(named: (dict?.object(forKey: "imagename") as? String)!)
            
            let imagename = item.imagename
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
                
                if let img = statusImageCache?[urlString]
                {
                    cell?.recipeIV.image = img
                }
                else
                {
                    cell?.recipeIV.image = UIImage(named: "profile-icon")
                    
                    self.downloadImage(imagename: imagename, urlString: urlString, iv: ((cell?.recipeIV!)!))
                }
            }
            
            //to do randall check if this used
            
//            let userimagename = item.userimage
//
//            if userimagename != nil
//            {
//                let urlString = "\(appDelegate.serverDestination!)userimages/\(userimagename)"
//
//                if let img = statusImageCache?[urlString]
//                {
//                    cell?.userIV.image = img
//                }
////                else
////                {
////                    cell?.userIV.image = UIImage(named: "profile-icon")
////
////                    self.downloadImage(imagename: imagename!, urlString: urlString, recipeCell: (cell)!, friendCell: nil, friendCell2: nil, friendCell3: (cell)!)
////                }
//            }
            
            let firstname = item.firstname
            let lastname = item.lastname
            
            cell?.nameLbl.text = "\(firstname) \(lastname)"
            cell?.timeLbl.text = "1 hour ago"
            
            
            cell?.headerLbl.text = item.recipename
            cell?.infoLbl.text = "\(item.preptime) • \(item.calories)"
            
            let attributedString = NSMutableAttributedString(string: "Source: \(item.source)", attributes: [
                .font: UIFont(name: "Avenir-Book", size: 13.0)!,
                .foregroundColor: appDelegate.crGray
                ])
            attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 13.0)!, range: NSRange(location: 0, length: 7))
            
            cell?.sourceLbl.attributedText = attributedString
            
            cell?.favoriteBtn.restorationIdentifier = item.recipeid
            cell?.favoriteBtn.accessibilityIdentifier = "\(indexPath.row)"
            
            let video = item.videoname
            
            if video == ""
            {
                cell?.videoBtn.isHidden = true
            }
            else
            {
                cell?.videoBtn.isHidden = false
            }
            
            let isFavorite = item.isFavorite
            
            if isFavorite == "1"
            {
                cell?.favoriteBtn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
                cell?.isFavorite = true
                //dict?.setValue(true, forKey: "isFavorite")
            }
            else
            {
                cell?.favoriteBtn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
                cell?.isFavorite = false
                //dict?.setValue(false, forKey: "isFavorite")
            }
            
            cell?.favoriteBtn.addTarget(self, action: #selector(self.manageFavorite(_:)), for: UIControlEvents.touchUpInside)
            
            return cell!
        }
        else
        {
            var cell : FriendRequestCell?
            
            if viewingRequests == false && inviteMode == 1
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "cell1")! as? FriendRequestCell
                dict = self.pendingInvites[(indexPath as NSIndexPath).row] as NSDictionary
                cell?.requestBtn.setBackgroundImage(#imageLiteral(resourceName: "open-circle"), for: .normal)
            }
            else
            {
                if inviteMode == 2 //email list
                {
                    let str = pendingEmailInvites[indexPath.row]
                    
                    cell = tableView.dequeueReusableCell(withIdentifier: "cell2")! as? FriendRequestCell
                    cell?.nameLbl.text = str
                }
                else
                {
                    cell = tableView.dequeueReusableCell(withIdentifier: "cell1")! as? FriendRequestCell

                    dict = self.friendRequests[(indexPath as NSIndexPath).row] as NSDictionary
                    
                    //cell?.friendIV.image = UIImage.init(named: (dict?.object(forKey: "userimage") as? String)!)
                    
                    let imagename = dict?.object(forKey: "imagename") as? String
                    
                    if imagename != nil
                    {
                        let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                        
                        if let img = statusImageCache?[urlString]
                        {
                            cell?.friendIV.image = img
                        }
                        else
                        {
                            cell?.friendIV.image = UIImage(named: "profile-icon")
                            
                            self.downloadImage(imagename: imagename!, urlString: urlString, iv: ((cell?.friendIV!)!))
                        }
                    }
                    
                    //let username = dict?.object(forKey: "username") as! String
                    let firstname = dict?.object(forKey: "firstname") as! String
                    let lastname = dict?.object(forKey: "lastname") as! String
                    
                    cell?.selectionStyle = .none
                    cell?.nameLbl.text = "\(firstname) \(lastname)"
                }
                
                cell?.requestBtn.restorationIdentifier = "\(indexPath.row)"
                cell?.requestBtn.addTarget(self, action: #selector(self.selectItem(_:)), for: UIControlEvents.touchUpInside)
            }
            
            return (cell)!
        }
    }
    
    @IBAction func manageFavorite(_ sender: Any) {
        
        offset = friendFeedTable.contentOffset
        
        let btn = sender as! UIButton
        
        let dict: NSMutableDictionary?
        
        if isFiltered == true
        {
            dict = filteredList[Int(btn.accessibilityIdentifier!)!]
        }
        else
        {
            dict = friendFeedList[Int(btn.accessibilityIdentifier!)!]
        }
        
        if dict?.object(forKey: "isFavorite") as! String == "0"
        {
            dict?.setValue("1", forKey: "isFavorite")
            btn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
        }
        else
        {
            dict?.setValue("0", forKey: "isFavorite")
            btn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
        }
        
        manageFavorite(recipeID: btn.restorationIdentifier!)
    }
    
    func manageFavorite( recipeID : String) {
        
        print("manageFavorite")
        
        let dataString = "favoriteData"
        
        let urlString = "\(appDelegate.serverDestination!)manageFavorites.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "recipeid=\(recipeID)&userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //                        self.activityView.stopAnimating()
                        //                        self.activityView.isHidden = true
                    })
                    
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "favorite item saved" || status == "favorite deleted"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func acceptRequest () {
        
        print("acceptRequest")
        
        let dataString = "friendsData"
        
        let requestid = selectedFriend?.object(forKey: "requestid") as! String
        let senderid = selectedFriend?.object(forKey: "senderid") as! String
        let receiverid = selectedFriend?.object(forKey: "receiverid") as! String
        
        let urlString = "\(appDelegate.serverDestination!)manageFriendRequests.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "requestid=\(requestid)&senderid=\(senderid)&receiverid=\(receiverid)&accept=true&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("accept request jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //                        self.activityView.stopAnimating()
                        //                        self.activityView.isHidden = true
                    })
                    
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "friend request accepted"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            let ind = self.friendRequests.index(of: self.selectedFriend as! NSMutableDictionary)
                            
//                            let indexPath = IndexPath(row: 0, section: 0)
//                            self.likesTable.scrollToRow(at: indexPath, at: .bottom, animated: false)
                            
                            self.friendRequests.remove(at: ind!)
                            let indexPath = IndexPath(row: ind!, section: 0)
                            self.requestsTable.deleteRows(at: [indexPath], with: .fade)
                            
                            self.getFriends()
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    @IBAction func selectItem(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        let ind = Int(btn.restorationIdentifier!)
        
        if viewingRequests == true
        {
            //accept request
            
            selectedFriend = friendRequests[ind!]
            
            acceptRequest()
        }
        else
        {
            if inviteMode == 2 //email list
            {
                let item1 = pendingEmailInvites[ind!]
                
                if selectedEmailInviteList.contains(item1)
                {
                    let ind2 = selectedEmailInviteList.index(of: item1)
                    
                    btn.setBackgroundImage(#imageLiteral(resourceName: "open-circle"), for: .normal) //replace with check
                    
                    selectedEmailInviteList.remove(at: ind2!)
                }
                else
                {
                    btn.setBackgroundImage(#imageLiteral(resourceName: "checkGreen"), for: .normal) //replace with check
                    selectedEmailInviteList.append(item1)
                }
                
                if selectedEmailInviteList.count > 0
                {
                    resendBtn.backgroundColor = appDelegate.crOrange
                    resendBtn.isEnabled = true
                }
                else
                {
                    resendBtn.backgroundColor = appDelegate.crWarmGray
                    resendBtn.isEnabled = false
                }
            }
            else
            {
                item = pendingInvites[ind!]
                
                if selectedInviteList.contains(item!)
                {
                    let ind2 = selectedInviteList.index(of: item!)
                    
                    btn.setBackgroundImage(#imageLiteral(resourceName: "open-circle"), for: .normal) //replace with check
                    
                    selectedInviteList.remove(at: ind2!)
                }
                else
                {
                    btn.setBackgroundImage(#imageLiteral(resourceName: "checkGreen"), for: .normal) //replace with check
                    selectedInviteList.append(item!)
                }
                
                if selectedInviteList.count > 0
                {
                    resendBtn.backgroundColor = appDelegate.crOrange
                    resendBtn.isEnabled = true
                }
                else
                {
                    resendBtn.backgroundColor = appDelegate.crWarmGray
                    resendBtn.isEnabled = false
                }
            }
            
        }
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.restorationIdentifier == "family"
        {
            return CGSize.init(width: 42, height: 42)
        }
        else
        {
            let kWhateverHeightYouWant = 190
            
            return CGSize.init(width: 600, height: CGFloat(kWhateverHeightYouWant))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.restorationIdentifier == "family"
        {
            return self.familyList.count
        }
        else if collectionView.restorationIdentifier == "tags"
        {
            return tagsList.count
        }
        else
        {
            return currentItemArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.restorationIdentifier == "family"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FamilyCVCell
            
            let dict = familyList[indexPath.row]
            
            let imagename = dict.object(forKey: "userimage") as? String
            
            if selectedUser == dict
            {
                let img = UIImage.init(named: "selectWhite")
                cell?.si.image = img
            }
            else
            {
                cell?.si.image = nil
            }
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                
                //let url2 = NSURL(string:urlString)!
                
                // If this image is already cached, don't re-download
                
                if let img = statusImageCache![urlString]
                {
                    //print("load cached status image")
                    
                    cell?.userImg.image = img
                }
                else
                {
                    //print("status image not
                    
                    let imagename = dict.object(forKey: "userimage") as? String
                    
                    if imagename != nil && imagename != ""
                    {
                        let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                        
                        print("img urlString: \(urlString)")
                        
                        if let img = statusImageCache![urlString]
                        {
                            cell?.userImg.image = img
                        }
                        else
                        {
                            cell?.userImg.image = UIImage(named: "profile-icon")
                            
                            self.downloadImage(imagename: imagename!, urlString: urlString, iv: ((cell?.userImg!)!))
                        }
                    }
                    else
                    {
                        cell?.userImg.image = UIImage(named: "profile-icon")
                    }
                }
            }
            
            return cell!
        }
        else if collectionView.restorationIdentifier == "tags"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as? TagsCollectionViewCell
            
            let tag = tagsList[indexPath.row]
            
            //cell?.frame.size = CGSize.init(width: tagsCV.frame.width * 0.5, height: 292) //Randall. check this
            
            //cell?.recipeIV.image = UIImage.init(named: (dict.object(forKey: "imagename") as? String)!)
            
            if indexPath.row == 3
            {
                cell?.leadingConstraint.constant = 5
            }
            else
            {
                 cell?.leadingConstraint.constant = 0
            }
            
            if indexPath.row == 5
            {
                cell?.trailingConstraint.constant = 5
            }
            
            if indexPath.row < 5
            {
                cell?.tagLbl.text = tag
            }
            else
            {
                cell?.tagLbl.text = "..."
            }
            
            cell?.tagLbl.layer.borderWidth = 0.5
            cell?.tagLbl.layer.borderColor = appDelegate.crGray.cgColor
            cell?.tagLbl.textColor = appDelegate.crGray
            cell?.tagLbl.layer.cornerRadius = 10.0
            
            return cell!
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FriendsCell
            
            let item = currentItemArray[indexPath.row]
            
            cell?.nameLbl.text = item.friendname
            
            if item.friendname != "placeholder"
            {
                //cell?.friendIV.image = UIImage.init(named: item.userimage)
                
                let imagename = item.userimage
    
                if imagename != ""
                {
                    let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
    
                    if let img = statusImageCache?[urlString]
                    {
                        cell?.friendIV.image = img
                    }
                    else
                    {
                        cell?.friendIV.image = UIImage(named: "profile-icon")
    
                        self.downloadImage(imagename: imagename, urlString: urlString, iv: cell?.friendIV)
                    }
                }
            }
            else
            {
                cell?.friendIV.image = #imageLiteral(resourceName: "light")
                cell?.nameLbl.text = "Invite"
            }
            
            return cell!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.restorationIdentifier == "family"
        {
            selectedUser = familyList[indexPath.row]
            
            print("selectedUser: \(selectedUser)")
            
            getFriends()
            getFriendRequests()
            getInvites()
            
            familyCV.reloadData()
        }
        else if collectionView.restorationIdentifier == "tags"
        {
            selectedCuisine = indexPath.row
            selectTag()
        }
        else
        {
            if indexPath.row == 0
            {
                performSegue(withIdentifier: "InviteFriends", sender: nil)
            }
            else
            {
                selectedFriendItem = currentItemArray[indexPath.row]
                friendHeaderLbl.text = selectedFriendItem?.friendname
                friendDescriptionLbl.text = selectedFriendItem?.story
                getFriendRecipes()
            }
        }
    }
    
    // MARK: Actions
    
    func selectTag() {
        
        print("selectedCuisine: \(selectedCuisine!)")
        
//        currentFriendItemArray = friendItemArray.filter({ item -> Bool in
//
//            let tags = item.cuisinetags.components(separatedBy: ",")
//
//            print("tags: \(tags)")
//
//            return tags.contains("\(selectedCuisine!)".lowercased())
//        })
        
        if selectedCuisine == 0
        {
            isFiltered = false
            currentFriendItemArray = friendItemArray
        }
        else
        {
            //change filter

            currentFriendItemArray = friendItemArray.filter({ item -> Bool in

                let tags = item.cuisinetags.components(separatedBy: ",")

                print("tags: \(tags)")

                return tags.contains("\(selectedCuisine!)".lowercased())
            })
        }
        
        friendFeedTable.reloadData()
    }
    
    @IBAction func acceptInvite(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        //let item = btn.restorationIdentifier as! String
        
        let ind = Int(btn.restorationIdentifier!)
        
        friendPopup.isHidden = false
    }
    
    @IBAction func closeFriendPopup(_ sender: Any) {
        
        friendPopup.isHidden = true
    }
    
    @IBAction func closePopup(_ sender: Any) {
        
        invitePopup.isHidden = true
    }
    
    @IBAction func resendInvite(_ sender: Any) {
        
        resendInvite()
    }
    
    @IBAction func shareAppFacebook(_ sender: Any) {
        
        let isInstalled = UIApplication.shared.canOpenURL(URL.init(string: "fb://")!)
        
        if (isInstalled) {
            
            let vc = SLComposeViewController(forServiceType:SLServiceTypeFacebook)
            //vc.add(imageView.image!)
            vc?.add(URL(string: "http://chewsritelink.com"))
            //vc?.setInitialText("Use my signup code: \(self.appDelegate.referralCode!)...")
            
            UIPasteboard.general.string = "Download Chewsrite at http://chewsritelink.com and use my signup code: \(self.appDelegate.referralCode!)..."
            
            self.present(vc!, animated: true, completion: nil)
        }
        else
        {
            print("no fb")
            
            let alert = UIAlertController(title: nil, message: "Facebook is not installed on this device.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func shareAppTwitter(_ sender: Any) {
        
        let isInstalled = UIApplication.shared.canOpenURL(URL.init(string: "twitter://")!)
        
        if (isInstalled) {
            
            let vc = SLComposeViewController(forServiceType:SLServiceTypeTwitter)
            //vc.add(imageView.image!)
            vc?.add(URL(string: "http://chewsritelink.com"))
            vc?.setInitialText("Use my signup code: \(self.appDelegate.referralCode!)...")
            self.present(vc!, animated: true, completion: nil)
        }
        else
        {
            print("no twitter")
            
            let alert = UIAlertController(title: nil, message: "Twitter is not installed on this device.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func managementFriend(_ sender: Any) {
        
        manageFriend()
    }
    
    // MARK: Search
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        currentItemArray = itemArray.filter({ item -> Bool in
            
            switch searchBar.selectedScopeButtonIndex {
            case 0:
                if searchText.isEmpty { return true }
                return item.friendname.lowercased().contains(searchText.lowercased())
            default:
                return false
            }
        })
        
        friendsCV.reloadData()
    }
    
    func downloadImage (imagename : String, urlString : String, iv: UIImageView?) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache?[urlString] = image
                        
                        iv!.image = image
                        
                        if urlString.contains(self.appDelegate.profileImg!)
                        {
                            self.appDelegate.userImage = image
                        }
                    }
                    
                    //println("test")
                })
                return nil
            }))
        }
        else
        {
            iv!.image = UIImage.init(named: "profile-icon")
        }
    }
    
    // MARK: Memory

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ViewRecipe"
        {
            let destination = segue.destination as! RecipeDetails
            destination.selectedItem = selectedRecipe
            destination.selectedImage = selectedImage
        }
    }
}

class FriendItem {
    
    let friendname: String
    let userid: String
    let userimage: String
    let story: String
    
//    $object->userid = blankNull($row_rsUserInfo['userid']);
//    $object->username = blankNull($row_rsUserInfo['username']);
//    $object->firstname = blankNull($row_rsUserInfo['firstname']);
//    $object->lastname = blankNull($row_rsFriends['lastname']);
//    $object->city = blankNull($row_rsUserInfo['city']);
//    $object->state = blankNull($row_rsUserInfo['state']);
//    $object->zip = blankNull($row_rsUserInfo['zip']);
//    $object->occupation = blankNull($row_rsUserInfo['occupation']);
//    $object->college = blankNull($row_rsUserInfo['college']);
//    $object->personality = blankNull($row_rsUserInfo['personality']);
//    $object->social = blankNull($row_rsUserInfo['social']);
//    $object->interest = blankNull($row_rsUserInfo['interest']);
//    $object->artinterests = blankNull($row_rsUserInfo['artinterests']);
//    $object->story = blankNull($row_rsUserInfo['story']);
    
    //reuqestid senierd
    
    init(friendname: String, userid: String, userimage: String, story: String) {
        
        self.friendname = friendname
        self.userid = userid
        self.userimage = userimage
        self.story = story
    }
}

class PersonItem {
    
    let firstname: String
    let lastname: String
    let story: String
    let userid: String
    let userimage: String
    let username: String
    
    init(firstname: String, lastname: String, story: String, userid: String, userimage: String, username: String) {
        self.firstname = firstname
        self.lastname = lastname
        self.story = story
        self.userid = userid
        self.userimage = userimage
        self.username = username
    }
}
