//
//  AddNewRecipe.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/10/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3
import AVFoundation

class AddNewRecipe: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var imageIV: UIImageView!
    @IBOutlet weak var ingredientTable: UITableView!
    
    @IBOutlet weak var privateSecretBtn: UISegmentedControl!
    
    var ingredientOptions = [NSDictionary]()
    var directionList = [NSDictionary]()
    var nutritionList = [NSDictionary]()
    var tipsList = [NSDictionary]()
    
    var enteredVals = [NSDictionary]()
    var isUpdating: Bool?
    var friendlySelected: Bool?
    
    var servingSize : Float = 1.0
    
    @IBOutlet weak var recipeNameBtn: UIButton!
    @IBOutlet weak var recipeNameLbl: UILabel!
    @IBOutlet weak var recipeNameTxt: UITextField!
    @IBOutlet weak var timePicker: UIPickerView!
    
    @IBOutlet weak var kidFriendlyBtn: UIButton!
    
    @IBOutlet weak var contentScrollview: UIScrollView!
    @IBOutlet weak var infoSegment: UISegmentedControl!
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var servingsLbl: UILabel!
    
    @IBOutlet weak var prepView: UIView!
    @IBOutlet weak var prepTimeLbl: UILabel!
    @IBOutlet weak var prepLblHeight: NSLayoutConstraint!
    
    @IBOutlet weak var cookView: UIView!
    @IBOutlet weak var cookTimeLbl: UILabel!
    @IBOutlet weak var cookLblHeight: NSLayoutConstraint!
    
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var servingsView: UIView!
    
    var hoursList = NSMutableArray()
    var minutesList = NSMutableArray()
    
    var editingPrep : Bool?
    
    @IBOutlet weak var timeHeaderLbl: UILabel!
    
    var selectedPrepHours = 0
    var selectedPrepMins = 1
    
    var selectedCookHours = 0
    var selectedCookMins = 1
    
    var selectedDirectionsDict : NSMutableDictionary?
    var selectedTipsDict : NSMutableDictionary?
    
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    var uploadImageName : String?
    var selectedImage : UIImage?
    
    var imagePicker : UIImagePickerController!
    
    var recipeDict : NSMutableDictionary?
    var nutritionValues = [String]()
    
    var currentTextfieldValue = ""
    
    var selectedVideoURL : URL?
    
//    var ingredients = [NSMutableDictionary]()
//    var directions = [NSMutableDictionary]()
//    var nutrition = [NSMutableDictionary]()
//    var tips = [NSMutableDictionary]()
    
    @IBOutlet weak var uploadLbl: UILabel!
    
//    @IBOutlet weak var prepTimeHeight: NSLayoutConstraint!
//    @IBOutlet weak var prepTimeLblTop: NSLayoutConstraint!
    
    var nutritionValLabels = ["wCalories", "wCarbohydrates", "wProtein", "wFat", "wSaturatedFat", "wUnsaturatedFat", "wTransFat", "wCholestrol", "wSodium", "wPotassium", "wFiber", "wSugars", "wVitaminA", "wVitaminC", "wCalcium", "wIron", "dCalories", "dCarbohydrates", "dProtein", "dFat", "dSaturatedFat", "dUnsaturatedFat", "dTransFat", "dCholestrol", "dSodium", "dPotassium", "dFiber", "dSugars", "dVitaminA", "dVitaminC", "dCalcium", "dIron"]
    var nutritionLabelList = ["Calories","Carbohydrates","Protein","Fat","     Saturated Fat","     Unsaturated Fat","     Trans Fat","Cholestrol","Sodium","Potassium","Fiber","Sugars","Vitamin A","Vitamin C","Calcium","Iron"]
    
    var nutritionFields = [UITextField]()
    var numberToolbar : UIToolbar?
    var currentTextfield : UITextField?

    func debug () {
        
        imageIV.image = UIImage.init(imageLiteralResourceName: "shrimp-alfredo")
        selectedImage = imageIV.image

        recipeNameTxt.text = "Seafood alfredo"
        descriptionLbl.text = "alfredo description"
        servingsLbl.text = "1.0 Serving"
        prepTimeLbl.text = "10 minutes"
        cookTimeLbl.text = "20 minutes"
//        cookTimeLbl.text = "20 min"
//        cookTimeLbl.text = "20 min"
        
        prepTimeLbl.isHidden = false
        cookTimeLbl.isHidden = false
        
        let c1 = prepTimeLbl.text?.split(separator: " ")
        let c2 = cookTimeLbl.text?.split(separator: " ")
        
        let c3 : Int = Int(c1![0])!
        let c4 : Int = Int(c2![0])!
        
        let c6 = c3 + c4
        
        recipeDict = ["active":"false"]
        
        recipeDict?.setValue(recipeNameTxt.text, forKey: "recipename")
        recipeDict?.setValue(descriptionLbl.text, forKey: "description")
        recipeDict?.setValue(servingSize, forKey: "servings")
        recipeDict?.setValue(prepTimeLbl.text, forKey: "preptime")
        recipeDict?.setValue(cookTimeLbl.text, forKey: "cooktime")
        recipeDict?.setValue("\(c6) minutes", forKey: "totaltime")
        recipeDict?.setValue(friendlySelected, forKey: "iskidfriendly")
        recipeDict?.setValue(privateSecretBtn.isSelected, forKey: "isprivate")
        
        //to do randall
        //add fields
        
        recipeDict?.setValue("Hello Fresh", forKey: "source")
        recipeDict?.setValue("1,2", forKey: "cuisinetags")
        recipeDict?.setValue("200", forKey: "calories")
        
        var dict : NSMutableDictionary = ["name" : "","ingredient" : "Shrimp","unit" : "oz", "active" : false,"quantity" : "1","recipeorder" : "0", ]
        
        ingredientOptions.append(dict)
        
        dict = ["name" : "","ingredient" : "alfredo sauce","unit" : "oz", "active" : false,"quantity" : "1","recipeorder" : "1", ]
        
        ingredientOptions.append(dict)
        
        dict = ["name" : "","ingredient" : "alfredo noodles","unit" : "oz", "active" : false,"quantity" : "8","recipeorder" : "1", ]
        
        ingredientOptions.append(dict)
        
        let dict1 : NSMutableDictionary = ["name" : "","title" : "direction 1","directions" : "Cook the fettuccine according to package directions. Meanwhile, in a large skillet, saute shrimp and scallops in 1 tablespoon oil for 3-5 minutes or until shrimp turn pink and scallops are opaque. Remove and keep warm.", "active" : false,"directionsorder" : "0"]
        
        directionList.append(dict1)
        
        let dict2 : NSMutableDictionary = ["name" : "","tips" : "tip 1", "active" : false]
        
        tipsList.append(dict2)

        ingredientTable.reloadData()
        
        descriptionLbl.textColor = appDelegate.gray51
        
        //performSegue(withIdentifier: "SaveRecipe", sender: nil)
    }
    
    override func viewDidLayoutSubviews() {
        
        contentScrollview.contentSize = CGSize.init(width: contentScrollview.frame.width, height: 1500)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tableHeight.constant = 600
        
        servingsLbl.text = "\(servingSize) Servings"
        
        ingredientTable.delegate = self
        ingredientTable.dataSource = self
        ingredientTable.tableFooterView = UIView()
        ingredientTable.separatorStyle = .none
        
//        ingredientTable.estimatedRowHeight = 100
//        ingredientTable.rowHeight = UITableViewAutomaticDimension
//        
        ingredientTable.estimatedRowHeight = UITableViewAutomaticDimension
        ingredientTable.rowHeight = UITableViewAutomaticDimension
        
        timePicker.delegate = self
        timePicker.dataSource = self
        
        recipeNameTxt.delegate = self
        
        //recipeNameTxt.isHidden = true
        
        initVals()
        
        kidFriendlyBtn.layer.borderWidth = 1.0
        kidFriendlyBtn.layer.borderColor = appDelegate.gray180.cgColor
        kidFriendlyBtn.layer.cornerRadius = 2.0
        
        prepView.layer.borderWidth = 1.0
        prepView.layer.borderColor = appDelegate.crLightBlue.cgColor
        prepView.layer.cornerRadius = 4.0
        
        cookView.layer.borderWidth = 1.0
        cookView.layer.borderColor = appDelegate.crLightBlue.cgColor
        cookView.layer.cornerRadius = 4.0
        
        servingsView.layer.borderWidth = 1.0
        servingsView.layer.borderColor = appDelegate.crWarmGray.cgColor
        servingsView.layer.cornerRadius = 4.0
        
        prepTimeLbl.isHidden = true
        prepLblHeight.constant = 10
        
        cookTimeLbl.isHidden = true
        cookLblHeight.constant = 10
        
        timeView.isHidden = true
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClickedRecipe))
        keyboardDoneButtonView.items = [doneButton]
        
        recipeNameTxt?.inputAccessoryView = keyboardDoneButtonView
        
        let btnItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(dismissKeyboard))
        
        numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width:  self.view.frame.size.width, height: 50))
        
        numberToolbar?.backgroundColor = UIColor.darkGray
        numberToolbar?.barStyle = UIBarStyle.default
        numberToolbar?.tintColor = UIColor.black
        numberToolbar?.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            btnItem]
        
        numberToolbar?.sizeToFit()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(descriptionSavedNotification),
            name: NSNotification.Name(rawValue: "descriptionSaved"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(directionSavedNotification),
            name: NSNotification.Name(rawValue: "directionSaved"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(tipsSavedNotification),
            name: NSNotification.Name(rawValue: "tipsSaved"),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(goToRecipesNotification),
            name: NSNotification.Name(rawValue: "goToRecipes"),
            object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(Login.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Login.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //debug()
    }
    
    func initVals() {
        
        for val in nutritionLabelList
        {
            if val == "Calories"
            {
                nutritionValues.append("0")
            }
            else
            {
                nutritionValues.append("0.0")
            }
        }
        
        for i in 0 ..< 100
        {
            let val = "\(i)"
            hoursList.add(val)
        }
        
        for i in 0 ..< 60
        {
            let val = "\(i)"
            minutesList.add(val)
        }
        
        timePicker.reloadAllComponents()
        timePicker.selectRow(1, inComponent: 1, animated: false)
        
        let dict : NSMutableDictionary = ["name" : "add ingredient", "active" : false]
        
        ingredientOptions.append(dict)
        
        
        let dict2 : NSMutableDictionary = ["name" : "add direction", "active" : false]
        
        directionList.append(dict2)
        
        let dict3 : NSMutableDictionary = ["name" : "add special tips", "active" : false]
        
        tipsList.append(dict3)
        
        ingredientTable.reloadData()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if currentTextfield?.restorationIdentifier != "RecipeName"
        {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                
                if self.view.frame.origin.y == 0 {
                    
                    //self.view.frame.origin.y -= keyboardSize.height
                    self.view.frame.origin.y -= 200
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if currentTextfield?.restorationIdentifier != "RecipeName"
        {
            if self.view.frame.origin.y != 0 {
                
                self.view.frame.origin.y = 0
            }
        }
    }
    
    @objc func dismissKeyboard () {
        
        self.view.endEditing(true)
    }
    
    // MARK: Notifications
        
    @objc func goToRecipesNotification (notification: NSNotification) {
        
        clearFields()
        initVals()
        
        NotificationCenter.default.post(name: Notification.Name("refreshHomeFeed"), object: nil)
        NotificationCenter.default.post(name: Notification.Name("refreshRecipes"), object: nil)
        
        print("goToRecipesNotification")
        self.tabBarController?.selectedIndex = 4
    }
    
    func clearFields () {
        
        recipeNameTxt.text = ""
        descriptionLbl.text = "Write a description"
        descriptionLbl.textColor = appDelegate.crWarmGray
        servingsLbl.text = ""
        prepTimeLbl.text = ""
        cookTimeLbl.text = ""
        
        prepTimeLbl.isHidden = true
        cookTimeLbl.isHidden = true
        
        selectedImage = nil
        recipeDict = nil
        
        privateSecretBtn.selectedSegmentIndex = 0
        infoSegment.selectedSegmentIndex = 0
        kidFriendlyBtn.setBackgroundImage(UIImage.init(named: "unselected"), for: .normal)

        ingredientOptions.removeAll()
        directionList.removeAll()
        tipsList.removeAll()
        nutritionValues.removeAll()
        //nutritionList.removeAll()
    }
    
    @objc func tipsSavedNotification (notification: NSNotification) {
        
        //print("port: \(notification.object)")
        
        let tips = notification.object as? NSDictionary
        
        //22?.setValue(tips?.object(forKey: "title"), forKey: "title")
        selectedTipsDict?.setValue(tips?.object(forKey: "tips"), forKey: "tips")
        selectedTipsDict?.setValue("", forKey: "name")
        
        //directionList.append(direction!)
        
        let dict2 : NSMutableDictionary = ["name" : "add special tips", "active" : false]
        
        tipsList.append(dict2)
        
        ingredientTable.reloadData()
    }
    
    @objc func directionSavedNotification (notification: NSNotification) {
        
        print("port: \(notification.object)")
        
        let direction = notification.object as? NSDictionary
        
        selectedDirectionsDict = ["name":""] as NSMutableDictionary
        
        selectedDirectionsDict?.setValue(direction?.object(forKey: "title"), forKey: "title")
        selectedDirectionsDict?.setValue(direction?.object(forKey: "directions"), forKey: "directions")
        //selectedDirectionsDict?.setValue("", forKey: "name")
        
        selectedDirectionsDict?.setValue(directionList.count, forKey: "directionsorder")
        
        directionList.append(selectedDirectionsDict!)
        
//        let dict2 : NSMutableDictionary = ["name" : "add direction", "active" : false]
//        
//        directionList.append(dict2)
//
        
        infoSegment.selectedSegmentIndex = 1
        ingredientTable.reloadData()
    }
    
    @objc func descriptionSavedNotification (notification: NSNotification) {
        
        //print("port: \(notification.object)")
        
        descriptionLbl.text = notification.object as? String
        descriptionLbl.font = UIFont(name: "Avenir-Book", size: 14.0)
        descriptionLbl.textColor = appDelegate.gray51
    }
    
    // MARK: Webservice
    
    func getNutritionData() {
        
        print("getNutritionData")
        
        let urlString = "\(appDelegate.serverDestination!)getNutrition.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let recipeID = ""
        
        let paramString = "recipeid=\(recipeID)&devStatus=\(appDelegate.devStage!)"
        
        print("paramString3: \(paramString)")
        
        //        else
        //        {
        //            isSearching = false
        //        }
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict["nutritionData"]! is NSNull {
                    
                    print("no nutrition data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.noResultsMain.isHidden = false
                    })
                    
                    //self.statusLbl.isHidden = false
                }
                else
                {
                    let uploadData = dataDict["nutritionData"]! as! NSDictionary
                    
                    if (uploadData != nil)
                    {
                        //var status = uploadData.object(forKey: "status") as? String
                        
                        //var dict = uploadData as! NSDictionary
                        
                        //print("nutritionList: \(self.nutritionList)")
                        
                        self.nutritionValues[0] = uploadData.object(forKey: "wCalories") as! String
                        self.nutritionValues[1] = uploadData.object(forKey: "wCarbohydrates") as! String
                        self.nutritionValues[2] = uploadData.object(forKey: "wProtein") as! String
                        self.nutritionValues[3] = uploadData.object(forKey: "wFat") as! String
                        self.nutritionValues[4] = uploadData.object(forKey: "wSaturatedFat") as! String
                        self.nutritionValues[5] = uploadData.object(forKey: "wUnsaturatedFat") as! String
                        self.nutritionValues[6] = uploadData.object(forKey: "wTransFat") as! String
                        self.nutritionValues[7] = uploadData.object(forKey: "wCholestrol") as! String
                        self.nutritionValues[8] = uploadData.object(forKey: "wSodium") as! String
                        self.nutritionValues[9] = uploadData.object(forKey: "wPotassium") as! String
                        self.nutritionValues[10] = uploadData.object(forKey: "wFiber") as! String
                        self.nutritionValues[11] = uploadData.object(forKey: "wSugars") as! String
                        self.nutritionValues[12] = uploadData.object(forKey: "wVitaminA") as! String
                        self.nutritionValues[13] = uploadData.object(forKey: "wVitaminC") as! String
                        self.nutritionValues[14] = uploadData.object(forKey: "wCalcium") as! String
                        self.nutritionValues[15] = uploadData.object(forKey: "wIron") as! String
                        self.nutritionValues[16] = uploadData.object(forKey: "dCalories") as! String
                        self.nutritionValues[17] = uploadData.object(forKey: "dCarbohydrates") as! String
                        self.nutritionValues[18] = uploadData.object(forKey: "dProtein") as! String
                        self.nutritionValues[19] = uploadData.object(forKey: "dFat") as! String
                        self.nutritionValues[20] = uploadData.object(forKey: "dSaturatedFat") as! String
                        self.nutritionValues[21] = uploadData.object(forKey: "dUnsaturatedFat") as! String
                        self.nutritionValues[22] = uploadData.object(forKey: "dTransFat") as! String
                        self.nutritionValues[23] = uploadData.object(forKey: "dCholestrol") as! String
                        self.nutritionValues[24] = uploadData.object(forKey: "dSodium") as! String
                        self.nutritionValues[25] = uploadData.object(forKey: "dPotassium") as! String
                        self.nutritionValues[26] = uploadData.object(forKey: "dFiber") as! String
                        self.nutritionValues[27] = uploadData.object(forKey: "dSugars") as! String
                        self.nutritionValues[28] = uploadData.object(forKey: "dVitaminA") as! String
                        self.nutritionValues[29] = uploadData.object(forKey: "dVitaminC") as! String
                        self.nutritionValues[30] = uploadData.object(forKey: "dCalcium") as! String
                        self.nutritionValues[31] = uploadData.object(forKey: "dIron") as! String
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.ingredientTable.reloadData()
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    // MARK: Actions
    
    @IBAction func publishRecipe(_ sender: Any) {
        
        let validate = true
        
        if validate == true
        {
            if selectedImage == nil
            {
                showBasicAlert(string: "Please select an image")
                return
            }
            
            if recipeNameTxt.text == ""
            {
                showBasicAlert(string: "Please Enter recipe name")
                return
            }
            
            if descriptionLbl.text == ""
            {
                showBasicAlert(string: "Please Enter recipe description")
                return
            }
            
            if prepTimeLbl.isHidden == true
            {
                showBasicAlert(string: "Please Enter Prep time")
                return
            }
            
            if cookTimeLbl.isHidden == true
            {
                showBasicAlert(string: "Please Enter Cook time")
                return
            }
            
            if ingredientOptions.count < 1
            {
                showBasicAlert(string: "Please Enter an Ingredient")
                return
            }
            
            if directionList.count < 1
            {
                showBasicAlert(string: "Please Enter Directions")
                return
            }
        }
        
        performSegue(withIdentifier: "SaveRecipe", sender: nil)
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    @IBAction func selectTime(_ sender: Any) {
        
        dismissKeyboard()
                
        if editingPrep == true
        {
            prepTimeLbl.isHidden = false
            prepLblHeight.constant = 16
            timeView.isHidden = true
            
            if selectedPrepHours > 0
            {
                prepTimeLbl.text = "\(selectedPrepHours) hours \(selectedPrepMins) mins"
            }
            else
            {
                prepTimeLbl.text = "\(selectedPrepMins) mins"
            }

        } else {
            
            cookTimeLbl.isHidden = false
            cookLblHeight.constant = 16
            timeView.isHidden = true
            
            if selectedCookHours > 0
            {
                cookTimeLbl.text = "\(selectedCookHours) hours \(selectedCookMins) mins"
            }
            else
            {
                cookTimeLbl.text = "\(selectedCookMins) mins"
            }
        }
    }
    
    @IBAction func showPrepTime(_ sender: Any) {
        
        dismissKeyboard()
        
        let btn = sender as! UIButton
        
        if btn.restorationIdentifier == "Prep"
        {
            editingPrep = true
            
            timeHeaderLbl.text = "Preparation Time"
            
            timePicker.selectRow(selectedPrepHours, inComponent: 0, animated: false)
            timePicker.selectRow(selectedPrepMins, inComponent: 1, animated: false)
        }
        else
        {
            editingPrep = false
            
            timeHeaderLbl.text = "Cooking Time"
            
            timePicker.selectRow(selectedCookHours, inComponent: 0, animated: false)
            timePicker.selectRow(selectedCookMins, inComponent: 1, animated: false)
        }
        
        timeView.isHidden = false
    }
    
    @IBAction func increaseServing(_ sender: Any) {
        
        servingSize += 0.5
        servingsLbl.text = "\(servingSize) Servings"
    }
    
    @IBAction func showRecipeName(_ sender: Any) {
        
//        recipeNameBtn.isHidden = true
//        recipeNameLbl.isHidden = true
//        recipeNameTxt.isHidden = false
    }
    
    
    @IBAction func decreaseServing(_ sender: Any) {
        
        if servingSize > 1
        {
            servingSize -= 0.5
            servingsLbl.text = "\(servingSize) Servings"
        }
        else
        {
            servingsLbl.text = "\(servingSize) Serving"
        }        
    }
    
    @IBAction func manageKidFriendly(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        if friendlySelected == true
        {
            btn.setBackgroundImage(UIImage.init(named: "unselected"), for: .normal)
            
            friendlySelected = false
        }
        else
        {
            btn.setBackgroundImage(UIImage.init(named: "selected"), for: .normal)
            
            friendlySelected = true
        }
    }
    
    @IBAction func showAssignedInfo(_ sender: Any) {}
    
    @IBAction func selectPhoto(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "Select Photo", preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Open Photo Library", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
                self.choosePhoto()
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
                self.takePhoto()
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    @IBAction func addDescription(_ sender: Any) {
        
        //performSegue(withIdentifier: "AddDirections", sender: nil)
    }
    
    // MARK: Image Picker
    
    func takePhoto () {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true)
        }
    }
    
    func choosePhoto () {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.mediaTypes = ["public.image", "public.movie"]
            
            self.present(imagePicker, animated: true)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        print("did pick")
        
        if(info[UIImagePickerControllerMediaURL]as? NSURL != nil)
        {
            let videoURL = info[UIImagePickerControllerMediaURL]as? NSURL
            selectedVideoURL = videoURL! as URL
            
            print("videoURL: \(videoURL!)")
            
            selectedImage = generateThumbnail(path: selectedVideoURL!)
            
            imageIV.image = selectedImage
        }
        else
        {
            selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            
            imageIV.image = selectedImage
            //uploadLbl.text = "Change Picture"
            
            //uploadLbl.textColor = .white
        }
        
        self.dismiss(animated: true, completion: {
            
            //self.uploadToServer(name:"registration-\(self.appDelegate.userid!)")
        });
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        print("cancel")
        
        self.dismiss(animated: true, completion: {
            
        });
    }
    
    func generateThumbnail(path: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: path, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    
    // MARK: Tableview Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if infoSegment.selectedSegmentIndex == 0
        {
            return self.ingredientOptions.count
        }
        else if infoSegment.selectedSegmentIndex == 1
        {
            return self.directionList.count
        }
        else if infoSegment.selectedSegmentIndex == 2
        {
            return self.nutritionLabelList.count
        }
        else if infoSegment.selectedSegmentIndex == 3
        {
            return self.tipsList.count
        }
        else
        {
           return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if infoSegment.selectedSegmentIndex == 1
        {
            performSegue(withIdentifier: "AddDirections", sender: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView,heightForRowAt indexPath:IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        if infoSegment.selectedSegmentIndex == 0
        {
            let dict = self.ingredientOptions[(indexPath as NSIndexPath).row] as NSDictionary
            
            let val = dict.object(forKey: "name") as! String
            
             cell.selectionStyle = .none
            
            let active = dict.object(forKey: "active") as! Bool
            
            if val == "add ingredient"
            {
                let cell : AddIngredientTableViewCell = tableView.dequeueReusableCell(withIdentifier: "add-ingredient-cell")! as! AddIngredientTableViewCell
                
                cell.startEditBtn.setTitle("", for: .normal)
                cell.selectionStyle = .none

                if active == true
                {
                    cell.startEditBtn.isHidden = true
                    cell.addIngredientLbl.isHidden = true
                    
                    cell.quantityTxt.isHidden = false
                    cell.unitTxt.isHidden = false
                    cell.ingredientNameTxt.isHidden = false
                    
                    cell.quantityTxt.restorationIdentifier = String(indexPath.row)
                    cell.unitTxt.restorationIdentifier = String(indexPath.row)
                    cell.ingredientNameTxt.restorationIdentifier = String(indexPath.row)
                    
                    cell.quantityTxt.accessibilityHint = "quantity"
                    cell.unitTxt.accessibilityHint = "unit"
                    cell.ingredientNameTxt.accessibilityHint = "ingredient"
                    
                    cell.quantityTxt.delegate = self
                    cell.unitTxt.delegate = self
                    cell.ingredientNameTxt.delegate = self
                    
                    cell.quantityTxt.placeholder = "qty"
                    cell.unitTxt.placeholder = "unit"
                    cell.ingredientNameTxt.placeholder = "ingredient name"
                    
                    let keyboardDoneButtonView = UIToolbar()
                    keyboardDoneButtonView.sizeToFit()
                    
                    cell.unitTxt.keyboardType = .asciiCapable
                    cell.quantityTxt.keyboardType = .numberPad
                    
                    //quantity
                    
                    let customViewQuantity = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
                    customViewQuantity.backgroundColor = appDelegate.crLightBlue
                    
                    let viewLbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
                    viewLbl.text = "Quantity"
                    viewLbl.textColor = .white
                    viewLbl.textAlignment = .center
                    viewLbl.font = UIFont(name: "Roboto-Regular", size: 16.0)
                    
                    customViewQuantity.addSubview(viewLbl)
                    
    //                let doneButton1 = UIButton(frame: CGRect(x: customViewQuantity.frame.width - 60, y: 0, width: 50, height: 44))
    //                doneButton1.setTitle("Done", for: .normal)
    //                doneButton1.addTarget(self, action: #selector(self.onSlideMenuButtonPressed2(_:)), for: UIControlEvents.touchUpInside)
                    
                    //customViewQuantity.addSubview(doneButton1)
                    
                    
                    //unit
                    
                    let customViewunit = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
                    customViewunit.backgroundColor = appDelegate.crLightBlue
                    
                    let viewLblUnit = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
                    viewLblUnit.text = "Unit"
                    viewLblUnit.textColor = .white
                    viewLblUnit.textAlignment = .center
                    viewLblUnit.font = UIFont(name: "Roboto-Regular", size: 16.0)
                    
                    customViewunit.addSubview(viewLblUnit)
                    
    //                let doneButton2 = UIButton(frame: CGRect(x: customViewunit.frame.width - 60, y: 0, width: 50, height: 44))
    //                doneButton2.setTitle("Done", for: .normal)
    //                doneButton2.addTarget(self, action: #selector(self.onSlideMenuButtonPressed2(_:)), for: UIControlEvents.touchUpInside)
                    
                    //customViewunit.addSubview(doneButton2)
                    
                    
                    //ingredient
                    
                    let customViewIngredient = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
                    customViewIngredient.backgroundColor = appDelegate.crLightBlue
                    
                    let viewLbl3 = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
                    viewLbl3.text = "Ingredient"
                    viewLbl3.textColor = .white
                    viewLbl3.textAlignment = .center
                    viewLbl3.font = UIFont(name: "Roboto-Regular", size: 16.0)
                    
                    customViewIngredient.addSubview(viewLbl3)
                    
                    let doneButton3 = UIButton(frame: CGRect(x: customViewIngredient.frame.width - 60, y: 0, width: 50, height: 44))
                    doneButton3.setTitle("Done", for: .normal)
                    doneButton3.addTarget(self, action: #selector(self.doneClicked(_:)), for: UIControlEvents.touchUpInside)
                    doneButton3.restorationIdentifier = String(indexPath.row)
                    customViewIngredient.addSubview(doneButton3)
                    
                    
                    let doneButton4 = UIButton(frame: CGRect(x: customViewunit.frame.width - 60, y: 0, width: 50, height: 44))
                    doneButton4.setTitle("Done", for: .normal)
                    doneButton4.addTarget(self, action: #selector(self.doneClicked1(_:)), for: UIControlEvents.touchUpInside)
                    doneButton4.restorationIdentifier = String(indexPath.row)
                    customViewunit.addSubview(doneButton4)
                    
                    let doneButton5 = UIButton(frame: CGRect(x: customViewQuantity.frame.width - 60, y: 0, width: 50, height: 44))
                    doneButton5.setTitle("Done", for: .normal)
                    doneButton5.addTarget(self, action: #selector(self.doneClicked1(_:)), for: UIControlEvents.touchUpInside)
                    doneButton5.restorationIdentifier = String(indexPath.row)
                    customViewQuantity.addSubview(doneButton5)
                    
                    
                    cell.quantityTxt?.inputAccessoryView = customViewQuantity
                    cell.unitTxt?.inputAccessoryView = customViewunit
                    cell.ingredientNameTxt?.inputAccessoryView = customViewIngredient
                }
                else
                {
                    cell.startEditBtn.isHidden = false
                    cell.addIngredientLbl.isHidden = false
                    
                    cell.quantityTxt.isHidden = true
                    cell.unitTxt.isHidden = true
                    cell.ingredientNameTxt.isHidden = true
                    
                    cell.startEditBtn.restorationIdentifier = String(indexPath.row)
                    cell.startEditBtn.addTarget(self, action: #selector(self.startEditing(_:)), for: UIControlEvents.touchUpInside)
                }
                
                return cell
            }
            else if val == ""
            {
                let cell : AddIngredientTableViewCell = tableView.dequeueReusableCell(withIdentifier: "add-ingredient-cell")! as! AddIngredientTableViewCell
                
                cell.startEditBtn.setTitle("", for: .normal)
                cell.selectionStyle = .none
                
                cell.startEditBtn.isHidden = false
                cell.addIngredientLbl.isHidden = false
                
                cell.quantityTxt.isHidden = true
                cell.unitTxt.isHidden = true
                cell.ingredientNameTxt.isHidden = true
                
                let qty = dict.object(forKey: "quantity") as! String
                let unit = dict.object(forKey: "unit") as! String
                let ingredient = dict.object(forKey: "ingredient") as! String
                
                cell.addIngredientLbl.text = "\(qty) \(unit) \(ingredient)"
                cell.addIngredientLbl.textColor = appDelegate.gray51
                
                if active == true
                {
                    cell.addIngredientLbl.isHidden = true
                    
                    cell.quantityTxt.text = qty
                    cell.unitTxt.text = unit
                    cell.ingredientNameTxt.text = ingredient

                    cell.quantityTxt.isHidden = false
                    cell.unitTxt.isHidden = false
                    cell.ingredientNameTxt.isHidden = false
                }
                
                return cell
            }
            
            print("val: \(val)")
        }
        else if infoSegment.selectedSegmentIndex == 1
        {
            let cell : AddDirectionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "add-directions-cell")! as! AddDirectionsTableViewCell
            
            let dict = self.directionList[(indexPath as NSIndexPath).row] as NSDictionary
            
            let val = dict.object(forKey: "name") as! String
            
            cell.selectionStyle = .none
            
            if val == "add direction"
            {
                let cell : AddDirectionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "add-directions-cell")! as! AddDirectionsTableViewCell
                
                cell.startEditBtn.setTitle("", for: .normal)
                cell.selectionStyle = .none
                //cell.startEditBtn.isHidden = true
                
                cell.addLbl.isHidden = false
                cell.addLbl.text = val
                cell.addLbl.font = UIFont(name: "Avenir-Book", size: 14.0)
                
                cell.startEditBtn.restorationIdentifier = String(indexPath.row)
                cell.startEditBtn.addTarget(self, action: #selector(self.addDirections(_:)), for: UIControlEvents.touchUpInside)
                
                return cell
            }
            else if val == ""
            {
                let cell : DirectionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "directions-cell")! as! DirectionsTableViewCell
                
                cell.startEditBtn.setTitle("", for: .normal)
                cell.selectionStyle = .none
                
                cell.startEditBtn.isHidden = true
                //cell.startEditBtn.isHidden = false
                
                let title = dict.object(forKey: "title") as! String
                let directions = dict.object(forKey: "directions") as! String
                
                cell.titleLbl.isHidden = false
                cell.directionsLbl.isHidden = false
                
                cell.titleLbl.text = title
                cell.directionsLbl.text = directions
                
                cell.titleLbl.font = UIFont(name: "Avenir-Medium", size: 14.0)
                cell.directionsLbl.font = UIFont(name: "Avenir-Book", size: 14.0)
                
                cell.startEditBtn.restorationIdentifier = String(indexPath.row)
                cell.startEditBtn.addTarget(self, action: #selector(self.addDirections(_:)), for: UIControlEvents.touchUpInside)
                
                return cell
            }
            
            print("val: \(val)")
        }
        else if infoSegment.selectedSegmentIndex == 2
        {
            let cell : GoalsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "add-nutrition-cell")! as! GoalsTableViewCell
            
            let val = self.nutritionLabelList[(indexPath as NSIndexPath).row]
            
            cell.itemLbl?.text = val
            
            cell.tf.text = nutritionValues[indexPath.row] as? String
            
            if val == "Cholestrol" || val == "Sodium" || val == "Potassium"
            {
                //cell.tf.text = "0.00"
                cell.unitLbl.text = "mg"
            }
            else if val == "Vitamin A" || val == "Vitamin C" || val == "Calcium" || val == "Iron"
            {
                //cell.tf.text = "0.00"
                cell.unitLbl.text = "%"
                
            }
            else if val == "Calories"
            {
                cell.unitLbl.text = ""
                cell.tf.keyboardType = .numberPad
            }
            else
            {
                //cell.tf.text = "0.00"
                cell.unitLbl.text = "g"
            }
            
            cell.tf.accessibilityIdentifier = "nutrition"
            cell.tf.inputAccessoryView = numberToolbar
            //cell.tf.restorationIdentifier = val
            cell.tf.restorationIdentifier = "\(indexPath.row)"
            cell.tf.delegate = self
            cell.tf.keyboardType = .decimalPad

            
            if !nutritionFields.contains(cell.tf)
            {
                nutritionFields.append(cell.tf)
            }
            
            return cell
        }
        else if infoSegment.selectedSegmentIndex == 3
        {
            let cell : AddTipsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "add-tips-cell")! as! AddTipsTableViewCell
            
            let dict = self.tipsList[(indexPath as NSIndexPath).row] as NSDictionary
            
            let val = dict.object(forKey: "name") as! String
            
            cell.selectionStyle = .none
            
            if val == "add special tips"
            {
                cell.startEditBtn.setTitle("", for: .normal)
                cell.selectionStyle = .none
                
                cell.tipLbl.isHidden = false
                cell.tipLbl.text = val
                cell.tipLbl.font = UIFont(name: "Avenir-Book", size: 14.0)
                cell.tipLbl.textColor = appDelegate.crWarmGray
                
                cell.startEditBtn.restorationIdentifier = String(indexPath.row)
                cell.startEditBtn.addTarget(self, action: #selector(self.addTips(_:)), for: UIControlEvents.touchUpInside)
                
                return cell
            }
            else
            {
                cell.startEditBtn.setTitle("", for: .normal)
                cell.selectionStyle = .none
                
                cell.startEditBtn.isHidden = false
                
                let title = dict.object(forKey: "tips") as! String
                
                cell.tipLbl.isHidden = false
                cell.tipLbl.text = title
                cell.tipLbl.font = UIFont(name: "Avenir-Medium", size: 14.0)
                cell.tipLbl.textColor = appDelegate.gray51
                
                cell.startEditBtn.restorationIdentifier = String(indexPath.row)
                //cell.startEditBtn.addTarget(self, action: #selector(self.addTips(_:)), for: UIControlEvents.touchUpInside)
                
                return cell
            }
            
            print("val: \(val)")
        }
        
        //cell.itemLbl?.text = val as? String
        
        return cell
    }
    
    // MARK: Textfield Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        dismissKeyboard()
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        currentTextfield = textField
        
        if textField.accessibilityIdentifier == "nutrition"
        {
            currentTextfieldValue = textField.text!
            
            print("currentTextfieldValue: \(currentTextfieldValue)")
            
            textField.text = ""
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {

    //func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.restorationIdentifier == "RecipeName"
        {
//            recipeNameBtn.isHidden = false
//            recipeNameLbl.isHidden = false
//            recipeNameTxt.isHidden = true
        }
        else if textField.accessibilityIdentifier == "nutrition"
        {
            if appDelegate.isStringAnInt(string: textField.text!)
            {
                print("tf ind: \(textField.restorationIdentifier!) \(textField.text!)")
                
                let ind = Int(textField.restorationIdentifier!)
                
                nutritionValues[ind!] = textField.text!
                
                ingredientTable.reloadData()
            }
            else
            {
                textField.text = currentTextfieldValue
                showBasicAlert(string: "Invalid format. Please enter numberic value.")
            }
        }
        else
        {
            let ind = Int(textField.restorationIdentifier!)
            
            if infoSegment.selectedSegmentIndex == 0
            {
                let dict = self.ingredientOptions[ind!] as NSDictionary
                
                dict.setValue(textField.text, forKey: textField.accessibilityHint!)
                dict.setValue(ind, forKey: "recipeorder")
                
                print("dict vals: \(dict)")
            }
        }
    }
    
    // MARK: Table Actions
    
    @objc func doneClickedRecipe(sender: UIButton!) {
        
//        recipeNameBtn.isHidden = false
//        recipeNameLbl.isHidden = false
//        recipeNameTxt.isHidden = true

        dismissKeyboard()
    }
    
    @objc func doneClicked1(_ sender : UIButton) {
        
        ingredientTable.reloadData()
        
        self.view.endEditing(true)
    }
    
    @objc func doneClicked(_ sender : UIButton) {
        
        let btn = sender as UIButton
        let ind = Int(btn.restorationIdentifier!)
        
        if infoSegment.selectedSegmentIndex == 0
        {
            let dict = self.ingredientOptions[ind!] as NSDictionary
            
            dict.setValue(false, forKey: "active")
            dict.setValue("", forKey: "name")
            
            if isUpdating != true
            {
                let dict2 : NSMutableDictionary = ["name" : "add ingredient", "active" : false]
                
                //add entered value labels to cell
                
                //remove button selector
                //add edit selector
                
                ingredientOptions.append(dict2)
            }            
        }
        
        ingredientTable.reloadData()
        
        self.view.endEditing(true)
    }
    
    @objc func addTips(_ sender : UIButton) {
        
        dismissKeyboard()
        
        selectedTipsDict = tipsList[Int((sender.restorationIdentifier)!)!] as? NSMutableDictionary
        
        performSegue(withIdentifier: "AddTips", sender: nil)
    }
    
    @objc func addDirections(_ sender : UIButton) {
        
        dismissKeyboard()
        
        selectedDirectionsDict = directionList[Int((sender.restorationIdentifier)!)!] as? NSMutableDictionary
        
        performSegue(withIdentifier: "AddDirections", sender: nil)
    }
    
    @objc func startEditing(_ sender : UIButton) {
        
        let btn = sender as UIButton
        let ind = Int(btn.restorationIdentifier!)
        
        if infoSegment.selectedSegmentIndex == 0
        {
            let dict = self.ingredientOptions[ind!] as NSDictionary
            
            dict.setValue(true, forKey: "active")
            
            if dict.object(forKey: "unit") != nil || dict.object(forKey: "quantity") != nil || dict.object(forKey: "ingredient") != nil
            {
                isUpdating = true
            }
        }
        
        contentScrollview.scrollRectToVisible(CGRect.init(x: 0, y: ingredientTable.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height), animated: true)
        
        ingredientTable.reloadData()
    }
    
    @IBAction func updateSegment(_ sender: Any) {
        
        //print("update segment")
        
        ingredientTable.reloadData()
    }
    
    // MARK: Pickerview
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if component == 0
        {
            return hoursList.count
        }
        else
        {
            return minutesList.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if editingPrep == true
        {
            if component == 0
            {
                let str = hoursList[row] as! String
                var comp = str.components(separatedBy: " ")
                selectedPrepHours = Int(comp[0])!
            }
            else
            {
                let str = minutesList[row] as! String
                var comp = str.components(separatedBy: " ")
                selectedPrepMins = Int(comp[0])!
            }
        }
        else
        {
            if component == 0
            {
                let str = hoursList[row] as! String
                var comp = str.components(separatedBy: " ")
                selectedCookHours = Int(comp[0])!
            }
            else
            {
                let str = minutesList[row] as! String
                var comp = str.components(separatedBy: " ")
                selectedCookMins = Int(comp[0])!
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var str = ""
        
        if component == 0
        {
            str = "\(hoursList[row]) hours"
        }
        else
        {
            str = "\(minutesList[row]) mins"
        }
        
        return str
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "SaveRecipe"
        {
            let c1 = prepTimeLbl.text?.split(separator: " ")
            let c2 = cookTimeLbl.text?.split(separator: " ")
            
            let c3 : Int = Int(c1![0])!
            let c4 : Int = Int(c2![0])!
            
            let c6 = c3 + c4
            
            recipeDict = ["active":"false"]
            
            recipeDict?.setValue(recipeNameTxt.text, forKey: "recipename")
            recipeDict?.setValue(descriptionLbl.text, forKey: "description")
            recipeDict?.setValue(servingsLbl.text, forKey: "servings")
            recipeDict?.setValue(prepTimeLbl.text, forKey: "preptime")
            recipeDict?.setValue(cookTimeLbl.text, forKey: "cooktime")
            recipeDict?.setValue("\(c6) minutes", forKey: "totaltime")
            recipeDict?.setValue(friendlySelected, forKey: "iskidfriendly")
            recipeDict?.setValue(privateSecretBtn.isSelected, forKey: "isprivate")
            
            //to do randall
            //add fields
            
            recipeDict?.setValue("Hello Fresh", forKey: "source")
            recipeDict?.setValue("https://hellofresh.com", forKey: "sourceurl")
            recipeDict?.setValue("1,2", forKey: "cuisinetags")
            recipeDict?.setValue("200", forKey: "calories")
            
            let destination = segue.destination as! SaveRecipe
            
            //poplate entered values
            
            var i = 0
            
            var labels = [String]()
            
            for val in nutritionValues
            {
                if i < 16
                {
                    let key = nutritionValLabels[i].dropFirst()
                    labels.append(String(key))
                    
                    let dict : NSDictionary = [key: val]
                    
                    nutritionList.append(dict)
                }
                
                i += 1
            }
            
            //print("nutritionList\(nutritionList)")
            print("ingredientOptions\(ingredientOptions)")
            
            destination.ingredientOptions = ingredientOptions
            destination.directionList = directionList
            //destination.nutritionList = nutritionList
            destination.tipsList = tipsList
            destination.selectedImage = selectedImage
            destination.recipeDict = recipeDict
            destination.nutritionValues = nutritionValues
            destination.nutritionValLabels = labels
            destination.selectedVideoURL = selectedVideoURL
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
