//
//  GroupFeed.swift
//  ChewsRite
//
//  Created by Randall Ridley on 9/14/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class GroupFeed: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UISearchBarDelegate, UICollectionViewDelegate,UICollectionViewDataSource {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    var imagePicker : UIImagePickerController!
    
    @IBOutlet weak var editBtn: DesignableButton!
    @IBOutlet weak var membersBtn: DesignableButton!
    @IBOutlet weak var imageIV: UIImageView!

    @IBOutlet weak var editLeading: NSLayoutConstraint!
    @IBOutlet weak var membersLeading: NSLayoutConstraint!
    @IBOutlet weak var membersTrailing: NSLayoutConstraint!
    @IBOutlet weak var memberWidth: NSLayoutConstraint!
    
    @IBOutlet weak var editWidth: NSLayoutConstraint!
    var feedList = [NSMutableDictionary]()
    var filteredList = [NSMutableDictionary]()
    var memberList = [NSMutableDictionary]()
    var adminList = [NSMutableDictionary]()
    var settingsList = [NSMutableDictionary]()

    var isFiltered : Bool?
    var isAdmin : Bool?
    
    @IBOutlet weak var feedTable: UITableView!

    var selectedImage : UIImage?
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var infoLbl: UILabel!
    
    @IBOutlet weak var camBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var settingsView: UIView!
    
    @IBOutlet weak var aboutTable: UITableView!
    @IBOutlet weak var settingsTable: UITableView!
    
    let prefs = UserDefaults.standard
    
    @IBOutlet weak var search: UISearchBar!
    
    var itemArray = [FeedItem]()
    var currentItemArray = [FeedItem]()
    
    var statusImageCache = [String:UIImage]()
    
    var selectedGroup : GroupItem?
    var groupId : String?
    var groupName : String?
    var userid : String?
    
    @IBOutlet weak var recipeStatusTV: UITextView!
    
    
    
    @IBOutlet weak var friendIV: UIImageView!
    
    @IBOutlet weak var friendDescriptionLbl: UILabel!
    @IBOutlet weak var friendHeaderLbl: UILabel!
    
    @IBOutlet weak var friendFeedTable: UITableView!
    
    @IBOutlet weak var friendPopup: UIView!
    
    @IBOutlet weak var tagsCollection: UICollectionView!
    
    var friendItemArray = [FeedItem]()
    var currentFriendItemArray = [FeedItem]()
    
    var tagsList = [String]()
    
    var selectedCuisine : Int?
    var selectedFriendItem : NSMutableDictionary?
    
    var friendFeedList = [NSMutableDictionary]()
    
    @IBOutlet weak var tagsHeight: NSLayoutConstraint!
    
    var notificationsSetting : Int?
    var selectedRecipe : FeedItem?
    
    @IBOutlet weak var unfriendBtn: UIButton!
    @IBOutlet weak var unfriendBtnHeight: NSLayoutConstraint!
    var isFamilyMember : Bool?
    
    var uploadImageName : String?
    
    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        search.delegate = self
        search.backgroundImage = UIImage()
        
        search.placeholder = "Search Recipes in Family"
        
        UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        
        //isAdmin = true
        
        groupName = selectedGroup?.groupname
        groupId = selectedGroup?.groupid
        
        titleLbl.text = groupName
        
        if userid == nil
        {
            userid = appDelegate.userid
            isFamilyMember = false
        }
        
        if selectedImage != nil
        {
            imageIV.image = selectedImage
        }
        else
        {
            let imagename = selectedGroup?.groupimage
            
            imageIV.image = UIImage(named: "profile-icon")
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
            
            self.downloadImage(imagename: imagename!, urlString: urlString, iv: imageIV)
        }
        
        feedTable.delegate = self
        feedTable.dataSource = self
        feedTable.tableFooterView = UIView()
        
        aboutTable.delegate = self
        aboutTable.dataSource = self
        aboutTable.tableFooterView = UIView()
        
        settingsTable.delegate = self
        settingsTable.dataSource = self
        settingsTable.tableFooterView = UIView()
        
        friendFeedTable.delegate = self
        friendFeedTable.dataSource = self
        friendFeedTable.tableFooterView = UIView()
        
        aboutView.isHidden = true
        settingsView.isHidden = true
        recipeStatusTV.isHidden = true
        friendPopup.isHidden = true
        
        tagsCollection.dataSource = self
        tagsCollection.delegate = self
        
        if isAdmin != true
        {
            editBtn.isHidden = true
            membersBtn.isHidden = true
            camBtn.isHidden = true
            
            membersLeading.constant = 0
            membersTrailing.constant = 0
            memberWidth.constant = 0
            editWidth.constant = 0
            //editLeading.constant = 0
        }
        
//        if prefs.integer(forKey: "notifications") != nil
//        {
//            appDelegate.notificationsSetting = prefs.integer(forKey: "notifications")
//        }
        
        getRecipes()
        //debug()
        initSettings()

        // Do any additional setup after loading the view.
        
        let btnItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(dismissKeyboard))
        
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width:  self.view.frame.size.width, height: 50))
        numberToolbar.backgroundColor = UIColor.darkGray
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.tintColor = UIColor.black
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil),
            btnItem]
        
        numberToolbar.sizeToFit()
        
        search.inputAccessoryView = numberToolbar
    
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refreshGroupNotification),
                                               name: NSNotification.Name(rawValue: "refreshGroup"),
                                               object: nil)
    }
    
    @objc func refreshGroupNotification (notification: NSNotification) {
        
        let gi = notification.object as! GroupItem
        
        selectedGroup = gi
        
        groupName = selectedGroup?.groupname
        titleLbl.text = groupName
    }
        
    @objc func dismissKeyboard () {
        
        self.view.endEditing(true)
    }
    
    func initSettings () {
        
        var dict : NSMutableDictionary?
        
        dict  = ["title" : "All Posts", "description" : "Any time members post in this group"]
        settingsList.append(dict!)
        
        dict  = ["title" : "Friends’ Posts", "description" : "Any time members post in this group"]
        settingsList.append(dict!)
        
        dict  = ["title" : "Off", "description" : "No notifications when members post"]
        settingsList.append(dict!)
        
        settingsTable.reloadData()
    }
    
    func debug () {
        
        titleLbl.text = "Group 1"
        infoLbl.text = "Private Group • 3 Members"
        
        var dict : NSMutableDictionary?
        
        dict  = ["friendname" : "Friend 1", "userid" : "1", "imagename" : "1"]
        memberList.append(dict!)
        
        dict  = ["friendname" : "Friend 2", "userid" : "1", "imagename" : "1"]
        memberList.append(dict!)
        
        dict  = ["friendname" : "Admin Name", "userid" : "1", "imagename" : "1"]
        adminList.append(dict!)
        
        aboutTable.reloadData()
    }
    
    // MARK: - Actions
    
    @IBAction func viewSettings(_ sender: Any) {
        
        aboutView.isHidden = true
        feedTable.isHidden = true
        settingsView.isHidden = false
        recipeStatusTV.isHidden = true
    }
    
    @IBAction func viewPhotos(_ sender: Any) {
        
        aboutView.isHidden = true
        feedTable.isHidden = false
        settingsView.isHidden = true
        
        if currentItemArray.count == 0
        {
            recipeStatusTV.isHidden = false
        }
        else
        {
            recipeStatusTV.isHidden = true
        }
    }
    
    @IBAction func addMember(_ sender: Any) {
        
        performSegue(withIdentifier: "AddMember", sender: nil)
    }
    
    @IBAction func editGroup(_ sender: Any) {
        
        performSegue(withIdentifier: "EditGroup", sender: nil)
    }
    
    @IBAction func showInfo(_ sender: Any) {
        
        aboutView.isHidden = false
        feedTable.isHidden = true
        settingsView.isHidden = true
        recipeStatusTV.isHidden = true
    }
    
    @IBAction func selectPhoto(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "Select Photo", preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Open Photo Library", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
                self.choosePhoto()
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
                self.takePhoto()
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    @IBAction func cancel(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeFriendPopup(_ sender: Any) {
        
        friendPopup.isHidden = true
    }
    
    // MARK: Image Picker
    
    func takePhoto () {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func choosePhoto () {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        print("cancel")
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        print("did pick")
        
        selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        imageIV.image = selectedImage
        
        self.dismiss(animated: true, completion: {
            
            self.uploadS3()
            
            //self.uploadToServer(name:"registration-\(self.appDelegate.userid!)")
        });
    }
    
    func uploadS3 () {
        
        self.activityView.isHidden = false
        self.activityView.startAnimating()
        
        backBtn.isEnabled = false
        
        if selectedGroup?.groupimage == ""
        {
            uploadImageName = "\(appDelegate.randomString(length: 10)).jpeg"
        }
        else
        {
            uploadImageName = selectedGroup?.groupimage
        }
        
        let image = selectedImage!
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(uploadImageName!)
        let imageData = UIImageJPEGRepresentation(image, 0)
        fileManager.createFile(atPath: path as String, contents: imageData, attributes: nil)
        
        let fileUrl = NSURL(fileURLWithPath: path)
        
        print("upload")
        
        let transferManager = AWSS3TransferManager.default()
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.bucket = "chewsrite/images"
        uploadRequest!.key = uploadImageName!
        uploadRequest?.body = fileUrl as URL
        uploadRequest?.contentType = "image/jpeg"
        
        uploadRequest?.acl = AWSS3ObjectCannedACL.publicReadWrite;
        
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            
            if let error = task.error as NSError? {
                
                if error.domain == AWSS3TransferManagerErrorDomain,
                    
                    let code = AWSS3TransferManagerErrorType(rawValue: error.code)
                {
                    switch code
                    {
                    case .cancelled, .paused:
                        break
                    default:
                        print("Error uploading 1: \(uploadRequest?.key!) Error: \(error)")
                    }
                }
                else
                {
                    print("Error uploading: \(uploadRequest?.key!) Code: \(error.code) Error: \(error)")
                }
                
                return nil
            }
            
            //let uploadOutput = task.result
            
            print("Upload complete for: \(uploadRequest!.key!)")
            
            if self.selectedGroup?.groupimage == ""
            {
                self.updateImageInfo()
            }
            else
            {
                self.selectedGroup?.groupimage = self.uploadImageName!
                
                let imgDict = ["selectedImage":self.selectedImage!, "imagename":self.uploadImageName!] as [String : Any]
                
                NotificationCenter.default.post(name: Notification.Name("updateGroupImageHome"), object: imgDict)
                
                //self.selectedImage = nil
                
                self.activityView.isHidden = true
                self.activityView.stopAnimating()
                
                self.backBtn.isEnabled = true
            }
            
            //Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.doneUploading), userInfo: nil, repeats: false)
            
            return nil
        })
    }
    
    // MARK: Query
    
    func updateImageInfo() {
        
        print("updateImageInfo")
        
        let dataString = "groupData"
        
        let urlString = "\(appDelegate.serverDestination!)updateGroupImage.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "userid=\(appDelegate.userid!)&groupid=\(groupId!)&devStatus=\(appDelegate.devStage!)&imagename=\(uploadImageName!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.activityView.stopAnimating()
                        self.activityView.isHidden = true
                    })
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status.contains("image info saved")
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            
                            self.selectedGroup?.groupimage = self.uploadImageName!
                            
                            let imgDict = ["selectedImage":self.selectedImage!, "imagename":self.uploadImageName!] as [String : Any]
                            
                            NotificationCenter.default.post(name: Notification.Name("updateGroupImageHome"), object: imgDict)

                            //self.selectedImage = nil

                            self.activityView.isHidden = true
                            self.activityView.stopAnimating()
                            self.backBtn.isEnabled = true
                        }
                        else
                        {
                            print("save error")
                            
                            //self.showBasicAlert(string: "Save Error")
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getRecipes() {
        
        activityView.isHidden = false
        activityView.startAnimating()
        
        feedList.removeAll()
        
        print("getMyRecipes")
        
        let dataString = "recipeData"
        
        let urlString = "\(appDelegate.serverDestination!)getRecipes.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        //let userid = "22"
        
        var paramString = "userid=\(userid!)&devStatus=\(appDelegate.devStage!)&isFamilyMember=\(isFamilyMember!)"
        
        //var paramString = ""
        
        if groupId != nil
        {
            paramString = "\(paramString)&groupid=\(groupId!)"
            //paramString = "groupid=\(groupId!)"
        }
//        else
//        {
//            paramString = "userid=\(appDelegate.userid!)"
//        }
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.recipeStatusTV.isHidden = false
                    })
                }
                else
                {
                    //let groupCountData = dataDict["groupCount"]! as! NSDictionary
                    
                    let adminData = dataDict["adminInfo"]! as! NSDictionary
                    
                    let dict : NSMutableDictionary = ["firstname" : "\(adminData.object(forKey: "firstname") as! String)","lastname" : "\(adminData.object(forKey: "lastname") as! String)", "userid" : adminData.object(forKey: "userid") as! String, "imagename" : adminData.object(forKey: "imagename") as! String]
                    
                    let selection = adminData.object(forKey: "selection") as? String
                    
                    
                    self.notificationsSetting = Int(selection!)
                    
                    if self.notificationsSetting == nil
                    {
                        self.notificationsSetting = 0
                    }
                    
                    self.adminList.append(dict)
                    
                    let membersData : NSMutableArray = (dataDict["membersData"]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let members = membersData as! [NSDictionary]
                    
                    if (members.count > 0)
                    {
                        for dict in members
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.memberList.append(dict2)
                        }
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.infoLbl.text = "Private Group • \(members.count) Members"
                        
                        self.aboutTable.reloadData()
                        self.settingsTable.reloadData()
                    })
                    
                    
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.feedList.append(dict2)
                            
                            self.itemArray.append(FeedItem(recipename: dict.object(forKey: "recipename") as! String,
                                                           preptime: dict.object(forKey: "preptime") as! String,
                                                           cooktime: dict.object(forKey: "cooktime") as! String,
                                                           imagename: dict.object(forKey: "imagename") as! String,
                                                           videoname: dict.object(forKey: "videoname") as! String,
                                                           calories: dict.object(forKey: "calories") as! String,
                                                           source: dict.object(forKey: "source") as! String, sourceurl: dict.object(forKey: "sourceurl") as! String,
                                                           isFavorite: dict.object(forKey: "isFavorite") as! String,
                                                           username: dict.object(forKey: "username") as! String,
                                                           recipeid: dict.object(forKey: "recipeid") as! String,
                                                           cuisinetags: dict.object(forKey: "cuisinetags") as! String,
                                                           mealplanid: "", mealplandate: "", mealid: "",
                                                           userimage: dict.object(forKey: "userimage") as! String,
                                                           firstname: dict.object(forKey: "firstname") as! String,
                                                           lastname: dict.object(forKey: "lastname") as! String,
                                                           description: dict.object(forKey: "recipedescription") as! String,
                                                           servings: dict.object(forKey: "servings") as! String, userid: dict.object(forKey: "userid") as! String, totaltime: dict.object(forKey: "totaltime") as! String, rating: dict.object(forKey: "rating") as! String, timeAdded: dict.object(forKey: "timeAdded") as! String))
                        }
                        
                        self.currentItemArray = self.itemArray
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if self.currentItemArray.count == 0
                            {
                                self.recipeStatusTV.isHidden = true
                            }
                            
                            self.activityView.stopAnimating()
                            self.activityView.isHidden = true
                            self.aboutTable.reloadData()
                            self.feedTable.reloadData()
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getFriendRecipes() {
        
        friendFeedList.removeAll()
        
        print("getMyRecipes")
        
        let imagename = selectedFriendItem?.object(forKey: "imagename") as! String
        
        if imagename != ""
        {
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
            
            if let img = statusImageCache[urlString]
            {
                friendIV.image = img
            }
        }
        
        let dataString = "recipeData"
        
        let urlString = "\(appDelegate.serverDestination!)getRecipes.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        //let userid = selectedFriend?.object(forKey: "userid") as! String
        let userid = selectedFriendItem?.object(forKey: "userid") as! String
        
        let paramString = "userid=\(userid)&friendid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("friend recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.friendFeedList.append(dict2)
                            
                            self.friendItemArray.append(FeedItem(recipename: dict2.object(forKey: "recipename") as! String, preptime: dict2.object(forKey: "preptime") as! String, cooktime: dict2.object(forKey: "cooktime") as! String, imagename: dict2.object(forKey: "imagename") as! String, videoname: "", calories: dict2.object(forKey: "calories") as! String, source: dict2.object(forKey: "source") as! String, sourceurl: dict2.object(forKey: "source") as! String, isFavorite: dict2.object(forKey: "isFavorite") as! String, username: dict2.object(forKey: "username") as! String, recipeid: dict2.object(forKey: "recipeid") as! String, cuisinetags: dict2.object(forKey: "cuisinetags") as! String, mealplanid: "", mealplandate: "", mealid: "", userimage: dict2.object(forKey: "userimage") as! String, firstname: dict2.object(forKey: "firstname") as! String, lastname: dict2.object(forKey: "lastname") as! String, description: dict2.object(forKey: "recipedescription") as! String, servings: dict2.object(forKey: "servings") as! String, userid: dict2.object(forKey: "userid") as! String, totaltime: dict2.object(forKey: "totaltime") as! String, rating: dict2.object(forKey: "rating") as! String, timeAdded: dict2.object(forKey: "timeAdded") as! String))
                        }
                        
                        self.currentFriendItemArray = self.friendItemArray
                        
                        DispatchQueue.main.sync(execute: {
                            
                            self.friendFeedTable.reloadData()
                            
                            self.friendPopup.isHidden = false
                            
                            if userid == self.appDelegate.userid
                            {
                                self.unfriendBtn.isHidden = true
                                self.unfriendBtnHeight.constant = 0
                            }
                            else
                            {
                                self.unfriendBtn.isHidden = false
                                self.unfriendBtnHeight.constant = 30
                            }
                            
                            //                            self.activityView.stopAnimating()
                            //                            self.activityView.isHidden = true
                            //
                            //                            if hasData == true
                            //                            {
                            //                                self.recipeStatusTV.text = "You currently have no recipes."
                            //
                            //                                self.updateDisplayList()
                            //                            }
                            //                            else
                            //                            {
                            //                                print("no data")
                            //
                            //                                //self.noResultsMain.isHidden = false
                            //                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getMyCuisines() {
        
        tagsList.removeAll()
        
        print("getMyCuisines")
        
        let dataString = "cuisineData"
        
        let urlString = "\(appDelegate.serverDestination!)getCuisines.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        
        let userid = selectedFriendItem?.object(forKey: "userid") as? String
        
        let paramString = "userid=\(userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("cuisines jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //self.recipeStatusTV.isHidden = false
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let tags = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (tags.count > 0)
                    {
                        hasData = true
                        
                        var ind = 0
                        
                        for dict in tags
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.tagsList.append(dict2.object(forKey: "cuisinename") as! String)
                            
                            ind += 1
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if self.tagsList.count > 3
                            {
                                self.tagsHeight.constant = 100
                            }
                            else
                            {
                                self.tagsHeight.constant = 30
                            }
                            
                            if hasData == true
                            {
                                self.tagsCollection.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
                                //self.noResultsMain.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.sync(execute: {
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    // MARK: Tableview
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.restorationIdentifier == "about"
        {
            if section == 0
            {
                return self.memberList.count
            }
            else
            {
                return self.adminList.count
            }
        }
        else if tableView.restorationIdentifier == "settings"
        {
            return self.settingsList.count
        }
        else if tableView.restorationIdentifier == "friendTable"
        {
            return self.currentFriendItemArray.count
        }
        else
        {
            return self.currentItemArray.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView.restorationIdentifier == "about"
        {
            return 2
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView.restorationIdentifier == "about"
        {
            let headerView = Bundle.main.loadNibNamed("MemberHeaderCell", owner: self, options: nil)?.first as! MemberHeaderCell
            
            if section == 0
            {
                
                headerView.headerLbl.text = "MEMBERS • \(self.memberList.count)"
            }
            else
            {
                headerView.headerLbl.text = "ADMIN"
            }
            
            return headerView
        }
        else
        {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView.restorationIdentifier == "about"
        {
            return 44
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                
        if tableView.restorationIdentifier == "about" || tableView.restorationIdentifier == "settings"
        {
            return 60
        }
        else if tableView.restorationIdentifier == "friendTable"
        {
            return 263
        }
        else
        {
            return 255
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var dict : NSDictionary?
        
        if tableView.restorationIdentifier == "about"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1")! as? FriendRequestCell
            
            cell?.selectionStyle = .none
            
            if indexPath.section == 0
            {
                dict = memberList[indexPath.row]
                
                let fn = dict?.object(forKey: "firstname") as? String
                let ln = dict?.object(forKey: "lastname") as? String
                
                cell?.nameLbl.text = "\(fn!) \(ln!)"
            }
            else
            {
                dict = adminList[indexPath.row]
                
                let fn = dict?.object(forKey: "firstname") as? String
                let ln = dict?.object(forKey: "lastname") as? String
                
                cell?.nameLbl.text = "\(fn!) \(ln!)"
                
                //cell?.nameLbl.text = dict?.object(forKey: "friendname") as? String
            }
            
            let imagename = dict?.object(forKey: "imagename") as? String
            
            cell?.friendIV.image = UIImage.init(named: imagename!)
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                
                if let img = statusImageCache[urlString]
                {
                    cell?.friendIV.image = img
                }
                else
                {
                    cell?.friendIV.image = UIImage(named: "profile-icon")
                    
                    self.downloadImage(imagename: imagename!, urlString: urlString, iv: (cell?.friendIV!)!)
                }
            }
            
            return cell!
        }
        else if tableView.restorationIdentifier == "settings"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2")! as? ReviewsTableViewCell
            
            dict = settingsList[indexPath.row]
            
            cell?.selectionStyle = .none
            
            cell?.usernameLbl.text = dict?.object(forKey: "title") as? String
            cell?.reviewLbl.text = dict?.object(forKey: "description") as? String
            
            if indexPath.row == 0
            {
                if notificationsSetting == 0
                {
                    cell?.ratingIV.image = #imageLiteral(resourceName: "checkGreen")
                }
                else
                {
                    cell?.ratingIV.image = nil
                }
            }
            else if indexPath.row == 1
            {
                if notificationsSetting == 1
                {
                    cell?.ratingIV.image = #imageLiteral(resourceName: "checkGreen")
                }
                else
                {
                    cell?.ratingIV.image = nil
                }
            }
            else if indexPath.row == 2
            {
                if notificationsSetting == 2
                {
                    cell?.ratingIV.image = #imageLiteral(resourceName: "checkGreen")
                }
                else
                {
                    cell?.ratingIV.image = nil
                }
            }
            
            return cell!
        }
        else if tableView.restorationIdentifier == "friendTable"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2")! as? FeaturedRecipeTableViewCell
            
            let item = currentFriendItemArray[indexPath.row]
            
            //cell?.recipeIV.image = UIImage.init(named: (dict?.object(forKey: "imagename") as? String)!)
            
            let imagename = item.imagename
            
            print("imagename: \(imagename)")
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
                
                if let img = statusImageCache[urlString]
                {
                    cell?.recipeIV.image = img
                }
                else
                {
                    cell?.recipeIV.image = UIImage(named: "profile-icon")
                    
                    self.downloadImage(imagename: imagename, urlString: urlString, iv: (cell?.recipeIV)!)
                }
            }
            else
            {
                cell?.recipeIV.image = UIImage(named: "profile-icon")
            }
            
//            let headerView = Bundle.main.loadNibNamed("MemberHeaderCell", owner: self, options: nil)?.first as! MemberHeaderCell
//
//            let headerView = tableView.header(withIdentifier: "cell2")! as? MemberHeaderCell
            
            let userimagename = item.userimage
            
            if userimagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(userimagename)"
                
                if let img = statusImageCache[urlString]
                {
                    cell?.userIV.image = img
                }
                //                else
                //                {
                //                    cell?.userIV.image = UIImage(named: "profile-icon")
                //
                //                    self.downloadImage(imagename: imagename!, urlString: urlString, recipeCell: (cell)!, friendCell: nil, friendCell2: nil, friendCell3: (cell)!)
                //                }
            }
            
            let firstname = item.firstname
            let lastname = item.lastname
            
            cell?.nameLbl.text = "\(firstname) \(lastname)"
            cell?.timeLbl.text = item.timeAdded
            
            cell?.headerLbl.text = item.recipename
            cell?.infoLbl.text = "\(item.preptime) • \(item.calories)"
            
            let attributedString = NSMutableAttributedString(string: "Source: \(item.source)", attributes: [
                .font: UIFont(name: "Avenir-Book", size: 13.0)!,
                .foregroundColor: appDelegate.crGray
                ])
            attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 13.0)!, range: NSRange(location: 0, length: 7))
            
            cell?.sourceLbl.attributedText = attributedString
            
            cell?.favoriteBtn.restorationIdentifier = item.recipeid
            cell?.favoriteBtn.accessibilityIdentifier = "\(indexPath.row)"
            
            let video = item.videoname
            
            if video == ""
            {
                cell?.videoBtn.isHidden = true
            }
            else
            {
                cell?.videoBtn.isHidden = false
            }
            
            let isFavorite = item.isFavorite
            
            if isFavorite == "1"
            {
                cell?.favoriteBtn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
                cell?.isFavorite = true
                //dict?.setValue(true, forKey: "isFavorite")
            }
            else
            {
                cell?.favoriteBtn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
                cell?.isFavorite = false
                //dict?.setValue(false, forKey: "isFavorite")
            }
            
            cell?.favoriteBtn.addTarget(self, action: #selector(self.manageFavorite(_:)), for: UIControlEvents.touchUpInside)
            
            return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as? FeaturedRecipeTableViewCell
            
//            if isFiltered == true
//            {
//                dict = filteredList[indexPath.row]
//            }
//            else
//            {
//                dict = feedList[indexPath.row]
//            }
            
            let item = currentItemArray[indexPath.row]
            
            cell?.selectionStyle = .none
            
//            cell?.recipeIV.image = UIImage.init(named: (dict?.object(forKey: "imagename") as? String)!)
//            cell?.headerLbl.text = dict?.object(forKey: "recipename") as? String
//            cell?.infoLbl.text = "\(dict?.object(forKey: "preptime") as! String) • \(dict?.object(forKey: "calories") as! String)"
            
            //cell?.recipeIV.image = UIImage.init(named: item.imagename)
            cell?.headerLbl.text = item.recipename
            cell?.infoLbl.text = "\(item.preptime) • \(item.calories)"
            
            let attributedString = NSMutableAttributedString(string: "Source: \(item.source)", attributes: [
                .font: UIFont(name: "Avenir-Book", size: 13.0)!,
                .foregroundColor: appDelegate.crGray
                ])
            attributedString.addAttribute(.font, value: UIFont(name: "Avenir-Heavy", size: 13.0)!, range: NSRange(location: 0, length: 7))
            
            cell?.sourceLbl.attributedText = attributedString
            
            cell?.favoriteBtn.restorationIdentifier = dict?.object(forKey: "recipeid") as? String
            cell?.favoriteBtn.accessibilityIdentifier = "\(indexPath.row)"
            
            cell?.nameLbl.text = item.username
            cell?.timeLbl.text = item.timeAdded
            
            
            //recipe photo
            
            let recipeimagename = item.imagename
            
            if recipeimagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(recipeimagename)"
                
                if let img = statusImageCache[urlString]
                {
                    cell?.recipeIV.image = img
                }
                else
                {
                    cell?.recipeIV.image = UIImage(named: "profile-icon")
                    
                    self.downloadImage(imagename: recipeimagename, urlString: urlString, iv: (cell?.recipeIV!)!)
                }
            }
            
            //user profile image
            
            let imagename = item.userimage
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
                
                if let img = statusImageCache[urlString]
                {
                    cell?.userIV.image = img
                }
                else
                {
                    cell?.userIV.image = UIImage(named: "profile-icon")
                    
                    self.downloadImage(imagename: imagename, urlString: urlString, iv: (cell?.userIV!)!)
                }
            }
            
            let video = item.videoname
            
            if video == ""
            {
                cell?.videoBtn.isHidden = true
            }
            else
            {
                cell?.videoBtn.isHidden = false
            }
            
            let isFavorite = item.isFavorite
            
            if isFavorite == "1"
            {
                cell?.favoriteBtn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
                cell?.isFavorite = true
                //dict?.setValue(true, forKey: "isFavorite")
            }
            else
            {
                cell?.favoriteBtn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
                cell?.isFavorite = false
                //dict?.setValue(false, forKey: "isFavorite")
            }
            
            cell?.favoriteBtn.addTarget(self, action: #selector(self.manageFavorite(_:)), for: UIControlEvents.touchUpInside)
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if tableView.restorationIdentifier == "settings"
        {
            notificationsSetting = indexPath.row
            settingsTable.reloadData()
            
            //self.prefs.setValue(appDelegate.notificationsSetting , forKey: "notifications")
            //self.prefs.synchronize()
            
            updateSettings()
        }
        else if tableView.restorationIdentifier == "about"
        {
            if indexPath.section == 0
            {
                selectedFriendItem = memberList[indexPath.row]
            }
            else
            {
                selectedFriendItem = adminList[indexPath.row]
            }
            
            friendHeaderLbl.text = "\(selectedFriendItem?.object(forKey: "firstname") as! String) \(selectedFriendItem?.object(forKey: "lastname") as! String)"
            friendDescriptionLbl.text = selectedFriendItem?.object(forKey: "story") as? String
            getMyCuisines()
            getFriendRecipes()
        }
        else if tableView.restorationIdentifier == "feedTable"
        {
            selectedRecipe = currentItemArray[indexPath.row]
            
            performSegue(withIdentifier: "RecipeDetails", sender: nil)
        }
    }
    
    @IBAction func manageFavorite(_ sender: Any) {
        
        let btn = sender as! UIButton
        
//        let dict: NSMutableDictionary?
//
//        if isFiltered == true
//        {
//            dict = filteredList[Int(btn.accessibilityIdentifier!)!]
//        }
//        else
//        {
//            dict = feedList[Int(btn.accessibilityIdentifier!)!]
//        }
        
        let item = currentItemArray[Int(btn.accessibilityIdentifier!)!]
        
        if item.isFavorite == "0"
        {
            item.isFavorite = "1"
            btn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
        }
        else
        {
            item.isFavorite = "0"
            btn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
        }

//        if dict?.object(forKey: "isFavorite") as! String == "0"
//        {
//            dict?.setValue("1", forKey: "isFavorite")
//            btn.setBackgroundImage(UIImage.init(named: "favorite"), for: .normal)
//        }
//        else
//        {
//            dict?.setValue("0", forKey: "isFavorite")
//            btn.setBackgroundImage(UIImage.init(named: "addToFavorites"), for: .normal)
//        }

        manageFavorite(recipeID: btn.restorationIdentifier!)
    }
    
    func manageFavorite( recipeID : String) {
        
        print("manageFavorite")
        
        let dataString = "favoriteData"
        
        let urlString = "\(appDelegate.serverDestination!)manageFavorites.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "recipeid=\(recipeID)&userid=\(userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //                        self.activityView.stopAnimating()
                        //                        self.activityView.isHidden = true
                    })
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "favorite item saved" || status == "favorite deleted"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func updateSettings() {
        
        print("updateSettings")
        
        let dataString = "settingsData"
        
        let urlString = "\(appDelegate.serverDestination!)updateSettings.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let paramString = "selection=\(notificationsSetting!)&userid=\(userid!)&groupid=\(groupId!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                //print("recipes jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
                        //                        self.activityView.stopAnimating()
                        //                        self.activityView.isHidden = true
                    })
                }
                else
                {
                    let uploadData : NSDictionary = dataDict[dataString]! as! NSDictionary
                    
                    print("uploadData: \(uploadData)")
                    
                    let status = uploadData.object(forKey: "status") as! String
                    
                    var isSaved = false
                    
                    if status == "settings updated"
                    {
                        isSaved = true
                    }
                    
                    DispatchQueue.main.sync(execute: {
                        
                        if isSaved == true
                        {
                            
                        }
                        else
                        {
                            print("save error")
                            
                            let alert = UIAlertController(title: nil, message: "Save Error", preferredStyle: UIAlertControllerStyle.alert)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                }
                            }))
                        }
                    })
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let kWhateverHeightYouWant = 190
        
        return CGSize.init(width: 600, height: CGFloat(kWhateverHeightYouWant))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return tagsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as? TagsCollectionViewCell
        
        let tag = tagsList[indexPath.row]
        
        //cell?.frame.size = CGSize.init(width: tagsCV.frame.width * 0.5, height: 292) //Randall. check this
        
        //cell?.recipeIV.image = UIImage.init(named: (dict.object(forKey: "imagename") as? String)!)
        
        if indexPath.row == 3
        {
            cell?.leadingConstraint.constant = 5
        }
        else
        {
            cell?.leadingConstraint.constant = 0
        }
        
        if indexPath.row == 5
        {
            cell?.trailingConstraint.constant = 5
        }
        
        if indexPath.row < 5
        {
            cell?.tagLbl.text = tag
        }
        else
        {
            cell?.tagLbl.text = "..."
        }
        
        cell?.tagLbl.layer.borderWidth = 0.5
        cell?.tagLbl.layer.borderColor = appDelegate.crGray.cgColor
        cell?.tagLbl.textColor = appDelegate.crGray
        cell?.tagLbl.layer.cornerRadius = 10.0
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedCuisine = indexPath.row
        selectTag()
    }
    
    func selectTag() {
        
        print("selectedCuisine: \(selectedCuisine!)")
        
        //        currentFriendItemArray = friendItemArray.filter({ item -> Bool in
        //
        //            let tags = item.cuisinetags.components(separatedBy: ",")
        //
        //            print("tags: \(tags)")
        //
        //            return tags.contains("\(selectedCuisine!)".lowercased())
        //        })
        
        if selectedCuisine == 0
        {
            isFiltered = false
            currentFriendItemArray = friendItemArray
        }
        else
        {
            //change filter
            
            currentFriendItemArray = friendItemArray.filter({ item -> Bool in
                
                let tags = item.cuisinetags.components(separatedBy: ",")
                
                print("tags: \(tags)")
                
                return tags.contains("\(selectedCuisine!)".lowercased())
            })
        }
        
        friendFeedTable.reloadData()
    }
    
    // MARK: Search
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        currentItemArray = itemArray.filter({ item -> Bool in
            
            switch searchBar.selectedScopeButtonIndex {
            case 0:
                if searchText.isEmpty { return true }
                return item.recipename.lowercased().contains(searchText.lowercased()) || item.source.lowercased().contains(searchText.lowercased()) || item.username.lowercased().contains(searchText.lowercased())
                
                //            case 1:
                //                if searchText.isEmpty { return animal.category == .dog }
                //                return animal.name.lowercased().contains(searchText.lowercased()) &&
                //                    animal.category == .dog
                //            case 2:
                //                if searchText.isEmpty { return animal.category == .cat }
                //                return animal.name.lowercased().contains(searchText.lowercased()) &&
            //                    animal.category == .cat
            default:
                return false
            }
        })
        
        feedTable.reloadData()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "EditGroup"
        {
            let destination = segue.destination as! CreateGroup
            destination.isEditing = true
            destination.selectedImage = selectedImage
            destination.selectedGroup = selectedGroup
        }
        else if segue.identifier == "AddMember"
        {
            let destination = segue.destination as! AddPersonGroup
            destination.isEditing = true
            destination.groupId = groupId
            destination.groupName = selectedGroup?.groupname
            destination.statusImageCache = statusImageCache
        }
        else if segue.identifier == "RecipeDetails"
        {
            let destination = segue.destination as! RecipeDetails
            
            let imagename = selectedRecipe?.imagename
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                
                if let img = statusImageCache[urlString]
                {
                    destination.selectedImage = img
                }
            }
            
            destination.selectedItem = selectedRecipe
        }
    }
    
    func downloadImage (imagename : String, urlString : String, iv: UIImageView ) {
        
        if appDelegate.downloadImages == true
        {
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        print("downloadOutput photo: \(downloadOutput)");
                        print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        //self.selectedImage = image
                        self.statusImageCache[urlString] = image
                        iv.image = image
                    }
                    
                    //println("test")
                })
                return nil
            }))
        }
        else
        {
            iv.image = UIImage.init(named: "profile-icon")
        }
    }
    
    // MARK: - Memory
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

class RecipeItem {
    
    let recipename: String
    let recipeid: String
    var recipeIngredientsCount: Int
    let calories: String
    let imagename: String
    let ingredients: [NSDictionary]
    var remainingCount: Int
    var groceryItemsCount: Int
    
    init(recipename: String, recipeid: String, recipeIngredientsCount: Int, calories: String, imagename: String, ingredients: [NSDictionary], remainingCount: Int, groceryItemsCount: Int) {
        
        self.recipename = recipename
        self.recipeid = recipeid
        self.recipeIngredientsCount = recipeIngredientsCount
        self.calories = calories
        self.imagename = imagename
        self.ingredients = ingredients
        self.remainingCount = remainingCount
        self.groceryItemsCount = groceryItemsCount
    }
}



class GroceryItem {
    
    let ingredientname: String
    let ingredientid: String
    let recipeid: String
    let serving: String
    let active: Bool
    let ingredientsid: String
    let isSubstitute: Bool
    let imagename: String
    
    init(ingredientname: String, serving: String, active: Bool, recipeid: String, ingredientid: String, ingredientsid: String, isSubstitute: Bool, imagename: String) {

        self.ingredientname = ingredientname
        self.serving = serving
        self.active = active
        self.recipeid = recipeid
        self.ingredientid = ingredientid
        self.ingredientsid = ingredientsid
        self.isSubstitute = isSubstitute
        self.imagename = imagename
    }
}

class FeedItem {
    
    let recipename: String
    let imagename: String
    let videoname: String
    let preptime: String
    let cooktime: String
    let calories: String
    let source: String
    let sourceurl: String
    var isFavorite: String
    var username: String
    var userid: String
    var recipeid: String
    var cuisinetags: String
    var mealplanid: String
    var mealplandate: String
    var mealid: String
    var userimage: String
    var firstname: String
    var lastname: String
    var description: String
    var servings: String
    var totaltime: String
    var rating: String
    var timeAdded: String
    
    init(recipename: String, preptime: String, cooktime: String, imagename: String, videoname: String, calories: String, source: String,sourceurl: String, isFavorite: String, username: String, recipeid: String, cuisinetags: String, mealplanid: String, mealplandate: String, mealid: String, userimage: String, firstname: String, lastname: String, description: String, servings: String, userid: String, totaltime: String, rating: String, timeAdded: String) {
        self.recipename = recipename
        self.preptime = preptime
        self.cooktime = cooktime
        self.calories = calories
        self.imagename = imagename
        self.videoname = videoname
        self.source = source
        self.sourceurl = sourceurl
        self.isFavorite = isFavorite
        self.username = username
        self.recipeid = recipeid
        self.cuisinetags = cuisinetags
        self.mealplanid = mealplanid
        self.mealplandate = mealplandate
        self.mealid = mealid
        self.userimage = userimage
        self.firstname = firstname
        self.lastname = lastname
        self.description = description
        self.servings = servings
        self.userid = userid
        self.totaltime = totaltime
        self.rating = rating
        self.timeAdded = timeAdded
    }
}

class Animal {
    let name: String
    let image: String
    let category: AnimalType
    let categoryName: String
    
    init(name: String, category: AnimalType, image: String, categoryName: String) {
        self.name = name
        self.categoryName = categoryName
        self.category = category
        self.image = image
    }
}

enum AnimalType: String {
    case cat = "Cat"
    case dog = "Dog"
}
