//
//  MyGroups.swift
//  ChewsRite
//
//  Created by Randall Ridley on 9/12/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit
import AWSS3

class MyGroups: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var friendList = [NSMutableDictionary]()
    var filteredList = [NSMutableDictionary]()
    var isFiltered : Bool?
    var selectedGroup : GroupItem?
    
    @IBOutlet weak var friendsCV: UICollectionView!
    @IBOutlet weak var menuBtn: UIButton!
    
    @IBOutlet weak var search: UISearchBar!
    var itemArray = [GroupItem]()
    var currentItemArray = [GroupItem]()
    
    var statusImageCache = [String:UIImage]()
    
    @IBOutlet weak var familyCV: UICollectionView!
    var selectedUser : NSDictionary?
    var familyList = [NSDictionary]()
    
    var isFamilyMember : Bool?
    var userid = ""
    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        selectedUser = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!, "usertype":"0"] as NSDictionary
        
        familyCV.delegate = self
        familyCV.dataSource = self
        
        friendsCV.delegate = self
        friendsCV.dataSource = self
        
        isFamilyMember = false
        //familyCV.cellsize = 80
        
        familyCV.reloadData()
        
        search.delegate = self
        search.backgroundImage = UIImage()
        
        search.placeholder = "Search Groups"
        
        UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.init(name: "Avenir-Book", size: 15.0)
        
        menuBtn.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
//        let tb : TabController = self.parent as! TabController
//        
//        menuBtn.addTarget(self, action: #selector(tb.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)

        // Do any additional setup after loading the view.
        
        getFamily()
        getGroups()
        //get groups2
        //debug()
        
        
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateGroupImageHomeNotification),
                                               name: NSNotification.Name(rawValue: "updateGroupImageHome"),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refreshGroupsNotification),
                                               name: NSNotification.Name(rawValue: "refreshGroups"),
                                               object: nil)
    }
    
    @objc func updateGroupImageHomeNotification (notification: NSNotification) {
     
        if (notification.object != nil)
        {
            let imgDict = notification.object as! [String : Any]
            
            let image = imgDict["selectedImage"] as! UIImage
            
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imgDict["imagename"] as! String)"
            
            self.statusImageCache[urlString] = image
        }
    }
    
    @objc func refreshGroupsNotification (notification: NSNotification) {
                        
        getGroups()
    }
    
    // MARK: Webservice
    
    func getFamily() {
        
        familyList.removeAll()
        
        print("getFamily")
        
        let dataString = "familyData"
        
        let urlString = "\(appDelegate.serverDestination!)getFamilyMembers.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        let dict = ["userid":appDelegate.userid!, "userimage":appDelegate.profileImg!,"usertype":"0"] as NSDictionary
        familyList.append(dict)
        
        self.familyCV.reloadData()
        
        let paramString = "userid=\(appDelegate.userid!)&devStatus=\(appDelegate.devStage!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("family jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    DispatchQueue.main.sync(execute: {
                        
//                        self.familyCV.isHidden = true
//                        self.familyCV.reloadData()
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let members = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (members.count > 0)
                    {
                        hasData = true
                        
                        for dict in members
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            self.familyList.append(dict2)
                        }
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if hasData == true
                            {
                                self.familyCV.isHidden = false
                                self.familyCV.reloadData()
                            }
                            else
                            {
                                print("no data")
                                
//                                self.familyCV.isHidden = true
//                                self.familyCV.reloadData()
                            }
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func getGroups() {
        
        self.itemArray.removeAll()
        
        self.itemArray.append(GroupItem(groupname: "placeholder", groupid: "1", groupimage: "", adminid: "", groupdescription: ""))
        
        print("getGroups")
        
        let dataString = "groupsData"
        
        let urlString = "\(appDelegate.serverDestination!)getUserCreatedGroups.php"
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        
        if selectedUser?.object(forKey: "usertype") as! String == "0"
        {
            userid = appDelegate.userid
            isFamilyMember = false
        }
        else
        {
            userid = selectedUser?.object(forKey: "familyid") as! String
            isFamilyMember = true
        }
        
        let paramString = "userid=\(userid)&devStatus=\(appDelegate.devStage!)&isFamilyMember=\(isFamilyMember!)"
        
        print("urlString: \(urlString)")
        print("paramString: \(paramString)")
        
        request.httpBody = paramString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            
            do {
                
                let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                
                print("groups jsonResult: \(jsonResult)")
                
                let dataDict : NSDictionary = jsonResult.object(forKey: "data") as! NSDictionary
                
                if dataDict[dataString]! is NSNull {
                    
                    print("no data")
                    
                    self.currentItemArray = self.itemArray
                    
                    DispatchQueue.main.sync(execute: {
                        
                        self.friendsCV.reloadData()
                        print("friends reload2")
                    })
                }
                else
                {
                    let uploadData : NSMutableArray = (dataDict[dataString]! as! NSArray).mutableCopy() as! NSMutableArray
                    
                    let recipes1 = uploadData as! [NSDictionary]
                    
                    var hasData = false
                    
                    if (recipes1.count > 0)
                    {
                        hasData = true
                        
                        for dict in recipes1
                        {
                            let dict2 = dict.mutableCopy() as! NSMutableDictionary
                            
                            let newItem = GroupItem(groupname: dict2.object(forKey: "groupname") as! String, groupid: dict2.object(forKey: "groupid") as! String, groupimage: dict2.object(forKey: "groupimage") as! String, adminid: dict2.object(forKey: "userid") as! String, groupdescription: dict2.object(forKey: "groupdescription") as! String)
                            
                            self.itemArray.append(newItem)
                            
                            if self.selectedGroup != nil
                            {
                                if self.selectedGroup?.groupid == newItem.groupid
                                {
                                    self.selectedGroup = newItem
                                }
                            }
                        }                        
                        
                        self.currentItemArray = self.itemArray
                        
                        DispatchQueue.main.sync(execute: {
                            
                            //                            self.activityView.stopAnimating()
                            //                            self.activityView.isHidden = true
                            
                            if hasData == true
                            {
                                //self.recipeStatusTV.text = "You currently have no friend requests."
                                
                                self.friendsCV.reloadData()
//                                print("friends reload")
                            }
                            else
                            {
                                print("no data")
                                
                                //self.noResultsMain.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        self.currentItemArray = self.itemArray
                        
                        DispatchQueue.main.sync(execute: {
                            
//                            self.friendsCV.reloadData()
//                            print("friends reload")
                            
                            //self.noResultsMain.isHidden = false
                        })
                    }
                }
            }
            catch let err as NSError
            {
                print("error: \(err.description)")
            }
            
        }.resume()
    }
    
    func downloadImage (imagename : String, urlString : String, iv: UIImageView ) {
        
        if appDelegate.downloadImages == true
        {
            //print("iv: \(iv.restorationIdentifier!)")
            
            let s3BucketName = "chewsrite/images"
            
            let downloadFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(imagename)
            let downloadingFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(imagename)
            
            // Set the logging to verbose so we can see in the debug console what is happening
            //AWSLogger.default().logLevel = .none
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest?.bucket = s3BucketName
            downloadRequest?.key = imagename
            downloadRequest?.downloadingFileURL = downloadingFileURL
            
            let transferManager = AWSS3TransferManager.default()
            
            //[[transferManager download:downloadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
            
            transferManager.download(downloadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: ({
                (task: AWSTask!) -> AWSTask<AnyObject>? in
                
                DispatchQueue.main.async(execute: {
                    
                    if task.error != nil
                    {
                        print("AWS Error downloading image")
                        
                        print(task.error.debugDescription)
                    }
                    else
                    {
                        //print("AWS download successful")
                        
                        var downloadOutput = AWSS3TransferManagerDownloadOutput()
                        
                        downloadOutput = task.result! as? AWSS3TransferManagerDownloadOutput
                        
                        //print("downloadOutput photo: \(downloadOutput)");
                        //print("downloadFilePath photo: \(downloadFilePath)");
                        
                        let image = UIImage(contentsOfFile: downloadFilePath)
                        
                        self.statusImageCache[urlString] = image
                        iv.image = image
                    }
                    
                    //println("test")
                })
                return nil
            }))
            
        }
        else
        {
            iv.image = UIImage.init(named: "profile-icon")
        }
    }
    
    func debug () {
        
//        var dict : NSMutableDictionary = ["groupname" : "placeholder"]
//        friendList.append(dict)
//        
//        //dict  = ["groupname" : "My Vegans", "groupid" : "1", "groupimage" : "1", "active" : false]
//        friendList.append(dict)
        
        self.itemArray.append(GroupItem(groupname: "placeholder", groupid: "1", groupimage: "1", adminid: "", groupdescription: ""))
        self.itemArray.append(GroupItem(groupname: "My Vegans", groupid: "1", groupimage: "1", adminid: "", groupdescription: ""))
        
        self.currentItemArray = self.itemArray
        
//        self.itemArray.append(FeedItem(recipename: dict.object(forKey: "recipename") as! String, preptime: dict.object(forKey: "preptime") as! String, imagename: dict.object(forKey: "imagename") as! String, videoname: dict.object(forKey: "videoname") as! String, calories: dict.object(forKey: "calories") as! String, source: dict.object(forKey: "source") as! String, isFavorite: dict.object(forKey: "isFavorite") as! String, username: dict.object(forKey: "username") as! String))
        
        friendsCV.reloadData()
    }
    
    // MARK: - UICollectionViewDataSource, UICollectionViewDelegate methods
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.restorationIdentifier! == "family"
        {
            return CGSize.init(width: 42, height: 42)
        }
        else
        {
            let kWhateverHeightYouWant = 190
            
            return CGSize.init(width: 600, height: CGFloat(kWhateverHeightYouWant))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.restorationIdentifier! == "family"
        {
            return self.familyList.count
        }
        else
        {
            return currentItemArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        print("cv: \(collectionView.restorationIdentifier!)")
        
        if collectionView.restorationIdentifier == "family"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FamilyCVCell
            
            let dict = familyList[indexPath.row]
            
            let imagename = dict.object(forKey: "userimage") as? String
            
            if selectedUser == dict
            {
                let img = UIImage.init(named: "selectWhite")
                cell?.si.image = img
            }
            else
            {
                cell?.si.image = nil
            }
            
            if imagename != nil
            {
                let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                
                //let url2 = NSURL(string:urlString)!
                
                // If this image is already cached, don't re-download
                
                if let img = statusImageCache[urlString]
                {
                    //print("load cached status image")
                    
                    cell?.userImg.image = img
                }
                else
                {
                    //print("status image not
                    
                    let imagename = dict.object(forKey: "userimage") as? String
                    
                    if imagename != nil && imagename != ""
                    {
                        let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
                        
                        //print("img urlString: \(urlString)")
                        
                        if let img = statusImageCache[urlString]
                        {
                            cell?.userImg.image = img
                        }
                        else
                        {
                            cell?.userImg.image = UIImage(named: "profile-icon")
                            
                            self.downloadImage(imagename: imagename!, urlString: urlString, iv: ((cell?.userImg!)!))
                        }
                    }
                    else
                    {
                        cell?.userImg.image = UIImage(named: "profile-icon")
                    }
                }
            }
            
            return cell!
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FriendsCell
            
            //80 iphone
            
            cell?.frame = CGRect.init(x: (cell?.frame.origin.x)!, y: (cell?.frame.origin.y)!, width: 90, height: 120)
            
            
            let group = currentItemArray[indexPath.row]
            
            cell?.friendIV.restorationIdentifier = group.groupname
            
            //print("group.groupname: \(group.groupname)")
            
            if group.groupname != "placeholder"
            {
                //cell?.friendIV.image = UIImage.init(named: group.groupimage)
                cell?.nameLbl.text = group.groupname
                
                let imagename = group.groupimage
                
                if imagename != nil && imagename != ""
                {
                    //print("imagename: \(imagename)")
                    let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename)"
                    
                    if let img = statusImageCache[urlString]
                    {
                        cell?.friendIV.image = img
                    }
                    else
                    {
                        cell?.friendIV.image = UIImage(named: "profile-icon")
                        
                        self.downloadImage(imagename: imagename, urlString: urlString, iv: (cell?.friendIV!)!)
                    }
                }
            }
            else
            {
                cell?.friendIV.image = #imageLiteral(resourceName: "light")
                cell?.nameLbl.text = "Create"
            }
            
            return cell!
        }
    }
    
    @IBAction func selectTag(_ sender: Any) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.restorationIdentifier == "family"
        {
            selectedUser = familyList[indexPath.row]
            //
            print("selectedUser: \(selectedUser)")
            
            //getRecipes(param: "")
            
            getGroups()
            
            familyCV.reloadData()
        }
        else
        {
            if indexPath.row == 0
            {
                performSegue(withIdentifier: "CreateGroup", sender: nil)
            }
            else
            {
                selectedGroup = currentItemArray[indexPath.row]
                
                performSegue(withIdentifier: "ViewGroupFeed", sender: nil)
                //            if isFiltered == true
                //            {
                //                selectedGroup = filteredList[indexPath.row]
                //            }
                //            else
                //            {
                //                selectedGroup = friendList[indexPath.row]
                //            }
            }
        }
    }
    
    // MARK: Search
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        currentItemArray = itemArray.filter({ item -> Bool in
            
            switch searchBar.selectedScopeButtonIndex {
            case 0:
                if searchText.isEmpty { return true }
                return item.groupname.lowercased().contains(searchText.lowercased())
            default:
                return false
            }
        })
        
        friendsCV.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "CreateGroup"
        {
            let destination = segue.destination as! CreateGroup
            destination.userid = userid
            destination.isFamilyMember = isFamilyMember
        }
        else if segue.identifier == "EditGroup"
        {
            let destination = segue.destination as! CreateGroup
            destination.selectedGroup = selectedGroup
            destination.isEditing = true
        }
        else
        {
            let destination = segue.destination as! GroupFeed
            destination.selectedGroup = selectedGroup
            
            let imagename = selectedGroup?.groupimage
            
            let urlString = "\(appDelegate.serverDestination!)userimages/\(imagename!)"
            
            var selectedImage : UIImage?
            
            if statusImageCache[urlString] != nil
            {
                selectedImage = statusImageCache[urlString]
            }
            else
            {
                print("no")
            }
            
            destination.selectedImage = selectedImage
            //destination.groupId = selectedGroup?.groupid
            //destination.groupName = selectedGroup?.groupname
            
            if selectedGroup?.adminid == appDelegate.userid!
            {
                destination.isAdmin = true
            }
        }
    }
}

class GroupItem {
    
    var groupname: String
    var groupid: String
    var groupimage: String
    var groupdescription: String
    var adminid: String
    
    init(groupname: String, groupid: String, groupimage: String, adminid: String, groupdescription: String) {
        self.groupname = groupname
        self.groupid = groupid
        self.groupimage = groupimage
        self.adminid = adminid
        self.groupdescription = groupdescription
    }
}


