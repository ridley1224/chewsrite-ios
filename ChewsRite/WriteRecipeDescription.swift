//
//  WriteRecipeDescription.swift
//  ChewsRite
//
//  Created by Randall Ridley on 6/12/18.
//  Copyright © 2018 RT. All rights reserved.
//

import UIKit

class WriteRecipeDescription: UIViewController, UITextViewDelegate {

    @IBOutlet weak var descriptionTV: UITextView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let keyboardDoneButtonView = UIToolbar()
        keyboardDoneButtonView.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClicked))
        keyboardDoneButtonView.items = [doneButton]
        
        descriptionTV?.inputAccessoryView = keyboardDoneButtonView
        descriptionTV.text = "Enter description"
        descriptionTV.delegate = self
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Enter description"
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""
        {
            textView.text = "Enter description"
        }
    }
    
    @IBAction func save(_ sender: Any) {
        
        NotificationCenter.default.post(name: Notification.Name("descriptionSaved"), object: descriptionTV.text)
        
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        
//        if descriptionTV.text == ""
//        {
//            showBasicAlert(string: "Please enter description")
//        }
//        else
//        {
//            NotificationCenter.default.post(name: Notification.Name("descriptionSaved"), object: descriptionTV.text)
//            
//            self.navigationController?.popViewController(animated: true)
//            self.dismiss(animated: true, completion: nil)
//        }
    }
    
    func showBasicAlert(string:String)
    {
        let alert = UIAlertController(title: nil, message: string, preferredStyle: UIAlertControllerStyle.alert)
        
        self.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
                
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
    }
    
    @objc func doneClicked(sender: UIButton!) {
        
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
